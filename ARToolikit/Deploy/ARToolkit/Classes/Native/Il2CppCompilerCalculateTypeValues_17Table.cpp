﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype1195946242.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMe3165007540.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaElement2433337156.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet614309579.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet_Facet3019654938.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInfo2864028808.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObject2050913741.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable3364835593.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle3365045970.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSet313318308.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleType248156492.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeCo1606103299.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeLi2170323082.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeRe1099506232.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeUnio91327365.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaType1795078578.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUtil3576230726.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationFla910489930.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity995929432.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeAtt850813783.h"
#include "System_Xml_System_Xml_Serialization_XmlElementAttr2182839281.h"
#include "System_Xml_System_Xml_Serialization_XmlEnumAttribut919400678.h"
#include "System_Xml_System_Xml_Serialization_XmlIgnoreAttri2333915871.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializerN3063656491.h"
#include "System_Xml_System_Xml_ConformanceLevel3761201363.h"
#include "System_Xml_Mono_Xml_DTDAutomataFactory3605390810.h"
#include "System_Xml_Mono_Xml_DTDObjectModel1113953282.h"
#include "System_Xml_Mono_Xml_DictionaryBase1005937181.h"
#include "System_Xml_Mono_Xml_DictionaryBase_U3CU3Ec__Iterat3518389200.h"
#include "System_Xml_Mono_Xml_DTDCollectionBase2621362935.h"
#include "System_Xml_Mono_Xml_DTDElementDeclarationCollectio2224069626.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclarationCollection243645429.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclarationCollection1212505713.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclarationCollectio228085060.h"
#include "System_Xml_Mono_Xml_DTDContentModel445576364.h"
#include "System_Xml_Mono_Xml_DTDContentModelCollection3164170484.h"
#include "System_Xml_Mono_Xml_DTDNode1758286970.h"
#include "System_Xml_Mono_Xml_DTDElementDeclaration8748002.h"
#include "System_Xml_Mono_Xml_DTDAttributeDefinition3692870749.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclaration2272374839.h"
#include "System_Xml_Mono_Xml_DTDEntityBase2353758560.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclaration4283284771.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclaration1758408116.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclarationC3496720022.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclaration252230634.h"
#include "System_Xml_Mono_Xml_DTDContentOrderType3150259539.h"
#include "System_Xml_Mono_Xml_DTDOccurence99371501.h"
#include "System_Xml_System_Xml_DTDReader2453137441.h"
#include "System_Xml_System_Xml_EntityHandling3960499440.h"
#include "System_Xml_System_Xml_NameTable594386929.h"
#include "System_Xml_System_Xml_NameTable_Entry2583369454.h"
#include "System_Xml_System_Xml_NamespaceHandling1452270444.h"
#include "System_Xml_System_Xml_NewLineHandling1737195169.h"
#include "System_Xml_System_Xml_ReadState3138905245.h"
#include "System_Xml_System_Xml_ValidationType1401987383.h"
#include "System_Xml_System_Xml_WhitespaceHandling3754063142.h"
#include "System_Xml_System_Xml_WriteState1534871862.h"
#include "System_Xml_System_Xml_XmlAttribute175731005.h"
#include "System_Xml_System_Xml_XmlAttributeCollection3359885287.h"
#include "System_Xml_System_Xml_XmlCDataSection1124775823.h"
#include "System_Xml_System_Xml_XmlChar1369421061.h"
#include "System_Xml_System_Xml_XmlCharacterData575748506.h"
#include "System_Xml_System_Xml_XmlComment3999331572.h"
#include "System_Xml_System_Xml_XmlConvert1936105738.h"
#include "System_Xml_System_Xml_XmlDeclaration1545359137.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlDocumentFragment3083262362.h"
#include "System_Xml_System_Xml_XmlDocumentType824160610.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"
#include "System_Xml_System_Xml_XmlEntity4027255380.h"
#include "System_Xml_System_Xml_XmlEntityReference3053868353.h"
#include "System_Xml_System_Xml_XmlException4188277960.h"
#include "System_Xml_System_Xml_XmlImplementation1664517635.h"
#include "System_Xml_System_Xml_XmlStreamReader2725532304.h"
#include "System_Xml_System_Xml_NonBlockingStreamReader3963211903.h"
#include "System_Xml_System_Xml_XmlInputStream2650744719.h"
#include "System_Xml_System_Xml_XmlLinkedNode1287616130.h"
#include "System_Xml_System_Xml_XmlNameEntry3745551716.h"
#include "System_Xml_System_Xml_XmlNameEntryCache3855584002.h"
#include "System_Xml_System_Xml_XmlNameTable1345805268.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap145210370.h"
#include "System_Xml_System_Xml_XmlNamespaceManager486731501.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl3210081295.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope2513625351.h"
#include "System_Xml_System_Xml_XmlNode616554813.h"
#include "System_Xml_System_Xml_XmlNode_EmptyNodeList1718403287.h"
#include "System_Xml_System_Xml_XmlNodeChangedAction1188489541.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventArgs4036174778.h"
#include "System_Xml_System_Xml_XmlNodeList497326455.h"
#include "System_Xml_System_Xml_XmlNodeListChildren2811458520.h"
#include "System_Xml_System_Xml_XmlNodeListChildren_Enumerato569056069.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_System_Xml_XmlNotation206561061.h"
#include "System_Xml_System_Xml_XmlParserContext2728039553.h"
#include "System_Xml_System_Xml_XmlParserContext_ContextItem1262420678.h"
#include "System_Xml_System_Xml_XmlParserInput2366782760.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (XmlSchemaDatatype_t1195946242), -1, sizeof(XmlSchemaDatatype_t1195946242_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1700[55] = 
{
	XmlSchemaDatatype_t1195946242::get_offset_of_WhitespaceValue_0(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_wsChars_1(),
	XmlSchemaDatatype_t1195946242::get_offset_of_sb_2(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeAnySimpleType_3(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeString_4(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNormalizedString_5(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeToken_6(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeLanguage_7(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNMToken_8(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNMTokens_9(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeName_10(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNCName_11(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeID_12(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeIDRef_13(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeIDRefs_14(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeEntity_15(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeEntities_16(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNotation_17(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDecimal_18(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeInteger_19(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeLong_20(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeInt_21(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeShort_22(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeByte_23(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNonNegativeInteger_24(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypePositiveInteger_25(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedLong_26(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedInt_27(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedShort_28(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedByte_29(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNonPositiveInteger_30(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNegativeInteger_31(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeFloat_32(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDouble_33(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeBase64Binary_34(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeBoolean_35(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeAnyURI_36(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDuration_37(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDateTime_38(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDate_39(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeTime_40(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeHexBinary_41(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeQName_42(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGYearMonth_43(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGMonthDay_44(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGYear_45(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGMonth_46(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGDay_47(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeAnyAtomicType_48(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUntypedAtomic_49(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDayTimeDuration_50(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeYearMonthDuration_51(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_U3CU3Ef__switchU24map2A_52(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_U3CU3Ef__switchU24map2B_53(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_U3CU3Ef__switchU24map2C_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (XmlSchemaDerivationMethod_t3165007540)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1701[9] = 
{
	XmlSchemaDerivationMethod_t3165007540::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (XmlSchemaElement_t2433337156), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (XmlSchemaFacet_t614309579), -1, sizeof(XmlSchemaFacet_t614309579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1703[1] = 
{
	XmlSchemaFacet_t614309579_StaticFields::get_offset_of_AllFacets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (Facet_t3019654938)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1704[14] = 
{
	Facet_t3019654938::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (XmlSchemaInfo_t2864028808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[7] = 
{
	XmlSchemaInfo_t2864028808::get_offset_of_isDefault_0(),
	XmlSchemaInfo_t2864028808::get_offset_of_isNil_1(),
	XmlSchemaInfo_t2864028808::get_offset_of_memberType_2(),
	XmlSchemaInfo_t2864028808::get_offset_of_attr_3(),
	XmlSchemaInfo_t2864028808::get_offset_of_elem_4(),
	XmlSchemaInfo_t2864028808::get_offset_of_type_5(),
	XmlSchemaInfo_t2864028808::get_offset_of_validity_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (XmlSchemaObject_t2050913741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[3] = 
{
	XmlSchemaObject_t2050913741::get_offset_of_namespaces_0(),
	XmlSchemaObject_t2050913741::get_offset_of_unhandledAttributeList_1(),
	XmlSchemaObject_t2050913741::get_offset_of_CompilationId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (XmlSchemaObjectTable_t3364835593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1707[1] = 
{
	XmlSchemaObjectTable_t3364835593::get_offset_of_table_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (XmlSchemaParticle_t3365045970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (XmlSchemaSet_t313318308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1709[12] = 
{
	XmlSchemaSet_t313318308::get_offset_of_nameTable_0(),
	XmlSchemaSet_t313318308::get_offset_of_xmlResolver_1(),
	XmlSchemaSet_t313318308::get_offset_of_schemas_2(),
	XmlSchemaSet_t313318308::get_offset_of_attributes_3(),
	XmlSchemaSet_t313318308::get_offset_of_elements_4(),
	XmlSchemaSet_t313318308::get_offset_of_types_5(),
	XmlSchemaSet_t313318308::get_offset_of_idCollection_6(),
	XmlSchemaSet_t313318308::get_offset_of_namedIdentities_7(),
	XmlSchemaSet_t313318308::get_offset_of_settings_8(),
	XmlSchemaSet_t313318308::get_offset_of_isCompiled_9(),
	XmlSchemaSet_t313318308::get_offset_of_CompilationId_10(),
	XmlSchemaSet_t313318308::get_offset_of_ValidationEventHandler_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (XmlSchemaSimpleType_t248156492), -1, sizeof(XmlSchemaSimpleType_t248156492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1710[53] = 
{
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_schemaLocationType_9(),
	XmlSchemaSimpleType_t248156492::get_offset_of_content_10(),
	XmlSchemaSimpleType_t248156492::get_offset_of_islocal_11(),
	XmlSchemaSimpleType_t248156492::get_offset_of_variety_12(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsAnySimpleType_13(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsString_14(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsBoolean_15(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDecimal_16(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsFloat_17(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDouble_18(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDuration_19(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDateTime_20(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsTime_21(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDate_22(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGYearMonth_23(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGYear_24(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGMonthDay_25(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGDay_26(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGMonth_27(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsHexBinary_28(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsBase64Binary_29(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsAnyUri_30(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsQName_31(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNotation_32(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNormalizedString_33(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsToken_34(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsLanguage_35(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNMToken_36(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNMTokens_37(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsName_38(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNCName_39(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsID_40(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsIDRef_41(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsIDRefs_42(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsEntity_43(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsEntities_44(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsInteger_45(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNonPositiveInteger_46(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNegativeInteger_47(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsLong_48(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsInt_49(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsShort_50(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsByte_51(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNonNegativeInteger_52(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedLong_53(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedInt_54(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedShort_55(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedByte_56(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsPositiveInteger_57(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtUntypedAtomic_58(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtAnyAtomicType_59(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtYearMonthDuration_60(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtDayTimeDuration_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (XmlSchemaSimpleTypeContent_t1606103299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (XmlSchemaSimpleTypeList_t2170323082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1712[2] = 
{
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_itemType_3(),
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_itemTypeName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (XmlSchemaSimpleTypeRestriction_t1099506232), -1, sizeof(XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1713[2] = 
{
	XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields::get_offset_of_lengthStyle_3(),
	XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields::get_offset_of_listFacets_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (XmlSchemaSimpleTypeUnion_t91327365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (XmlSchemaType_t1795078578), -1, sizeof(XmlSchemaType_t1795078578_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1715[6] = 
{
	XmlSchemaType_t1795078578::get_offset_of_final_3(),
	XmlSchemaType_t1795078578::get_offset_of_BaseXmlSchemaTypeInternal_4(),
	XmlSchemaType_t1795078578::get_offset_of_DatatypeInternal_5(),
	XmlSchemaType_t1795078578::get_offset_of_QNameInternal_6(),
	XmlSchemaType_t1795078578_StaticFields::get_offset_of_U3CU3Ef__switchU24map2E_7(),
	XmlSchemaType_t1795078578_StaticFields::get_offset_of_U3CU3Ef__switchU24map2F_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (XmlSchemaUtil_t3576230726), -1, sizeof(XmlSchemaUtil_t3576230726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1716[4] = 
{
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_FinalAllowed_0(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_ElementBlockAllowed_1(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_ComplexTypeBlockAllowed_2(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_StrictMsCompliant_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (XmlSchemaValidationFlags_t910489930)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1717[7] = 
{
	XmlSchemaValidationFlags_t910489930::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (XmlSchemaValidity_t995929432)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1718[4] = 
{
	XmlSchemaValidity_t995929432::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (XmlAttributeAttribute_t850813783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1719[1] = 
{
	XmlAttributeAttribute_t850813783::get_offset_of_attributeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (XmlElementAttribute_t2182839281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1720[3] = 
{
	XmlElementAttribute_t2182839281::get_offset_of_elementName_0(),
	XmlElementAttribute_t2182839281::get_offset_of_type_1(),
	XmlElementAttribute_t2182839281::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (XmlEnumAttribute_t919400678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[1] = 
{
	XmlEnumAttribute_t919400678::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (XmlIgnoreAttribute_t2333915871), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (XmlSerializerNamespaces_t3063656491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[1] = 
{
	XmlSerializerNamespaces_t3063656491::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (ConformanceLevel_t3761201363)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1725[4] = 
{
	ConformanceLevel_t3761201363::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (DTDAutomataFactory_t3605390810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1726[3] = 
{
	DTDAutomataFactory_t3605390810::get_offset_of_root_0(),
	DTDAutomataFactory_t3605390810::get_offset_of_choiceTable_1(),
	DTDAutomataFactory_t3605390810::get_offset_of_sequenceTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (DTDObjectModel_t1113953282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1727[19] = 
{
	DTDObjectModel_t1113953282::get_offset_of_factory_0(),
	DTDObjectModel_t1113953282::get_offset_of_elementDecls_1(),
	DTDObjectModel_t1113953282::get_offset_of_attListDecls_2(),
	DTDObjectModel_t1113953282::get_offset_of_peDecls_3(),
	DTDObjectModel_t1113953282::get_offset_of_entityDecls_4(),
	DTDObjectModel_t1113953282::get_offset_of_notationDecls_5(),
	DTDObjectModel_t1113953282::get_offset_of_validationErrors_6(),
	DTDObjectModel_t1113953282::get_offset_of_resolver_7(),
	DTDObjectModel_t1113953282::get_offset_of_nameTable_8(),
	DTDObjectModel_t1113953282::get_offset_of_externalResources_9(),
	DTDObjectModel_t1113953282::get_offset_of_baseURI_10(),
	DTDObjectModel_t1113953282::get_offset_of_name_11(),
	DTDObjectModel_t1113953282::get_offset_of_publicId_12(),
	DTDObjectModel_t1113953282::get_offset_of_systemId_13(),
	DTDObjectModel_t1113953282::get_offset_of_intSubset_14(),
	DTDObjectModel_t1113953282::get_offset_of_intSubsetHasPERef_15(),
	DTDObjectModel_t1113953282::get_offset_of_isStandalone_16(),
	DTDObjectModel_t1113953282::get_offset_of_lineNumber_17(),
	DTDObjectModel_t1113953282::get_offset_of_linePosition_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (DictionaryBase_t1005937181), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (U3CU3Ec__Iterator3_t3518389200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1729[5] = 
{
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CU24s_431U3E__0_0(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CpU3E__1_1(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U24PC_2(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (DTDCollectionBase_t2621362935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1730[1] = 
{
	DTDCollectionBase_t2621362935::get_offset_of_root_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (DTDElementDeclarationCollection_t2224069626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (DTDAttListDeclarationCollection_t243645429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (DTDEntityDeclarationCollection_t1212505713), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (DTDNotationDeclarationCollection_t228085060), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (DTDContentModel_t445576364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1735[6] = 
{
	DTDContentModel_t445576364::get_offset_of_root_5(),
	DTDContentModel_t445576364::get_offset_of_ownerElementName_6(),
	DTDContentModel_t445576364::get_offset_of_elementName_7(),
	DTDContentModel_t445576364::get_offset_of_orderType_8(),
	DTDContentModel_t445576364::get_offset_of_childModels_9(),
	DTDContentModel_t445576364::get_offset_of_occurence_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (DTDContentModelCollection_t3164170484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1736[1] = 
{
	DTDContentModelCollection_t3164170484::get_offset_of_contentModel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (DTDNode_t1758286970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[5] = 
{
	DTDNode_t1758286970::get_offset_of_root_0(),
	DTDNode_t1758286970::get_offset_of_isInternalSubset_1(),
	DTDNode_t1758286970::get_offset_of_baseURI_2(),
	DTDNode_t1758286970::get_offset_of_lineNumber_3(),
	DTDNode_t1758286970::get_offset_of_linePosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (DTDElementDeclaration_t8748002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1738[6] = 
{
	DTDElementDeclaration_t8748002::get_offset_of_root_5(),
	DTDElementDeclaration_t8748002::get_offset_of_contentModel_6(),
	DTDElementDeclaration_t8748002::get_offset_of_name_7(),
	DTDElementDeclaration_t8748002::get_offset_of_isEmpty_8(),
	DTDElementDeclaration_t8748002::get_offset_of_isAny_9(),
	DTDElementDeclaration_t8748002::get_offset_of_isMixedContent_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (DTDAttributeDefinition_t3692870749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1739[4] = 
{
	DTDAttributeDefinition_t3692870749::get_offset_of_name_5(),
	DTDAttributeDefinition_t3692870749::get_offset_of_datatype_6(),
	DTDAttributeDefinition_t3692870749::get_offset_of_unresolvedDefault_7(),
	DTDAttributeDefinition_t3692870749::get_offset_of_resolvedDefaultValue_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (DTDAttListDeclaration_t2272374839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1740[3] = 
{
	DTDAttListDeclaration_t2272374839::get_offset_of_name_5(),
	DTDAttListDeclaration_t2272374839::get_offset_of_attributeOrders_6(),
	DTDAttListDeclaration_t2272374839::get_offset_of_attributes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (DTDEntityBase_t2353758560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1741[10] = 
{
	DTDEntityBase_t2353758560::get_offset_of_name_5(),
	DTDEntityBase_t2353758560::get_offset_of_publicId_6(),
	DTDEntityBase_t2353758560::get_offset_of_systemId_7(),
	DTDEntityBase_t2353758560::get_offset_of_literalValue_8(),
	DTDEntityBase_t2353758560::get_offset_of_replacementText_9(),
	DTDEntityBase_t2353758560::get_offset_of_uriString_10(),
	DTDEntityBase_t2353758560::get_offset_of_absUri_11(),
	DTDEntityBase_t2353758560::get_offset_of_isInvalid_12(),
	DTDEntityBase_t2353758560::get_offset_of_loadFailed_13(),
	DTDEntityBase_t2353758560::get_offset_of_resolver_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (DTDEntityDeclaration_t4283284771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1742[6] = 
{
	DTDEntityDeclaration_t4283284771::get_offset_of_entityValue_15(),
	DTDEntityDeclaration_t4283284771::get_offset_of_notationName_16(),
	DTDEntityDeclaration_t4283284771::get_offset_of_ReferencingEntities_17(),
	DTDEntityDeclaration_t4283284771::get_offset_of_scanned_18(),
	DTDEntityDeclaration_t4283284771::get_offset_of_recursed_19(),
	DTDEntityDeclaration_t4283284771::get_offset_of_hasExternalReference_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (DTDNotationDeclaration_t1758408116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1743[5] = 
{
	DTDNotationDeclaration_t1758408116::get_offset_of_name_5(),
	DTDNotationDeclaration_t1758408116::get_offset_of_localName_6(),
	DTDNotationDeclaration_t1758408116::get_offset_of_prefix_7(),
	DTDNotationDeclaration_t1758408116::get_offset_of_publicId_8(),
	DTDNotationDeclaration_t1758408116::get_offset_of_systemId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (DTDParameterEntityDeclarationCollection_t3496720022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1744[2] = 
{
	DTDParameterEntityDeclarationCollection_t3496720022::get_offset_of_peDecls_0(),
	DTDParameterEntityDeclarationCollection_t3496720022::get_offset_of_root_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (DTDParameterEntityDeclaration_t252230634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (DTDContentOrderType_t3150259539)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1746[4] = 
{
	DTDContentOrderType_t3150259539::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (DTDOccurence_t99371501)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1747[5] = 
{
	DTDOccurence_t99371501::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (DTDReader_t2453137441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1748[14] = 
{
	DTDReader_t2453137441::get_offset_of_currentInput_0(),
	DTDReader_t2453137441::get_offset_of_parserInputStack_1(),
	DTDReader_t2453137441::get_offset_of_nameBuffer_2(),
	DTDReader_t2453137441::get_offset_of_nameLength_3(),
	DTDReader_t2453137441::get_offset_of_nameCapacity_4(),
	DTDReader_t2453137441::get_offset_of_valueBuffer_5(),
	DTDReader_t2453137441::get_offset_of_currentLinkedNodeLineNumber_6(),
	DTDReader_t2453137441::get_offset_of_currentLinkedNodeLinePosition_7(),
	DTDReader_t2453137441::get_offset_of_dtdIncludeSect_8(),
	DTDReader_t2453137441::get_offset_of_normalization_9(),
	DTDReader_t2453137441::get_offset_of_processingInternalSubset_10(),
	DTDReader_t2453137441::get_offset_of_cachedPublicId_11(),
	DTDReader_t2453137441::get_offset_of_cachedSystemId_12(),
	DTDReader_t2453137441::get_offset_of_DTD_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (EntityHandling_t3960499440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1749[3] = 
{
	EntityHandling_t3960499440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (NameTable_t594386929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1753[3] = 
{
	NameTable_t594386929::get_offset_of_count_0(),
	NameTable_t594386929::get_offset_of_buckets_1(),
	NameTable_t594386929::get_offset_of_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (Entry_t2583369454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1754[4] = 
{
	Entry_t2583369454::get_offset_of_str_0(),
	Entry_t2583369454::get_offset_of_hash_1(),
	Entry_t2583369454::get_offset_of_len_2(),
	Entry_t2583369454::get_offset_of_next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (NamespaceHandling_t1452270444)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1755[3] = 
{
	NamespaceHandling_t1452270444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (NewLineHandling_t1737195169)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1756[4] = 
{
	NewLineHandling_t1737195169::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (ReadState_t3138905245)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1757[6] = 
{
	ReadState_t3138905245::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (ValidationType_t1401987383)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1758[6] = 
{
	ValidationType_t1401987383::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (WhitespaceHandling_t3754063142)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1759[4] = 
{
	WhitespaceHandling_t3754063142::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (WriteState_t1534871862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1760[8] = 
{
	WriteState_t1534871862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (XmlAttribute_t175731005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1761[4] = 
{
	XmlAttribute_t175731005::get_offset_of_name_5(),
	XmlAttribute_t175731005::get_offset_of_isDefault_6(),
	XmlAttribute_t175731005::get_offset_of_lastLinkedChild_7(),
	XmlAttribute_t175731005::get_offset_of_schemaInfo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (XmlAttributeCollection_t3359885287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1762[2] = 
{
	XmlAttributeCollection_t3359885287::get_offset_of_ownerElement_4(),
	XmlAttributeCollection_t3359885287::get_offset_of_ownerDocument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (XmlCDataSection_t1124775823), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (XmlChar_t1369421061), -1, sizeof(XmlChar_t1369421061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1764[5] = 
{
	XmlChar_t1369421061_StaticFields::get_offset_of_WhitespaceChars_0(),
	XmlChar_t1369421061_StaticFields::get_offset_of_firstNamePages_1(),
	XmlChar_t1369421061_StaticFields::get_offset_of_namePages_2(),
	XmlChar_t1369421061_StaticFields::get_offset_of_nameBitmap_3(),
	XmlChar_t1369421061_StaticFields::get_offset_of_U3CU3Ef__switchU24map47_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (XmlCharacterData_t575748506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[1] = 
{
	XmlCharacterData_t575748506::get_offset_of_data_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (XmlComment_t3999331572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (XmlConvert_t1936105738), -1, sizeof(XmlConvert_t1936105738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1767[7] = 
{
	XmlConvert_t1936105738_StaticFields::get_offset_of_datetimeFormats_0(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_defaultDateTimeFormats_1(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_roundtripDateTimeFormats_2(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_localDateTimeFormats_3(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_utcDateTimeFormats_4(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_unspecifiedDateTimeFormats_5(),
	XmlConvert_t1936105738_StaticFields::get_offset_of__defaultStyle_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (XmlDeclaration_t1545359137), -1, sizeof(XmlDeclaration_t1545359137_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1768[4] = 
{
	XmlDeclaration_t1545359137::get_offset_of_encoding_6(),
	XmlDeclaration_t1545359137::get_offset_of_standalone_7(),
	XmlDeclaration_t1545359137::get_offset_of_version_8(),
	XmlDeclaration_t1545359137_StaticFields::get_offset_of_U3CU3Ef__switchU24map4A_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (XmlDocument_t3649534162), -1, sizeof(XmlDocument_t3649534162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1769[20] = 
{
	XmlDocument_t3649534162_StaticFields::get_offset_of_optimal_create_types_5(),
	XmlDocument_t3649534162::get_offset_of_optimal_create_element_6(),
	XmlDocument_t3649534162::get_offset_of_optimal_create_attribute_7(),
	XmlDocument_t3649534162::get_offset_of_nameTable_8(),
	XmlDocument_t3649534162::get_offset_of_baseURI_9(),
	XmlDocument_t3649534162::get_offset_of_implementation_10(),
	XmlDocument_t3649534162::get_offset_of_preserveWhitespace_11(),
	XmlDocument_t3649534162::get_offset_of_resolver_12(),
	XmlDocument_t3649534162::get_offset_of_idTable_13(),
	XmlDocument_t3649534162::get_offset_of_nameCache_14(),
	XmlDocument_t3649534162::get_offset_of_lastLinkedChild_15(),
	XmlDocument_t3649534162::get_offset_of_schemas_16(),
	XmlDocument_t3649534162::get_offset_of_schemaInfo_17(),
	XmlDocument_t3649534162::get_offset_of_loadMode_18(),
	XmlDocument_t3649534162::get_offset_of_NodeChanged_19(),
	XmlDocument_t3649534162::get_offset_of_NodeChanging_20(),
	XmlDocument_t3649534162::get_offset_of_NodeInserted_21(),
	XmlDocument_t3649534162::get_offset_of_NodeInserting_22(),
	XmlDocument_t3649534162::get_offset_of_NodeRemoved_23(),
	XmlDocument_t3649534162::get_offset_of_NodeRemoving_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (XmlDocumentFragment_t3083262362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1770[1] = 
{
	XmlDocumentFragment_t3083262362::get_offset_of_lastLinkedChild_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (XmlDocumentType_t824160610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1771[3] = 
{
	XmlDocumentType_t824160610::get_offset_of_entities_6(),
	XmlDocumentType_t824160610::get_offset_of_notations_7(),
	XmlDocumentType_t824160610::get_offset_of_dtd_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (XmlElement_t2877111883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1772[5] = 
{
	XmlElement_t2877111883::get_offset_of_attributes_6(),
	XmlElement_t2877111883::get_offset_of_name_7(),
	XmlElement_t2877111883::get_offset_of_lastLinkedChild_8(),
	XmlElement_t2877111883::get_offset_of_isNotEmpty_9(),
	XmlElement_t2877111883::get_offset_of_schemaInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (XmlEntity_t4027255380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1773[7] = 
{
	XmlEntity_t4027255380::get_offset_of_name_5(),
	XmlEntity_t4027255380::get_offset_of_NDATA_6(),
	XmlEntity_t4027255380::get_offset_of_publicId_7(),
	XmlEntity_t4027255380::get_offset_of_systemId_8(),
	XmlEntity_t4027255380::get_offset_of_baseUri_9(),
	XmlEntity_t4027255380::get_offset_of_lastLinkedChild_10(),
	XmlEntity_t4027255380::get_offset_of_contentAlreadySet_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (XmlEntityReference_t3053868353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[2] = 
{
	XmlEntityReference_t3053868353::get_offset_of_entityName_6(),
	XmlEntityReference_t3053868353::get_offset_of_lastLinkedChild_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (XmlException_t4188277960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[5] = 
{
	XmlException_t4188277960::get_offset_of_lineNumber_11(),
	XmlException_t4188277960::get_offset_of_linePosition_12(),
	XmlException_t4188277960::get_offset_of_sourceUri_13(),
	XmlException_t4188277960::get_offset_of_res_14(),
	XmlException_t4188277960::get_offset_of_messages_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (XmlImplementation_t1664517635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1776[1] = 
{
	XmlImplementation_t1664517635::get_offset_of_InternalNameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (XmlStreamReader_t2725532304), -1, sizeof(XmlStreamReader_t2725532304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1777[2] = 
{
	XmlStreamReader_t2725532304::get_offset_of_input_12(),
	XmlStreamReader_t2725532304_StaticFields::get_offset_of_invalidDataException_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (NonBlockingStreamReader_t3963211903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1778[11] = 
{
	NonBlockingStreamReader_t3963211903::get_offset_of_input_buffer_1(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_buffer_2(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_count_3(),
	NonBlockingStreamReader_t3963211903::get_offset_of_pos_4(),
	NonBlockingStreamReader_t3963211903::get_offset_of_buffer_size_5(),
	NonBlockingStreamReader_t3963211903::get_offset_of_encoding_6(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoder_7(),
	NonBlockingStreamReader_t3963211903::get_offset_of_base_stream_8(),
	NonBlockingStreamReader_t3963211903::get_offset_of_mayBlock_9(),
	NonBlockingStreamReader_t3963211903::get_offset_of_line_builder_10(),
	NonBlockingStreamReader_t3963211903::get_offset_of_foundCR_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (XmlInputStream_t2650744719), -1, sizeof(XmlInputStream_t2650744719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1779[7] = 
{
	XmlInputStream_t2650744719_StaticFields::get_offset_of_StrictUTF8_1(),
	XmlInputStream_t2650744719::get_offset_of_enc_2(),
	XmlInputStream_t2650744719::get_offset_of_stream_3(),
	XmlInputStream_t2650744719::get_offset_of_buffer_4(),
	XmlInputStream_t2650744719::get_offset_of_bufLength_5(),
	XmlInputStream_t2650744719::get_offset_of_bufPos_6(),
	XmlInputStream_t2650744719_StaticFields::get_offset_of_encodingException_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (XmlLinkedNode_t1287616130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1780[1] = 
{
	XmlLinkedNode_t1287616130::get_offset_of_nextSibling_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (XmlNameEntry_t3745551716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[5] = 
{
	XmlNameEntry_t3745551716::get_offset_of_Prefix_0(),
	XmlNameEntry_t3745551716::get_offset_of_LocalName_1(),
	XmlNameEntry_t3745551716::get_offset_of_NS_2(),
	XmlNameEntry_t3745551716::get_offset_of_Hash_3(),
	XmlNameEntry_t3745551716::get_offset_of_prefixed_name_cache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (XmlNameEntryCache_t3855584002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[4] = 
{
	XmlNameEntryCache_t3855584002::get_offset_of_table_0(),
	XmlNameEntryCache_t3855584002::get_offset_of_nameTable_1(),
	XmlNameEntryCache_t3855584002::get_offset_of_dummy_2(),
	XmlNameEntryCache_t3855584002::get_offset_of_cacheBuffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (XmlNameTable_t1345805268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (XmlNamedNodeMap_t145210370), -1, sizeof(XmlNamedNodeMap_t145210370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1784[4] = 
{
	XmlNamedNodeMap_t145210370_StaticFields::get_offset_of_emptyEnumerator_0(),
	XmlNamedNodeMap_t145210370::get_offset_of_parent_1(),
	XmlNamedNodeMap_t145210370::get_offset_of_nodeList_2(),
	XmlNamedNodeMap_t145210370::get_offset_of_readOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (XmlNamespaceManager_t486731501), -1, sizeof(XmlNamespaceManager_t486731501_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1785[9] = 
{
	XmlNamespaceManager_t486731501::get_offset_of_decls_0(),
	XmlNamespaceManager_t486731501::get_offset_of_declPos_1(),
	XmlNamespaceManager_t486731501::get_offset_of_scopes_2(),
	XmlNamespaceManager_t486731501::get_offset_of_scopePos_3(),
	XmlNamespaceManager_t486731501::get_offset_of_defaultNamespace_4(),
	XmlNamespaceManager_t486731501::get_offset_of_count_5(),
	XmlNamespaceManager_t486731501::get_offset_of_nameTable_6(),
	XmlNamespaceManager_t486731501::get_offset_of_internalAtomizedNames_7(),
	XmlNamespaceManager_t486731501_StaticFields::get_offset_of_U3CU3Ef__switchU24map28_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (NsDecl_t3210081295)+ sizeof (Il2CppObject), sizeof(NsDecl_t3210081295_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1786[2] = 
{
	NsDecl_t3210081295::get_offset_of_Prefix_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsDecl_t3210081295::get_offset_of_Uri_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (NsScope_t2513625351)+ sizeof (Il2CppObject), sizeof(NsScope_t2513625351_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1787[2] = 
{
	NsScope_t2513625351::get_offset_of_DeclCount_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsScope_t2513625351::get_offset_of_DefaultNamespace_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (XmlNode_t616554813), -1, sizeof(XmlNode_t616554813_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1788[5] = 
{
	XmlNode_t616554813_StaticFields::get_offset_of_emptyList_0(),
	XmlNode_t616554813::get_offset_of_ownerDocument_1(),
	XmlNode_t616554813::get_offset_of_parentNode_2(),
	XmlNode_t616554813::get_offset_of_childNodes_3(),
	XmlNode_t616554813_StaticFields::get_offset_of_U3CU3Ef__switchU24map44_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (EmptyNodeList_t1718403287), -1, sizeof(EmptyNodeList_t1718403287_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1789[1] = 
{
	EmptyNodeList_t1718403287_StaticFields::get_offset_of_emptyEnumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (XmlNodeChangedAction_t1188489541)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1790[4] = 
{
	XmlNodeChangedAction_t1188489541::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (XmlNodeChangedEventArgs_t4036174778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1791[6] = 
{
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__oldParent_1(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__newParent_2(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__action_3(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__node_4(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__oldValue_5(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__newValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (XmlNodeList_t497326455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (XmlNodeListChildren_t2811458520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1793[1] = 
{
	XmlNodeListChildren_t2811458520::get_offset_of_parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (Enumerator_t569056069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[3] = 
{
	Enumerator_t569056069::get_offset_of_parent_0(),
	Enumerator_t569056069::get_offset_of_currentChild_1(),
	Enumerator_t569056069::get_offset_of_passedLastNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (XmlNodeType_t739504597)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1795[19] = 
{
	XmlNodeType_t739504597::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (XmlNotation_t206561061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[4] = 
{
	XmlNotation_t206561061::get_offset_of_localName_5(),
	XmlNotation_t206561061::get_offset_of_publicId_6(),
	XmlNotation_t206561061::get_offset_of_systemId_7(),
	XmlNotation_t206561061::get_offset_of_prefix_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (XmlParserContext_t2728039553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1797[13] = 
{
	XmlParserContext_t2728039553::get_offset_of_baseURI_0(),
	XmlParserContext_t2728039553::get_offset_of_docTypeName_1(),
	XmlParserContext_t2728039553::get_offset_of_encoding_2(),
	XmlParserContext_t2728039553::get_offset_of_internalSubset_3(),
	XmlParserContext_t2728039553::get_offset_of_namespaceManager_4(),
	XmlParserContext_t2728039553::get_offset_of_nameTable_5(),
	XmlParserContext_t2728039553::get_offset_of_publicID_6(),
	XmlParserContext_t2728039553::get_offset_of_systemID_7(),
	XmlParserContext_t2728039553::get_offset_of_xmlLang_8(),
	XmlParserContext_t2728039553::get_offset_of_xmlSpace_9(),
	XmlParserContext_t2728039553::get_offset_of_contextItems_10(),
	XmlParserContext_t2728039553::get_offset_of_contextItemCount_11(),
	XmlParserContext_t2728039553::get_offset_of_dtd_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (ContextItem_t1262420678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[3] = 
{
	ContextItem_t1262420678::get_offset_of_BaseURI_0(),
	ContextItem_t1262420678::get_offset_of_XmlLang_1(),
	ContextItem_t1262420678::get_offset_of_XmlSpace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (XmlParserInput_t2366782760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1799[5] = 
{
	XmlParserInput_t2366782760::get_offset_of_sourceStack_0(),
	XmlParserInput_t2366782760::get_offset_of_source_1(),
	XmlParserInput_t2366782760::get_offset_of_has_peek_2(),
	XmlParserInput_t2366782760::get_offset_of_peek_char_3(),
	XmlParserInput_t2366782760::get_offset_of_allowTextDecl_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
