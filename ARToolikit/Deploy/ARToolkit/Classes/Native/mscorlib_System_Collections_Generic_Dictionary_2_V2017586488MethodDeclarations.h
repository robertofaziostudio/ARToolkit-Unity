﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2677815550MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2602015409(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2017586488 *, Dictionary_2_t3314526645 *, const MethodInfo*))ValueCollection__ctor_m2980136029_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.String>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m233723579(__this, ___item0, method) ((  void (*) (ValueCollection_t2017586488 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2829838359_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.String>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1403516766(__this, method) ((  void (*) (ValueCollection_t2017586488 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1940960952_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.String>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1057221761(__this, ___item0, method) ((  bool (*) (ValueCollection_t2017586488 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1947576125_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.String>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3694836300(__this, ___item0, method) ((  bool (*) (ValueCollection_t2017586488 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1913987922_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.String>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2766249386(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2017586488 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2934412752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3005696226(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2017586488 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3344617736_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m200414211(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2017586488 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3663875303_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.String>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1907030080(__this, method) ((  bool (*) (ValueCollection_t2017586488 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3860584182_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3679163958(__this, method) ((  bool (*) (ValueCollection_t2017586488 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1004418236_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.String>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m424878258(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2017586488 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1281476600_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.String>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1611232490(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2017586488 *, StringU5BU5D_t1642385972*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3814159732_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.String>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1558511557(__this, method) ((  Enumerator_t706092113  (*) (ValueCollection_t2017586488 *, const MethodInfo*))ValueCollection_GetEnumerator_m1655154457_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.String>::get_Count()
#define ValueCollection_get_Count_m2751328342(__this, method) ((  int32_t (*) (ValueCollection_t2017586488 *, const MethodInfo*))ValueCollection_get_Count_m1967611356_gshared)(__this, method)
