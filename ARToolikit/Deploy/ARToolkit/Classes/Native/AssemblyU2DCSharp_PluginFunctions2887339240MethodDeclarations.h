﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginFunctions/LogCallback
struct LogCallback_t2143553514;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PluginFunctions_LogCallback2143553514.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PluginFunctions::arwRegisterLogCallback(PluginFunctions/LogCallback)
extern "C"  void PluginFunctions_arwRegisterLogCallback_m1389878145 (Il2CppObject * __this /* static, unused */, LogCallback_t2143553514 * ___lcb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::arwSetLogLevel(System.Int32)
extern "C"  void PluginFunctions_arwSetLogLevel_m3400122846 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwInitialiseAR(System.Int32,System.Int32)
extern "C"  bool PluginFunctions_arwInitialiseAR_m1868305941 (Il2CppObject * __this /* static, unused */, int32_t ___pattSize0, int32_t ___pattCountMax1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginFunctions::arwGetARToolKitVersion()
extern "C"  String_t* PluginFunctions_arwGetARToolKitVersion_m1242319639 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginFunctions::arwGetError()
extern "C"  int32_t PluginFunctions_arwGetError_m1333834619 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwShutdownAR()
extern "C"  bool PluginFunctions_arwShutdownAR_m3422793632 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwStartRunningB(System.String,System.Byte[],System.Int32,System.Single,System.Single)
extern "C"  bool PluginFunctions_arwStartRunningB_m1651599480 (Il2CppObject * __this /* static, unused */, String_t* ___vconf0, ByteU5BU5D_t3397334013* ___cparaBuff1, int32_t ___cparaBuffLen2, float ___nearPlane3, float ___farPlane4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwStartRunningStereoB(System.String,System.Byte[],System.Int32,System.String,System.Byte[],System.Int32,System.Byte[],System.Int32,System.Single,System.Single)
extern "C"  bool PluginFunctions_arwStartRunningStereoB_m1451019968 (Il2CppObject * __this /* static, unused */, String_t* ___vconfL0, ByteU5BU5D_t3397334013* ___cparaBuffL1, int32_t ___cparaBuffLenL2, String_t* ___vconfR3, ByteU5BU5D_t3397334013* ___cparaBuffR4, int32_t ___cparaBuffLenR5, ByteU5BU5D_t3397334013* ___transL2RBuff6, int32_t ___transL2RBuffLen7, float ___nearPlane8, float ___farPlane9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwIsRunning()
extern "C"  bool PluginFunctions_arwIsRunning_m3605556890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwStopRunning()
extern "C"  bool PluginFunctions_arwStopRunning_m1121675878 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwGetProjectionMatrix(System.Single[])
extern "C"  bool PluginFunctions_arwGetProjectionMatrix_m2140236978 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwGetProjectionMatrixStereo(System.Single[],System.Single[])
extern "C"  bool PluginFunctions_arwGetProjectionMatrixStereo_m3127493433 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___matrixL0, SingleU5BU5D_t577127397* ___matrixR1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwGetVideoParams(System.Int32&,System.Int32&,System.Int32&,System.String&)
extern "C"  bool PluginFunctions_arwGetVideoParams_m4086655695 (Il2CppObject * __this /* static, unused */, int32_t* ___width0, int32_t* ___height1, int32_t* ___pixelSize2, String_t** ___pixelFormatString3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwGetVideoParamsStereo(System.Int32&,System.Int32&,System.Int32&,System.String&,System.Int32&,System.Int32&,System.Int32&,System.String&)
extern "C"  bool PluginFunctions_arwGetVideoParamsStereo_m3050262472 (Il2CppObject * __this /* static, unused */, int32_t* ___widthL0, int32_t* ___heightL1, int32_t* ___pixelSizeL2, String_t** ___pixelFormatL3, int32_t* ___widthR4, int32_t* ___heightR5, int32_t* ___pixelSizeR6, String_t** ___pixelFormatR7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwCapture()
extern "C"  bool PluginFunctions_arwCapture_m650910371 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwUpdateAR()
extern "C"  bool PluginFunctions_arwUpdateAR_m598243641 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwUpdateTexture(UnityEngine.Color[])
extern "C"  bool PluginFunctions_arwUpdateTexture_m210610373 (Il2CppObject * __this /* static, unused */, ColorU5BU5D_t672350442* ___colors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwUpdateTextureStereo(UnityEngine.Color[],UnityEngine.Color[])
extern "C"  bool PluginFunctions_arwUpdateTextureStereo_m3642194655 (Il2CppObject * __this /* static, unused */, ColorU5BU5D_t672350442* ___colorsL0, ColorU5BU5D_t672350442* ___colorsR1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwUpdateTexture32(UnityEngine.Color32[])
extern "C"  bool PluginFunctions_arwUpdateTexture32_m3012693829 (Il2CppObject * __this /* static, unused */, Color32U5BU5D_t30278651* ___colors320, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwUpdateTexture32Stereo(UnityEngine.Color32[],UnityEngine.Color32[])
extern "C"  bool PluginFunctions_arwUpdateTexture32Stereo_m3306506678 (Il2CppObject * __this /* static, unused */, Color32U5BU5D_t30278651* ___colors32L0, Color32U5BU5D_t30278651* ___colors32R1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwUpdateTextureGL(System.Int32)
extern "C"  bool PluginFunctions_arwUpdateTextureGL_m358316523 (Il2CppObject * __this /* static, unused */, int32_t ___textureID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwUpdateTextureGLStereo(System.Int32,System.Int32)
extern "C"  bool PluginFunctions_arwUpdateTextureGLStereo_m2096161246 (Il2CppObject * __this /* static, unused */, int32_t ___textureID_L0, int32_t ___textureID_R1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::arwSetUnityRenderEventUpdateTextureGLTextureID(System.Int32)
extern "C"  void PluginFunctions_arwSetUnityRenderEventUpdateTextureGLTextureID_m3634330186 (Il2CppObject * __this /* static, unused */, int32_t ___textureID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs(System.Int32,System.Int32)
extern "C"  void PluginFunctions_arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs_m10115510 (Il2CppObject * __this /* static, unused */, int32_t ___textureID_L0, int32_t ___textureID_R1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginFunctions::arwGetMarkerPatternCount(System.Int32)
extern "C"  int32_t PluginFunctions_arwGetMarkerPatternCount_m3403487639 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwGetMarkerPatternConfig(System.Int32,System.Int32,System.Single[],System.Single&,System.Single&,System.Int32&,System.Int32&)
extern "C"  bool PluginFunctions_arwGetMarkerPatternConfig_m3210735196 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___patternID1, SingleU5BU5D_t577127397* ___matrix2, float* ___width3, float* ___height4, int32_t* ___imageSizeX5, int32_t* ___imageSizeY6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwGetMarkerPatternImage(System.Int32,System.Int32,UnityEngine.Color[])
extern "C"  bool PluginFunctions_arwGetMarkerPatternImage_m1110189182 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___patternID1, ColorU5BU5D_t672350442* ___colors2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwGetMarkerOptionBool(System.Int32,System.Int32)
extern "C"  bool PluginFunctions_arwGetMarkerOptionBool_m1227531826 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::arwSetMarkerOptionBool(System.Int32,System.Int32,System.Boolean)
extern "C"  void PluginFunctions_arwSetMarkerOptionBool_m1349018745 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, bool ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginFunctions::arwGetMarkerOptionInt(System.Int32,System.Int32)
extern "C"  int32_t PluginFunctions_arwGetMarkerOptionInt_m4058957085 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::arwSetMarkerOptionInt(System.Int32,System.Int32,System.Int32)
extern "C"  void PluginFunctions_arwSetMarkerOptionInt_m4162112020 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, int32_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PluginFunctions::arwGetMarkerOptionFloat(System.Int32,System.Int32)
extern "C"  float PluginFunctions_arwGetMarkerOptionFloat_m3375944890 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::arwSetMarkerOptionFloat(System.Int32,System.Int32,System.Single)
extern "C"  void PluginFunctions_arwSetMarkerOptionFloat_m35491069 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::arwSetVideoDebugMode(System.Boolean)
extern "C"  void PluginFunctions_arwSetVideoDebugMode_m3436558569 (Il2CppObject * __this /* static, unused */, bool ___debug0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwGetVideoDebugMode()
extern "C"  bool PluginFunctions_arwGetVideoDebugMode_m445557160 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::arwSetVideoThreshold(System.Int32)
extern "C"  void PluginFunctions_arwSetVideoThreshold_m770268112 (Il2CppObject * __this /* static, unused */, int32_t ___threshold0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginFunctions::arwGetVideoThreshold()
extern "C"  int32_t PluginFunctions_arwGetVideoThreshold_m4164593841 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::arwSetVideoThresholdMode(System.Int32)
extern "C"  void PluginFunctions_arwSetVideoThresholdMode_m3956560441 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginFunctions::arwGetVideoThresholdMode()
extern "C"  int32_t PluginFunctions_arwGetVideoThresholdMode_m1232578952 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::arwSetLabelingMode(System.Int32)
extern "C"  void PluginFunctions_arwSetLabelingMode_m2460252585 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginFunctions::arwGetLabelingMode()
extern "C"  int32_t PluginFunctions_arwGetLabelingMode_m2141811176 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::arwSetBorderSize(System.Single)
extern "C"  void PluginFunctions_arwSetBorderSize_m3074401841 (Il2CppObject * __this /* static, unused */, float ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PluginFunctions::arwGetBorderSize()
extern "C"  float PluginFunctions_arwGetBorderSize_m756391518 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::arwSetPatternDetectionMode(System.Int32)
extern "C"  void PluginFunctions_arwSetPatternDetectionMode_m825573592 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginFunctions::arwGetPatternDetectionMode()
extern "C"  int32_t PluginFunctions_arwGetPatternDetectionMode_m3294842179 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::arwSetMatrixCodeType(System.Int32)
extern "C"  void PluginFunctions_arwSetMatrixCodeType_m4153092434 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginFunctions::arwGetMatrixCodeType()
extern "C"  int32_t PluginFunctions_arwGetMatrixCodeType_m2398804801 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::arwSetImageProcMode(System.Int32)
extern "C"  void PluginFunctions_arwSetImageProcMode_m2102756940 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginFunctions::arwGetImageProcMode()
extern "C"  int32_t PluginFunctions_arwGetImageProcMode_m3143829701 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::arwSetNFTMultiMode(System.Boolean)
extern "C"  void PluginFunctions_arwSetNFTMultiMode_m2677128016 (Il2CppObject * __this /* static, unused */, bool ___on0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwGetNFTMultiMode()
extern "C"  bool PluginFunctions_arwGetNFTMultiMode_m149465679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginFunctions::arwAddMarker(System.String)
extern "C"  int32_t PluginFunctions_arwAddMarker_m2914794118 (Il2CppObject * __this /* static, unused */, String_t* ___cfg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwRemoveMarker(System.Int32)
extern "C"  bool PluginFunctions_arwRemoveMarker_m1199729578 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginFunctions::arwRemoveAllMarkers()
extern "C"  int32_t PluginFunctions_arwRemoveAllMarkers_m3243259227 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwQueryMarkerVisibility(System.Int32)
extern "C"  bool PluginFunctions_arwQueryMarkerVisibility_m1271849078 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwQueryMarkerTransformation(System.Int32,System.Single[])
extern "C"  bool PluginFunctions_arwQueryMarkerTransformation_m3138795604 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, SingleU5BU5D_t577127397* ___matrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwQueryMarkerTransformationStereo(System.Int32,System.Single[],System.Single[])
extern "C"  bool PluginFunctions_arwQueryMarkerTransformationStereo_m3818845253 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, SingleU5BU5D_t577127397* ___matrixL1, SingleU5BU5D_t577127397* ___matrixR2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFunctions::arwLoadOpticalParams(System.String,System.Byte[],System.Int32,System.Single&,System.Single&,System.Single[],System.Single[])
extern "C"  bool PluginFunctions_arwLoadOpticalParams_m780578185 (Il2CppObject * __this /* static, unused */, String_t* ___optical_param_name0, ByteU5BU5D_t3397334013* ___optical_param_buff1, int32_t ___optical_param_buffLen2, float* ___fovy_p3, float* ___aspect_p4, SingleU5BU5D_t577127397* ___m5, SingleU5BU5D_t577127397* ___p6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFunctions::.cctor()
extern "C"  void PluginFunctions__cctor_m453146558 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
