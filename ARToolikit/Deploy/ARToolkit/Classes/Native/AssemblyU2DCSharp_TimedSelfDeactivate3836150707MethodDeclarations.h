﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimedSelfDeactivate
struct TimedSelfDeactivate_t3836150707;

#include "codegen/il2cpp-codegen.h"

// System.Void TimedSelfDeactivate::.ctor()
extern "C"  void TimedSelfDeactivate__ctor_m3836804226 (TimedSelfDeactivate_t3836150707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedSelfDeactivate::OnEnable()
extern "C"  void TimedSelfDeactivate_OnEnable_m681472206 (TimedSelfDeactivate_t3836150707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedSelfDeactivate::Update()
extern "C"  void TimedSelfDeactivate_Update_m1600353847 (TimedSelfDeactivate_t3836150707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
