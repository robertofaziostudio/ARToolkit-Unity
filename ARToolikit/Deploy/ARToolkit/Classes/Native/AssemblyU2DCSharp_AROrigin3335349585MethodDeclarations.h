﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AROrigin
struct AROrigin_t3335349585;
// ARMarker
struct ARMarker_t1554260723;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AROrigin_FindMode1452273230.h"
#include "AssemblyU2DCSharp_ARMarker1554260723.h"

// System.Void AROrigin::.ctor()
extern "C"  void AROrigin__ctor_m3064099192 (AROrigin_t3335349585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AROrigin/FindMode AROrigin::get_findMarkerMode()
extern "C"  int32_t AROrigin_get_findMarkerMode_m3132928938 (AROrigin_t3335349585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AROrigin::set_findMarkerMode(AROrigin/FindMode)
extern "C"  void AROrigin_set_findMarkerMode_m3601119679 (AROrigin_t3335349585 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AROrigin::AddMarker(ARMarker,System.Boolean)
extern "C"  void AROrigin_AddMarker_m1409079797 (AROrigin_t3335349585 * __this, ARMarker_t1554260723 * ___marker0, bool ___atHeadOfList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AROrigin::RemoveMarker(ARMarker)
extern "C"  bool AROrigin_RemoveMarker_m1452674393 (AROrigin_t3335349585 * __this, ARMarker_t1554260723 * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AROrigin::RemoveAllMarkers()
extern "C"  void AROrigin_RemoveAllMarkers_m3158628672 (AROrigin_t3335349585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AROrigin::FindMarkers()
extern "C"  void AROrigin_FindMarkers_m3022729598 (AROrigin_t3335349585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AROrigin::Start()
extern "C"  void AROrigin_Start_m3396998224 (AROrigin_t3335349585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARMarker AROrigin::GetBaseMarker()
extern "C"  ARMarker_t1554260723 * AROrigin_GetBaseMarker_m4103772195 (AROrigin_t3335349585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AROrigin::OnApplicationQuit()
extern "C"  void AROrigin_OnApplicationQuit_m3423715038 (AROrigin_t3335349585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
