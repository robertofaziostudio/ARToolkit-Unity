﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// AROrigin
struct AROrigin_t3335349585;
// ARMarker
struct ARMarker_t1554260723;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_ARCamera_ViewEye928886047.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARCamera
struct  ARCamera_t431610428  : public MonoBehaviour_t1158329972
{
public:
	// AROrigin ARCamera::_origin
	AROrigin_t3335349585 * ____origin_3;
	// ARMarker ARCamera::_marker
	ARMarker_t1554260723 * ____marker_4;
	// UnityEngine.Vector3 ARCamera::arPosition
	Vector3_t2243707580  ___arPosition_5;
	// UnityEngine.Quaternion ARCamera::arRotation
	Quaternion_t4030073918  ___arRotation_6;
	// System.Boolean ARCamera::arVisible
	bool ___arVisible_7;
	// System.Single ARCamera::timeLastUpdate
	float ___timeLastUpdate_8;
	// System.Single ARCamera::timeTrackingLost
	float ___timeTrackingLost_9;
	// UnityEngine.GameObject ARCamera::eventReceiver
	GameObject_t1756533147 * ___eventReceiver_10;
	// System.Boolean ARCamera::Stereo
	bool ___Stereo_11;
	// ARCamera/ViewEye ARCamera::StereoEye
	int32_t ___StereoEye_12;
	// System.Boolean ARCamera::Optical
	bool ___Optical_13;
	// System.Boolean ARCamera::opticalSetupOK
	bool ___opticalSetupOK_14;
	// System.Int32 ARCamera::OpticalParamsFilenameIndex
	int32_t ___OpticalParamsFilenameIndex_15;
	// System.String ARCamera::OpticalParamsFilename
	String_t* ___OpticalParamsFilename_16;
	// System.Byte[] ARCamera::OpticalParamsFileContents
	ByteU5BU5D_t3397334013* ___OpticalParamsFileContents_17;
	// System.Single ARCamera::OpticalEyeLateralOffsetRight
	float ___OpticalEyeLateralOffsetRight_18;
	// UnityEngine.Matrix4x4 ARCamera::opticalViewMatrix
	Matrix4x4_t2933234003  ___opticalViewMatrix_19;

public:
	inline static int32_t get_offset_of__origin_3() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ____origin_3)); }
	inline AROrigin_t3335349585 * get__origin_3() const { return ____origin_3; }
	inline AROrigin_t3335349585 ** get_address_of__origin_3() { return &____origin_3; }
	inline void set__origin_3(AROrigin_t3335349585 * value)
	{
		____origin_3 = value;
		Il2CppCodeGenWriteBarrier(&____origin_3, value);
	}

	inline static int32_t get_offset_of__marker_4() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ____marker_4)); }
	inline ARMarker_t1554260723 * get__marker_4() const { return ____marker_4; }
	inline ARMarker_t1554260723 ** get_address_of__marker_4() { return &____marker_4; }
	inline void set__marker_4(ARMarker_t1554260723 * value)
	{
		____marker_4 = value;
		Il2CppCodeGenWriteBarrier(&____marker_4, value);
	}

	inline static int32_t get_offset_of_arPosition_5() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ___arPosition_5)); }
	inline Vector3_t2243707580  get_arPosition_5() const { return ___arPosition_5; }
	inline Vector3_t2243707580 * get_address_of_arPosition_5() { return &___arPosition_5; }
	inline void set_arPosition_5(Vector3_t2243707580  value)
	{
		___arPosition_5 = value;
	}

	inline static int32_t get_offset_of_arRotation_6() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ___arRotation_6)); }
	inline Quaternion_t4030073918  get_arRotation_6() const { return ___arRotation_6; }
	inline Quaternion_t4030073918 * get_address_of_arRotation_6() { return &___arRotation_6; }
	inline void set_arRotation_6(Quaternion_t4030073918  value)
	{
		___arRotation_6 = value;
	}

	inline static int32_t get_offset_of_arVisible_7() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ___arVisible_7)); }
	inline bool get_arVisible_7() const { return ___arVisible_7; }
	inline bool* get_address_of_arVisible_7() { return &___arVisible_7; }
	inline void set_arVisible_7(bool value)
	{
		___arVisible_7 = value;
	}

	inline static int32_t get_offset_of_timeLastUpdate_8() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ___timeLastUpdate_8)); }
	inline float get_timeLastUpdate_8() const { return ___timeLastUpdate_8; }
	inline float* get_address_of_timeLastUpdate_8() { return &___timeLastUpdate_8; }
	inline void set_timeLastUpdate_8(float value)
	{
		___timeLastUpdate_8 = value;
	}

	inline static int32_t get_offset_of_timeTrackingLost_9() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ___timeTrackingLost_9)); }
	inline float get_timeTrackingLost_9() const { return ___timeTrackingLost_9; }
	inline float* get_address_of_timeTrackingLost_9() { return &___timeTrackingLost_9; }
	inline void set_timeTrackingLost_9(float value)
	{
		___timeTrackingLost_9 = value;
	}

	inline static int32_t get_offset_of_eventReceiver_10() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ___eventReceiver_10)); }
	inline GameObject_t1756533147 * get_eventReceiver_10() const { return ___eventReceiver_10; }
	inline GameObject_t1756533147 ** get_address_of_eventReceiver_10() { return &___eventReceiver_10; }
	inline void set_eventReceiver_10(GameObject_t1756533147 * value)
	{
		___eventReceiver_10 = value;
		Il2CppCodeGenWriteBarrier(&___eventReceiver_10, value);
	}

	inline static int32_t get_offset_of_Stereo_11() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ___Stereo_11)); }
	inline bool get_Stereo_11() const { return ___Stereo_11; }
	inline bool* get_address_of_Stereo_11() { return &___Stereo_11; }
	inline void set_Stereo_11(bool value)
	{
		___Stereo_11 = value;
	}

	inline static int32_t get_offset_of_StereoEye_12() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ___StereoEye_12)); }
	inline int32_t get_StereoEye_12() const { return ___StereoEye_12; }
	inline int32_t* get_address_of_StereoEye_12() { return &___StereoEye_12; }
	inline void set_StereoEye_12(int32_t value)
	{
		___StereoEye_12 = value;
	}

	inline static int32_t get_offset_of_Optical_13() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ___Optical_13)); }
	inline bool get_Optical_13() const { return ___Optical_13; }
	inline bool* get_address_of_Optical_13() { return &___Optical_13; }
	inline void set_Optical_13(bool value)
	{
		___Optical_13 = value;
	}

	inline static int32_t get_offset_of_opticalSetupOK_14() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ___opticalSetupOK_14)); }
	inline bool get_opticalSetupOK_14() const { return ___opticalSetupOK_14; }
	inline bool* get_address_of_opticalSetupOK_14() { return &___opticalSetupOK_14; }
	inline void set_opticalSetupOK_14(bool value)
	{
		___opticalSetupOK_14 = value;
	}

	inline static int32_t get_offset_of_OpticalParamsFilenameIndex_15() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ___OpticalParamsFilenameIndex_15)); }
	inline int32_t get_OpticalParamsFilenameIndex_15() const { return ___OpticalParamsFilenameIndex_15; }
	inline int32_t* get_address_of_OpticalParamsFilenameIndex_15() { return &___OpticalParamsFilenameIndex_15; }
	inline void set_OpticalParamsFilenameIndex_15(int32_t value)
	{
		___OpticalParamsFilenameIndex_15 = value;
	}

	inline static int32_t get_offset_of_OpticalParamsFilename_16() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ___OpticalParamsFilename_16)); }
	inline String_t* get_OpticalParamsFilename_16() const { return ___OpticalParamsFilename_16; }
	inline String_t** get_address_of_OpticalParamsFilename_16() { return &___OpticalParamsFilename_16; }
	inline void set_OpticalParamsFilename_16(String_t* value)
	{
		___OpticalParamsFilename_16 = value;
		Il2CppCodeGenWriteBarrier(&___OpticalParamsFilename_16, value);
	}

	inline static int32_t get_offset_of_OpticalParamsFileContents_17() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ___OpticalParamsFileContents_17)); }
	inline ByteU5BU5D_t3397334013* get_OpticalParamsFileContents_17() const { return ___OpticalParamsFileContents_17; }
	inline ByteU5BU5D_t3397334013** get_address_of_OpticalParamsFileContents_17() { return &___OpticalParamsFileContents_17; }
	inline void set_OpticalParamsFileContents_17(ByteU5BU5D_t3397334013* value)
	{
		___OpticalParamsFileContents_17 = value;
		Il2CppCodeGenWriteBarrier(&___OpticalParamsFileContents_17, value);
	}

	inline static int32_t get_offset_of_OpticalEyeLateralOffsetRight_18() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ___OpticalEyeLateralOffsetRight_18)); }
	inline float get_OpticalEyeLateralOffsetRight_18() const { return ___OpticalEyeLateralOffsetRight_18; }
	inline float* get_address_of_OpticalEyeLateralOffsetRight_18() { return &___OpticalEyeLateralOffsetRight_18; }
	inline void set_OpticalEyeLateralOffsetRight_18(float value)
	{
		___OpticalEyeLateralOffsetRight_18 = value;
	}

	inline static int32_t get_offset_of_opticalViewMatrix_19() { return static_cast<int32_t>(offsetof(ARCamera_t431610428, ___opticalViewMatrix_19)); }
	inline Matrix4x4_t2933234003  get_opticalViewMatrix_19() const { return ___opticalViewMatrix_19; }
	inline Matrix4x4_t2933234003 * get_address_of_opticalViewMatrix_19() { return &___opticalViewMatrix_19; }
	inline void set_opticalViewMatrix_19(Matrix4x4_t2933234003  value)
	{
		___opticalViewMatrix_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
