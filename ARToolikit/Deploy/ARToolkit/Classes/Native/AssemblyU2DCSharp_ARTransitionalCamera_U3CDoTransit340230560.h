﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARController
struct ARController_t593870669;
// ARTransitionalCamera
struct ARTransitionalCamera_t3582277450;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARTransitionalCamera/<DoTransition>c__Iterator0
struct  U3CDoTransitionU3Ec__Iterator0_t340230560  : public Il2CppObject
{
public:
	// ARController ARTransitionalCamera/<DoTransition>c__Iterator0::<artoolkit>__0
	ARController_t593870669 * ___U3CartoolkitU3E__0_0;
	// System.Boolean ARTransitionalCamera/<DoTransition>c__Iterator0::flyIn
	bool ___flyIn_1;
	// System.Single ARTransitionalCamera/<DoTransition>c__Iterator0::<transitionSpeed>__1
	float ___U3CtransitionSpeedU3E__1_2;
	// System.Boolean ARTransitionalCamera/<DoTransition>c__Iterator0::<transitioning>__2
	bool ___U3CtransitioningU3E__2_3;
	// ARTransitionalCamera ARTransitionalCamera/<DoTransition>c__Iterator0::$this
	ARTransitionalCamera_t3582277450 * ___U24this_4;
	// System.Object ARTransitionalCamera/<DoTransition>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean ARTransitionalCamera/<DoTransition>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 ARTransitionalCamera/<DoTransition>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CartoolkitU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoTransitionU3Ec__Iterator0_t340230560, ___U3CartoolkitU3E__0_0)); }
	inline ARController_t593870669 * get_U3CartoolkitU3E__0_0() const { return ___U3CartoolkitU3E__0_0; }
	inline ARController_t593870669 ** get_address_of_U3CartoolkitU3E__0_0() { return &___U3CartoolkitU3E__0_0; }
	inline void set_U3CartoolkitU3E__0_0(ARController_t593870669 * value)
	{
		___U3CartoolkitU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CartoolkitU3E__0_0, value);
	}

	inline static int32_t get_offset_of_flyIn_1() { return static_cast<int32_t>(offsetof(U3CDoTransitionU3Ec__Iterator0_t340230560, ___flyIn_1)); }
	inline bool get_flyIn_1() const { return ___flyIn_1; }
	inline bool* get_address_of_flyIn_1() { return &___flyIn_1; }
	inline void set_flyIn_1(bool value)
	{
		___flyIn_1 = value;
	}

	inline static int32_t get_offset_of_U3CtransitionSpeedU3E__1_2() { return static_cast<int32_t>(offsetof(U3CDoTransitionU3Ec__Iterator0_t340230560, ___U3CtransitionSpeedU3E__1_2)); }
	inline float get_U3CtransitionSpeedU3E__1_2() const { return ___U3CtransitionSpeedU3E__1_2; }
	inline float* get_address_of_U3CtransitionSpeedU3E__1_2() { return &___U3CtransitionSpeedU3E__1_2; }
	inline void set_U3CtransitionSpeedU3E__1_2(float value)
	{
		___U3CtransitionSpeedU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CtransitioningU3E__2_3() { return static_cast<int32_t>(offsetof(U3CDoTransitionU3Ec__Iterator0_t340230560, ___U3CtransitioningU3E__2_3)); }
	inline bool get_U3CtransitioningU3E__2_3() const { return ___U3CtransitioningU3E__2_3; }
	inline bool* get_address_of_U3CtransitioningU3E__2_3() { return &___U3CtransitioningU3E__2_3; }
	inline void set_U3CtransitioningU3E__2_3(bool value)
	{
		___U3CtransitioningU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CDoTransitionU3Ec__Iterator0_t340230560, ___U24this_4)); }
	inline ARTransitionalCamera_t3582277450 * get_U24this_4() const { return ___U24this_4; }
	inline ARTransitionalCamera_t3582277450 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(ARTransitionalCamera_t3582277450 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CDoTransitionU3Ec__Iterator0_t340230560, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CDoTransitionU3Ec__Iterator0_t340230560, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CDoTransitionU3Ec__Iterator0_t340230560, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
