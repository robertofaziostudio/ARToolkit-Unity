﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARTransitionalCamera
struct ARTransitionalCamera_t3582277450;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void ARTransitionalCamera::.ctor()
extern "C"  void ARTransitionalCamera__ctor_m2916672927 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ARTransitionalCamera::DoTransition(System.Boolean)
extern "C"  Il2CppObject * ARTransitionalCamera_DoTransition_m1186448882 (ARTransitionalCamera_t3582277450 * __this, bool ___flyIn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARTransitionalCamera::transitionIn()
extern "C"  void ARTransitionalCamera_transitionIn_m3162725215 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARTransitionalCamera::transitionOut()
extern "C"  void ARTransitionalCamera_transitionOut_m4140032048 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARTransitionalCamera::Start()
extern "C"  void ARTransitionalCamera_Start_m1838311147 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARTransitionalCamera::Update()
extern "C"  void ARTransitionalCamera_Update_m1478680 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARTransitionalCamera::OnPreRender()
extern "C"  void ARTransitionalCamera_OnPreRender_m1212776491 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARTransitionalCamera::ApplyTracking()
extern "C"  void ARTransitionalCamera_ApplyTracking_m3447604840 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARTransitionalCamera::OnGUI()
extern "C"  void ARTransitionalCamera_OnGUI_m997302209 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
