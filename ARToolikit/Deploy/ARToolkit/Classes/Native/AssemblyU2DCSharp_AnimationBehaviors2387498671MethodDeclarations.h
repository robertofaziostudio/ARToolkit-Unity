﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationBehaviors
struct AnimationBehaviors_t2387498671;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AnimationBehaviors::.ctor()
extern "C"  void AnimationBehaviors__ctor_m3357602052 (AnimationBehaviors_t2387498671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationBehaviors::Awake()
extern "C"  void AnimationBehaviors_Awake_m2728602575 (AnimationBehaviors_t2387498671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationBehaviors::DeactivateSelf()
extern "C"  void AnimationBehaviors_DeactivateSelf_m2230540046 (AnimationBehaviors_t2387498671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationBehaviors::PlaySound(System.String)
extern "C"  void AnimationBehaviors_PlaySound_m3927245007 (AnimationBehaviors_t2387498671 * __this, String_t* ___whichSound0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationBehaviors::PlayMainAudioLoop()
extern "C"  void AnimationBehaviors_PlayMainAudioLoop_m20930399 (AnimationBehaviors_t2387498671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationBehaviors::StopAudio()
extern "C"  void AnimationBehaviors_StopAudio_m2575889834 (AnimationBehaviors_t2387498671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationBehaviors::TriggerGameStart()
extern "C"  void AnimationBehaviors_TriggerGameStart_m1506305612 (AnimationBehaviors_t2387498671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
