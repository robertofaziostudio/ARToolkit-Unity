﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.WebClient
struct WebClient_t1432723993;
// System.String
struct String_t;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String[]
struct StringU5BU5D_t1642385972;
// ARController
struct ARController_t593870669;
// ARTrackedObject
struct ARTrackedObject_t1684152848;
// Main
struct Main_t2809994845;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Main_AppState1441107354.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main
struct  Main_t2809994845  : public MonoBehaviour_t1158329972
{
public:
	// Main/AppState Main::curState
	int32_t ___curState_2;
	// System.Net.WebClient Main::web
	WebClient_t1432723993 * ___web_3;
	// System.String Main::stored
	String_t* ___stored_5;
	// UnityEngine.UI.InputField Main::codice
	InputField_t1631627530 * ___codice_7;
	// UnityEngine.UI.InputField Main::totale
	InputField_t1631627530 * ___totale_8;
	// UnityEngine.GameObject Main::panel
	GameObject_t1756533147 * ___panel_9;
	// UnityEngine.GameObject Main::panelPostPlay
	GameObject_t1756533147 * ___panelPostPlay_10;
	// UnityEngine.GameObject Main::panelRecap
	GameObject_t1756533147 * ___panelRecap_11;
	// UnityEngine.UI.Text Main::txtResult
	Text_t356221433 * ___txtResult_12;
	// UnityEngine.UI.Text Main::txtPremi
	Text_t356221433 * ___txtPremi_13;
	// System.String Main::playerID
	String_t* ___playerID_14;
	// System.Int32 Main::curPlay
	int32_t ___curPlay_16;
	// System.Int32 Main::numPlays
	int32_t ___numPlays_17;
	// System.String[] Main::gameResults
	StringU5BU5D_t1642385972* ___gameResults_18;
	// ARController Main::myARController
	ARController_t593870669 * ___myARController_20;
	// ARTrackedObject Main::myARTtrackedObject
	ARTrackedObject_t1684152848 * ___myARTtrackedObject_21;

public:
	inline static int32_t get_offset_of_curState_2() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___curState_2)); }
	inline int32_t get_curState_2() const { return ___curState_2; }
	inline int32_t* get_address_of_curState_2() { return &___curState_2; }
	inline void set_curState_2(int32_t value)
	{
		___curState_2 = value;
	}

	inline static int32_t get_offset_of_web_3() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___web_3)); }
	inline WebClient_t1432723993 * get_web_3() const { return ___web_3; }
	inline WebClient_t1432723993 ** get_address_of_web_3() { return &___web_3; }
	inline void set_web_3(WebClient_t1432723993 * value)
	{
		___web_3 = value;
		Il2CppCodeGenWriteBarrier(&___web_3, value);
	}

	inline static int32_t get_offset_of_stored_5() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___stored_5)); }
	inline String_t* get_stored_5() const { return ___stored_5; }
	inline String_t** get_address_of_stored_5() { return &___stored_5; }
	inline void set_stored_5(String_t* value)
	{
		___stored_5 = value;
		Il2CppCodeGenWriteBarrier(&___stored_5, value);
	}

	inline static int32_t get_offset_of_codice_7() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___codice_7)); }
	inline InputField_t1631627530 * get_codice_7() const { return ___codice_7; }
	inline InputField_t1631627530 ** get_address_of_codice_7() { return &___codice_7; }
	inline void set_codice_7(InputField_t1631627530 * value)
	{
		___codice_7 = value;
		Il2CppCodeGenWriteBarrier(&___codice_7, value);
	}

	inline static int32_t get_offset_of_totale_8() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___totale_8)); }
	inline InputField_t1631627530 * get_totale_8() const { return ___totale_8; }
	inline InputField_t1631627530 ** get_address_of_totale_8() { return &___totale_8; }
	inline void set_totale_8(InputField_t1631627530 * value)
	{
		___totale_8 = value;
		Il2CppCodeGenWriteBarrier(&___totale_8, value);
	}

	inline static int32_t get_offset_of_panel_9() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___panel_9)); }
	inline GameObject_t1756533147 * get_panel_9() const { return ___panel_9; }
	inline GameObject_t1756533147 ** get_address_of_panel_9() { return &___panel_9; }
	inline void set_panel_9(GameObject_t1756533147 * value)
	{
		___panel_9 = value;
		Il2CppCodeGenWriteBarrier(&___panel_9, value);
	}

	inline static int32_t get_offset_of_panelPostPlay_10() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___panelPostPlay_10)); }
	inline GameObject_t1756533147 * get_panelPostPlay_10() const { return ___panelPostPlay_10; }
	inline GameObject_t1756533147 ** get_address_of_panelPostPlay_10() { return &___panelPostPlay_10; }
	inline void set_panelPostPlay_10(GameObject_t1756533147 * value)
	{
		___panelPostPlay_10 = value;
		Il2CppCodeGenWriteBarrier(&___panelPostPlay_10, value);
	}

	inline static int32_t get_offset_of_panelRecap_11() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___panelRecap_11)); }
	inline GameObject_t1756533147 * get_panelRecap_11() const { return ___panelRecap_11; }
	inline GameObject_t1756533147 ** get_address_of_panelRecap_11() { return &___panelRecap_11; }
	inline void set_panelRecap_11(GameObject_t1756533147 * value)
	{
		___panelRecap_11 = value;
		Il2CppCodeGenWriteBarrier(&___panelRecap_11, value);
	}

	inline static int32_t get_offset_of_txtResult_12() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___txtResult_12)); }
	inline Text_t356221433 * get_txtResult_12() const { return ___txtResult_12; }
	inline Text_t356221433 ** get_address_of_txtResult_12() { return &___txtResult_12; }
	inline void set_txtResult_12(Text_t356221433 * value)
	{
		___txtResult_12 = value;
		Il2CppCodeGenWriteBarrier(&___txtResult_12, value);
	}

	inline static int32_t get_offset_of_txtPremi_13() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___txtPremi_13)); }
	inline Text_t356221433 * get_txtPremi_13() const { return ___txtPremi_13; }
	inline Text_t356221433 ** get_address_of_txtPremi_13() { return &___txtPremi_13; }
	inline void set_txtPremi_13(Text_t356221433 * value)
	{
		___txtPremi_13 = value;
		Il2CppCodeGenWriteBarrier(&___txtPremi_13, value);
	}

	inline static int32_t get_offset_of_playerID_14() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___playerID_14)); }
	inline String_t* get_playerID_14() const { return ___playerID_14; }
	inline String_t** get_address_of_playerID_14() { return &___playerID_14; }
	inline void set_playerID_14(String_t* value)
	{
		___playerID_14 = value;
		Il2CppCodeGenWriteBarrier(&___playerID_14, value);
	}

	inline static int32_t get_offset_of_curPlay_16() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___curPlay_16)); }
	inline int32_t get_curPlay_16() const { return ___curPlay_16; }
	inline int32_t* get_address_of_curPlay_16() { return &___curPlay_16; }
	inline void set_curPlay_16(int32_t value)
	{
		___curPlay_16 = value;
	}

	inline static int32_t get_offset_of_numPlays_17() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___numPlays_17)); }
	inline int32_t get_numPlays_17() const { return ___numPlays_17; }
	inline int32_t* get_address_of_numPlays_17() { return &___numPlays_17; }
	inline void set_numPlays_17(int32_t value)
	{
		___numPlays_17 = value;
	}

	inline static int32_t get_offset_of_gameResults_18() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___gameResults_18)); }
	inline StringU5BU5D_t1642385972* get_gameResults_18() const { return ___gameResults_18; }
	inline StringU5BU5D_t1642385972** get_address_of_gameResults_18() { return &___gameResults_18; }
	inline void set_gameResults_18(StringU5BU5D_t1642385972* value)
	{
		___gameResults_18 = value;
		Il2CppCodeGenWriteBarrier(&___gameResults_18, value);
	}

	inline static int32_t get_offset_of_myARController_20() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___myARController_20)); }
	inline ARController_t593870669 * get_myARController_20() const { return ___myARController_20; }
	inline ARController_t593870669 ** get_address_of_myARController_20() { return &___myARController_20; }
	inline void set_myARController_20(ARController_t593870669 * value)
	{
		___myARController_20 = value;
		Il2CppCodeGenWriteBarrier(&___myARController_20, value);
	}

	inline static int32_t get_offset_of_myARTtrackedObject_21() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___myARTtrackedObject_21)); }
	inline ARTrackedObject_t1684152848 * get_myARTtrackedObject_21() const { return ___myARTtrackedObject_21; }
	inline ARTrackedObject_t1684152848 ** get_address_of_myARTtrackedObject_21() { return &___myARTtrackedObject_21; }
	inline void set_myARTtrackedObject_21(ARTrackedObject_t1684152848 * value)
	{
		___myARTtrackedObject_21 = value;
		Il2CppCodeGenWriteBarrier(&___myARTtrackedObject_21, value);
	}
};

struct Main_t2809994845_StaticFields
{
public:
	// System.String Main::reply
	String_t* ___reply_6;
	// System.Boolean Main::bWin
	bool ___bWin_15;
	// System.Boolean Main::bFound
	bool ___bFound_19;
	// Main Main::<main>k__BackingField
	Main_t2809994845 * ___U3CmainU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_reply_6() { return static_cast<int32_t>(offsetof(Main_t2809994845_StaticFields, ___reply_6)); }
	inline String_t* get_reply_6() const { return ___reply_6; }
	inline String_t** get_address_of_reply_6() { return &___reply_6; }
	inline void set_reply_6(String_t* value)
	{
		___reply_6 = value;
		Il2CppCodeGenWriteBarrier(&___reply_6, value);
	}

	inline static int32_t get_offset_of_bWin_15() { return static_cast<int32_t>(offsetof(Main_t2809994845_StaticFields, ___bWin_15)); }
	inline bool get_bWin_15() const { return ___bWin_15; }
	inline bool* get_address_of_bWin_15() { return &___bWin_15; }
	inline void set_bWin_15(bool value)
	{
		___bWin_15 = value;
	}

	inline static int32_t get_offset_of_bFound_19() { return static_cast<int32_t>(offsetof(Main_t2809994845_StaticFields, ___bFound_19)); }
	inline bool get_bFound_19() const { return ___bFound_19; }
	inline bool* get_address_of_bFound_19() { return &___bFound_19; }
	inline void set_bFound_19(bool value)
	{
		___bFound_19 = value;
	}

	inline static int32_t get_offset_of_U3CmainU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(Main_t2809994845_StaticFields, ___U3CmainU3Ek__BackingField_22)); }
	inline Main_t2809994845 * get_U3CmainU3Ek__BackingField_22() const { return ___U3CmainU3Ek__BackingField_22; }
	inline Main_t2809994845 ** get_address_of_U3CmainU3Ek__BackingField_22() { return &___U3CmainU3Ek__BackingField_22; }
	inline void set_U3CmainU3Ek__BackingField_22(Main_t2809994845 * value)
	{
		___U3CmainU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmainU3Ek__BackingField_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
