﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FollowCameraPrecise
struct FollowCameraPrecise_t1971515421;

#include "codegen/il2cpp-codegen.h"

// System.Void FollowCameraPrecise::.ctor()
extern "C"  void FollowCameraPrecise__ctor_m207961366 (FollowCameraPrecise_t1971515421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FollowCameraPrecise::LateUpdate()
extern "C"  void FollowCameraPrecise_LateUpdate_m1186855005 (FollowCameraPrecise_t1971515421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
