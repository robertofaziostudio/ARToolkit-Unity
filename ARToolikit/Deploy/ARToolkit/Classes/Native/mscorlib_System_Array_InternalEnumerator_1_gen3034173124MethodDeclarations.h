﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3034173124.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22175420862.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ContentMode,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2631294164_gshared (InternalEnumerator_1_t3034173124 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2631294164(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3034173124 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2631294164_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ContentMode,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m904812740_gshared (InternalEnumerator_1_t3034173124 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m904812740(__this, method) ((  void (*) (InternalEnumerator_1_t3034173124 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m904812740_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ContentMode,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3135116272_gshared (InternalEnumerator_1_t3034173124 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3135116272(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3034173124 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3135116272_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ContentMode,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3736893827_gshared (InternalEnumerator_1_t3034173124 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3736893827(__this, method) ((  void (*) (InternalEnumerator_1_t3034173124 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3736893827_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ContentMode,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m421894364_gshared (InternalEnumerator_1_t3034173124 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m421894364(__this, method) ((  bool (*) (InternalEnumerator_1_t3034173124 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m421894364_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ContentMode,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t2175420862  InternalEnumerator_1_get_Current_m3029400715_gshared (InternalEnumerator_1_t3034173124 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3029400715(__this, method) ((  KeyValuePair_2_t2175420862  (*) (InternalEnumerator_1_t3034173124 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3029400715_gshared)(__this, method)
