﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AnimationBehaviors
struct AnimationBehaviors_t2387498671;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// Animazioni
struct Animazioni_t3270717623;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// ARCamera
struct ARCamera_t431610428;
// UnityEngine.Camera
struct Camera_t189460977;
// AROrigin
struct AROrigin_t3335349585;
// ARMarker
struct ARMarker_t1554260723;
// ARController
struct ARController_t593870669;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// ARPattern[]
struct ARPatternU5BU5D_t59458152;
// PluginFunctions/LogCallback
struct LogCallback_t2143553514;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Single[]
struct SingleU5BU5D_t577127397;
// ARMarker[]
struct ARMarkerU5BU5D_t3553995746;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// ARPattern
struct ARPattern_t3906840165;
// ARTrackedCamera
struct ARTrackedCamera_t3950879424;
// ARTrackedObject
struct ARTrackedObject_t1684152848;
// ARTransitionalCamera
struct ARTransitionalCamera_t3582277450;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Skybox
struct Skybox_t2033495038;
// ARTransitionalCamera/<DoTransition>c__Iterator0
struct U3CDoTransitionU3Ec__Iterator0_t340230560;
// CharacterBehaviors
struct CharacterBehaviors_t674834278;
// UnityEngine.Animation
struct Animation_t2068071072;
// UnityEngine.Collider
struct Collider_t3497673348;
// FisticuffsController
struct FisticuffsController_t308090938;
// FisticuffsController/<EndBell>c__Iterator1
struct U3CEndBellU3Ec__Iterator1_t868562403;
// FisticuffsController/<IntroStart>c__Iterator0
struct U3CIntroStartU3Ec__Iterator0_t3871887700;
// FollowCameraPrecise
struct FollowCameraPrecise_t1971515421;
// GloveScript
struct GloveScript_t3172908102;
// JsonManager
struct JsonManager_t1028810929;
// Logo
struct Logo_t2205197457;
// LookAtCameraFisticuffs
struct LookAtCameraFisticuffs_t2130711525;
// Main
struct Main_t2809994845;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// TimedSelfDeactivate
struct TimedSelfDeactivate_t3836150707;
// TimedSelfDestruct
struct TimedSelfDestruct_t4043215813;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3762068664.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3762068664MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationBehaviors2387498671.h"
#include "AssemblyU2DCSharp_AnimationBehaviors2387498671MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_FisticuffsController308090938MethodDeclarations.h"
#include "AssemblyU2DCSharp_FisticuffsController308090938.h"
#include "AssemblyU2DCSharp_Animazioni3270717623.h"
#include "AssemblyU2DCSharp_Animazioni3270717623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "AssemblyU2DCSharp_Main2809994845.h"
#include "AssemblyU2DCSharp_Main2809994845MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_ARCamera431610428.h"
#include "AssemblyU2DCSharp_ARCamera431610428MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARCamera_ViewEye928886047.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte3683104436.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "AssemblyU2DCSharp_PluginFunctions2887339240MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARController593870669MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARUtilityFunctions4213713322MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CameraClearFlags452084705.h"
#include "AssemblyU2DCSharp_AROrigin3335349585.h"
#include "AssemblyU2DCSharp_ARMarker1554260723.h"
#include "AssemblyU2DCSharp_AROrigin3335349585MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARMarker1554260723MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARCamera_ViewEye928886047MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARController593870669.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "AssemblyU2DCSharp_ContentAlign2517740362.h"
#include "AssemblyU2DCSharp_ContentMode2920913530.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitLabelingMod925999490.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitMatrixCode3871848761.h"
#include "AssemblyU2DCSharp_ARController_AR_LOG_LEVEL1976762171.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Action_1_gen1831019615.h"
#include "AssemblyU2DCSharp_ARNativePluginStatic4005840597MethodDeclarations.h"
#include "mscorlib_System_Threading_Thread241561612MethodDeclarations.h"
#include "AssemblyU2DCSharp_PluginFunctions_LogCallback2143553514MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "AssemblyU2DCSharp_PluginFunctions_LogCallback2143553514.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "UnityEngine_UnityEngine_SystemInfo2353426895MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources339470017MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845.h"
#include "AssemblyU2DCSharp_ARController_ARW_ERROR1424361661.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitThresholdM2161580787.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitPatternDet1396884775.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitImageProcM1927279569.h"
#include "mscorlib_System_Environment3662374671MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenOrientation4019489636.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GL1765937205MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader2430389951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader2430389951.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_MeshRenderer1268241104.h"
#include "UnityEngine_UnityEngine_TextureFormat1386130234.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "UnityEngine_UnityEngine_FilterMode10814199.h"
#include "UnityEngine_UnityEngine_TextureWrapMode3683976566.h"
#include "UnityEngine_UnityEngine_Color32874517518MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh1356156583MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107MethodDeclarations.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1831019615MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "UnityEngine_UnityEngine_GUI4082743951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin1436893342MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleState3801000545MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin1436893342.h"
#include "UnityEngine_UnityEngine_GUIStyleState3801000545.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3486805455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayout2579273657MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3757846578MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3486805455.h"
#include "UnityEngine_UnityEngine_GUILayoutOption4183744904.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3757846578.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resolution3693662728.h"
#include "UnityEngine_UnityEngine_Resolution3693662728MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent4210063000MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent4210063000.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3314526645MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3314526645.h"
#include "AssemblyU2DCSharp_ARController_AR_LOG_LEVEL1976762171MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitImageProcM1927279569MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitLabelingMod925999490MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitMatrixCode3871848761MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitPatternDet1396884775MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitThresholdM2161580787MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARController_ARW_ERROR1424361661MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARController_ARW_UNITY_RENDER_EV3085043805.h"
#include "AssemblyU2DCSharp_ARController_ARW_UNITY_RENDER_EV3085043805MethodDeclarations.h"
#include "mscorlib_System_IO_Path41728875MethodDeclarations.h"
#include "mscorlib_System_IO_File1930543328MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "AssemblyU2DCSharp_ARPattern3906840165MethodDeclarations.h"
#include "AssemblyU2DCSharp_MarkerType961965194.h"
#include "AssemblyU2DCSharp_PluginFunctions2887339240.h"
#include "AssemblyU2DCSharp_ARPattern3906840165.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1569850338MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1569850338.h"
#include "AssemblyU2DCSharp_ARNativePlugin494404617.h"
#include "AssemblyU2DCSharp_ARNativePlugin494404617MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_ARNativePluginStatic4005840597.h"
#include "mscorlib_System_Collections_Generic_List_1_gen923381855MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen923381855.h"
#include "AssemblyU2DCSharp_AROrigin_FindMode1452273230.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat458111529.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat458111529MethodDeclarations.h"
#include "AssemblyU2DCSharp_AROrigin_FindMode1452273230MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "AssemblyU2DCSharp_ARTrackedCamera3950879424.h"
#include "AssemblyU2DCSharp_ARTrackedCamera3950879424MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMessageOptions1414041951.h"
#include "AssemblyU2DCSharp_ARTrackedObject1684152848.h"
#include "AssemblyU2DCSharp_ARTrackedObject1684152848MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARTransitionalCamera3582277450.h"
#include "AssemblyU2DCSharp_ARTransitionalCamera3582277450MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARTransitionalCamera_U3CDoTransit340230560MethodDeclarations.h"
#include "AssemblyU2DCSharp_ARTransitionalCamera_U3CDoTransit340230560.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_Skybox2033495038MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Skybox2033495038.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "UnityEngine_UnityEngine_DeviceType2044541946.h"
#include "UnityEngine_UnityEngine_Touch407273883MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_ARUtilityFunctions4213713322.h"
#include "UnityEngine_UnityEngine_Vector42243707581MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "AssemblyU2DCSharp_ARWMarkerOption4121286531.h"
#include "AssemblyU2DCSharp_ARWMarkerOption4121286531MethodDeclarations.h"
#include "AssemblyU2DCSharp_CharacterBehaviors674834278.h"
#include "AssemblyU2DCSharp_CharacterBehaviors674834278MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation2068071072.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1125654279MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1125654279.h"
#include "UnityEngine_UnityEngine_Animation2068071072MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider3497673348MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_Physics634932869MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_TouchPhase2458120420.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320MethodDeclarations.h"
#include "AssemblyU2DCSharp_CharacterBehaviors_PopupMode1897899772.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "AssemblyU2DCSharp_CharacterBehaviors_PopupMode1897899772MethodDeclarations.h"
#include "AssemblyU2DCSharp_ContentAlign2517740362MethodDeclarations.h"
#include "AssemblyU2DCSharp_ContentMode2920913530MethodDeclarations.h"
#include "AssemblyU2DCSharp_FisticuffsController_U3CIntroSta3871887700MethodDeclarations.h"
#include "AssemblyU2DCSharp_FisticuffsController_U3CIntroSta3871887700.h"
#include "AssemblyU2DCSharp_FisticuffsController_U3CEndBellU3868562403MethodDeclarations.h"
#include "AssemblyU2DCSharp_FisticuffsController_U3CEndBellU3868562403.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "AssemblyU2DCSharp_FollowCameraPrecise1971515421.h"
#include "AssemblyU2DCSharp_FollowCameraPrecise1971515421MethodDeclarations.h"
#include "AssemblyU2DCSharp_GloveScript3172908102.h"
#include "AssemblyU2DCSharp_GloveScript3172908102MethodDeclarations.h"
#include "AssemblyU2DCSharp_JsonManager1028810929.h"
#include "AssemblyU2DCSharp_JsonManager1028810929MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlDocument3649534162MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlAttributeCollection3359885287MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlNode616554813.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"
#include "System_Xml_System_Xml_XmlNode616554813MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlAttributeCollection3359885287.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap145210370MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap145210370.h"
#include "System_Xml_System_Xml_XmlAttribute175731005.h"
#include "AssemblyU2DCSharp_Logo2205197457.h"
#include "AssemblyU2DCSharp_Logo2205197457MethodDeclarations.h"
#include "AssemblyU2DCSharp_LookAtCameraFisticuffs2130711525.h"
#include "AssemblyU2DCSharp_LookAtCameraFisticuffs2130711525MethodDeclarations.h"
#include "System_System_Net_WebClient1432723993MethodDeclarations.h"
#include "System_System_Net_WebClient1432723993.h"
#include "AssemblyU2DCSharp_Main_AppState1441107354.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530.h"
#include "mscorlib_System_Char3454481338.h"
#include "AssemblyU2DCSharp_Main_AppState1441107354MethodDeclarations.h"
#include "AssemblyU2DCSharp_MarkerType961965194MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle3409268066MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle3409268066.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandleTy1970708122.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AssemblyU2DCSharp_TimedSelfDeactivate3836150707.h"
#include "AssemblyU2DCSharp_TimedSelfDeactivate3836150707MethodDeclarations.h"
#include "AssemblyU2DCSharp_TimedSelfDestruct4043215813.h"
#include "AssemblyU2DCSharp_TimedSelfDestruct4043215813MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, method) ((  AudioSource_t1135106623 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3813873105(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.AudioSource>()
#define GameObject_AddComponent_TisAudioSource_t1135106623_m1060393270(__this, method) ((  AudioSource_t1135106623 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3829784634_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3829784634(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3064851704(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
#define GameObject_GetComponent_TisCamera_t189460977_m4200645945(__this, method) ((  Camera_t189460977 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInParent_TisIl2CppObject_m781417156_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponentInParent_TisIl2CppObject_m781417156(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInParent_TisIl2CppObject_m781417156_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInParent<AROrigin>()
#define GameObject_GetComponentInParent_TisAROrigin_t3335349585_m1108684983(__this, method) ((  AROrigin_t3335349585 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInParent_TisIl2CppObject_m781417156_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Camera>()
#define GameObject_AddComponent_TisCamera_t189460977_m2871093954(__this, method) ((  Camera_t189460977 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.MeshFilter>()
#define GameObject_AddComponent_TisMeshFilter_t3026937449_m2144094854(__this, method) ((  MeshFilter_t3026937449 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.MeshRenderer>()
#define GameObject_AddComponent_TisMeshRenderer_t1268241104_m740921633(__this, method) ((  MeshRenderer_t1268241104 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t257310565_m1312615893(__this, method) ((  Renderer_t257310565 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Reverse<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject* Enumerable_Reverse_TisIl2CppObject_m3692844913_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Reverse_TisIl2CppObject_m3692844913(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Reverse_TisIl2CppObject_m3692844913_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Reverse<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Reverse_TisString_t_m4073804421(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Reverse_TisIl2CppObject_m3692844913_gshared)(__this /* static, unused */, p0, method)
// !!0[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Object_FindObjectsOfType_TisIl2CppObject_m1140156812_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectsOfType_TisIl2CppObject_m1140156812(__this /* static, unused */, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisIl2CppObject_m1140156812_gshared)(__this /* static, unused */, method)
// !!0[] UnityEngine.Object::FindObjectsOfType<ARMarker>()
#define Object_FindObjectsOfType_TisARMarker_t1554260723_m2341901897(__this /* static, unused */, method) ((  ARMarkerU5BU5D_t3553995746* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisIl2CppObject_m1140156812_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Skybox>()
#define Component_GetComponent_TisSkybox_t2033495038_m3670937650(__this, method) ((  Skybox_t2033495038 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animation>()
#define GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(__this, method) ((  Animation_t2068071072 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Animation>()
#define GameObject_AddComponent_TisAnimation_t2068071072_m2090634241(__this, method) ((  Animation_t2068071072 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Collider>()
#define GameObject_GetComponent_TisCollider_t3497673348_m2914479322(__this, method) ((  Collider_t3497673348 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<CharacterBehaviors>()
#define GameObject_GetComponent_TisCharacterBehaviors_t674834278_m289165397(__this, method) ((  CharacterBehaviors_t674834278 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<CharacterBehaviors>()
#define Component_GetComponent_TisCharacterBehaviors_t674834278_m152693849(__this, method) ((  CharacterBehaviors_t674834278 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInParent<CharacterBehaviors>()
#define GameObject_GetComponentInParent_TisCharacterBehaviors_t674834278_m2550472710(__this, method) ((  CharacterBehaviors_t674834278 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInParent_TisIl2CppObject_m781417156_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Logo>()
#define GameObject_GetComponent_TisLogo_t2205197457_m4031711006(__this, method) ((  Logo_t2205197457 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<ARController>()
#define Component_GetComponent_TisARController_t593870669_m2574969960(__this, method) ((  ARController_t593870669 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<ARTrackedObject>()
#define Component_GetComponent_TisARTrackedObject_t1684152848_m1251800011(__this, method) ((  ARTrackedObject_t1684152848 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AnimationBehaviors::.ctor()
extern "C"  void AnimationBehaviors__ctor_m3357602052 (AnimationBehaviors_t2387498671 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationBehaviors::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisAudioSource_t1135106623_m1060393270_MethodInfo_var;
extern const uint32_t AnimationBehaviors_Awake_m2728602575_MetadataUsageId;
extern "C"  void AnimationBehaviors_Awake_m2728602575 (AnimationBehaviors_t2387498671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationBehaviors_Awake_m2728602575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_t1135106623 * L_0 = Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var);
		__this->set_audioSource_5(L_0);
		AudioSource_t1135106623 * L_1 = __this->get_audioSource_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		AudioSource_t1135106623 * L_4 = GameObject_AddComponent_TisAudioSource_t1135106623_m1060393270(L_3, /*hidden argument*/GameObject_AddComponent_TisAudioSource_t1135106623_m1060393270_MethodInfo_var);
		__this->set_audioSource_5(L_4);
	}

IL_002e:
	{
		return;
	}
}
// System.Void AnimationBehaviors::DeactivateSelf()
extern "C"  void AnimationBehaviors_DeactivateSelf_m2230540046 (AnimationBehaviors_t2387498671 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationBehaviors::PlaySound(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2294719110;
extern Il2CppCodeGenString* _stringLiteral1837800021;
extern Il2CppCodeGenString* _stringLiteral1987287481;
extern Il2CppCodeGenString* _stringLiteral1801206250;
extern Il2CppCodeGenString* _stringLiteral2197699722;
extern const uint32_t AnimationBehaviors_PlaySound_m3927245007_MetadataUsageId;
extern "C"  void AnimationBehaviors_PlaySound_m3927245007 (AnimationBehaviors_t2387498671 * __this, String_t* ___whichSound0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationBehaviors_PlaySound_m3927245007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___whichSound0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, _stringLiteral2294719110, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		AudioSource_t1135106623 * L_2 = __this->get_audioSource_5();
		AudioClip_t1932558630 * L_3 = __this->get_ding_2();
		NullCheck(L_2);
		AudioSource_PlayOneShot_m286472761(L_2, L_3, /*hidden argument*/NULL);
		goto IL_0087;
	}

IL_0026:
	{
		String_t* L_4 = ___whichSound0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, _stringLiteral1837800021, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004c;
		}
	}
	{
		AudioSource_t1135106623 * L_6 = __this->get_audioSource_5();
		AudioClip_t1932558630 * L_7 = __this->get_beepCount_3();
		NullCheck(L_6);
		AudioSource_PlayOneShot_m286472761(L_6, L_7, /*hidden argument*/NULL);
		goto IL_0087;
	}

IL_004c:
	{
		String_t* L_8 = ___whichSound0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral1987287481, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0072;
		}
	}
	{
		AudioSource_t1135106623 * L_10 = __this->get_audioSource_5();
		AudioClip_t1932558630 * L_11 = __this->get_fanfare_4();
		NullCheck(L_10);
		AudioSource_PlayOneShot_m286472761(L_10, L_11, /*hidden argument*/NULL);
		goto IL_0087;
	}

IL_0072:
	{
		String_t* L_12 = ___whichSound0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1801206250, L_12, _stringLiteral2197699722, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_0087:
	{
		return;
	}
}
// System.Void AnimationBehaviors::PlayMainAudioLoop()
extern "C"  void AnimationBehaviors_PlayMainAudioLoop_m20930399 (AnimationBehaviors_t2387498671 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1135106623 * L_0 = __this->get_audioSource_5();
		NullCheck(L_0);
		AudioSource_Play_m353744792(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationBehaviors::StopAudio()
extern "C"  void AnimationBehaviors_StopAudio_m2575889834 (AnimationBehaviors_t2387498671 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1135106623 * L_0 = __this->get_audioSource_5();
		NullCheck(L_0);
		AudioSource_Stop_m3452679614(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationBehaviors::TriggerGameStart()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2659413466;
extern const uint32_t AnimationBehaviors_TriggerGameStart_m1506305612_MetadataUsageId;
extern "C"  void AnimationBehaviors_TriggerGameStart_m1506305612 (AnimationBehaviors_t2387498671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationBehaviors_TriggerGameStart_m1506305612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FisticuffsController_t308090938 * L_0 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral2659413466, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		FisticuffsController_t308090938 * L_2 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		FisticuffsController_GameStart_m702030753(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Animazioni::.ctor()
extern "C"  void Animazioni__ctor_m2857345758 (Animazioni_t3270717623 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Animazioni::CheckWinner()
extern Il2CppClass* Main_t2809994845_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const uint32_t Animazioni_CheckWinner_m2031567265_MetadataUsageId;
extern "C"  void Animazioni_CheckWinner_m2031567265 (Animazioni_t3270717623 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Animazioni_CheckWinner_m2031567265_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2809994845_il2cpp_TypeInfo_var);
		bool L_0 = ((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->get_bWin_15();
		if (!L_0)
		{
			goto IL_0096;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_animationWin_2();
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_4 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_5 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_1, L_3, L_4, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_win_5(L_5);
		GameObject_t1756533147 * L_6 = __this->get_win_5();
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_parent_m3281327839(L_7, L_8, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_9 = __this->get_fireworks_4();
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_position_m1104419803(L_10, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_12 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_13 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_9, L_11, L_12, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_0 = L_13;
		GameObject_t1756533147 * L_14 = V_0;
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_16 = __this->get_win_5();
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = GameObject_get_transform_m909382139(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_parent_m3281327839(L_15, L_17, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_18 = __this->get_win_5();
		NullCheck(L_18);
		GameObject_SetActive_m2887581199(L_18, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_19 = __this->get_animationNotWin_3();
		NullCheck(L_19);
		GameObject_SetActive_m2887581199(L_19, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2809994845_il2cpp_TypeInfo_var);
		((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->set_bWin_15((bool)0);
		goto IL_00ef;
	}

IL_0096:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2809994845_il2cpp_TypeInfo_var);
		bool L_20 = ((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->get_bWin_15();
		if (L_20)
		{
			goto IL_00ef;
		}
	}
	{
		GameObject_t1756533147 * L_21 = __this->get_animationNotWin_3();
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t2243707580  L_23 = Transform_get_position_m1104419803(L_22, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_24 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_25 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_21, L_23, L_24, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_notWin_6(L_25);
		GameObject_t1756533147 * L_26 = __this->get_notWin_6();
		NullCheck(L_26);
		Transform_t3275118058 * L_27 = GameObject_get_transform_m909382139(L_26, /*hidden argument*/NULL);
		Transform_t3275118058 * L_28 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_parent_m3281327839(L_27, L_28, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_29 = __this->get_notWin_6();
		NullCheck(L_29);
		GameObject_SetActive_m2887581199(L_29, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_30 = __this->get_animationWin_2();
		NullCheck(L_30);
		GameObject_SetActive_m2887581199(L_30, (bool)0, /*hidden argument*/NULL);
	}

IL_00ef:
	{
		return;
	}
}
// System.Void ARCamera::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t ARCamera__ctor_m4072460825_MetadataUsageId;
extern "C"  void ARCamera__ctor_m4072460825 (ARCamera_t431610428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARCamera__ctor_m4072460825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t2243707580  L_0 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_arPosition_5(L_0);
		Quaternion_t4030073918  L_1 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_arRotation_6(L_1);
		__this->set_StereoEye_12(1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_OpticalParamsFilename_16(L_2);
		__this->set_OpticalParamsFileContents_17(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)0)));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ARCamera::SetupCamera(System.Single,System.Single,UnityEngine.Matrix4x4,System.Boolean&)
extern Il2CppClass* SingleU5BU5D_t577127397_il2cpp_TypeInfo_var;
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3225191644;
extern Il2CppCodeGenString* _stringLiteral2186320595;
extern Il2CppCodeGenString* _stringLiteral344882153;
extern Il2CppCodeGenString* _stringLiteral3176856118;
extern Il2CppCodeGenString* _stringLiteral1664928785;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029393;
extern const uint32_t ARCamera_SetupCamera_m2259267036_MetadataUsageId;
extern "C"  bool ARCamera_SetupCamera_m2259267036 (ARCamera_t431610428 * __this, float ___nearClipPlane0, float ___farClipPlane1, Matrix4x4_t2933234003  ___projectionMatrix2, bool* ___opticalOut3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARCamera_SetupCamera_m2259267036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Camera_t189460977 * V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	SingleU5BU5D_t577127397* V_3 = NULL;
	SingleU5BU5D_t577127397* V_4 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Camera_t189460977 * L_1 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_0, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var);
		V_0 = L_1;
		Camera_t189460977 * L_2 = V_0;
		NullCheck(L_2);
		Camera_set_orthographic_m2132888580(L_2, (bool)0, /*hidden argument*/NULL);
		Camera_t189460977 * L_3 = V_0;
		float L_4 = ___nearClipPlane0;
		NullCheck(L_3);
		Camera_set_nearClipPlane_m3510849382(L_3, L_4, /*hidden argument*/NULL);
		Camera_t189460977 * L_5 = V_0;
		float L_6 = ___farClipPlane1;
		NullCheck(L_5);
		Camera_set_farClipPlane_m1845065941(L_5, L_6, /*hidden argument*/NULL);
		bool L_7 = __this->get_Optical_13();
		if (!L_7)
		{
			goto IL_01b5;
		}
	}
	{
		V_3 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		V_4 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		ByteU5BU5D_t3397334013* L_8 = __this->get_OpticalParamsFileContents_17();
		ByteU5BU5D_t3397334013* L_9 = __this->get_OpticalParamsFileContents_17();
		NullCheck(L_9);
		SingleU5BU5D_t577127397* L_10 = V_3;
		SingleU5BU5D_t577127397* L_11 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_12 = PluginFunctions_arwLoadOpticalParams_m780578185(NULL /*static, unused*/, (String_t*)NULL, L_8, (((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))), (&V_1), (&V_2), L_10, L_11, /*hidden argument*/NULL);
		__this->set_opticalSetupOK_14(L_12);
		bool L_13 = __this->get_opticalSetupOK_14();
		if (L_13)
		{
			goto IL_0075;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral3225191644, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0075:
	{
		SingleU5BU5D_t577127397* L_14 = V_3;
		NullCheck(L_14);
		float* L_15 = ((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)12))));
		*((float*)(L_15)) = (float)((float)((float)(*((float*)L_15))*(float)(0.001f)));
		SingleU5BU5D_t577127397* L_16 = V_3;
		NullCheck(L_16);
		float* L_17 = ((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)13))));
		*((float*)(L_17)) = (float)((float)((float)(*((float*)L_17))*(float)(0.001f)));
		SingleU5BU5D_t577127397* L_18 = V_3;
		NullCheck(L_18);
		float* L_19 = ((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)14))));
		*((float*)(L_19)) = (float)((float)((float)(*((float*)L_19))*(float)(0.001f)));
		ObjectU5BU5D_t3614634134* L_20 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)((int32_t)11)));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral2186320595);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2186320595);
		ObjectU5BU5D_t3614634134* L_21 = L_20;
		float L_22 = V_1;
		float L_23 = L_22;
		Il2CppObject * L_24 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_24);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_24);
		ObjectU5BU5D_t3614634134* L_25 = L_21;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral344882153);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral344882153);
		ObjectU5BU5D_t3614634134* L_26 = L_25;
		float L_27 = V_2;
		float L_28 = L_27;
		Il2CppObject * L_29 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_29);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_29);
		ObjectU5BU5D_t3614634134* L_30 = L_26;
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, _stringLiteral3176856118);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3176856118);
		ObjectU5BU5D_t3614634134* L_31 = L_30;
		SingleU5BU5D_t577127397* L_32 = V_3;
		NullCheck(L_32);
		String_t* L_33 = Single_ToString_m2359963436(((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)12)))), _stringLiteral1664928785, /*hidden argument*/NULL);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_33);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_33);
		ObjectU5BU5D_t3614634134* L_34 = L_31;
		NullCheck(L_34);
		ArrayElementTypeCheck (L_34, _stringLiteral811305474);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral811305474);
		ObjectU5BU5D_t3614634134* L_35 = L_34;
		SingleU5BU5D_t577127397* L_36 = V_3;
		NullCheck(L_36);
		String_t* L_37 = Single_ToString_m2359963436(((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)13)))), _stringLiteral1664928785, /*hidden argument*/NULL);
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_37);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_37);
		ObjectU5BU5D_t3614634134* L_38 = L_35;
		NullCheck(L_38);
		ArrayElementTypeCheck (L_38, _stringLiteral811305474);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral811305474);
		ObjectU5BU5D_t3614634134* L_39 = L_38;
		SingleU5BU5D_t577127397* L_40 = V_3;
		NullCheck(L_40);
		String_t* L_41 = Single_ToString_m2359963436(((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)14)))), _stringLiteral1664928785, /*hidden argument*/NULL);
		NullCheck(L_39);
		ArrayElementTypeCheck (L_39, L_41);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_41);
		ObjectU5BU5D_t3614634134* L_42 = L_39;
		NullCheck(L_42);
		ArrayElementTypeCheck (L_42, _stringLiteral372029393);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral372029393);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = String_Concat_m3881798623(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		Camera_t189460977 * L_44 = V_0;
		SingleU5BU5D_t577127397* L_45 = V_4;
		Matrix4x4_t2933234003  L_46 = ARUtilityFunctions_MatrixFromFloatArray_m3542072061(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		Camera_set_projectionMatrix_m2059836755(L_44, L_46, /*hidden argument*/NULL);
		SingleU5BU5D_t577127397* L_47 = V_3;
		Matrix4x4_t2933234003  L_48 = ARUtilityFunctions_MatrixFromFloatArray_m3542072061(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		__this->set_opticalViewMatrix_19(L_48);
		float L_49 = __this->get_OpticalEyeLateralOffsetRight_18();
		if ((((float)L_49) == ((float)(0.0f))))
		{
			goto IL_019b;
		}
	}
	{
		float L_50 = __this->get_OpticalEyeLateralOffsetRight_18();
		Vector3_t2243707580  L_51;
		memset(&L_51, 0, sizeof(L_51));
		Vector3__ctor_m2638739322(&L_51, ((-L_50)), (0.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_52 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_54 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_51, L_52, L_53, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_55 = __this->get_opticalViewMatrix_19();
		Matrix4x4_t2933234003  L_56 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_54, L_55, /*hidden argument*/NULL);
		__this->set_opticalViewMatrix_19(L_56);
	}

IL_019b:
	{
		Matrix4x4_t2933234003  L_57 = __this->get_opticalViewMatrix_19();
		Matrix4x4_t2933234003  L_58 = ARUtilityFunctions_LHMatrixFromRHMatrix_m2988276788(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		__this->set_opticalViewMatrix_19(L_58);
		bool* L_59 = ___opticalOut3;
		*((int8_t*)(L_59)) = (int8_t)1;
		goto IL_01bc;
	}

IL_01b5:
	{
		Camera_t189460977 * L_60 = V_0;
		Matrix4x4_t2933234003  L_61 = ___projectionMatrix2;
		NullCheck(L_60);
		Camera_set_projectionMatrix_m2059836755(L_60, L_61, /*hidden argument*/NULL);
	}

IL_01bc:
	{
		Camera_t189460977 * L_62 = V_0;
		NullCheck(L_62);
		Camera_set_clearFlags_m4142614199(L_62, 4, /*hidden argument*/NULL);
		Camera_t189460977 * L_63 = V_0;
		NullCheck(L_63);
		Camera_set_depth_m1570376177(L_63, (2.0f), /*hidden argument*/NULL);
		Camera_t189460977 * L_64 = V_0;
		NullCheck(L_64);
		Transform_t3275118058 * L_65 = Component_get_transform_m2697483695(L_64, /*hidden argument*/NULL);
		Vector3_t2243707580  L_66;
		memset(&L_66, 0, sizeof(L_66));
		Vector3__ctor_m2638739322(&L_66, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_65);
		Transform_set_position_m2469242620(L_65, L_66, /*hidden argument*/NULL);
		Camera_t189460977 * L_67 = V_0;
		NullCheck(L_67);
		Transform_t3275118058 * L_68 = Component_get_transform_m2697483695(L_67, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_69;
		memset(&L_69, 0, sizeof(L_69));
		Quaternion__ctor_m3196903881(&L_69, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_68);
		Transform_set_rotation_m3411284563(L_68, L_69, /*hidden argument*/NULL);
		Camera_t189460977 * L_70 = V_0;
		NullCheck(L_70);
		Transform_t3275118058 * L_71 = Component_get_transform_m2697483695(L_70, /*hidden argument*/NULL);
		Vector3_t2243707580  L_72;
		memset(&L_72, 0, sizeof(L_72));
		Vector3__ctor_m2638739322(&L_72, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_71);
		Transform_set_localScale_m2325460848(L_71, L_72, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// AROrigin ARCamera::GetOrigin()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentInParent_TisAROrigin_t3335349585_m1108684983_MethodInfo_var;
extern const uint32_t ARCamera_GetOrigin_m398559295_MetadataUsageId;
extern "C"  AROrigin_t3335349585 * ARCamera_GetOrigin_m398559295 (ARCamera_t431610428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARCamera_GetOrigin_m398559295_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AROrigin_t3335349585 * L_0 = __this->get__origin_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		AROrigin_t3335349585 * L_3 = GameObject_GetComponentInParent_TisAROrigin_t3335349585_m1108684983(L_2, /*hidden argument*/GameObject_GetComponentInParent_TisAROrigin_t3335349585_m1108684983_MethodInfo_var);
		__this->set__origin_3(L_3);
	}

IL_0022:
	{
		AROrigin_t3335349585 * L_4 = __this->get__origin_3();
		return L_4;
	}
}
// ARMarker ARCamera::GetMarker()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ARCamera_GetMarker_m1448161535_MetadataUsageId;
extern "C"  ARMarker_t1554260723 * ARCamera_GetMarker_m1448161535 (ARCamera_t431610428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARCamera_GetMarker_m1448161535_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AROrigin_t3335349585 * V_0 = NULL;
	{
		AROrigin_t3335349585 * L_0 = VirtFuncInvoker0< AROrigin_t3335349585 * >::Invoke(4 /* AROrigin ARCamera::GetOrigin() */, __this);
		V_0 = L_0;
		AROrigin_t3335349585 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		return (ARMarker_t1554260723 *)NULL;
	}

IL_0015:
	{
		AROrigin_t3335349585 * L_3 = V_0;
		NullCheck(L_3);
		ARMarker_t1554260723 * L_4 = AROrigin_GetBaseMarker_m4103772195(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void ARCamera::UpdateTracking()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ARCamera_UpdateTracking_m1132195383_MetadataUsageId;
extern "C"  void ARCamera_UpdateTracking_m1132195383 (ARCamera_t431610428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARCamera_UpdateTracking_m1132195383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARMarker_t1554260723 * V_0 = NULL;
	Matrix4x4_t2933234003  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Matrix4x4_t2933234003  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Matrix4x4_t2933234003  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		float L_0 = Time_get_realtimeSinceStartup_m357614587(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timeLastUpdate_8(L_0);
		ARMarker_t1554260723 * L_1 = VirtFuncInvoker0< ARMarker_t1554260723 * >::Invoke(5 /* ARMarker ARCamera::GetMarker() */, __this);
		V_0 = L_1;
		ARMarker_t1554260723 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		bool L_4 = __this->get_arVisible_7();
		if (!L_4)
		{
			goto IL_003c;
		}
	}
	{
		float L_5 = __this->get_timeLastUpdate_8();
		__this->set_timeTrackingLost_9(L_5);
		__this->set_arVisible_7((bool)0);
	}

IL_003c:
	{
		goto IL_00dd;
	}

IL_0041:
	{
		ARMarker_t1554260723 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = ARMarker_get_Visible_m4014965141(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00bf;
		}
	}
	{
		bool L_8 = __this->get_Optical_13();
		if (!L_8)
		{
			goto IL_0081;
		}
	}
	{
		bool L_9 = __this->get_opticalSetupOK_14();
		if (!L_9)
		{
			goto IL_0081;
		}
	}
	{
		Matrix4x4_t2933234003  L_10 = __this->get_opticalViewMatrix_19();
		ARMarker_t1554260723 * L_11 = V_0;
		NullCheck(L_11);
		Matrix4x4_t2933234003  L_12 = ARMarker_get_TransformationMatrix_m3960336120(L_11, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_13 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		Matrix4x4_t2933234003  L_14 = Matrix4x4_get_inverse_m2479387736((&V_2), /*hidden argument*/NULL);
		V_1 = L_14;
		goto IL_0090;
	}

IL_0081:
	{
		ARMarker_t1554260723 * L_15 = V_0;
		NullCheck(L_15);
		Matrix4x4_t2933234003  L_16 = ARMarker_get_TransformationMatrix_m3960336120(L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		Matrix4x4_t2933234003  L_17 = Matrix4x4_get_inverse_m2479387736((&V_3), /*hidden argument*/NULL);
		V_1 = L_17;
	}

IL_0090:
	{
		Matrix4x4_t2933234003  L_18 = V_1;
		Vector3_t2243707580  L_19 = ARUtilityFunctions_PositionFromMatrix_m2391904295(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		__this->set_arPosition_5(L_19);
		Matrix4x4_t2933234003  L_20 = V_1;
		Quaternion_t4030073918  L_21 = ARUtilityFunctions_QuaternionFromMatrix_m2564935062(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		__this->set_arRotation_6(L_21);
		bool L_22 = __this->get_arVisible_7();
		if (L_22)
		{
			goto IL_00ba;
		}
	}
	{
		__this->set_arVisible_7((bool)1);
	}

IL_00ba:
	{
		goto IL_00dd;
	}

IL_00bf:
	{
		bool L_23 = __this->get_arVisible_7();
		if (!L_23)
		{
			goto IL_00dd;
		}
	}
	{
		float L_24 = __this->get_timeLastUpdate_8();
		__this->set_timeTrackingLost_9(L_24);
		__this->set_arVisible_7((bool)0);
	}

IL_00dd:
	{
		return;
	}
}
// System.Void ARCamera::ApplyTracking()
extern "C"  void ARCamera_ApplyTracking_m1784467530 (ARCamera_t431610428 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_arVisible_7();
		if (!L_0)
		{
			goto IL_002d;
		}
	}
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = __this->get_arPosition_5();
		NullCheck(L_1);
		Transform_set_localPosition_m1026930133(L_1, L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_4 = __this->get_arRotation_6();
		NullCheck(L_3);
		Transform_set_localRotation_m2055111962(L_3, L_4, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void ARCamera::LateUpdate()
extern "C"  void ARCamera_LateUpdate_m2396630374 (ARCamera_t431610428 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localScale_m2325460848(L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		ARCamera_UpdateTracking_m1132195383(__this, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(6 /* System.Void ARCamera::ApplyTracking() */, __this);
	}

IL_0026:
	{
		return;
	}
}
// System.Void ARController::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyleU5BU5D_t2497716199_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral362376272;
extern Il2CppCodeGenString* _stringLiteral2603174998;
extern Il2CppCodeGenString* _stringLiteral1653140563;
extern Il2CppCodeGenString* _stringLiteral3944192078;
extern Il2CppCodeGenString* _stringLiteral2310223924;
extern Il2CppCodeGenString* _stringLiteral256286678;
extern Il2CppCodeGenString* _stringLiteral895562925;
extern Il2CppCodeGenString* _stringLiteral2891987919;
extern Il2CppCodeGenString* _stringLiteral3779996703;
extern const uint32_t ARController__ctor_m2991573286_MetadataUsageId;
extern "C"  void ARController__ctor_m2991573286 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController__ctor_m2991573286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_UseNativeGLTexturingIfAvailable_7((bool)1);
		__this->set_QuitOnEscOrBack_9((bool)1);
		__this->set_AutoStartAR_10((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__version_11(L_0);
		__this->set__useColor32_17((bool)1);
		__this->set_videoCParamName0_18(_stringLiteral362376272);
		__this->set_videoConfigurationWindows0_19(_stringLiteral2603174998);
		__this->set_videoConfigurationMacOSX0_20(_stringLiteral1653140563);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_videoConfigurationiOS0_21(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_videoConfigurationAndroid0_22(L_2);
		__this->set_videoConfigurationWindowsStore0_23(_stringLiteral3944192078);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_videoConfigurationLinux0_24(L_3);
		__this->set_BackgroundLayer0_25(8);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__videoPixelFormatString0_29(L_4);
		__this->set_transL2RName_37(_stringLiteral2310223924);
		__this->set_videoCParamName1_38(_stringLiteral256286678);
		__this->set_videoConfigurationWindows1_39(_stringLiteral895562925);
		__this->set_videoConfigurationMacOSX1_40(_stringLiteral2891987919);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_videoConfigurationiOS1_41(L_5);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_videoConfigurationAndroid1_42(L_6);
		__this->set_videoConfigurationWindowsStore1_43(_stringLiteral3779996703);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_videoConfigurationLinux1_44(L_7);
		__this->set_BackgroundLayer1_45(((int32_t)9));
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__videoPixelFormatString1_49(L_8);
		__this->set_NearPlane_61((0.01f));
		__this->set_FarPlane_62((5.0f));
		__this->set_ContentAlign_66(4);
		__this->set_refreshTime_71((0.5f));
		__this->set_currentContentMode_73(1);
		__this->set_currentThreshold_75(((int32_t)100));
		__this->set_currentLabelingMode_76(1);
		__this->set_currentTemplateSize_77(((int32_t)16));
		__this->set_currentTemplateCountMax_78(((int32_t)25));
		__this->set_currentBorderSize_79((0.25f));
		__this->set_currentMatrixCodeType_81(3);
		__this->set_currentUseVideoBackground_83((bool)1);
		__this->set_currentLogLevel_85(1);
		__this->set_style_86(((GUIStyleU5BU5D_t2497716199*)SZArrayNew(GUIStyleU5BU5D_t2497716199_il2cpp_TypeInfo_var, (uint32_t)3)));
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_showGUIErrorDialogContent_89(L_9);
		Rect_t3681755626  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Rect__ctor_m1220545469(&L_10, (0.0f), (0.0f), (320.0f), (240.0f), /*hidden argument*/NULL);
		__this->set_showGUIErrorDialogWinRect_90(L_10);
		__this->set_showGUIDebugInfo_92((bool)1);
		Vector2_t2243707579  L_11 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scrollPosition_94(L_11);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Action`1<System.String> ARController::get_logCallback()
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern const uint32_t ARController_get_logCallback_m620268047_MetadataUsageId;
extern "C"  Action_1_t1831019615 * ARController_get_logCallback_m620268047 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_get_logCallback_m620268047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		Action_1_t1831019615 * L_0 = ((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->get_U3ClogCallbackU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void ARController::set_logCallback(System.Action`1<System.String>)
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern const uint32_t ARController_set_logCallback_m1311593722_MetadataUsageId;
extern "C"  void ARController_set_logCallback_m1311593722 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_set_logCallback_m1311593722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t1831019615 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->set_U3ClogCallbackU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void ARController::Awake()
extern Il2CppClass* Thread_t241561612_il2cpp_TypeInfo_var;
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1487360282;
extern Il2CppCodeGenString* _stringLiteral2354908751;
extern Il2CppCodeGenString* _stringLiteral298118666;
extern const uint32_t ARController_Awake_m2362708713_MetadataUsageId;
extern "C"  void ARController_Awake_m2362708713 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_Awake_m2362708713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_bFound_2((bool)0);
		ARNativePluginStatic_aruRequestCamera_m4285056894(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t241561612_il2cpp_TypeInfo_var);
		Thread_Sleep_m1248422015(NULL /*static, unused*/, ((int32_t)2000), /*hidden argument*/NULL);
		int32_t L_0 = ARController_get_TemplateSize_m4162121116(__this, /*hidden argument*/NULL);
		int32_t L_1 = ARController_get_TemplateCountMax_m507601286(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_2 = PluginFunctions_arwInitialiseAR_m1868305941(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		String_t* L_3 = PluginFunctions_arwGetARToolKitVersion_m1242319639(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__version_11(L_3);
		String_t* L_4 = __this->get__version_11();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1487360282, L_4, _stringLiteral2354908751, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		goto IL_0060;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral298118666, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void ARController::OnEnable()
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppClass* LogCallback_t2143553514_il2cpp_TypeInfo_var;
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const MethodInfo* ARController_Log_m1311826870_MethodInfo_var;
extern const uint32_t ARController_OnEnable_m2249051214_MetadataUsageId;
extern "C"  void ARController_OnEnable_m2249051214 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_OnEnable_m2249051214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_005e;
		}
		if (L_1 == 1)
		{
			goto IL_005e;
		}
		if (L_1 == 2)
		{
			goto IL_0063;
		}
		if (L_1 == 3)
		{
			goto IL_0044;
		}
		if (L_1 == 4)
		{
			goto IL_0044;
		}
		if (L_1 == 5)
		{
			goto IL_0044;
		}
		if (L_1 == 6)
		{
			goto IL_0044;
		}
		if (L_1 == 7)
		{
			goto IL_0063;
		}
		if (L_1 == 8)
		{
			goto IL_008f;
		}
		if (L_1 == 9)
		{
			goto IL_0044;
		}
		if (L_1 == 10)
		{
			goto IL_0044;
		}
		if (L_1 == 11)
		{
			goto IL_008a;
		}
		if (L_1 == 12)
		{
			goto IL_0044;
		}
		if (L_1 == 13)
		{
			goto IL_0063;
		}
	}

IL_0044:
	{
		int32_t L_2 = V_0;
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 0)
		{
			goto IL_0094;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 1)
		{
			goto IL_0094;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 2)
		{
			goto IL_0094;
		}
	}
	{
		goto IL_00bb;
	}

IL_005e:
	{
		goto IL_0063;
	}

IL_0063:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		LogCallback_t2143553514 * L_3 = ((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_95();
		if (L_3)
		{
			goto IL_007b;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)ARController_Log_m1311826870_MethodInfo_var);
		LogCallback_t2143553514 * L_5 = (LogCallback_t2143553514 *)il2cpp_codegen_object_new(LogCallback_t2143553514_il2cpp_TypeInfo_var);
		LogCallback__ctor_m1707937529(L_5, NULL, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_95(L_5);
	}

IL_007b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		LogCallback_t2143553514 * L_6 = ((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_95();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwRegisterLogCallback_m1389878145(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		goto IL_00c0;
	}

IL_008a:
	{
		goto IL_00c0;
	}

IL_008f:
	{
		goto IL_00c0;
	}

IL_0094:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		LogCallback_t2143553514 * L_7 = ((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1_96();
		if (L_7)
		{
			goto IL_00ac;
		}
	}
	{
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)ARController_Log_m1311826870_MethodInfo_var);
		LogCallback_t2143553514 * L_9 = (LogCallback_t2143553514 *)il2cpp_codegen_object_new(LogCallback_t2143553514_il2cpp_TypeInfo_var);
		LogCallback__ctor_m1707937529(L_9, NULL, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache1_96(L_9);
	}

IL_00ac:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		LogCallback_t2143553514 * L_10 = ((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1_96();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwRegisterLogCallback_m1389878145(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		goto IL_00c0;
	}

IL_00bb:
	{
		goto IL_00c0;
	}

IL_00c0:
	{
		return;
	}
}
// System.Void ARController::Start()
extern const Il2CppType* ARMarker_t1554260723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ARMarkerU5BU5D_t3553995746_il2cpp_TypeInfo_var;
extern const uint32_t ARController_Start_m433899566_MetadataUsageId;
extern "C"  void ARController_Start_m433899566 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_Start_m433899566_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARMarkerU5BU5D_t3553995746* V_0 = NULL;
	ARMarker_t1554260723 * V_1 = NULL;
	ARMarkerU5BU5D_t3553995746* V_2 = NULL;
	int32_t V_3 = 0;
	{
		__this->set_bFound_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ARMarker_t1554260723_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_1 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((ARMarkerU5BU5D_t3553995746*)IsInst(L_1, ARMarkerU5BU5D_t3553995746_il2cpp_TypeInfo_var));
		ARMarkerU5BU5D_t3553995746* L_2 = V_0;
		V_2 = L_2;
		V_3 = 0;
		goto IL_0033;
	}

IL_0025:
	{
		ARMarkerU5BU5D_t3553995746* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		ARMarker_t1554260723 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		ARMarker_t1554260723 * L_7 = V_1;
		NullCheck(L_7);
		ARMarker_Load_m2962895258(L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_3;
		V_3 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_9 = V_3;
		ARMarkerU5BU5D_t3553995746* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0025;
		}
	}
	{
		bool L_11 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0066;
		}
	}
	{
		bool L_12 = __this->get_AutoStartAR_10();
		if (!L_12)
		{
			goto IL_0061;
		}
	}
	{
		bool L_13 = ARController_StartAR_m3868647255(__this, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0061;
		}
	}
	{
		Application_Quit_m3885595876(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0061:
	{
		goto IL_0066;
	}

IL_0066:
	{
		return;
	}
}
// System.Void ARController::OnApplicationPause(System.Boolean)
extern "C"  void ARController_OnApplicationPause_m1181163288 (ARController_t593870669 * __this, bool ___paused0, const MethodInfo* method)
{
	{
		bool L_0 = ___paused0;
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		bool L_1 = __this->get__running_12();
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		ARController_StopAR_m3264126367(__this, /*hidden argument*/NULL);
		__this->set__runOnUnpause_13((bool)1);
	}

IL_001f:
	{
		goto IL_003d;
	}

IL_0024:
	{
		bool L_2 = __this->get__runOnUnpause_13();
		if (!L_2)
		{
			goto IL_003d;
		}
	}
	{
		ARController_StartAR_m3868647255(__this, /*hidden argument*/NULL);
		__this->set__runOnUnpause_13((bool)0);
	}

IL_003d:
	{
		return;
	}
}
// System.Void ARController::Update()
extern const Il2CppType* ARMarker_t1554260723_0_0_0_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ARMarkerU5BU5D_t3553995746_il2cpp_TypeInfo_var;
extern const uint32_t ARController_Update_m4055977229_MetadataUsageId;
extern "C"  void ARController_Update_m4055977229 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_Update_m4055977229_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARMarkerU5BU5D_t3553995746* V_0 = NULL;
	ARMarker_t1554260723 * V_1 = NULL;
	ARMarkerU5BU5D_t3553995746* V_2 = NULL;
	int32_t V_3 = 0;
	{
		bool L_0 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00aa;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)319), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)13), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0034;
		}
	}

IL_0025:
	{
		bool L_3 = __this->get_showGUIDebug_91();
		__this->set_showGUIDebug_91((bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0));
	}

IL_0034:
	{
		bool L_4 = __this->get_QuitOnEscOrBack_9();
		if (!L_4)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_5 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0050;
		}
	}
	{
		Application_Quit_m3885595876(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0050:
	{
		ARController_CalculateFPS_m1910495181(__this, /*hidden argument*/NULL);
		ARController_UpdateAR_m562781644(__this, /*hidden argument*/NULL);
		__this->set_bFound_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ARMarker_t1554260723_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_7 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = ((ARMarkerU5BU5D_t3553995746*)IsInst(L_7, ARMarkerU5BU5D_t3553995746_il2cpp_TypeInfo_var));
		ARMarkerU5BU5D_t3553995746* L_8 = V_0;
		V_2 = L_8;
		V_3 = 0;
		goto IL_009c;
	}

IL_0082:
	{
		ARMarkerU5BU5D_t3553995746* L_9 = V_2;
		int32_t L_10 = V_3;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		ARMarker_t1554260723 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_1 = L_12;
		ARMarker_t1554260723 * L_13 = V_1;
		NullCheck(L_13);
		bool L_14 = ARMarker_get_Visible_m4014965141(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0098;
		}
	}
	{
		__this->set_bFound_2((bool)1);
	}

IL_0098:
	{
		int32_t L_15 = V_3;
		V_3 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_009c:
	{
		int32_t L_16 = V_3;
		ARMarkerU5BU5D_t3553995746* L_17 = V_2;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_0082;
		}
	}
	{
		goto IL_00aa;
	}

IL_00aa:
	{
		return;
	}
}
// System.Void ARController::OnApplicationQuit()
extern "C"  void ARController_OnApplicationQuit_m331055932 (ARController_t593870669 * __this, const MethodInfo* method)
{
	{
		ARController_StopAR_m3264126367(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARController::OnDisable()
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_OnDisable_m3810669353_MetadataUsageId;
extern "C"  void ARController_OnDisable_m3810669353 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_OnDisable_m3810669353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_005e;
		}
		if (L_1 == 1)
		{
			goto IL_005e;
		}
		if (L_1 == 2)
		{
			goto IL_0063;
		}
		if (L_1 == 3)
		{
			goto IL_0044;
		}
		if (L_1 == 4)
		{
			goto IL_0044;
		}
		if (L_1 == 5)
		{
			goto IL_0044;
		}
		if (L_1 == 6)
		{
			goto IL_0044;
		}
		if (L_1 == 7)
		{
			goto IL_0063;
		}
		if (L_1 == 8)
		{
			goto IL_0073;
		}
		if (L_1 == 9)
		{
			goto IL_0044;
		}
		if (L_1 == 10)
		{
			goto IL_0044;
		}
		if (L_1 == 11)
		{
			goto IL_006e;
		}
		if (L_1 == 12)
		{
			goto IL_0044;
		}
		if (L_1 == 13)
		{
			goto IL_0063;
		}
	}

IL_0044:
	{
		int32_t L_2 = V_0;
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 0)
		{
			goto IL_0078;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 1)
		{
			goto IL_0078;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 2)
		{
			goto IL_0078;
		}
	}
	{
		goto IL_0083;
	}

IL_005e:
	{
		goto IL_0063;
	}

IL_0063:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwRegisterLogCallback_m1389878145(NULL /*static, unused*/, (LogCallback_t2143553514 *)NULL, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_006e:
	{
		goto IL_0088;
	}

IL_0073:
	{
		goto IL_0088;
	}

IL_0078:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwRegisterLogCallback_m1389878145(NULL /*static, unused*/, (LogCallback_t2143553514 *)NULL, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_0083:
	{
		goto IL_0088;
	}

IL_0088:
	{
		return;
	}
}
// System.Void ARController::OnDestroy()
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral108469348;
extern Il2CppCodeGenString* _stringLiteral1346353992;
extern const uint32_t ARController_OnDestroy_m2882413523_MetadataUsageId;
extern "C"  void ARController_OnDestroy_m2882413523 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_OnDestroy_m2882413523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral108469348, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_0 = PluginFunctions_arwShutdownAR_m3422793632(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral1346353992, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Boolean ARController::StartAR()
extern const Il2CppType* TextAsset_t3973159845_0_0_0_var;
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TextAsset_t3973159845_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3091976569;
extern Il2CppCodeGenString* _stringLiteral1371681844;
extern Il2CppCodeGenString* _stringLiteral815864769;
extern Il2CppCodeGenString* _stringLiteral3622522833;
extern Il2CppCodeGenString* _stringLiteral589966586;
extern Il2CppCodeGenString* _stringLiteral3821155141;
extern Il2CppCodeGenString* _stringLiteral4247341107;
extern Il2CppCodeGenString* _stringLiteral3925812764;
extern Il2CppCodeGenString* _stringLiteral2462442105;
extern Il2CppCodeGenString* _stringLiteral2384690847;
extern Il2CppCodeGenString* _stringLiteral1019070289;
extern Il2CppCodeGenString* _stringLiteral3351768253;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern Il2CppCodeGenString* _stringLiteral2342976849;
extern Il2CppCodeGenString* _stringLiteral2043597422;
extern Il2CppCodeGenString* _stringLiteral2743879420;
extern Il2CppCodeGenString* _stringLiteral2058084087;
extern Il2CppCodeGenString* _stringLiteral4247154226;
extern Il2CppCodeGenString* _stringLiteral2899658620;
extern Il2CppCodeGenString* _stringLiteral1617874521;
extern Il2CppCodeGenString* _stringLiteral3673377874;
extern Il2CppCodeGenString* _stringLiteral2574980712;
extern Il2CppCodeGenString* _stringLiteral319004338;
extern Il2CppCodeGenString* _stringLiteral1525900594;
extern Il2CppCodeGenString* _stringLiteral2358555457;
extern Il2CppCodeGenString* _stringLiteral1086047458;
extern const uint32_t ARController_StartAR_m3868647255_MetadataUsageId;
extern "C"  bool ARController_StartAR_m3868647255 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_StartAR_m3868647255_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	int32_t V_4 = 0;
	TextAsset_t3973159845 * V_5 = NULL;
	ByteU5BU5D_t3397334013* V_6 = NULL;
	ByteU5BU5D_t3397334013* V_7 = NULL;
	ByteU5BU5D_t3397334013* V_8 = NULL;
	int32_t V_9 = 0;
	ARController_t593870669 * G_B4_0 = NULL;
	ARController_t593870669 * G_B3_0 = NULL;
	int32_t G_B5_0 = 0;
	ARController_t593870669 * G_B5_1 = NULL;
	int32_t G_B29_0 = 0;
	StringU5BU5D_t1642385972* G_B29_1 = NULL;
	StringU5BU5D_t1642385972* G_B29_2 = NULL;
	int32_t G_B28_0 = 0;
	StringU5BU5D_t1642385972* G_B28_1 = NULL;
	StringU5BU5D_t1642385972* G_B28_2 = NULL;
	int32_t G_B30_0 = 0;
	StringU5BU5D_t1642385972* G_B30_1 = NULL;
	StringU5BU5D_t1642385972* G_B30_2 = NULL;
	String_t* G_B31_0 = NULL;
	int32_t G_B31_1 = 0;
	StringU5BU5D_t1642385972* G_B31_2 = NULL;
	StringU5BU5D_t1642385972* G_B31_3 = NULL;
	int32_t G_B33_0 = 0;
	StringU5BU5D_t1642385972* G_B33_1 = NULL;
	StringU5BU5D_t1642385972* G_B33_2 = NULL;
	int32_t G_B32_0 = 0;
	StringU5BU5D_t1642385972* G_B32_1 = NULL;
	StringU5BU5D_t1642385972* G_B32_2 = NULL;
	int32_t G_B34_0 = 0;
	StringU5BU5D_t1642385972* G_B34_1 = NULL;
	StringU5BU5D_t1642385972* G_B34_2 = NULL;
	String_t* G_B35_0 = NULL;
	int32_t G_B35_1 = 0;
	StringU5BU5D_t1642385972* G_B35_2 = NULL;
	StringU5BU5D_t1642385972* G_B35_3 = NULL;
	String_t* G_B38_0 = NULL;
	String_t* G_B37_0 = NULL;
	String_t* G_B39_0 = NULL;
	String_t* G_B40_0 = NULL;
	String_t* G_B40_1 = NULL;
	String_t* G_B42_0 = NULL;
	String_t* G_B41_0 = NULL;
	String_t* G_B43_0 = NULL;
	String_t* G_B44_0 = NULL;
	String_t* G_B44_1 = NULL;
	{
		bool L_0 = __this->get__running_12();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral3091976569, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral1371681844, /*hidden argument*/NULL);
		int32_t L_1 = 0;
		V_0 = (bool)L_1;
		__this->set__sceneConfiguredForVideoWaitingMessageLogged_15((bool)L_1);
		bool L_2 = V_0;
		__this->set__sceneConfiguredForVideo_14(L_2);
		String_t* L_3 = SystemInfo_get_graphicsDeviceVersion_m2838077281(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = String_StartsWith_m1841920685(L_4, _stringLiteral815864769, /*hidden argument*/NULL);
		G_B3_0 = __this;
		if (L_5)
		{
			G_B4_0 = __this;
			goto IL_0050;
		}
	}
	{
		bool L_6 = __this->get_UseNativeGLTexturingIfAvailable_7();
		G_B5_0 = ((int32_t)(L_6));
		G_B5_1 = G_B3_0;
		goto IL_0051;
	}

IL_0050:
	{
		G_B5_0 = 0;
		G_B5_1 = G_B4_0;
	}

IL_0051:
	{
		NullCheck(G_B5_1);
		G_B5_1->set__useNativeGLTexturing_16((bool)G_B5_0);
		bool L_7 = __this->get__useNativeGLTexturing_16();
		if (!L_7)
		{
			goto IL_007b;
		}
	}
	{
		String_t* L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3622522833, L_8, _stringLiteral589966586, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		goto IL_0090;
	}

IL_007b:
	{
		String_t* L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3622522833, L_10, _stringLiteral3821155141, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_0090:
	{
		ARController_CreateClearCamera_m2815811798(__this, /*hidden argument*/NULL);
		int32_t L_12 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_12;
		int32_t L_13 = V_4;
		if (L_13 == 0)
		{
			goto IL_00f8;
		}
		if (L_13 == 1)
		{
			goto IL_00f8;
		}
		if (L_13 == 2)
		{
			goto IL_017d;
		}
		if (L_13 == 3)
		{
			goto IL_00dd;
		}
		if (L_13 == 4)
		{
			goto IL_00dd;
		}
		if (L_13 == 5)
		{
			goto IL_00dd;
		}
		if (L_13 == 6)
		{
			goto IL_00dd;
		}
		if (L_13 == 7)
		{
			goto IL_017d;
		}
		if (L_13 == 8)
		{
			goto IL_028f;
		}
		if (L_13 == 9)
		{
			goto IL_00dd;
		}
		if (L_13 == 10)
		{
			goto IL_00dd;
		}
		if (L_13 == 11)
		{
			goto IL_01e0;
		}
		if (L_13 == 12)
		{
			goto IL_00dd;
		}
		if (L_13 == 13)
		{
			goto IL_0309;
		}
	}

IL_00dd:
	{
		int32_t L_14 = V_4;
		if (((int32_t)((int32_t)L_14-(int32_t)((int32_t)18))) == 0)
		{
			goto IL_02f6;
		}
		if (((int32_t)((int32_t)L_14-(int32_t)((int32_t)18))) == 1)
		{
			goto IL_02f6;
		}
		if (((int32_t)((int32_t)L_14-(int32_t)((int32_t)18))) == 2)
		{
			goto IL_02f6;
		}
	}
	{
		goto IL_031c;
	}

IL_00f8:
	{
		String_t* L_15 = __this->get_videoConfigurationMacOSX0_20();
		V_2 = L_15;
		String_t* L_16 = __this->get_videoConfigurationMacOSX1_40();
		V_3 = L_16;
		bool L_17 = __this->get__useNativeGLTexturing_16();
		if (L_17)
		{
			goto IL_011c;
		}
	}
	{
		bool L_18 = __this->get_AllowNonRGBVideo_8();
		if (L_18)
		{
			goto IL_0178;
		}
	}

IL_011c:
	{
		String_t* L_19 = V_2;
		NullCheck(L_19);
		int32_t L_20 = String_IndexOf_m4251815737(L_19, _stringLiteral4247341107, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)(-1)))))
		{
			goto IL_013e;
		}
	}
	{
		String_t* L_21 = V_2;
		NullCheck(L_21);
		int32_t L_22 = String_IndexOf_m4251815737(L_21, _stringLiteral3925812764, /*hidden argument*/NULL);
		if ((((int32_t)L_22) == ((int32_t)(-1))))
		{
			goto IL_014a;
		}
	}

IL_013e:
	{
		String_t* L_23 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Concat_m2596409543(NULL /*static, unused*/, L_23, _stringLiteral2462442105, /*hidden argument*/NULL);
		V_2 = L_24;
	}

IL_014a:
	{
		String_t* L_25 = V_3;
		NullCheck(L_25);
		int32_t L_26 = String_IndexOf_m4251815737(L_25, _stringLiteral4247341107, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_016c;
		}
	}
	{
		String_t* L_27 = V_3;
		NullCheck(L_27);
		int32_t L_28 = String_IndexOf_m4251815737(L_27, _stringLiteral3925812764, /*hidden argument*/NULL);
		if ((((int32_t)L_28) == ((int32_t)(-1))))
		{
			goto IL_0178;
		}
	}

IL_016c:
	{
		String_t* L_29 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m2596409543(NULL /*static, unused*/, L_29, _stringLiteral2462442105, /*hidden argument*/NULL);
		V_3 = L_30;
	}

IL_0178:
	{
		goto IL_032d;
	}

IL_017d:
	{
		String_t* L_31 = __this->get_videoConfigurationWindows0_19();
		V_2 = L_31;
		String_t* L_32 = __this->get_videoConfigurationWindows1_39();
		V_3 = L_32;
		bool L_33 = __this->get__useNativeGLTexturing_16();
		if (L_33)
		{
			goto IL_01a1;
		}
	}
	{
		bool L_34 = __this->get_AllowNonRGBVideo_8();
		if (L_34)
		{
			goto IL_01db;
		}
	}

IL_01a1:
	{
		String_t* L_35 = V_2;
		NullCheck(L_35);
		int32_t L_36 = String_IndexOf_m4251815737(L_35, _stringLiteral2384690847, /*hidden argument*/NULL);
		if ((((int32_t)L_36) == ((int32_t)(-1))))
		{
			goto IL_01be;
		}
	}
	{
		String_t* L_37 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_38 = String_Concat_m2596409543(NULL /*static, unused*/, L_37, _stringLiteral1019070289, /*hidden argument*/NULL);
		V_2 = L_38;
	}

IL_01be:
	{
		String_t* L_39 = V_3;
		NullCheck(L_39);
		int32_t L_40 = String_IndexOf_m4251815737(L_39, _stringLiteral2384690847, /*hidden argument*/NULL);
		if ((((int32_t)L_40) == ((int32_t)(-1))))
		{
			goto IL_01db;
		}
	}
	{
		String_t* L_41 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_42 = String_Concat_m2596409543(NULL /*static, unused*/, L_41, _stringLiteral1019070289, /*hidden argument*/NULL);
		V_3 = L_42;
	}

IL_01db:
	{
		goto IL_032d;
	}

IL_01e0:
	{
		StringU5BU5D_t1642385972* L_43 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_44 = __this->get_videoConfigurationAndroid0_22();
		NullCheck(L_43);
		ArrayElementTypeCheck (L_43, L_44);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_44);
		StringU5BU5D_t1642385972* L_45 = L_43;
		NullCheck(L_45);
		ArrayElementTypeCheck (L_45, _stringLiteral3351768253);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3351768253);
		StringU5BU5D_t1642385972* L_46 = L_45;
		String_t* L_47 = Application_get_temporaryCachePath_m1120802989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_46);
		ArrayElementTypeCheck (L_46, L_47);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_47);
		StringU5BU5D_t1642385972* L_48 = L_46;
		NullCheck(L_48);
		ArrayElementTypeCheck (L_48, _stringLiteral372029312);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral372029312);
		StringU5BU5D_t1642385972* L_49 = L_48;
		bool L_50 = __this->get__useNativeGLTexturing_16();
		G_B28_0 = 4;
		G_B28_1 = L_49;
		G_B28_2 = L_49;
		if (L_50)
		{
			G_B29_0 = 4;
			G_B29_1 = L_49;
			G_B29_2 = L_49;
			goto IL_021f;
		}
	}
	{
		bool L_51 = __this->get_AllowNonRGBVideo_8();
		G_B29_0 = G_B28_0;
		G_B29_1 = G_B28_1;
		G_B29_2 = G_B28_2;
		if (L_51)
		{
			G_B30_0 = G_B28_0;
			G_B30_1 = G_B28_1;
			G_B30_2 = G_B28_2;
			goto IL_0229;
		}
	}

IL_021f:
	{
		G_B31_0 = _stringLiteral2342976849;
		G_B31_1 = G_B29_0;
		G_B31_2 = G_B29_1;
		G_B31_3 = G_B29_2;
		goto IL_022e;
	}

IL_0229:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_52 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B31_0 = L_52;
		G_B31_1 = G_B30_0;
		G_B31_2 = G_B30_1;
		G_B31_3 = G_B30_2;
	}

IL_022e:
	{
		NullCheck(G_B31_2);
		ArrayElementTypeCheck (G_B31_2, G_B31_0);
		(G_B31_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B31_1), (String_t*)G_B31_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_53 = String_Concat_m626692867(NULL /*static, unused*/, G_B31_3, /*hidden argument*/NULL);
		V_2 = L_53;
		StringU5BU5D_t1642385972* L_54 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_55 = __this->get_videoConfigurationAndroid1_42();
		NullCheck(L_54);
		ArrayElementTypeCheck (L_54, L_55);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_55);
		StringU5BU5D_t1642385972* L_56 = L_54;
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, _stringLiteral3351768253);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3351768253);
		StringU5BU5D_t1642385972* L_57 = L_56;
		String_t* L_58 = Application_get_temporaryCachePath_m1120802989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_57);
		ArrayElementTypeCheck (L_57, L_58);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_58);
		StringU5BU5D_t1642385972* L_59 = L_57;
		NullCheck(L_59);
		ArrayElementTypeCheck (L_59, _stringLiteral372029312);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral372029312);
		StringU5BU5D_t1642385972* L_60 = L_59;
		bool L_61 = __this->get__useNativeGLTexturing_16();
		G_B32_0 = 4;
		G_B32_1 = L_60;
		G_B32_2 = L_60;
		if (L_61)
		{
			G_B33_0 = 4;
			G_B33_1 = L_60;
			G_B33_2 = L_60;
			goto IL_0274;
		}
	}
	{
		bool L_62 = __this->get_AllowNonRGBVideo_8();
		G_B33_0 = G_B32_0;
		G_B33_1 = G_B32_1;
		G_B33_2 = G_B32_2;
		if (L_62)
		{
			G_B34_0 = G_B32_0;
			G_B34_1 = G_B32_1;
			G_B34_2 = G_B32_2;
			goto IL_027e;
		}
	}

IL_0274:
	{
		G_B35_0 = _stringLiteral2342976849;
		G_B35_1 = G_B33_0;
		G_B35_2 = G_B33_1;
		G_B35_3 = G_B33_2;
		goto IL_0283;
	}

IL_027e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_63 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B35_0 = L_63;
		G_B35_1 = G_B34_0;
		G_B35_2 = G_B34_1;
		G_B35_3 = G_B34_2;
	}

IL_0283:
	{
		NullCheck(G_B35_2);
		ArrayElementTypeCheck (G_B35_2, G_B35_0);
		(G_B35_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B35_1), (String_t*)G_B35_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_64 = String_Concat_m626692867(NULL /*static, unused*/, G_B35_3, /*hidden argument*/NULL);
		V_3 = L_64;
		goto IL_032d;
	}

IL_028f:
	{
		String_t* L_65 = __this->get_videoConfigurationiOS0_21();
		bool L_66 = __this->get__useNativeGLTexturing_16();
		G_B37_0 = L_65;
		if (L_66)
		{
			G_B38_0 = L_65;
			goto IL_02ab;
		}
	}
	{
		bool L_67 = __this->get_AllowNonRGBVideo_8();
		G_B38_0 = G_B37_0;
		if (L_67)
		{
			G_B39_0 = G_B37_0;
			goto IL_02b5;
		}
	}

IL_02ab:
	{
		G_B40_0 = _stringLiteral1019070289;
		G_B40_1 = G_B38_0;
		goto IL_02ba;
	}

IL_02b5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_68 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B40_0 = L_68;
		G_B40_1 = G_B39_0;
	}

IL_02ba:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_69 = String_Concat_m2596409543(NULL /*static, unused*/, G_B40_1, G_B40_0, /*hidden argument*/NULL);
		V_2 = L_69;
		String_t* L_70 = __this->get_videoConfigurationiOS1_41();
		bool L_71 = __this->get__useNativeGLTexturing_16();
		G_B41_0 = L_70;
		if (L_71)
		{
			G_B42_0 = L_70;
			goto IL_02dc;
		}
	}
	{
		bool L_72 = __this->get_AllowNonRGBVideo_8();
		G_B42_0 = G_B41_0;
		if (L_72)
		{
			G_B43_0 = G_B41_0;
			goto IL_02e6;
		}
	}

IL_02dc:
	{
		G_B44_0 = _stringLiteral1019070289;
		G_B44_1 = G_B42_0;
		goto IL_02eb;
	}

IL_02e6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_73 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B44_0 = L_73;
		G_B44_1 = G_B43_0;
	}

IL_02eb:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_74 = String_Concat_m2596409543(NULL /*static, unused*/, G_B44_1, G_B44_0, /*hidden argument*/NULL);
		V_3 = L_74;
		goto IL_032d;
	}

IL_02f6:
	{
		String_t* L_75 = __this->get_videoConfigurationWindowsStore0_23();
		V_2 = L_75;
		String_t* L_76 = __this->get_videoConfigurationWindowsStore1_43();
		V_3 = L_76;
		goto IL_032d;
	}

IL_0309:
	{
		String_t* L_77 = __this->get_videoConfigurationLinux0_24();
		V_2 = L_77;
		String_t* L_78 = __this->get_videoConfigurationLinux1_44();
		V_3 = L_78;
		goto IL_032d;
	}

IL_031c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_79 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_2 = L_79;
		String_t* L_80 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_80;
		goto IL_032d;
	}

IL_032d:
	{
		V_6 = (ByteU5BU5D_t3397334013*)NULL;
		V_7 = (ByteU5BU5D_t3397334013*)NULL;
		V_8 = (ByteU5BU5D_t3397334013*)NULL;
		String_t* L_81 = __this->get_videoCParamName0_18();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_82 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2043597422, L_81, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_83 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(TextAsset_t3973159845_0_0_0_var), /*hidden argument*/NULL);
		Object_t1021602117 * L_84 = Resources_Load_m243305716(NULL /*static, unused*/, L_82, L_83, /*hidden argument*/NULL);
		V_5 = ((TextAsset_t3973159845 *)IsInstClass(L_84, TextAsset_t3973159845_il2cpp_TypeInfo_var));
		TextAsset_t3973159845 * L_85 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_86 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_85, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_86)
		{
			goto IL_0385;
		}
	}
	{
		String_t* L_87 = __this->get_videoCParamName0_18();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_88 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2743879420, L_87, _stringLiteral2058084087, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_88, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0385:
	{
		TextAsset_t3973159845 * L_89 = V_5;
		NullCheck(L_89);
		ByteU5BU5D_t3397334013* L_90 = TextAsset_get_bytes_m1699690326(L_89, /*hidden argument*/NULL);
		V_6 = L_90;
		bool L_91 = __this->get_VideoIsStereo_36();
		if (!L_91)
		{
			goto IL_0449;
		}
	}
	{
		String_t* L_92 = __this->get_videoCParamName1_38();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_93 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2043597422, L_92, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_94 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(TextAsset_t3973159845_0_0_0_var), /*hidden argument*/NULL);
		Object_t1021602117 * L_95 = Resources_Load_m243305716(NULL /*static, unused*/, L_93, L_94, /*hidden argument*/NULL);
		V_5 = ((TextAsset_t3973159845 *)IsInstClass(L_95, TextAsset_t3973159845_il2cpp_TypeInfo_var));
		TextAsset_t3973159845 * L_96 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_97 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_96, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_97)
		{
			goto IL_03e8;
		}
	}
	{
		String_t* L_98 = __this->get_videoCParamName1_38();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_99 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2743879420, L_98, _stringLiteral2058084087, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_99, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_03e8:
	{
		TextAsset_t3973159845 * L_100 = V_5;
		NullCheck(L_100);
		ByteU5BU5D_t3397334013* L_101 = TextAsset_get_bytes_m1699690326(L_100, /*hidden argument*/NULL);
		V_7 = L_101;
		String_t* L_102 = __this->get_transL2RName_37();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_103 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2043597422, L_102, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_104 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(TextAsset_t3973159845_0_0_0_var), /*hidden argument*/NULL);
		Object_t1021602117 * L_105 = Resources_Load_m243305716(NULL /*static, unused*/, L_103, L_104, /*hidden argument*/NULL);
		V_5 = ((TextAsset_t3973159845 *)IsInstClass(L_105, TextAsset_t3973159845_il2cpp_TypeInfo_var));
		TextAsset_t3973159845 * L_106 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_107 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_106, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_107)
		{
			goto IL_0440;
		}
	}
	{
		String_t* L_108 = __this->get_transL2RName_37();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_109 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral4247154226, L_108, _stringLiteral2058084087, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_109, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0440:
	{
		TextAsset_t3973159845 * L_110 = V_5;
		NullCheck(L_110);
		ByteU5BU5D_t3397334013* L_111 = TextAsset_get_bytes_m1699690326(L_110, /*hidden argument*/NULL);
		V_8 = L_111;
	}

IL_0449:
	{
		bool L_112 = __this->get_VideoIsStereo_36();
		if (L_112)
		{
			goto IL_048c;
		}
	}
	{
		String_t* L_113 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_114 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2899658620, L_113, _stringLiteral1617874521, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_114, /*hidden argument*/NULL);
		String_t* L_115 = V_2;
		ByteU5BU5D_t3397334013* L_116 = V_6;
		ByteU5BU5D_t3397334013* L_117 = V_6;
		NullCheck(L_117);
		float L_118 = __this->get_NearPlane_61();
		float L_119 = __this->get_FarPlane_62();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_120 = PluginFunctions_arwStartRunningB_m1651599480(NULL /*static, unused*/, L_115, L_116, (((int32_t)((int32_t)(((Il2CppArray *)L_117)->max_length)))), L_118, L_119, /*hidden argument*/NULL);
		__this->set__running_12(L_120);
		goto IL_04e7;
	}

IL_048c:
	{
		StringU5BU5D_t1642385972* L_121 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_121);
		ArrayElementTypeCheck (L_121, _stringLiteral3673377874);
		(L_121)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3673377874);
		StringU5BU5D_t1642385972* L_122 = L_121;
		String_t* L_123 = V_2;
		NullCheck(L_122);
		ArrayElementTypeCheck (L_122, L_123);
		(L_122)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_123);
		StringU5BU5D_t1642385972* L_124 = L_122;
		NullCheck(L_124);
		ArrayElementTypeCheck (L_124, _stringLiteral2574980712);
		(L_124)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2574980712);
		StringU5BU5D_t1642385972* L_125 = L_124;
		String_t* L_126 = V_3;
		NullCheck(L_125);
		ArrayElementTypeCheck (L_125, L_126);
		(L_125)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_126);
		StringU5BU5D_t1642385972* L_127 = L_125;
		NullCheck(L_127);
		ArrayElementTypeCheck (L_127, _stringLiteral1617874521);
		(L_127)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1617874521);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_128 = String_Concat_m626692867(NULL /*static, unused*/, L_127, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_128, /*hidden argument*/NULL);
		String_t* L_129 = V_2;
		ByteU5BU5D_t3397334013* L_130 = V_6;
		ByteU5BU5D_t3397334013* L_131 = V_6;
		NullCheck(L_131);
		String_t* L_132 = V_3;
		ByteU5BU5D_t3397334013* L_133 = V_7;
		ByteU5BU5D_t3397334013* L_134 = V_7;
		NullCheck(L_134);
		ByteU5BU5D_t3397334013* L_135 = V_8;
		ByteU5BU5D_t3397334013* L_136 = V_8;
		NullCheck(L_136);
		float L_137 = __this->get_NearPlane_61();
		float L_138 = __this->get_FarPlane_62();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_139 = PluginFunctions_arwStartRunningStereoB_m1451019968(NULL /*static, unused*/, L_129, L_130, (((int32_t)((int32_t)(((Il2CppArray *)L_131)->max_length)))), L_132, L_133, (((int32_t)((int32_t)(((Il2CppArray *)L_134)->max_length)))), L_135, (((int32_t)((int32_t)(((Il2CppArray *)L_136)->max_length)))), L_137, L_138, /*hidden argument*/NULL);
		__this->set__running_12(L_139);
	}

IL_04e7:
	{
		bool L_140 = __this->get__running_12();
		if (L_140)
		{
			goto IL_0530;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral319004338, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		int32_t L_141 = PluginFunctions_arwGetError_m1333834619(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_9 = L_141;
		int32_t L_142 = V_9;
		if ((!(((uint32_t)L_142) == ((uint32_t)((int32_t)-13)))))
		{
			goto IL_051c;
		}
	}
	{
		__this->set_showGUIErrorDialogContent_89(_stringLiteral1525900594);
		goto IL_0527;
	}

IL_051c:
	{
		__this->set_showGUIErrorDialogContent_89(_stringLiteral2358555457);
	}

IL_0527:
	{
		__this->set_showGUIErrorDialog_88((bool)1);
		return (bool)0;
	}

IL_0530:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral1086047458, /*hidden argument*/NULL);
		int32_t L_143 = __this->get_currentThreshold_75();
		ARController_set_VideoThreshold_m2326990118(__this, L_143, /*hidden argument*/NULL);
		int32_t L_144 = __this->get_currentThresholdMode_74();
		ARController_set_VideoThresholdMode_m3535350045(__this, L_144, /*hidden argument*/NULL);
		int32_t L_145 = __this->get_currentLabelingMode_76();
		ARController_set_LabelingMode_m2847600302(__this, L_145, /*hidden argument*/NULL);
		float L_146 = __this->get_currentBorderSize_79();
		ARController_set_BorderSize_m2688897581(__this, L_146, /*hidden argument*/NULL);
		int32_t L_147 = __this->get_currentPatternDetectionMode_80();
		ARController_set_PatternDetectionMode_m2120827272(__this, L_147, /*hidden argument*/NULL);
		int32_t L_148 = __this->get_currentMatrixCodeType_81();
		ARController_set_MatrixCodeType_m2903938876(__this, L_148, /*hidden argument*/NULL);
		int32_t L_149 = __this->get_currentImageProcMode_82();
		ARController_set_ImageProcMode_m2544291598(__this, L_149, /*hidden argument*/NULL);
		bool L_150 = __this->get_currentNFTMultiMode_84();
		ARController_set_NFTMultiMode_m1466211986(__this, L_150, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean ARController::UpdateAR()
extern const Il2CppType* ARCamera_t431610428_0_0_0_var;
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* SingleU5BU5D_t577127397_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ARCameraU5BU5D_t1286580565_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382889523;
extern Il2CppCodeGenString* _stringLiteral3546602266;
extern Il2CppCodeGenString* _stringLiteral3191465354;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029406;
extern Il2CppCodeGenString* _stringLiteral1023885896;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern Il2CppCodeGenString* _stringLiteral1201305906;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern Il2CppCodeGenString* _stringLiteral2406077377;
extern Il2CppCodeGenString* _stringLiteral2172287039;
extern Il2CppCodeGenString* _stringLiteral1911857621;
extern Il2CppCodeGenString* _stringLiteral2738709257;
extern Il2CppCodeGenString* _stringLiteral4240208064;
extern Il2CppCodeGenString* _stringLiteral2340739555;
extern Il2CppCodeGenString* _stringLiteral1678595967;
extern Il2CppCodeGenString* _stringLiteral946294561;
extern Il2CppCodeGenString* _stringLiteral4034366264;
extern Il2CppCodeGenString* _stringLiteral4034366258;
extern Il2CppCodeGenString* _stringLiteral1251222247;
extern const uint32_t ARController_UpdateAR_m562781644_MetadataUsageId;
extern "C"  bool ARController_UpdateAR_m562781644 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_UpdateAR_m562781644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	SingleU5BU5D_t577127397* V_1 = NULL;
	bool V_2 = false;
	SingleU5BU5D_t577127397* V_3 = NULL;
	SingleU5BU5D_t577127397* V_4 = NULL;
	bool V_5 = false;
	ARCameraU5BU5D_t1286580565* V_6 = NULL;
	ARCamera_t431610428 * V_7 = NULL;
	ARCameraU5BU5D_t1286580565* V_8 = NULL;
	int32_t V_9 = 0;
	bool V_10 = false;
	bool V_11 = false;
	String_t* G_B54_0 = NULL;
	ARController_t593870669 * G_B54_1 = NULL;
	ARController_t593870669 * G_B54_2 = NULL;
	String_t* G_B53_0 = NULL;
	ARController_t593870669 * G_B53_1 = NULL;
	ARController_t593870669 * G_B53_2 = NULL;
	int32_t G_B55_0 = 0;
	String_t* G_B55_1 = NULL;
	ARController_t593870669 * G_B55_2 = NULL;
	ARController_t593870669 * G_B55_3 = NULL;
	{
		bool L_0 = __this->get__running_12();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		bool L_1 = __this->get__sceneConfiguredForVideo_14();
		if (L_1)
		{
			goto IL_0810;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_2 = PluginFunctions_arwIsRunning_m3605556890(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0043;
		}
	}
	{
		bool L_3 = __this->get__sceneConfiguredForVideoWaitingMessageLogged_15();
		if (L_3)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral2382889523, /*hidden argument*/NULL);
		__this->set__sceneConfiguredForVideoWaitingMessageLogged_15((bool)1);
	}

IL_003e:
	{
		goto IL_0810;
	}

IL_0043:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral3546602266, /*hidden argument*/NULL);
		bool L_4 = __this->get_VideoIsStereo_36();
		if (L_4)
		{
			goto IL_0265;
		}
	}
	{
		int32_t* L_5 = __this->get_address_of__videoWidth0_26();
		int32_t* L_6 = __this->get_address_of__videoHeight0_27();
		int32_t* L_7 = __this->get_address_of__videoPixelSize0_28();
		String_t** L_8 = __this->get_address_of__videoPixelFormatString0_29();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_9 = PluginFunctions_arwGetVideoParams_m4086655695(NULL /*static, unused*/, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		bool L_10 = V_0;
		if (L_10)
		{
			goto IL_007e;
		}
	}
	{
		return (bool)0;
	}

IL_007e:
	{
		ObjectU5BU5D_t3614634134* L_11 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9)));
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral3191465354);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3191465354);
		ObjectU5BU5D_t3614634134* L_12 = L_11;
		int32_t L_13 = __this->get__videoWidth0_26();
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_15);
		ObjectU5BU5D_t3614634134* L_16 = L_12;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral372029398);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029398);
		ObjectU5BU5D_t3614634134* L_17 = L_16;
		int32_t L_18 = __this->get__videoHeight0_27();
		int32_t L_19 = L_18;
		Il2CppObject * L_20 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_20);
		ObjectU5BU5D_t3614634134* L_21 = L_17;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral372029406);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029406);
		ObjectU5BU5D_t3614634134* L_22 = L_21;
		int32_t L_23 = __this->get__videoPixelSize0_28();
		int32_t L_24 = L_23;
		Il2CppObject * L_25 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_24);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_25);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = L_22;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral1023885896);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral1023885896);
		ObjectU5BU5D_t3614634134* L_27 = L_26;
		String_t* L_28 = __this->get__videoPixelFormatString0_29();
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, L_28);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_28);
		ObjectU5BU5D_t3614634134* L_29 = L_27;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, _stringLiteral372029317);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m3881798623(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		V_1 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		SingleU5BU5D_t577127397* L_31 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwGetProjectionMatrix_m2140236978(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		SingleU5BU5D_t577127397* L_32 = V_1;
		Matrix4x4_t2933234003  L_33 = ARUtilityFunctions_MatrixFromFloatArray_m3542072061(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		__this->set__videoProjectionMatrix0_30(L_33);
		String_t* L_34 = Environment_get_NewLine_m266316410(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t2933234003 * L_35 = __this->get_address_of__videoProjectionMatrix0_30();
		String_t* L_36 = Matrix4x4_ToString_m1982554457(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		String_t* L_37 = String_Trim_m2668767713(L_36, /*hidden argument*/NULL);
		String_t* L_38 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral1201305906, L_34, L_37, _stringLiteral372029425, /*hidden argument*/NULL);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		bool L_39 = __this->get_ContentRotate90_63();
		if (!L_39)
		{
			goto IL_016e;
		}
	}
	{
		Vector3_t2243707580  L_40 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = Vector3_get_back_m4246539215(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_42 = Quaternion_AngleAxis_m2806222563(NULL /*static, unused*/, (90.0f), L_41, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_44 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_40, L_42, L_43, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_45 = __this->get__videoProjectionMatrix0_30();
		Matrix4x4_t2933234003  L_46 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
		__this->set__videoProjectionMatrix0_30(L_46);
	}

IL_016e:
	{
		bool L_47 = __this->get_ContentFlipV_65();
		if (!L_47)
		{
			goto IL_01ad;
		}
	}
	{
		Vector3_t2243707580  L_48 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_49 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50;
		memset(&L_50, 0, sizeof(L_50));
		Vector3__ctor_m2638739322(&L_50, (1.0f), (-1.0f), (1.0f), /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_51 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_48, L_49, L_50, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_52 = __this->get__videoProjectionMatrix0_30();
		Matrix4x4_t2933234003  L_53 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/NULL);
		__this->set__videoProjectionMatrix0_30(L_53);
	}

IL_01ad:
	{
		bool L_54 = __this->get_ContentFlipH_64();
		if (!L_54)
		{
			goto IL_01ec;
		}
	}
	{
		Vector3_t2243707580  L_55 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_56 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_57;
		memset(&L_57, 0, sizeof(L_57));
		Vector3__ctor_m2638739322(&L_57, (-1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_58 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_55, L_56, L_57, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_59 = __this->get__videoProjectionMatrix0_30();
		Matrix4x4_t2933234003  L_60 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_58, L_59, /*hidden argument*/NULL);
		__this->set__videoProjectionMatrix0_30(L_60);
	}

IL_01ec:
	{
		int32_t L_61 = __this->get__videoWidth0_26();
		int32_t L_62 = __this->get__videoHeight0_27();
		int32_t L_63 = __this->get_BackgroundLayer0_25();
		ColorU5BU5D_t672350442** L_64 = __this->get_address_of__videoColorArray0_32();
		Color32U5BU5D_t30278651** L_65 = __this->get_address_of__videoColor32Array0_33();
		Texture2D_t3542995729 ** L_66 = __this->get_address_of__videoTexture0_34();
		Material_t193706927 ** L_67 = __this->get_address_of__videoMaterial0_35();
		GameObject_t1756533147 * L_68 = ARController_CreateVideoBackgroundMesh_m3804930034(__this, 0, L_61, L_62, L_63, L_64, L_65, L_66, L_67, /*hidden argument*/NULL);
		__this->set__videoBackgroundMeshGO0_31(L_68);
		GameObject_t1756533147 * L_69 = __this->get__videoBackgroundMeshGO0_31();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_70 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_69, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_70)
		{
			goto IL_0256;
		}
	}
	{
		Texture2D_t3542995729 * L_71 = __this->get__videoTexture0_34();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_72 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_71, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_72)
		{
			goto IL_0256;
		}
	}
	{
		Material_t193706927 * L_73 = __this->get__videoMaterial0_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_74 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_73, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_0260;
		}
	}

IL_0256:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral2406077377, /*hidden argument*/NULL);
	}

IL_0260:
	{
		goto IL_0655;
	}

IL_0265:
	{
		int32_t* L_75 = __this->get_address_of__videoWidth0_26();
		int32_t* L_76 = __this->get_address_of__videoHeight0_27();
		int32_t* L_77 = __this->get_address_of__videoPixelSize0_28();
		String_t** L_78 = __this->get_address_of__videoPixelFormatString0_29();
		int32_t* L_79 = __this->get_address_of__videoWidth1_46();
		int32_t* L_80 = __this->get_address_of__videoHeight1_47();
		int32_t* L_81 = __this->get_address_of__videoPixelSize1_48();
		String_t** L_82 = __this->get_address_of__videoPixelFormatString1_49();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_83 = PluginFunctions_arwGetVideoParamsStereo_m3050262472(NULL /*static, unused*/, L_75, L_76, L_77, L_78, L_79, L_80, L_81, L_82, /*hidden argument*/NULL);
		V_2 = L_83;
		bool L_84 = V_2;
		if (L_84)
		{
			goto IL_02a3;
		}
	}
	{
		return (bool)0;
	}

IL_02a3:
	{
		ObjectU5BU5D_t3614634134* L_85 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)((int32_t)17)));
		NullCheck(L_85);
		ArrayElementTypeCheck (L_85, _stringLiteral2172287039);
		(L_85)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2172287039);
		ObjectU5BU5D_t3614634134* L_86 = L_85;
		int32_t L_87 = __this->get__videoWidth0_26();
		int32_t L_88 = L_87;
		Il2CppObject * L_89 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_88);
		NullCheck(L_86);
		ArrayElementTypeCheck (L_86, L_89);
		(L_86)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_89);
		ObjectU5BU5D_t3614634134* L_90 = L_86;
		NullCheck(L_90);
		ArrayElementTypeCheck (L_90, _stringLiteral372029398);
		(L_90)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029398);
		ObjectU5BU5D_t3614634134* L_91 = L_90;
		int32_t L_92 = __this->get__videoHeight0_27();
		int32_t L_93 = L_92;
		Il2CppObject * L_94 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_93);
		NullCheck(L_91);
		ArrayElementTypeCheck (L_91, L_94);
		(L_91)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_94);
		ObjectU5BU5D_t3614634134* L_95 = L_91;
		NullCheck(L_95);
		ArrayElementTypeCheck (L_95, _stringLiteral372029406);
		(L_95)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029406);
		ObjectU5BU5D_t3614634134* L_96 = L_95;
		int32_t L_97 = __this->get__videoPixelSize0_28();
		int32_t L_98 = L_97;
		Il2CppObject * L_99 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_98);
		NullCheck(L_96);
		ArrayElementTypeCheck (L_96, L_99);
		(L_96)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_99);
		ObjectU5BU5D_t3614634134* L_100 = L_96;
		NullCheck(L_100);
		ArrayElementTypeCheck (L_100, _stringLiteral1023885896);
		(L_100)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral1023885896);
		ObjectU5BU5D_t3614634134* L_101 = L_100;
		String_t* L_102 = __this->get__videoPixelFormatString0_29();
		NullCheck(L_101);
		ArrayElementTypeCheck (L_101, L_102);
		(L_101)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_102);
		ObjectU5BU5D_t3614634134* L_103 = L_101;
		NullCheck(L_103);
		ArrayElementTypeCheck (L_103, _stringLiteral1911857621);
		(L_103)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral1911857621);
		ObjectU5BU5D_t3614634134* L_104 = L_103;
		int32_t L_105 = __this->get__videoWidth1_46();
		int32_t L_106 = L_105;
		Il2CppObject * L_107 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_106);
		NullCheck(L_104);
		ArrayElementTypeCheck (L_104, L_107);
		(L_104)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_107);
		ObjectU5BU5D_t3614634134* L_108 = L_104;
		NullCheck(L_108);
		ArrayElementTypeCheck (L_108, _stringLiteral372029398);
		(L_108)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral372029398);
		ObjectU5BU5D_t3614634134* L_109 = L_108;
		int32_t L_110 = __this->get__videoHeight1_47();
		int32_t L_111 = L_110;
		Il2CppObject * L_112 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_111);
		NullCheck(L_109);
		ArrayElementTypeCheck (L_109, L_112);
		(L_109)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_112);
		ObjectU5BU5D_t3614634134* L_113 = L_109;
		NullCheck(L_113);
		ArrayElementTypeCheck (L_113, _stringLiteral372029406);
		(L_113)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral372029406);
		ObjectU5BU5D_t3614634134* L_114 = L_113;
		int32_t L_115 = __this->get__videoPixelSize1_48();
		int32_t L_116 = L_115;
		Il2CppObject * L_117 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_116);
		NullCheck(L_114);
		ArrayElementTypeCheck (L_114, L_117);
		(L_114)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)L_117);
		ObjectU5BU5D_t3614634134* L_118 = L_114;
		NullCheck(L_118);
		ArrayElementTypeCheck (L_118, _stringLiteral1023885896);
		(L_118)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral1023885896);
		ObjectU5BU5D_t3614634134* L_119 = L_118;
		String_t* L_120 = __this->get__videoPixelFormatString1_49();
		NullCheck(L_119);
		ArrayElementTypeCheck (L_119, L_120);
		(L_119)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_120);
		ObjectU5BU5D_t3614634134* L_121 = L_119;
		NullCheck(L_121);
		ArrayElementTypeCheck (L_121, _stringLiteral372029317);
		(L_121)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_122 = String_Concat_m3881798623(NULL /*static, unused*/, L_121, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_122, /*hidden argument*/NULL);
		V_3 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		V_4 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		SingleU5BU5D_t577127397* L_123 = V_3;
		SingleU5BU5D_t577127397* L_124 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwGetProjectionMatrixStereo_m3127493433(NULL /*static, unused*/, L_123, L_124, /*hidden argument*/NULL);
		SingleU5BU5D_t577127397* L_125 = V_3;
		Matrix4x4_t2933234003  L_126 = ARUtilityFunctions_MatrixFromFloatArray_m3542072061(NULL /*static, unused*/, L_125, /*hidden argument*/NULL);
		__this->set__videoProjectionMatrix0_30(L_126);
		SingleU5BU5D_t577127397* L_127 = V_4;
		Matrix4x4_t2933234003  L_128 = ARUtilityFunctions_MatrixFromFloatArray_m3542072061(NULL /*static, unused*/, L_127, /*hidden argument*/NULL);
		__this->set__videoProjectionMatrix1_50(L_128);
		StringU5BU5D_t1642385972* L_129 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_129);
		ArrayElementTypeCheck (L_129, _stringLiteral2738709257);
		(L_129)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2738709257);
		StringU5BU5D_t1642385972* L_130 = L_129;
		String_t* L_131 = Environment_get_NewLine_m266316410(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_130);
		ArrayElementTypeCheck (L_130, L_131);
		(L_130)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_131);
		StringU5BU5D_t1642385972* L_132 = L_130;
		Matrix4x4_t2933234003 * L_133 = __this->get_address_of__videoProjectionMatrix0_30();
		String_t* L_134 = Matrix4x4_ToString_m1982554457(L_133, /*hidden argument*/NULL);
		NullCheck(L_134);
		String_t* L_135 = String_Trim_m2668767713(L_134, /*hidden argument*/NULL);
		NullCheck(L_132);
		ArrayElementTypeCheck (L_132, L_135);
		(L_132)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_135);
		StringU5BU5D_t1642385972* L_136 = L_132;
		NullCheck(L_136);
		ArrayElementTypeCheck (L_136, _stringLiteral4240208064);
		(L_136)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral4240208064);
		StringU5BU5D_t1642385972* L_137 = L_136;
		String_t* L_138 = Environment_get_NewLine_m266316410(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_137);
		ArrayElementTypeCheck (L_137, L_138);
		(L_137)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_138);
		StringU5BU5D_t1642385972* L_139 = L_137;
		Matrix4x4_t2933234003 * L_140 = __this->get_address_of__videoProjectionMatrix1_50();
		String_t* L_141 = Matrix4x4_ToString_m1982554457(L_140, /*hidden argument*/NULL);
		NullCheck(L_141);
		String_t* L_142 = String_Trim_m2668767713(L_141, /*hidden argument*/NULL);
		NullCheck(L_139);
		ArrayElementTypeCheck (L_139, L_142);
		(L_139)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_142);
		StringU5BU5D_t1642385972* L_143 = L_139;
		NullCheck(L_143);
		ArrayElementTypeCheck (L_143, _stringLiteral372029425);
		(L_143)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral372029425);
		String_t* L_144 = String_Concat_m626692867(NULL /*static, unused*/, L_143, /*hidden argument*/NULL);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_144, /*hidden argument*/NULL);
		bool L_145 = __this->get_ContentRotate90_63();
		if (!L_145)
		{
			goto IL_0441;
		}
	}
	{
		Vector3_t2243707580  L_146 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_147 = Vector3_get_back_m4246539215(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_148 = Quaternion_AngleAxis_m2806222563(NULL /*static, unused*/, (90.0f), L_147, /*hidden argument*/NULL);
		Vector3_t2243707580  L_149 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_150 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_146, L_148, L_149, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_151 = __this->get__videoProjectionMatrix0_30();
		Matrix4x4_t2933234003  L_152 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_150, L_151, /*hidden argument*/NULL);
		__this->set__videoProjectionMatrix0_30(L_152);
	}

IL_0441:
	{
		bool L_153 = __this->get_ContentRotate90_63();
		if (!L_153)
		{
			goto IL_047b;
		}
	}
	{
		Vector3_t2243707580  L_154 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_155 = Vector3_get_back_m4246539215(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_156 = Quaternion_AngleAxis_m2806222563(NULL /*static, unused*/, (90.0f), L_155, /*hidden argument*/NULL);
		Vector3_t2243707580  L_157 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_158 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_154, L_156, L_157, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_159 = __this->get__videoProjectionMatrix1_50();
		Matrix4x4_t2933234003  L_160 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_158, L_159, /*hidden argument*/NULL);
		__this->set__videoProjectionMatrix1_50(L_160);
	}

IL_047b:
	{
		bool L_161 = __this->get_ContentFlipV_65();
		if (!L_161)
		{
			goto IL_04ba;
		}
	}
	{
		Vector3_t2243707580  L_162 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_163 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_164;
		memset(&L_164, 0, sizeof(L_164));
		Vector3__ctor_m2638739322(&L_164, (1.0f), (-1.0f), (1.0f), /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_165 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_162, L_163, L_164, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_166 = __this->get__videoProjectionMatrix0_30();
		Matrix4x4_t2933234003  L_167 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_165, L_166, /*hidden argument*/NULL);
		__this->set__videoProjectionMatrix0_30(L_167);
	}

IL_04ba:
	{
		bool L_168 = __this->get_ContentFlipV_65();
		if (!L_168)
		{
			goto IL_04f9;
		}
	}
	{
		Vector3_t2243707580  L_169 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_170 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_171;
		memset(&L_171, 0, sizeof(L_171));
		Vector3__ctor_m2638739322(&L_171, (1.0f), (-1.0f), (1.0f), /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_172 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_169, L_170, L_171, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_173 = __this->get__videoProjectionMatrix1_50();
		Matrix4x4_t2933234003  L_174 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_172, L_173, /*hidden argument*/NULL);
		__this->set__videoProjectionMatrix1_50(L_174);
	}

IL_04f9:
	{
		bool L_175 = __this->get_ContentFlipH_64();
		if (!L_175)
		{
			goto IL_0538;
		}
	}
	{
		Vector3_t2243707580  L_176 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_177 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_178;
		memset(&L_178, 0, sizeof(L_178));
		Vector3__ctor_m2638739322(&L_178, (-1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_179 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_176, L_177, L_178, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_180 = __this->get__videoProjectionMatrix0_30();
		Matrix4x4_t2933234003  L_181 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_179, L_180, /*hidden argument*/NULL);
		__this->set__videoProjectionMatrix0_30(L_181);
	}

IL_0538:
	{
		bool L_182 = __this->get_ContentFlipH_64();
		if (!L_182)
		{
			goto IL_0577;
		}
	}
	{
		Vector3_t2243707580  L_183 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_184 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_185;
		memset(&L_185, 0, sizeof(L_185));
		Vector3__ctor_m2638739322(&L_185, (-1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_186 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_183, L_184, L_185, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_187 = __this->get__videoProjectionMatrix1_50();
		Matrix4x4_t2933234003  L_188 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_186, L_187, /*hidden argument*/NULL);
		__this->set__videoProjectionMatrix1_50(L_188);
	}

IL_0577:
	{
		int32_t L_189 = __this->get__videoWidth0_26();
		int32_t L_190 = __this->get__videoHeight0_27();
		int32_t L_191 = __this->get_BackgroundLayer0_25();
		ColorU5BU5D_t672350442** L_192 = __this->get_address_of__videoColorArray0_32();
		Color32U5BU5D_t30278651** L_193 = __this->get_address_of__videoColor32Array0_33();
		Texture2D_t3542995729 ** L_194 = __this->get_address_of__videoTexture0_34();
		Material_t193706927 ** L_195 = __this->get_address_of__videoMaterial0_35();
		GameObject_t1756533147 * L_196 = ARController_CreateVideoBackgroundMesh_m3804930034(__this, 0, L_189, L_190, L_191, L_192, L_193, L_194, L_195, /*hidden argument*/NULL);
		__this->set__videoBackgroundMeshGO0_31(L_196);
		int32_t L_197 = __this->get__videoWidth1_46();
		int32_t L_198 = __this->get__videoHeight1_47();
		int32_t L_199 = __this->get_BackgroundLayer1_45();
		ColorU5BU5D_t672350442** L_200 = __this->get_address_of__videoColorArray1_52();
		Color32U5BU5D_t30278651** L_201 = __this->get_address_of__videoColor32Array1_53();
		Texture2D_t3542995729 ** L_202 = __this->get_address_of__videoTexture1_54();
		Material_t193706927 ** L_203 = __this->get_address_of__videoMaterial1_55();
		GameObject_t1756533147 * L_204 = ARController_CreateVideoBackgroundMesh_m3804930034(__this, 1, L_197, L_198, L_199, L_200, L_201, L_202, L_203, /*hidden argument*/NULL);
		__this->set__videoBackgroundMeshGO1_51(L_204);
		GameObject_t1756533147 * L_205 = __this->get__videoBackgroundMeshGO0_31();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_206 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_205, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_206)
		{
			goto IL_064b;
		}
	}
	{
		Texture2D_t3542995729 * L_207 = __this->get__videoTexture0_34();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_208 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_207, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_208)
		{
			goto IL_064b;
		}
	}
	{
		Material_t193706927 * L_209 = __this->get__videoMaterial0_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_210 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_209, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_210)
		{
			goto IL_064b;
		}
	}
	{
		GameObject_t1756533147 * L_211 = __this->get__videoBackgroundMeshGO1_51();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_212 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_211, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_212)
		{
			goto IL_064b;
		}
	}
	{
		Texture2D_t3542995729 * L_213 = __this->get__videoTexture1_54();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_214 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_213, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_214)
		{
			goto IL_064b;
		}
	}
	{
		Material_t193706927 * L_215 = __this->get__videoMaterial1_55();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_216 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_215, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_216)
		{
			goto IL_0655;
		}
	}

IL_064b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral2340739555, /*hidden argument*/NULL);
	}

IL_0655:
	{
		V_5 = (bool)0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_217 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ARCamera_t431610428_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_218 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_217, /*hidden argument*/NULL);
		V_6 = ((ARCameraU5BU5D_t1286580565*)IsInst(L_218, ARCameraU5BU5D_t1286580565_il2cpp_TypeInfo_var));
		ARCameraU5BU5D_t1286580565* L_219 = V_6;
		V_8 = L_219;
		V_9 = 0;
		goto IL_0696;
	}

IL_067a:
	{
		ARCameraU5BU5D_t1286580565* L_220 = V_8;
		int32_t L_221 = V_9;
		NullCheck(L_220);
		int32_t L_222 = L_221;
		ARCamera_t431610428 * L_223 = (L_220)->GetAt(static_cast<il2cpp_array_size_t>(L_222));
		V_7 = L_223;
		ARCamera_t431610428 * L_224 = V_7;
		NullCheck(L_224);
		bool L_225 = L_224->get_Stereo_11();
		if (!L_225)
		{
			goto IL_0690;
		}
	}
	{
		V_5 = (bool)1;
	}

IL_0690:
	{
		int32_t L_226 = V_9;
		V_9 = ((int32_t)((int32_t)L_226+(int32_t)1));
	}

IL_0696:
	{
		int32_t L_227 = V_9;
		ARCameraU5BU5D_t1286580565* L_228 = V_8;
		NullCheck(L_228);
		if ((((int32_t)L_227) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_228)->max_length)))))))
		{
			goto IL_067a;
		}
	}
	{
		bool L_229 = V_5;
		if (L_229)
		{
			goto IL_06f6;
		}
	}
	{
		int32_t L_230 = __this->get_BackgroundLayer0_25();
		Camera_t189460977 ** L_231 = __this->get_address_of__videoBackgroundCamera0_58();
		GameObject_t1756533147 * L_232 = ARController_CreateVideoBackgroundCamera_m781252442(__this, _stringLiteral1678595967, L_230, L_231, /*hidden argument*/NULL);
		__this->set__videoBackgroundCameraGO0_57(L_232);
		GameObject_t1756533147 * L_233 = __this->get__videoBackgroundCameraGO0_57();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_234 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_233, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_234)
		{
			goto IL_06e7;
		}
	}
	{
		Camera_t189460977 * L_235 = __this->get__videoBackgroundCamera0_58();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_236 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_235, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_236)
		{
			goto IL_06f1;
		}
	}

IL_06e7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral946294561, /*hidden argument*/NULL);
	}

IL_06f1:
	{
		goto IL_0794;
	}

IL_06f6:
	{
		int32_t L_237 = __this->get_BackgroundLayer0_25();
		Camera_t189460977 ** L_238 = __this->get_address_of__videoBackgroundCamera0_58();
		GameObject_t1756533147 * L_239 = ARController_CreateVideoBackgroundCamera_m781252442(__this, _stringLiteral4034366264, L_237, L_238, /*hidden argument*/NULL);
		__this->set__videoBackgroundCameraGO0_57(L_239);
		bool L_240 = __this->get_VideoIsStereo_36();
		G_B53_0 = _stringLiteral4034366258;
		G_B53_1 = __this;
		G_B53_2 = __this;
		if (!L_240)
		{
			G_B54_0 = _stringLiteral4034366258;
			G_B54_1 = __this;
			G_B54_2 = __this;
			goto IL_0730;
		}
	}
	{
		int32_t L_241 = __this->get_BackgroundLayer1_45();
		G_B55_0 = L_241;
		G_B55_1 = G_B53_0;
		G_B55_2 = G_B53_1;
		G_B55_3 = G_B53_2;
		goto IL_0736;
	}

IL_0730:
	{
		int32_t L_242 = __this->get_BackgroundLayer0_25();
		G_B55_0 = L_242;
		G_B55_1 = G_B54_0;
		G_B55_2 = G_B54_1;
		G_B55_3 = G_B54_2;
	}

IL_0736:
	{
		Camera_t189460977 ** L_243 = __this->get_address_of__videoBackgroundCamera1_60();
		NullCheck(G_B55_2);
		GameObject_t1756533147 * L_244 = ARController_CreateVideoBackgroundCamera_m781252442(G_B55_2, G_B55_1, G_B55_0, L_243, /*hidden argument*/NULL);
		NullCheck(G_B55_3);
		G_B55_3->set__videoBackgroundCameraGO1_59(L_244);
		GameObject_t1756533147 * L_245 = __this->get__videoBackgroundCameraGO0_57();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_246 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_245, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_246)
		{
			goto IL_078a;
		}
	}
	{
		Camera_t189460977 * L_247 = __this->get__videoBackgroundCamera0_58();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_248 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_247, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_248)
		{
			goto IL_078a;
		}
	}
	{
		GameObject_t1756533147 * L_249 = __this->get__videoBackgroundCameraGO1_59();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_250 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_249, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_250)
		{
			goto IL_078a;
		}
	}
	{
		Camera_t189460977 * L_251 = __this->get__videoBackgroundCamera1_60();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_252 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_251, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_252)
		{
			goto IL_0794;
		}
	}

IL_078a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral946294561, /*hidden argument*/NULL);
	}

IL_0794:
	{
		ARController_ConfigureForegroundCameras_m229175717(__this, /*hidden argument*/NULL);
		ARController_ConfigureViewports_m2051600675(__this, /*hidden argument*/NULL);
		bool L_253 = __this->get__useNativeGLTexturing_16();
		if (!L_253)
		{
			goto IL_07ff;
		}
	}
	{
		int32_t L_254 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_254) == ((int32_t)8)))
		{
			goto IL_07ff;
		}
	}
	{
		int32_t L_255 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_255) == ((int32_t)((int32_t)11))))
		{
			goto IL_07ff;
		}
	}
	{
		bool L_256 = __this->get_VideoIsStereo_36();
		if (L_256)
		{
			goto IL_07e4;
		}
	}
	{
		Texture2D_t3542995729 * L_257 = __this->get__videoTexture0_34();
		NullCheck(L_257);
		int32_t L_258 = Texture_GetNativeTextureID_m1317239321(L_257, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetUnityRenderEventUpdateTextureGLTextureID_m3634330186(NULL /*static, unused*/, L_258, /*hidden argument*/NULL);
		goto IL_07ff;
	}

IL_07e4:
	{
		Texture2D_t3542995729 * L_259 = __this->get__videoTexture0_34();
		NullCheck(L_259);
		int32_t L_260 = Texture_GetNativeTextureID_m1317239321(L_259, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_261 = __this->get__videoTexture1_54();
		NullCheck(L_261);
		int32_t L_262 = Texture_GetNativeTextureID_m1317239321(L_261, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs_m10115510(NULL /*static, unused*/, L_260, L_262, /*hidden argument*/NULL);
	}

IL_07ff:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral1251222247, /*hidden argument*/NULL);
		__this->set__sceneConfiguredForVideo_14((bool)1);
	}

IL_0810:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_263 = PluginFunctions_arwCapture_m650910371(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_10 = L_263;
		bool L_264 = PluginFunctions_arwUpdateAR_m598243641(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_11 = L_264;
		bool L_265 = V_11;
		if (L_265)
		{
			goto IL_0827;
		}
	}
	{
		return (bool)0;
	}

IL_0827:
	{
		bool L_266 = V_10;
		if (!L_266)
		{
			goto IL_084a;
		}
	}
	{
		bool L_267 = __this->get__sceneConfiguredForVideo_14();
		if (!L_267)
		{
			goto IL_084a;
		}
	}
	{
		bool L_268 = ARController_get_UseVideoBackground_m1201911505(__this, /*hidden argument*/NULL);
		if (!L_268)
		{
			goto IL_084a;
		}
	}
	{
		ARController_UpdateTexture_m1367383666(__this, /*hidden argument*/NULL);
	}

IL_084a:
	{
		return (bool)1;
	}
}
// System.Boolean ARController::StopAR()
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral432438640;
extern Il2CppCodeGenString* _stringLiteral1325541366;
extern const uint32_t ARController_StopAR_m3264126367_MetadataUsageId;
extern "C"  bool ARController_StopAR_m3264126367 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_StopAR_m3264126367_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__running_12();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral432438640, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_1 = PluginFunctions_arwStopRunning_m1121675878(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral1325541366, /*hidden argument*/NULL);
	}

IL_002b:
	{
		ARController_DestroyVideoBackground_m152274941(__this, /*hidden argument*/NULL);
		ARController_DestroyClearCamera_m113337958(__this, /*hidden argument*/NULL);
		__this->set__running_12((bool)0);
		return (bool)1;
	}
}
// System.Void ARController::SetContentForScreenOrientation(System.Boolean)
extern "C"  void ARController_SetContentForScreenOrientation_m3593716101 (ARController_t593870669 * __this, bool ___cameraIsFrontFacing0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Screen_get_orientation_m879255848(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0027;
		}
	}
	{
		__this->set_ContentRotate90_63((bool)1);
		__this->set_ContentFlipV_65((bool)0);
		bool L_2 = ___cameraIsFrontFacing0;
		__this->set_ContentFlipH_64(L_2);
		goto IL_008b;
	}

IL_0027:
	{
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_3) == ((uint32_t)3))))
		{
			goto IL_0048;
		}
	}
	{
		__this->set_ContentRotate90_63((bool)0);
		__this->set_ContentFlipV_65((bool)0);
		bool L_4 = ___cameraIsFrontFacing0;
		__this->set_ContentFlipH_64(L_4);
		goto IL_008b;
	}

IL_0048:
	{
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_006c;
		}
	}
	{
		__this->set_ContentRotate90_63((bool)1);
		__this->set_ContentFlipV_65((bool)1);
		bool L_6 = ___cameraIsFrontFacing0;
		__this->set_ContentFlipH_64((bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0));
		goto IL_008b;
	}

IL_006c:
	{
		int32_t L_7 = V_0;
		if ((!(((uint32_t)L_7) == ((uint32_t)4))))
		{
			goto IL_008b;
		}
	}
	{
		__this->set_ContentRotate90_63((bool)0);
		__this->set_ContentFlipV_65((bool)1);
		bool L_8 = ___cameraIsFrontFacing0;
		__this->set_ContentFlipH_64((bool)((((int32_t)L_8) == ((int32_t)0))? 1 : 0));
	}

IL_008b:
	{
		return;
	}
}
// System.Void ARController::SetVideoAlpha(System.Single)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ARController_SetVideoAlpha_m3263902264_MetadataUsageId;
extern "C"  void ARController_SetVideoAlpha_m3263902264 (ARController_t593870669 * __this, float ___a0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_SetVideoAlpha_m3263902264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get__videoMaterial0_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get__videoMaterial0_35();
		float L_3 = ___a0;
		Color_t2020392075  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m1909920690(&L_4, (1.0f), (1.0f), (1.0f), L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_set_color_m577844242(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0031:
	{
		Material_t193706927 * L_5 = __this->get__videoMaterial1_55();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0062;
		}
	}
	{
		Material_t193706927 * L_7 = __this->get__videoMaterial1_55();
		float L_8 = ___a0;
		Color_t2020392075  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m1909920690(&L_9, (1.0f), (1.0f), (1.0f), L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_set_color_m577844242(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Boolean ARController::get_DebugVideo()
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_get_DebugVideo_m3800461305_MetadataUsageId;
extern "C"  bool ARController_get_DebugVideo_m3800461305 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_get_DebugVideo_m3800461305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_0 = PluginFunctions_arwGetVideoDebugMode_m445557160(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void ARController::set_DebugVideo(System.Boolean)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_set_DebugVideo_m539547094_MetadataUsageId;
extern "C"  void ARController_set_DebugVideo_m539547094 (ARController_t593870669 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_set_DebugVideo_m539547094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetVideoDebugMode_m3436558569(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String ARController::get_Version()
extern "C"  String_t* ARController_get_Version_m2150833958 (ARController_t593870669 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__version_11();
		return L_0;
	}
}
// ARController/ARToolKitThresholdMode ARController::get_VideoThresholdMode()
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_get_VideoThresholdMode_m1767546950_MetadataUsageId;
extern "C"  int32_t ARController_get_VideoThresholdMode_m1767546950 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_get_VideoThresholdMode_m1767546950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get__running_12();
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		int32_t L_1 = PluginFunctions_arwGetVideoThresholdMode_m1232578952(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		__this->set_currentThresholdMode_74(L_3);
		goto IL_002b;
	}

IL_0024:
	{
		__this->set_currentThresholdMode_74(0);
	}

IL_002b:
	{
		int32_t L_4 = __this->get_currentThresholdMode_74();
		return L_4;
	}
}
// System.Void ARController::set_VideoThresholdMode(ARController/ARToolKitThresholdMode)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_set_VideoThresholdMode_m3535350045_MetadataUsageId;
extern "C"  void ARController_set_VideoThresholdMode_m3535350045 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_set_VideoThresholdMode_m3535350045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		__this->set_currentThresholdMode_74(L_0);
		bool L_1 = __this->get__running_12();
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = __this->get_currentThresholdMode_74();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetVideoThresholdMode_m3956560441(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Int32 ARController::get_VideoThreshold()
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_get_VideoThreshold_m208759273_MetadataUsageId;
extern "C"  int32_t ARController_get_VideoThreshold_m208759273 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_get_VideoThreshold_m208759273_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__running_12();
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		int32_t L_1 = PluginFunctions_arwGetVideoThreshold_m4164593841(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_currentThreshold_75(L_1);
		int32_t L_2 = __this->get_currentThreshold_75();
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_3 = __this->get_currentThreshold_75();
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)255))))
		{
			goto IL_003a;
		}
	}

IL_0032:
	{
		__this->set_currentThreshold_75(((int32_t)100));
	}

IL_003a:
	{
		int32_t L_4 = __this->get_currentThreshold_75();
		return L_4;
	}
}
// System.Void ARController::set_VideoThreshold(System.Int32)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_set_VideoThreshold_m2326990118_MetadataUsageId;
extern "C"  void ARController_set_VideoThreshold_m2326990118 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_set_VideoThreshold_m2326990118_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		__this->set_currentThreshold_75(L_0);
		bool L_1 = __this->get__running_12();
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetVideoThreshold_m770268112(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// ARController/ARToolKitLabelingMode ARController::get_LabelingMode()
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_get_LabelingMode_m2521454447_MetadataUsageId;
extern "C"  int32_t ARController_get_LabelingMode_m2521454447 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_get_LabelingMode_m2521454447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get__running_12();
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		int32_t L_1 = PluginFunctions_arwGetLabelingMode_m2141811176(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		__this->set_currentLabelingMode_76(L_3);
		goto IL_002b;
	}

IL_0024:
	{
		__this->set_currentLabelingMode_76(1);
	}

IL_002b:
	{
		int32_t L_4 = __this->get_currentLabelingMode_76();
		return L_4;
	}
}
// System.Void ARController::set_LabelingMode(ARController/ARToolKitLabelingMode)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_set_LabelingMode_m2847600302_MetadataUsageId;
extern "C"  void ARController_set_LabelingMode_m2847600302 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_set_LabelingMode_m2847600302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		__this->set_currentLabelingMode_76(L_0);
		bool L_1 = __this->get__running_12();
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = __this->get_currentLabelingMode_76();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetLabelingMode_m2460252585(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Single ARController::get_BorderSize()
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_get_BorderSize_m4181028184_MetadataUsageId;
extern "C"  float ARController_get_BorderSize_m4181028184 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_get_BorderSize_m4181028184_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		bool L_0 = __this->get__running_12();
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		float L_1 = PluginFunctions_arwGetBorderSize_m756391518(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		if ((!(((float)L_2) > ((float)(0.0f)))))
		{
			goto IL_0033;
		}
	}
	{
		float L_3 = V_0;
		if ((!(((float)L_3) < ((float)(0.5f)))))
		{
			goto IL_0033;
		}
	}
	{
		float L_4 = V_0;
		__this->set_currentBorderSize_79(L_4);
		goto IL_003e;
	}

IL_0033:
	{
		__this->set_currentBorderSize_79((0.25f));
	}

IL_003e:
	{
		float L_5 = __this->get_currentBorderSize_79();
		return L_5;
	}
}
// System.Void ARController::set_BorderSize(System.Single)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_set_BorderSize_m2688897581_MetadataUsageId;
extern "C"  void ARController_set_BorderSize_m2688897581 (ARController_t593870669 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_set_BorderSize_m2688897581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___value0;
		__this->set_currentBorderSize_79(L_0);
		bool L_1 = __this->get__running_12();
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		float L_2 = __this->get_currentBorderSize_79();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetBorderSize_m3074401841(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Int32 ARController::get_TemplateSize()
extern "C"  int32_t ARController_get_TemplateSize_m4162121116 (ARController_t593870669 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_currentTemplateSize_77();
		return L_0;
	}
}
// System.Void ARController::set_TemplateSize(System.Int32)
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2813226659;
extern const uint32_t ARController_set_TemplateSize_m480258561_MetadataUsageId;
extern "C"  void ARController_set_TemplateSize_m480258561 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_set_TemplateSize_m480258561_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		__this->set_currentTemplateSize_77(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral2813226659, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ARController::get_TemplateCountMax()
extern "C"  int32_t ARController_get_TemplateCountMax_m507601286 (ARController_t593870669 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_currentTemplateCountMax_78();
		return L_0;
	}
}
// System.Void ARController::set_TemplateCountMax(System.Int32)
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3753263601;
extern const uint32_t ARController_set_TemplateCountMax_m3403685427_MetadataUsageId;
extern "C"  void ARController_set_TemplateCountMax_m3403685427 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_set_TemplateCountMax_m3403685427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		__this->set_currentTemplateCountMax_78(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral3753263601, /*hidden argument*/NULL);
		return;
	}
}
// ARController/ARToolKitPatternDetectionMode ARController::get_PatternDetectionMode()
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_get_PatternDetectionMode_m2852381811_MetadataUsageId;
extern "C"  int32_t ARController_get_PatternDetectionMode_m2852381811 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_get_PatternDetectionMode_m2852381811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get__running_12();
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		int32_t L_1 = PluginFunctions_arwGetPatternDetectionMode_m3294842179(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		__this->set_currentPatternDetectionMode_80(L_3);
		goto IL_002b;
	}

IL_0024:
	{
		__this->set_currentPatternDetectionMode_80(0);
	}

IL_002b:
	{
		int32_t L_4 = __this->get_currentPatternDetectionMode_80();
		return L_4;
	}
}
// System.Void ARController::set_PatternDetectionMode(ARController/ARToolKitPatternDetectionMode)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_set_PatternDetectionMode_m2120827272_MetadataUsageId;
extern "C"  void ARController_set_PatternDetectionMode_m2120827272 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_set_PatternDetectionMode_m2120827272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		__this->set_currentPatternDetectionMode_80(L_0);
		bool L_1 = __this->get__running_12();
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = __this->get_currentPatternDetectionMode_80();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetPatternDetectionMode_m825573592(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// ARController/ARToolKitMatrixCodeType ARController::get_MatrixCodeType()
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_get_MatrixCodeType_m3814669595_MetadataUsageId;
extern "C"  int32_t ARController_get_MatrixCodeType_m3814669595 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_get_MatrixCodeType_m3814669595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get__running_12();
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		int32_t L_1 = PluginFunctions_arwGetMatrixCodeType_m2398804801(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		__this->set_currentMatrixCodeType_81(L_3);
		goto IL_002b;
	}

IL_0024:
	{
		__this->set_currentMatrixCodeType_81(3);
	}

IL_002b:
	{
		int32_t L_4 = __this->get_currentMatrixCodeType_81();
		return L_4;
	}
}
// System.Void ARController::set_MatrixCodeType(ARController/ARToolKitMatrixCodeType)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_set_MatrixCodeType_m2903938876_MetadataUsageId;
extern "C"  void ARController_set_MatrixCodeType_m2903938876 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_set_MatrixCodeType_m2903938876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		__this->set_currentMatrixCodeType_81(L_0);
		bool L_1 = __this->get__running_12();
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = __this->get_currentMatrixCodeType_81();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetMatrixCodeType_m4153092434(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// ARController/ARToolKitImageProcMode ARController::get_ImageProcMode()
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_get_ImageProcMode_m695111051_MetadataUsageId;
extern "C"  int32_t ARController_get_ImageProcMode_m695111051 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_get_ImageProcMode_m695111051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get__running_12();
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		int32_t L_1 = PluginFunctions_arwGetImageProcMode_m3143829701(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		__this->set_currentImageProcMode_82(L_3);
		goto IL_002b;
	}

IL_0024:
	{
		__this->set_currentImageProcMode_82(0);
	}

IL_002b:
	{
		int32_t L_4 = __this->get_currentImageProcMode_82();
		return L_4;
	}
}
// System.Void ARController::set_ImageProcMode(ARController/ARToolKitImageProcMode)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_set_ImageProcMode_m2544291598_MetadataUsageId;
extern "C"  void ARController_set_ImageProcMode_m2544291598 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_set_ImageProcMode_m2544291598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		__this->set_currentImageProcMode_82(L_0);
		bool L_1 = __this->get__running_12();
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = __this->get_currentImageProcMode_82();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetImageProcMode_m2102756940(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean ARController::get_NFTMultiMode()
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_get_NFTMultiMode_m2506531219_MetadataUsageId;
extern "C"  bool ARController_get_NFTMultiMode_m2506531219 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_get_NFTMultiMode_m2506531219_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__running_12();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_1 = PluginFunctions_arwGetNFTMultiMode_m149465679(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_currentNFTMultiMode_84(L_1);
	}

IL_0016:
	{
		bool L_2 = __this->get_currentNFTMultiMode_84();
		return L_2;
	}
}
// System.Void ARController::set_NFTMultiMode(System.Boolean)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_set_NFTMultiMode_m1466211986_MetadataUsageId;
extern "C"  void ARController_set_NFTMultiMode_m1466211986 (ARController_t593870669 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_set_NFTMultiMode_m1466211986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		__this->set_currentNFTMultiMode_84(L_0);
		bool L_1 = __this->get__running_12();
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		bool L_2 = __this->get_currentNFTMultiMode_84();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetNFTMultiMode_m2677128016(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// ARController/AR_LOG_LEVEL ARController::get_LogLevel()
extern "C"  int32_t ARController_get_LogLevel_m3380931765 (ARController_t593870669 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_currentLogLevel_85();
		return L_0;
	}
}
// System.Void ARController::set_LogLevel(ARController/AR_LOG_LEVEL)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARController_set_LogLevel_m3866560960_MetadataUsageId;
extern "C"  void ARController_set_LogLevel_m3866560960 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_set_LogLevel_m3866560960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		__this->set_currentLogLevel_85(L_0);
		int32_t L_1 = __this->get_currentLogLevel_85();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetLogLevel_m3400122846(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// ContentMode ARController::get_ContentMode()
extern "C"  int32_t ARController_get_ContentMode_m862413810 (ARController_t593870669 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_currentContentMode_73();
		return L_0;
	}
}
// System.Void ARController::set_ContentMode(ContentMode)
extern "C"  void ARController_set_ContentMode_m1004281933 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_currentContentMode_73();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_2 = ___value0;
		__this->set_currentContentMode_73(L_2);
		bool L_3 = __this->get__running_12();
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		ARController_ConfigureViewports_m2051600675(__this, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Boolean ARController::get_UseVideoBackground()
extern "C"  bool ARController_get_UseVideoBackground_m1201911505 (ARController_t593870669 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_currentUseVideoBackground_83();
		return L_0;
	}
}
// System.Void ARController::set_UseVideoBackground(System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ARController_set_UseVideoBackground_m3884950314_MetadataUsageId;
extern "C"  void ARController_set_UseVideoBackground_m3884950314 (ARController_t593870669 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_set_UseVideoBackground_m3884950314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	float G_B3_2 = 0.0f;
	Camera_t189460977 * G_B3_3 = NULL;
	float G_B2_0 = 0.0f;
	float G_B2_1 = 0.0f;
	float G_B2_2 = 0.0f;
	Camera_t189460977 * G_B2_3 = NULL;
	float G_B4_0 = 0.0f;
	float G_B4_1 = 0.0f;
	float G_B4_2 = 0.0f;
	float G_B4_3 = 0.0f;
	Camera_t189460977 * G_B4_4 = NULL;
	{
		bool L_0 = ___value0;
		__this->set_currentUseVideoBackground_83(L_0);
		Camera_t189460977 * L_1 = __this->get_clearCamera_56();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		Camera_t189460977 * L_3 = __this->get_clearCamera_56();
		bool L_4 = __this->get_currentUseVideoBackground_83();
		G_B2_0 = (0.0f);
		G_B2_1 = (0.0f);
		G_B2_2 = (0.0f);
		G_B2_3 = L_3;
		if (!L_4)
		{
			G_B3_0 = (0.0f);
			G_B3_1 = (0.0f);
			G_B3_2 = (0.0f);
			G_B3_3 = L_3;
			goto IL_0042;
		}
	}
	{
		G_B4_0 = (1.0f);
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		G_B4_3 = G_B2_2;
		G_B4_4 = G_B2_3;
		goto IL_0047;
	}

IL_0042:
	{
		G_B4_0 = (0.0f);
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
		G_B4_3 = G_B3_2;
		G_B4_4 = G_B3_3;
	}

IL_0047:
	{
		Color_t2020392075  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Color__ctor_m1909920690(&L_5, G_B4_3, G_B4_2, G_B4_1, G_B4_0, /*hidden argument*/NULL);
		NullCheck(G_B4_4);
		Camera_set_backgroundColor_m2927893592(G_B4_4, L_5, /*hidden argument*/NULL);
	}

IL_0051:
	{
		Camera_t189460977 * L_6 = __this->get__videoBackgroundCamera0_58();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		Camera_t189460977 * L_8 = __this->get__videoBackgroundCamera0_58();
		bool L_9 = __this->get_currentUseVideoBackground_83();
		NullCheck(L_8);
		Behaviour_set_enabled_m1796096907(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0073:
	{
		Camera_t189460977 * L_10 = __this->get__videoBackgroundCamera1_60();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_10, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0095;
		}
	}
	{
		Camera_t189460977 * L_12 = __this->get__videoBackgroundCamera1_60();
		bool L_13 = __this->get_currentUseVideoBackground_83();
		NullCheck(L_12);
		Behaviour_set_enabled_m1796096907(L_12, L_13, /*hidden argument*/NULL);
	}

IL_0095:
	{
		return;
	}
}
// System.Void ARController::UpdateTexture()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1586811326;
extern Il2CppCodeGenString* _stringLiteral4257839027;
extern Il2CppCodeGenString* _stringLiteral3944004047;
extern const uint32_t ARController_UpdateTexture_m1367383666_MetadataUsageId;
extern "C"  void ARController_UpdateTexture_m1367383666 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_UpdateTexture_m1367383666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	{
		bool L_0 = __this->get__running_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		bool L_1 = __this->get_VideoIsStereo_36();
		if (L_1)
		{
			goto IL_0115;
		}
	}
	{
		Texture2D_t3542995729 * L_2 = __this->get__videoTexture0_34();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral1586811326, /*hidden argument*/NULL);
		goto IL_0110;
	}

IL_0037:
	{
		bool L_4 = __this->get__useNativeGLTexturing_16();
		if (!L_4)
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_5 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)8)))
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_6 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_006f;
		}
	}

IL_0059:
	{
		Texture2D_t3542995729 * L_7 = __this->get__videoTexture0_34();
		NullCheck(L_7);
		int32_t L_8 = Texture_GetNativeTextureID_m1317239321(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwUpdateTextureGL_m358316523(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		goto IL_0075;
	}

IL_006f:
	{
		GL_IssuePluginEvent_m993229995(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_0075:
	{
		goto IL_0110;
	}

IL_007a:
	{
		Color32U5BU5D_t30278651* L_9 = __this->get__videoColor32Array0_33();
		if (!L_9)
		{
			goto IL_00b9;
		}
	}
	{
		Color32U5BU5D_t30278651* L_10 = __this->get__videoColor32Array0_33();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_11 = PluginFunctions_arwUpdateTexture32_m3012693829(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		bool L_12 = V_0;
		if (!L_12)
		{
			goto IL_00b4;
		}
	}
	{
		Texture2D_t3542995729 * L_13 = __this->get__videoTexture0_34();
		Color32U5BU5D_t30278651* L_14 = __this->get__videoColor32Array0_33();
		NullCheck(L_13);
		Texture2D_SetPixels32_m2480505405(L_13, L_14, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_15 = __this->get__videoTexture0_34();
		NullCheck(L_15);
		Texture2D_Apply_m3971781551(L_15, (bool)0, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		goto IL_0110;
	}

IL_00b9:
	{
		ColorU5BU5D_t672350442* L_16 = __this->get__videoColorArray0_32();
		if (!L_16)
		{
			goto IL_0106;
		}
	}
	{
		ColorU5BU5D_t672350442* L_17 = __this->get__videoColorArray0_32();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_18 = PluginFunctions_arwUpdateTexture_m210610373(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		bool L_19 = V_1;
		if (!L_19)
		{
			goto IL_0101;
		}
	}
	{
		Texture2D_t3542995729 * L_20 = __this->get__videoTexture0_34();
		int32_t L_21 = __this->get__videoWidth0_26();
		int32_t L_22 = __this->get__videoHeight0_27();
		ColorU5BU5D_t672350442* L_23 = __this->get__videoColorArray0_32();
		NullCheck(L_20);
		Texture2D_SetPixels_m2034528253(L_20, 0, 0, L_21, L_22, L_23, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_24 = __this->get__videoTexture0_34();
		NullCheck(L_24);
		Texture2D_Apply_m3971781551(L_24, (bool)0, /*hidden argument*/NULL);
	}

IL_0101:
	{
		goto IL_0110;
	}

IL_0106:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral4257839027, /*hidden argument*/NULL);
	}

IL_0110:
	{
		goto IL_0294;
	}

IL_0115:
	{
		Texture2D_t3542995729 * L_25 = __this->get__videoTexture0_34();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_25, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_0137;
		}
	}
	{
		Texture2D_t3542995729 * L_27 = __this->get__videoTexture1_54();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_28 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_27, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_0146;
		}
	}

IL_0137:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral3944004047, /*hidden argument*/NULL);
		goto IL_0294;
	}

IL_0146:
	{
		bool L_29 = __this->get__useNativeGLTexturing_16();
		if (!L_29)
		{
			goto IL_0194;
		}
	}
	{
		int32_t L_30 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_30) == ((int32_t)8)))
		{
			goto IL_0168;
		}
	}
	{
		int32_t L_31 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_31) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0189;
		}
	}

IL_0168:
	{
		Texture2D_t3542995729 * L_32 = __this->get__videoTexture0_34();
		NullCheck(L_32);
		int32_t L_33 = Texture_GetNativeTextureID_m1317239321(L_32, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_34 = __this->get__videoTexture1_54();
		NullCheck(L_34);
		int32_t L_35 = Texture_GetNativeTextureID_m1317239321(L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwUpdateTextureGLStereo_m2096161246(NULL /*static, unused*/, L_33, L_35, /*hidden argument*/NULL);
		goto IL_018f;
	}

IL_0189:
	{
		GL_IssuePluginEvent_m993229995(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
	}

IL_018f:
	{
		goto IL_0294;
	}

IL_0194:
	{
		Color32U5BU5D_t30278651* L_36 = __this->get__videoColor32Array0_33();
		if (!L_36)
		{
			goto IL_0201;
		}
	}
	{
		Color32U5BU5D_t30278651* L_37 = __this->get__videoColor32Array1_53();
		if (!L_37)
		{
			goto IL_0201;
		}
	}
	{
		Color32U5BU5D_t30278651* L_38 = __this->get__videoColor32Array0_33();
		Color32U5BU5D_t30278651* L_39 = __this->get__videoColor32Array1_53();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_40 = PluginFunctions_arwUpdateTexture32Stereo_m3306506678(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
		V_2 = L_40;
		bool L_41 = V_2;
		if (!L_41)
		{
			goto IL_01fc;
		}
	}
	{
		Texture2D_t3542995729 * L_42 = __this->get__videoTexture0_34();
		Color32U5BU5D_t30278651* L_43 = __this->get__videoColor32Array0_33();
		NullCheck(L_42);
		Texture2D_SetPixels32_m2480505405(L_42, L_43, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_44 = __this->get__videoTexture1_54();
		Color32U5BU5D_t30278651* L_45 = __this->get__videoColor32Array1_53();
		NullCheck(L_44);
		Texture2D_SetPixels32_m2480505405(L_44, L_45, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_46 = __this->get__videoTexture0_34();
		NullCheck(L_46);
		Texture2D_Apply_m3971781551(L_46, (bool)0, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_47 = __this->get__videoTexture1_54();
		NullCheck(L_47);
		Texture2D_Apply_m3971781551(L_47, (bool)0, /*hidden argument*/NULL);
	}

IL_01fc:
	{
		goto IL_0294;
	}

IL_0201:
	{
		ColorU5BU5D_t672350442* L_48 = __this->get__videoColorArray0_32();
		if (!L_48)
		{
			goto IL_028a;
		}
	}
	{
		ColorU5BU5D_t672350442* L_49 = __this->get__videoColorArray1_52();
		if (!L_49)
		{
			goto IL_028a;
		}
	}
	{
		ColorU5BU5D_t672350442* L_50 = __this->get__videoColorArray0_32();
		ColorU5BU5D_t672350442* L_51 = __this->get__videoColorArray1_52();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_52 = PluginFunctions_arwUpdateTextureStereo_m3642194655(NULL /*static, unused*/, L_50, L_51, /*hidden argument*/NULL);
		V_3 = L_52;
		bool L_53 = V_3;
		if (!L_53)
		{
			goto IL_0285;
		}
	}
	{
		Texture2D_t3542995729 * L_54 = __this->get__videoTexture0_34();
		int32_t L_55 = __this->get__videoWidth0_26();
		int32_t L_56 = __this->get__videoHeight0_27();
		ColorU5BU5D_t672350442* L_57 = __this->get__videoColorArray0_32();
		NullCheck(L_54);
		Texture2D_SetPixels_m2034528253(L_54, 0, 0, L_55, L_56, L_57, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_58 = __this->get__videoTexture1_54();
		int32_t L_59 = __this->get__videoWidth1_46();
		int32_t L_60 = __this->get__videoHeight1_47();
		ColorU5BU5D_t672350442* L_61 = __this->get__videoColorArray1_52();
		NullCheck(L_58);
		Texture2D_SetPixels_m2034528253(L_58, 0, 0, L_59, L_60, L_61, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_62 = __this->get__videoTexture0_34();
		NullCheck(L_62);
		Texture2D_Apply_m3971781551(L_62, (bool)0, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_63 = __this->get__videoTexture1_54();
		NullCheck(L_63);
		Texture2D_Apply_m3971781551(L_63, (bool)0, /*hidden argument*/NULL);
	}

IL_0285:
	{
		goto IL_0294;
	}

IL_028a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral4257839027, /*hidden argument*/NULL);
	}

IL_0294:
	{
		return;
	}
}
// System.Boolean ARController::CreateClearCamera()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisCamera_t189460977_m2871093954_MethodInfo_var;
extern const uint32_t ARController_CreateClearCamera_m2815811798_MetadataUsageId;
extern "C"  bool ARController_CreateClearCamera_m2815811798 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_CreateClearCamera_m2815811798_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Camera_t189460977 * L_1 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_0, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var);
		__this->set_clearCamera_56(L_1);
		Camera_t189460977 * L_2 = __this->get_clearCamera_56();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0033;
		}
	}
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Camera_t189460977 * L_5 = GameObject_AddComponent_TisCamera_t189460977_m2871093954(L_4, /*hidden argument*/GameObject_AddComponent_TisCamera_t189460977_m2871093954_MethodInfo_var);
		__this->set_clearCamera_56(L_5);
	}

IL_0033:
	{
		Camera_t189460977 * L_6 = __this->get_clearCamera_56();
		NullCheck(L_6);
		Camera_set_depth_m1570376177(L_6, (0.0f), /*hidden argument*/NULL);
		Camera_t189460977 * L_7 = __this->get_clearCamera_56();
		NullCheck(L_7);
		Camera_set_cullingMask_m2396665826(L_7, 0, /*hidden argument*/NULL);
		Camera_t189460977 * L_8 = __this->get_clearCamera_56();
		NullCheck(L_8);
		Camera_set_clearFlags_m4142614199(L_8, 2, /*hidden argument*/NULL);
		bool L_9 = ARController_get_UseVideoBackground_m1201911505(__this, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_008f;
		}
	}
	{
		Camera_t189460977 * L_10 = __this->get_clearCamera_56();
		Color_t2020392075  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Color__ctor_m1909920690(&L_11, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_10);
		Camera_set_backgroundColor_m2927893592(L_10, L_11, /*hidden argument*/NULL);
		goto IL_00b3;
	}

IL_008f:
	{
		Camera_t189460977 * L_12 = __this->get_clearCamera_56();
		Color_t2020392075  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Color__ctor_m1909920690(&L_13, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		Camera_set_backgroundColor_m2927893592(L_12, L_13, /*hidden argument*/NULL);
	}

IL_00b3:
	{
		return (bool)1;
	}
}
// UnityEngine.GameObject ARController::CreateVideoBackgroundMesh(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[]&,UnityEngine.Color32[]&,UnityEngine.Texture2D&,UnityEngine.Material&)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Color32U5BU5D_t30278651_il2cpp_TypeInfo_var;
extern Il2CppClass* ColorU5BU5D_t672350442_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t193706927_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMeshFilter_t3026937449_m2144094854_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMeshRenderer_t1268241104_m740921633_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t257310565_m1312615893_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral831842661;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral2724778450;
extern Il2CppCodeGenString* _stringLiteral2467673021;
extern Il2CppCodeGenString* _stringLiteral3234307629;
extern Il2CppCodeGenString* _stringLiteral732608431;
extern Il2CppCodeGenString* _stringLiteral372029316;
extern Il2CppCodeGenString* _stringLiteral4268569290;
extern const uint32_t ARController_CreateVideoBackgroundMesh_m3804930034_MetadataUsageId;
extern "C"  GameObject_t1756533147 * ARController_CreateVideoBackgroundMesh_m3804930034 (ARController_t593870669 * __this, int32_t ___index0, int32_t ___w1, int32_t ___h2, int32_t ___layer3, ColorU5BU5D_t672350442** ___vbca4, Color32U5BU5D_t30278651** ___vbc32a5, Texture2D_t3542995729 ** ___vbt6, Material_t193706927 ** ___vbm7, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_CreateVideoBackgroundMesh_m3804930034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Color32U5BU5D_t30278651* V_5 = NULL;
	Color32_t874517518  V_6;
	memset(&V_6, 0, sizeof(V_6));
	int32_t V_7 = 0;
	Shader_t2430389951 * V_8 = NULL;
	MeshFilter_t3026937449 * V_9 = NULL;
	MeshRenderer_t1268241104 * V_10 = NULL;
	{
		int32_t L_0 = ___w1;
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___h2;
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0052;
		}
	}

IL_000e:
	{
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral831842661);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral831842661);
		ObjectU5BU5D_t3614634134* L_3 = L_2;
		int32_t L_4 = ___w1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		ObjectU5BU5D_t3614634134* L_7 = L_3;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral372029398);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029398);
		ObjectU5BU5D_t3614634134* L_8 = L_7;
		int32_t L_9 = ___h2;
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m3881798623(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		ColorU5BU5D_t672350442** L_13 = ___vbca4;
		*((Il2CppObject **)(L_13)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_13), (Il2CppObject *)NULL);
		Color32U5BU5D_t30278651** L_14 = ___vbc32a5;
		*((Il2CppObject **)(L_14)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_14), (Il2CppObject *)NULL);
		Texture2D_t3542995729 ** L_15 = ___vbt6;
		*((Il2CppObject **)(L_15)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_15), (Il2CppObject *)NULL);
		Material_t193706927 ** L_16 = ___vbm7;
		*((Il2CppObject **)(L_16)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_16), (Il2CppObject *)NULL);
		return (GameObject_t1756533147 *)NULL;
	}

IL_0052:
	{
		int32_t L_17 = ___index0;
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2724778450, L_19, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_21 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_21, L_20, /*hidden argument*/NULL);
		V_0 = L_21;
		GameObject_t1756533147 * L_22 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_23 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_22, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0090;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral2467673021, /*hidden argument*/NULL);
		ColorU5BU5D_t672350442** L_24 = ___vbca4;
		*((Il2CppObject **)(L_24)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_24), (Il2CppObject *)NULL);
		Color32U5BU5D_t30278651** L_25 = ___vbc32a5;
		*((Il2CppObject **)(L_25)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_25), (Il2CppObject *)NULL);
		Texture2D_t3542995729 ** L_26 = ___vbt6;
		*((Il2CppObject **)(L_26)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_26), (Il2CppObject *)NULL);
		Material_t193706927 ** L_27 = ___vbm7;
		*((Il2CppObject **)(L_27)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_27), (Il2CppObject *)NULL);
		return (GameObject_t1756533147 *)NULL;
	}

IL_0090:
	{
		GameObject_t1756533147 * L_28 = V_0;
		int32_t L_29 = ___layer3;
		NullCheck(L_28);
		GameObject_set_layer_m2712461877(L_28, L_29, /*hidden argument*/NULL);
		int32_t L_30 = ___w1;
		V_1 = L_30;
		int32_t L_31 = ___h2;
		V_2 = L_31;
		ObjectU5BU5D_t3614634134* L_32 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9)));
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, _stringLiteral3234307629);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3234307629);
		ObjectU5BU5D_t3614634134* L_33 = L_32;
		int32_t L_34 = ___w1;
		int32_t L_35 = L_34;
		Il2CppObject * L_36 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_35);
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_36);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_36);
		ObjectU5BU5D_t3614634134* L_37 = L_33;
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, _stringLiteral372029398);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029398);
		ObjectU5BU5D_t3614634134* L_38 = L_37;
		int32_t L_39 = ___h2;
		int32_t L_40 = L_39;
		Il2CppObject * L_41 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_40);
		NullCheck(L_38);
		ArrayElementTypeCheck (L_38, L_41);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_41);
		ObjectU5BU5D_t3614634134* L_42 = L_38;
		NullCheck(L_42);
		ArrayElementTypeCheck (L_42, _stringLiteral732608431);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral732608431);
		ObjectU5BU5D_t3614634134* L_43 = L_42;
		int32_t L_44 = V_1;
		int32_t L_45 = L_44;
		Il2CppObject * L_46 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_45);
		NullCheck(L_43);
		ArrayElementTypeCheck (L_43, L_46);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_46);
		ObjectU5BU5D_t3614634134* L_47 = L_43;
		NullCheck(L_47);
		ArrayElementTypeCheck (L_47, _stringLiteral372029398);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral372029398);
		ObjectU5BU5D_t3614634134* L_48 = L_47;
		int32_t L_49 = V_2;
		int32_t L_50 = L_49;
		Il2CppObject * L_51 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_48);
		ArrayElementTypeCheck (L_48, L_51);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_51);
		ObjectU5BU5D_t3614634134* L_52 = L_48;
		NullCheck(L_52);
		ArrayElementTypeCheck (L_52, _stringLiteral372029316);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral372029316);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_53 = String_Concat_m3881798623(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		int32_t L_54 = ___w1;
		int32_t L_55 = V_1;
		V_3 = ((float)((float)(((float)((float)L_54)))/(float)(((float)((float)L_55)))));
		int32_t L_56 = ___h2;
		int32_t L_57 = V_2;
		V_4 = ((float)((float)(((float)((float)L_56)))/(float)(((float)((float)L_57)))));
		bool L_58 = __this->get__useNativeGLTexturing_16();
		if (L_58)
		{
			goto IL_0144;
		}
	}
	{
		bool L_59 = __this->get__useColor32_17();
		if (!L_59)
		{
			goto IL_0130;
		}
	}
	{
		ColorU5BU5D_t672350442** L_60 = ___vbca4;
		*((Il2CppObject **)(L_60)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_60), (Il2CppObject *)NULL);
		Color32U5BU5D_t30278651** L_61 = ___vbc32a5;
		int32_t L_62 = ___w1;
		int32_t L_63 = ___h2;
		*((Il2CppObject **)(L_61)) = (Il2CppObject *)((Color32U5BU5D_t30278651*)SZArrayNew(Color32U5BU5D_t30278651_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_62*(int32_t)L_63))));
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_61), (Il2CppObject *)((Color32U5BU5D_t30278651*)SZArrayNew(Color32U5BU5D_t30278651_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_62*(int32_t)L_63)))));
		goto IL_013f;
	}

IL_0130:
	{
		ColorU5BU5D_t672350442** L_64 = ___vbca4;
		int32_t L_65 = ___w1;
		int32_t L_66 = ___h2;
		*((Il2CppObject **)(L_64)) = (Il2CppObject *)((ColorU5BU5D_t672350442*)SZArrayNew(ColorU5BU5D_t672350442_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_65*(int32_t)L_66))));
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_64), (Il2CppObject *)((ColorU5BU5D_t672350442*)SZArrayNew(ColorU5BU5D_t672350442_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_65*(int32_t)L_66)))));
		Color32U5BU5D_t30278651** L_67 = ___vbc32a5;
		*((Il2CppObject **)(L_67)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_67), (Il2CppObject *)NULL);
	}

IL_013f:
	{
		goto IL_014c;
	}

IL_0144:
	{
		ColorU5BU5D_t672350442** L_68 = ___vbca4;
		*((Il2CppObject **)(L_68)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_68), (Il2CppObject *)NULL);
		Color32U5BU5D_t30278651** L_69 = ___vbc32a5;
		*((Il2CppObject **)(L_69)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_69), (Il2CppObject *)NULL);
	}

IL_014c:
	{
		bool L_70 = __this->get__useNativeGLTexturing_16();
		if (L_70)
		{
			goto IL_0173;
		}
	}
	{
		bool L_71 = __this->get__useColor32_17();
		if (!L_71)
		{
			goto IL_0173;
		}
	}
	{
		Texture2D_t3542995729 ** L_72 = ___vbt6;
		int32_t L_73 = V_1;
		int32_t L_74 = V_2;
		Texture2D_t3542995729 * L_75 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_75, L_73, L_74, 5, (bool)0, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_72)) = (Il2CppObject *)L_75;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_72), (Il2CppObject *)L_75);
		goto IL_017f;
	}

IL_0173:
	{
		Texture2D_t3542995729 ** L_76 = ___vbt6;
		int32_t L_77 = V_1;
		int32_t L_78 = V_2;
		Texture2D_t3542995729 * L_79 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_79, L_77, L_78, 5, (bool)0, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_76)) = (Il2CppObject *)L_79;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_76), (Il2CppObject *)L_79);
	}

IL_017f:
	{
		Texture2D_t3542995729 ** L_80 = ___vbt6;
		NullCheck((*((Texture2D_t3542995729 **)L_80)));
		Object_set_hideFlags_m2204253440((*((Texture2D_t3542995729 **)L_80)), ((int32_t)61), /*hidden argument*/NULL);
		Texture2D_t3542995729 ** L_81 = ___vbt6;
		NullCheck((*((Texture2D_t3542995729 **)L_81)));
		Texture_set_filterMode_m3838996656((*((Texture2D_t3542995729 **)L_81)), 1, /*hidden argument*/NULL);
		Texture2D_t3542995729 ** L_82 = ___vbt6;
		NullCheck((*((Texture2D_t3542995729 **)L_82)));
		Texture_set_wrapMode_m333956747((*((Texture2D_t3542995729 **)L_82)), 1, /*hidden argument*/NULL);
		Texture2D_t3542995729 ** L_83 = ___vbt6;
		NullCheck((*((Texture2D_t3542995729 **)L_83)));
		Texture_set_anisoLevel_m4242988344((*((Texture2D_t3542995729 **)L_83)), 0, /*hidden argument*/NULL);
		int32_t L_84 = V_1;
		int32_t L_85 = V_2;
		V_5 = ((Color32U5BU5D_t30278651*)SZArrayNew(Color32U5BU5D_t30278651_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_84*(int32_t)L_85))));
		Color32__ctor_m1932627809((&V_6), 0, 0, 0, ((int32_t)255), /*hidden argument*/NULL);
		V_7 = 0;
		goto IL_01db;
	}

IL_01c5:
	{
		Color32U5BU5D_t30278651* L_86 = V_5;
		int32_t L_87 = V_7;
		NullCheck(L_86);
		Color32_t874517518  L_88 = V_6;
		(*(Color32_t874517518 *)((L_86)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_87)))) = L_88;
		int32_t L_89 = V_7;
		V_7 = ((int32_t)((int32_t)L_89+(int32_t)1));
	}

IL_01db:
	{
		int32_t L_90 = V_7;
		Color32U5BU5D_t30278651* L_91 = V_5;
		NullCheck(L_91);
		if ((((int32_t)L_90) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_91)->max_length)))))))
		{
			goto IL_01c5;
		}
	}
	{
		Texture2D_t3542995729 ** L_92 = ___vbt6;
		Color32U5BU5D_t30278651* L_93 = V_5;
		NullCheck((*((Texture2D_t3542995729 **)L_92)));
		Texture2D_SetPixels32_m2480505405((*((Texture2D_t3542995729 **)L_92)), L_93, /*hidden argument*/NULL);
		Texture2D_t3542995729 ** L_94 = ___vbt6;
		NullCheck((*((Texture2D_t3542995729 **)L_94)));
		Texture2D_Apply_m3543341930((*((Texture2D_t3542995729 **)L_94)), /*hidden argument*/NULL);
		V_5 = (Color32U5BU5D_t30278651*)NULL;
		Shader_t2430389951 * L_95 = Shader_Find_m4179408078(NULL /*static, unused*/, _stringLiteral4268569290, /*hidden argument*/NULL);
		V_8 = L_95;
		Material_t193706927 ** L_96 = ___vbm7;
		Shader_t2430389951 * L_97 = V_8;
		Material_t193706927 * L_98 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1897560860(L_98, L_97, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_96)) = (Il2CppObject *)L_98;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_96), (Il2CppObject *)L_98);
		Material_t193706927 ** L_99 = ___vbm7;
		NullCheck((*((Material_t193706927 **)L_99)));
		Object_set_hideFlags_m2204253440((*((Material_t193706927 **)L_99)), ((int32_t)61), /*hidden argument*/NULL);
		Material_t193706927 ** L_100 = ___vbm7;
		Texture2D_t3542995729 ** L_101 = ___vbt6;
		NullCheck((*((Material_t193706927 **)L_100)));
		Material_set_mainTexture_m3584203343((*((Material_t193706927 **)L_100)), (*((Texture2D_t3542995729 **)L_101)), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_102 = V_0;
		NullCheck(L_102);
		MeshFilter_t3026937449 * L_103 = GameObject_AddComponent_TisMeshFilter_t3026937449_m2144094854(L_102, /*hidden argument*/GameObject_AddComponent_TisMeshFilter_t3026937449_m2144094854_MethodInfo_var);
		V_9 = L_103;
		MeshFilter_t3026937449 * L_104 = V_9;
		bool L_105 = __this->get_ContentFlipH_64();
		bool L_106 = __this->get_ContentFlipV_65();
		float L_107 = V_3;
		float L_108 = V_4;
		Mesh_t1356156583 * L_109 = ARController_newVideoMesh_m1266137681(__this, L_105, (bool)((((int32_t)L_106) == ((int32_t)0))? 1 : 0), L_107, L_108, /*hidden argument*/NULL);
		NullCheck(L_104);
		MeshFilter_set_mesh_m3839924176(L_104, L_109, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_110 = V_0;
		NullCheck(L_110);
		MeshRenderer_t1268241104 * L_111 = GameObject_AddComponent_TisMeshRenderer_t1268241104_m740921633(L_110, /*hidden argument*/GameObject_AddComponent_TisMeshRenderer_t1268241104_m740921633_MethodInfo_var);
		V_10 = L_111;
		MeshRenderer_t1268241104 * L_112 = V_10;
		NullCheck(L_112);
		Renderer_set_castShadows_m3636153210(L_112, (bool)0, /*hidden argument*/NULL);
		MeshRenderer_t1268241104 * L_113 = V_10;
		NullCheck(L_113);
		Renderer_set_receiveShadows_m2366149940(L_113, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_114 = V_0;
		NullCheck(L_114);
		Renderer_t257310565 * L_115 = GameObject_GetComponent_TisRenderer_t257310565_m1312615893(L_114, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m1312615893_MethodInfo_var);
		Material_t193706927 ** L_116 = ___vbm7;
		NullCheck(L_115);
		Renderer_set_material_m1053097112(L_115, (*((Material_t193706927 **)L_116)), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_117 = V_0;
		return L_117;
	}
}
// UnityEngine.GameObject ARController::CreateVideoBackgroundCamera(System.String,System.Int32,UnityEngine.Camera&)
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisCamera_t189460977_m2871093954_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2467673021;
extern Il2CppCodeGenString* _stringLiteral3227500512;
extern const uint32_t ARController_CreateVideoBackgroundCamera_m781252442_MetadataUsageId;
extern "C"  GameObject_t1756533147 * ARController_CreateVideoBackgroundCamera_m781252442 (ARController_t593870669 * __this, String_t* ___name0, int32_t ___layer1, Camera_t189460977 ** ___vbc2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_CreateVideoBackgroundCamera_m781252442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		GameObject_t1756533147 * L_1 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1756533147 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral2467673021, /*hidden argument*/NULL);
		Camera_t189460977 ** L_4 = ___vbc2;
		*((Il2CppObject **)(L_4)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_4), (Il2CppObject *)NULL);
		return (GameObject_t1756533147 *)NULL;
	}

IL_0022:
	{
		Camera_t189460977 ** L_5 = ___vbc2;
		GameObject_t1756533147 * L_6 = V_0;
		NullCheck(L_6);
		Camera_t189460977 * L_7 = GameObject_AddComponent_TisCamera_t189460977_m2871093954(L_6, /*hidden argument*/GameObject_AddComponent_TisCamera_t189460977_m2871093954_MethodInfo_var);
		*((Il2CppObject **)(L_5)) = (Il2CppObject *)L_7;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_5), (Il2CppObject *)L_7);
		Camera_t189460977 ** L_8 = ___vbc2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (*((Camera_t189460977 **)L_8)), (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral3227500512, /*hidden argument*/NULL);
		return (GameObject_t1756533147 *)NULL;
	}

IL_0043:
	{
		Camera_t189460977 ** L_10 = ___vbc2;
		NullCheck((*((Camera_t189460977 **)L_10)));
		Camera_set_orthographic_m2132888580((*((Camera_t189460977 **)L_10)), (bool)1, /*hidden argument*/NULL);
		Camera_t189460977 ** L_11 = ___vbc2;
		Matrix4x4_t2933234003  L_12 = Matrix4x4_get_identity_m3039560904(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((*((Camera_t189460977 **)L_11)));
		Camera_set_projectionMatrix_m2059836755((*((Camera_t189460977 **)L_11)), L_12, /*hidden argument*/NULL);
		bool L_13 = __this->get_ContentRotate90_63();
		if (!L_13)
		{
			goto IL_0093;
		}
	}
	{
		Camera_t189460977 ** L_14 = ___vbc2;
		Vector3_t2243707580  L_15 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = Vector3_get_back_m4246539215(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_17 = Quaternion_AngleAxis_m2806222563(NULL /*static, unused*/, (90.0f), L_16, /*hidden argument*/NULL);
		Vector3_t2243707580  L_18 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_19 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_15, L_17, L_18, /*hidden argument*/NULL);
		Camera_t189460977 ** L_20 = ___vbc2;
		NullCheck((*((Camera_t189460977 **)L_20)));
		Matrix4x4_t2933234003  L_21 = Camera_get_projectionMatrix_m2365994324((*((Camera_t189460977 **)L_20)), /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_22 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_19, L_21, /*hidden argument*/NULL);
		NullCheck((*((Camera_t189460977 **)L_14)));
		Camera_set_projectionMatrix_m2059836755((*((Camera_t189460977 **)L_14)), L_22, /*hidden argument*/NULL);
	}

IL_0093:
	{
		Camera_t189460977 ** L_23 = ___vbc2;
		Matrix4x4_t2933234003  L_24 = Matrix4x4_Ortho_m462958343(NULL /*static, unused*/, (-1.0f), (1.0f), (-1.0f), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Camera_t189460977 ** L_25 = ___vbc2;
		NullCheck((*((Camera_t189460977 **)L_25)));
		Matrix4x4_t2933234003  L_26 = Camera_get_projectionMatrix_m2365994324((*((Camera_t189460977 **)L_25)), /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_27 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_24, L_26, /*hidden argument*/NULL);
		NullCheck((*((Camera_t189460977 **)L_23)));
		Camera_set_projectionMatrix_m2059836755((*((Camera_t189460977 **)L_23)), L_27, /*hidden argument*/NULL);
		Camera_t189460977 ** L_28 = ___vbc2;
		NullCheck((*((Camera_t189460977 **)L_28)));
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695((*((Camera_t189460977 **)L_28)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Vector3__ctor_m2638739322(&L_30, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_set_position_m2469242620(L_29, L_30, /*hidden argument*/NULL);
		Camera_t189460977 ** L_31 = ___vbc2;
		NullCheck((*((Camera_t189460977 **)L_31)));
		Transform_t3275118058 * L_32 = Component_get_transform_m2697483695((*((Camera_t189460977 **)L_31)), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Quaternion__ctor_m3196903881(&L_33, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_32);
		Transform_set_rotation_m3411284563(L_32, L_33, /*hidden argument*/NULL);
		Camera_t189460977 ** L_34 = ___vbc2;
		NullCheck((*((Camera_t189460977 **)L_34)));
		Transform_t3275118058 * L_35 = Component_get_transform_m2697483695((*((Camera_t189460977 **)L_34)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Vector3__ctor_m2638739322(&L_36, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_set_localScale_m2325460848(L_35, L_36, /*hidden argument*/NULL);
		Camera_t189460977 ** L_37 = ___vbc2;
		NullCheck((*((Camera_t189460977 **)L_37)));
		Camera_set_clearFlags_m4142614199((*((Camera_t189460977 **)L_37)), 4, /*hidden argument*/NULL);
		Camera_t189460977 ** L_38 = ___vbc2;
		int32_t L_39 = ___layer1;
		NullCheck((*((Camera_t189460977 **)L_38)));
		Camera_set_cullingMask_m2396665826((*((Camera_t189460977 **)L_38)), ((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_39&(int32_t)((int32_t)31))))), /*hidden argument*/NULL);
		Camera_t189460977 ** L_40 = ___vbc2;
		NullCheck((*((Camera_t189460977 **)L_40)));
		Camera_set_depth_m1570376177((*((Camera_t189460977 **)L_40)), (1.0f), /*hidden argument*/NULL);
		Camera_t189460977 ** L_41 = ___vbc2;
		bool L_42 = ARController_get_UseVideoBackground_m1201911505(__this, /*hidden argument*/NULL);
		NullCheck((*((Camera_t189460977 **)L_41)));
		Behaviour_set_enabled_m1796096907((*((Camera_t189460977 **)L_41)), L_42, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_43 = V_0;
		return L_43;
	}
}
// System.Void ARController::DestroyVideoBackground()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ARController_DestroyVideoBackground_m152274941_MetadataUsageId;
extern "C"  void ARController_DestroyVideoBackground_m152274941 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_DestroyVideoBackground_m152274941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		__this->set__videoBackgroundCamera0_58((Camera_t189460977 *)NULL);
		__this->set__videoBackgroundCamera1_60((Camera_t189460977 *)NULL);
		GameObject_t1756533147 * L_1 = __this->get__videoBackgroundCameraGO0_57();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004d;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		GameObject_t1756533147 * L_4 = __this->get__videoBackgroundCameraGO0_57();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_0046;
	}

IL_003b:
	{
		GameObject_t1756533147 * L_5 = __this->get__videoBackgroundCameraGO0_57();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0046:
	{
		__this->set__videoBackgroundCameraGO0_57((GameObject_t1756533147 *)NULL);
	}

IL_004d:
	{
		GameObject_t1756533147 * L_6 = __this->get__videoBackgroundCameraGO1_59();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0086;
		}
	}
	{
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0074;
		}
	}
	{
		GameObject_t1756533147 * L_9 = __this->get__videoBackgroundCameraGO1_59();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_0074:
	{
		GameObject_t1756533147 * L_10 = __this->get__videoBackgroundCameraGO1_59();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_007f:
	{
		__this->set__videoBackgroundCameraGO1_59((GameObject_t1756533147 *)NULL);
	}

IL_0086:
	{
		Material_t193706927 * L_11 = __this->get__videoMaterial0_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_11, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00bf;
		}
	}
	{
		bool L_13 = V_0;
		if (!L_13)
		{
			goto IL_00ad;
		}
	}
	{
		Material_t193706927 * L_14 = __this->get__videoMaterial0_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		goto IL_00b8;
	}

IL_00ad:
	{
		Material_t193706927 * L_15 = __this->get__videoMaterial0_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		__this->set__videoMaterial0_35((Material_t193706927 *)NULL);
	}

IL_00bf:
	{
		Material_t193706927 * L_16 = __this->get__videoMaterial1_55();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_16, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00f8;
		}
	}
	{
		bool L_18 = V_0;
		if (!L_18)
		{
			goto IL_00e6;
		}
	}
	{
		Material_t193706927 * L_19 = __this->get__videoMaterial1_55();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		goto IL_00f1;
	}

IL_00e6:
	{
		Material_t193706927 * L_20 = __this->get__videoMaterial1_55();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
	}

IL_00f1:
	{
		__this->set__videoMaterial1_55((Material_t193706927 *)NULL);
	}

IL_00f8:
	{
		Texture2D_t3542995729 * L_21 = __this->get__videoTexture0_34();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0131;
		}
	}
	{
		bool L_23 = V_0;
		if (!L_23)
		{
			goto IL_011f;
		}
	}
	{
		Texture2D_t3542995729 * L_24 = __this->get__videoTexture0_34();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		goto IL_012a;
	}

IL_011f:
	{
		Texture2D_t3542995729 * L_25 = __this->get__videoTexture0_34();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
	}

IL_012a:
	{
		__this->set__videoTexture0_34((Texture2D_t3542995729 *)NULL);
	}

IL_0131:
	{
		Texture2D_t3542995729 * L_26 = __this->get__videoTexture1_54();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_27 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_26, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_016a;
		}
	}
	{
		bool L_28 = V_0;
		if (!L_28)
		{
			goto IL_0158;
		}
	}
	{
		Texture2D_t3542995729 * L_29 = __this->get__videoTexture1_54();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		goto IL_0163;
	}

IL_0158:
	{
		Texture2D_t3542995729 * L_30 = __this->get__videoTexture1_54();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
	}

IL_0163:
	{
		__this->set__videoTexture1_54((Texture2D_t3542995729 *)NULL);
	}

IL_016a:
	{
		ColorU5BU5D_t672350442* L_31 = __this->get__videoColorArray0_32();
		if (!L_31)
		{
			goto IL_017c;
		}
	}
	{
		__this->set__videoColorArray0_32((ColorU5BU5D_t672350442*)NULL);
	}

IL_017c:
	{
		ColorU5BU5D_t672350442* L_32 = __this->get__videoColorArray1_52();
		if (!L_32)
		{
			goto IL_018e;
		}
	}
	{
		__this->set__videoColorArray1_52((ColorU5BU5D_t672350442*)NULL);
	}

IL_018e:
	{
		Color32U5BU5D_t30278651* L_33 = __this->get__videoColor32Array0_33();
		if (!L_33)
		{
			goto IL_01a0;
		}
	}
	{
		__this->set__videoColor32Array0_33((Color32U5BU5D_t30278651*)NULL);
	}

IL_01a0:
	{
		Color32U5BU5D_t30278651* L_34 = __this->get__videoColor32Array1_53();
		if (!L_34)
		{
			goto IL_01b2;
		}
	}
	{
		__this->set__videoColor32Array1_53((Color32U5BU5D_t30278651*)NULL);
	}

IL_01b2:
	{
		GameObject_t1756533147 * L_35 = __this->get__videoBackgroundMeshGO0_31();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_36 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_35, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_01eb;
		}
	}
	{
		bool L_37 = V_0;
		if (!L_37)
		{
			goto IL_01d9;
		}
	}
	{
		GameObject_t1756533147 * L_38 = __this->get__videoBackgroundMeshGO0_31();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		goto IL_01e4;
	}

IL_01d9:
	{
		GameObject_t1756533147 * L_39 = __this->get__videoBackgroundMeshGO0_31();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
	}

IL_01e4:
	{
		__this->set__videoBackgroundMeshGO0_31((GameObject_t1756533147 *)NULL);
	}

IL_01eb:
	{
		GameObject_t1756533147 * L_40 = __this->get__videoBackgroundMeshGO1_51();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_41 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_40, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0224;
		}
	}
	{
		bool L_42 = V_0;
		if (!L_42)
		{
			goto IL_0212;
		}
	}
	{
		GameObject_t1756533147 * L_43 = __this->get__videoBackgroundMeshGO1_51();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		goto IL_021d;
	}

IL_0212:
	{
		GameObject_t1756533147 * L_44 = __this->get__videoBackgroundMeshGO1_51();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
	}

IL_021d:
	{
		__this->set__videoBackgroundMeshGO1_51((GameObject_t1756533147 *)NULL);
	}

IL_0224:
	{
		Resources_UnloadUnusedAssets_m2770025609(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ARController::DestroyClearCamera()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ARController_DestroyClearCamera_m113337958_MetadataUsageId;
extern "C"  bool ARController_DestroyClearCamera_m113337958 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_DestroyClearCamera_m113337958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = __this->get_clearCamera_56();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		__this->set_clearCamera_56((Camera_t189460977 *)NULL);
	}

IL_0018:
	{
		return (bool)1;
	}
}
// UnityEngine.Rect ARController::getViewport(System.Int32,System.Int32,System.Boolean,ARCamera/ViewEye)
extern "C"  Rect_t3681755626  ARController_getViewport_m115664776 (ARController_t593870669 * __this, int32_t ___contentWidth0, int32_t ___contentHeight1, bool ___stereo2, int32_t ___viewEye3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	int32_t G_B10_0 = 0;
	int32_t G_B13_0 = 0;
	{
		int32_t L_0 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = ___stereo2;
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_3 = V_0;
		V_4 = ((int32_t)((int32_t)L_3/(int32_t)2));
		int32_t L_4 = V_1;
		V_5 = L_4;
		int32_t L_5 = ___viewEye3;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_0029;
		}
	}
	{
		V_2 = 0;
		goto IL_002d;
	}

IL_0029:
	{
		int32_t L_6 = V_0;
		V_2 = ((int32_t)((int32_t)L_6/(int32_t)2));
	}

IL_002d:
	{
		V_3 = 0;
		goto IL_019e;
	}

IL_0034:
	{
		int32_t L_7 = ARController_get_ContentMode_m862413810(__this, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_8 = V_0;
		V_4 = L_8;
		int32_t L_9 = V_1;
		V_5 = L_9;
		goto IL_00e0;
	}

IL_004a:
	{
		bool L_10 = __this->get_ContentRotate90_63();
		if (!L_10)
		{
			goto IL_005b;
		}
	}
	{
		int32_t L_11 = ___contentHeight1;
		G_B10_0 = L_11;
		goto IL_005c;
	}

IL_005b:
	{
		int32_t L_12 = ___contentWidth0;
		G_B10_0 = L_12;
	}

IL_005c:
	{
		V_6 = G_B10_0;
		bool L_13 = __this->get_ContentRotate90_63();
		if (!L_13)
		{
			goto IL_006f;
		}
	}
	{
		int32_t L_14 = ___contentWidth0;
		G_B13_0 = L_14;
		goto IL_0070;
	}

IL_006f:
	{
		int32_t L_15 = ___contentHeight1;
		G_B13_0 = L_15;
	}

IL_0070:
	{
		V_7 = G_B13_0;
		int32_t L_16 = ARController_get_ContentMode_m862413810(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_16) == ((int32_t)1)))
		{
			goto IL_008a;
		}
	}
	{
		int32_t L_17 = ARController_get_ContentMode_m862413810(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_17) == ((uint32_t)2))))
		{
			goto IL_00d8;
		}
	}

IL_008a:
	{
		int32_t L_18 = V_0;
		int32_t L_19 = V_6;
		V_8 = ((float)((float)(((float)((float)L_18)))/(float)(((float)((float)L_19)))));
		int32_t L_20 = V_1;
		int32_t L_21 = V_7;
		V_9 = ((float)((float)(((float)((float)L_20)))/(float)(((float)((float)L_21)))));
		int32_t L_22 = ARController_get_ContentMode_m862413810(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)2))))
		{
			goto IL_00b6;
		}
	}
	{
		float L_23 = V_9;
		float L_24 = V_8;
		float L_25 = Math_Max_m3360711905(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		V_10 = L_25;
		goto IL_00c1;
	}

IL_00b6:
	{
		float L_26 = V_9;
		float L_27 = V_8;
		float L_28 = Math_Min_m105057611(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		V_10 = L_28;
	}

IL_00c1:
	{
		int32_t L_29 = V_6;
		float L_30 = V_10;
		V_4 = (((int32_t)((int32_t)((float)((float)(((float)((float)L_29)))*(float)L_30)))));
		int32_t L_31 = V_7;
		float L_32 = V_10;
		V_5 = (((int32_t)((int32_t)((float)((float)(((float)((float)L_31)))*(float)L_32)))));
		goto IL_00e0;
	}

IL_00d8:
	{
		int32_t L_33 = V_6;
		V_4 = L_33;
		int32_t L_34 = V_7;
		V_5 = L_34;
	}

IL_00e0:
	{
		int32_t L_35 = __this->get_ContentAlign_66();
		if (!L_35)
		{
			goto IL_0103;
		}
	}
	{
		int32_t L_36 = __this->get_ContentAlign_66();
		if ((((int32_t)L_36) == ((int32_t)3)))
		{
			goto IL_0103;
		}
	}
	{
		int32_t L_37 = __this->get_ContentAlign_66();
		if ((!(((uint32_t)L_37) == ((uint32_t)6))))
		{
			goto IL_010a;
		}
	}

IL_0103:
	{
		V_2 = 0;
		goto IL_013f;
	}

IL_010a:
	{
		int32_t L_38 = __this->get_ContentAlign_66();
		if ((((int32_t)L_38) == ((int32_t)2)))
		{
			goto IL_012e;
		}
	}
	{
		int32_t L_39 = __this->get_ContentAlign_66();
		if ((((int32_t)L_39) == ((int32_t)5)))
		{
			goto IL_012e;
		}
	}
	{
		int32_t L_40 = __this->get_ContentAlign_66();
		if ((!(((uint32_t)L_40) == ((uint32_t)8))))
		{
			goto IL_0138;
		}
	}

IL_012e:
	{
		int32_t L_41 = V_0;
		int32_t L_42 = V_4;
		V_2 = ((int32_t)((int32_t)L_41-(int32_t)L_42));
		goto IL_013f;
	}

IL_0138:
	{
		int32_t L_43 = V_0;
		int32_t L_44 = V_4;
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)L_43-(int32_t)L_44))/(int32_t)2));
	}

IL_013f:
	{
		int32_t L_45 = __this->get_ContentAlign_66();
		if ((((int32_t)L_45) == ((int32_t)6)))
		{
			goto IL_0163;
		}
	}
	{
		int32_t L_46 = __this->get_ContentAlign_66();
		if ((((int32_t)L_46) == ((int32_t)7)))
		{
			goto IL_0163;
		}
	}
	{
		int32_t L_47 = __this->get_ContentAlign_66();
		if ((!(((uint32_t)L_47) == ((uint32_t)8))))
		{
			goto IL_016a;
		}
	}

IL_0163:
	{
		V_3 = 0;
		goto IL_019e;
	}

IL_016a:
	{
		int32_t L_48 = __this->get_ContentAlign_66();
		if (!L_48)
		{
			goto IL_018d;
		}
	}
	{
		int32_t L_49 = __this->get_ContentAlign_66();
		if ((((int32_t)L_49) == ((int32_t)1)))
		{
			goto IL_018d;
		}
	}
	{
		int32_t L_50 = __this->get_ContentAlign_66();
		if ((!(((uint32_t)L_50) == ((uint32_t)2))))
		{
			goto IL_0197;
		}
	}

IL_018d:
	{
		int32_t L_51 = V_1;
		int32_t L_52 = V_5;
		V_3 = ((int32_t)((int32_t)L_51-(int32_t)L_52));
		goto IL_019e;
	}

IL_0197:
	{
		int32_t L_53 = V_1;
		int32_t L_54 = V_5;
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)L_53-(int32_t)L_54))/(int32_t)2));
	}

IL_019e:
	{
		int32_t L_55 = V_2;
		int32_t L_56 = V_3;
		int32_t L_57 = V_4;
		int32_t L_58 = V_5;
		Rect_t3681755626  L_59;
		memset(&L_59, 0, sizeof(L_59));
		Rect__ctor_m1220545469(&L_59, (((float)((float)L_55))), (((float)((float)L_56))), (((float)((float)L_57))), (((float)((float)L_58))), /*hidden argument*/NULL);
		return L_59;
	}
}
// System.Void ARController::CycleContentMode()
extern "C"  void ARController_CycleContentMode_m220124596 (ARController_t593870669 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ARController_get_ContentMode_m862413810(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0013;
		}
	}
	{
		goto IL_001f;
	}

IL_0013:
	{
		ARController_set_ContentMode_m1004281933(__this, 0, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_001f:
	{
		ARController_set_ContentMode_m1004281933(__this, 1, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_002b:
	{
		return;
	}
}
// System.Boolean ARController::ConfigureForegroundCameras()
extern const Il2CppType* ARCamera_t431610428_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ARCameraU5BU5D_t1286580565_il2cpp_TypeInfo_var;
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2714561458;
extern const uint32_t ARController_ConfigureForegroundCameras_m229175717_MetadataUsageId;
extern "C"  bool ARController_ConfigureForegroundCameras_m229175717 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_ConfigureForegroundCameras_m229175717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	ARCameraU5BU5D_t1286580565* V_1 = NULL;
	ARCamera_t431610428 * V_2 = NULL;
	ARCameraU5BU5D_t1286580565* V_3 = NULL;
	int32_t V_4 = 0;
	bool V_5 = false;
	float G_B7_0 = 0.0f;
	float G_B7_1 = 0.0f;
	ARCamera_t431610428 * G_B7_2 = NULL;
	float G_B6_0 = 0.0f;
	float G_B6_1 = 0.0f;
	ARCamera_t431610428 * G_B6_2 = NULL;
	Matrix4x4_t2933234003  G_B8_0;
	memset(&G_B8_0, 0, sizeof(G_B8_0));
	float G_B8_1 = 0.0f;
	float G_B8_2 = 0.0f;
	ARCamera_t431610428 * G_B8_3 = NULL;
	{
		V_0 = (bool)0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ARCamera_t431610428_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_1 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_1 = ((ARCameraU5BU5D_t1286580565*)IsInst(L_1, ARCameraU5BU5D_t1286580565_il2cpp_TypeInfo_var));
		ARCameraU5BU5D_t1286580565* L_2 = V_1;
		V_3 = L_2;
		V_4 = 0;
		goto IL_00c8;
	}

IL_0021:
	{
		ARCameraU5BU5D_t1286580565* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		ARCamera_t431610428 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		ARCamera_t431610428 * L_7 = V_2;
		NullCheck(L_7);
		bool L_8 = L_7->get_Stereo_11();
		if (L_8)
		{
			goto IL_0052;
		}
	}
	{
		ARCamera_t431610428 * L_9 = V_2;
		float L_10 = __this->get_NearPlane_61();
		float L_11 = __this->get_FarPlane_62();
		Matrix4x4_t2933234003  L_12 = __this->get__videoProjectionMatrix0_30();
		NullCheck(L_9);
		bool L_13 = ARCamera_SetupCamera_m2259267036(L_9, L_10, L_11, L_12, (&V_0), /*hidden argument*/NULL);
		V_5 = L_13;
		goto IL_00b1;
	}

IL_0052:
	{
		ARCamera_t431610428 * L_14 = V_2;
		NullCheck(L_14);
		int32_t L_15 = L_14->get_StereoEye_12();
		if ((!(((uint32_t)L_15) == ((uint32_t)1))))
		{
			goto IL_007f;
		}
	}
	{
		ARCamera_t431610428 * L_16 = V_2;
		float L_17 = __this->get_NearPlane_61();
		float L_18 = __this->get_FarPlane_62();
		Matrix4x4_t2933234003  L_19 = __this->get__videoProjectionMatrix0_30();
		NullCheck(L_16);
		bool L_20 = ARCamera_SetupCamera_m2259267036(L_16, L_17, L_18, L_19, (&V_0), /*hidden argument*/NULL);
		V_5 = L_20;
		goto IL_00b1;
	}

IL_007f:
	{
		ARCamera_t431610428 * L_21 = V_2;
		float L_22 = __this->get_NearPlane_61();
		float L_23 = __this->get_FarPlane_62();
		bool L_24 = __this->get_VideoIsStereo_36();
		G_B6_0 = L_23;
		G_B6_1 = L_22;
		G_B6_2 = L_21;
		if (!L_24)
		{
			G_B7_0 = L_23;
			G_B7_1 = L_22;
			G_B7_2 = L_21;
			goto IL_00a2;
		}
	}
	{
		Matrix4x4_t2933234003  L_25 = __this->get__videoProjectionMatrix1_50();
		G_B8_0 = L_25;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		goto IL_00a8;
	}

IL_00a2:
	{
		Matrix4x4_t2933234003  L_26 = __this->get__videoProjectionMatrix0_30();
		G_B8_0 = L_26;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
	}

IL_00a8:
	{
		NullCheck(G_B8_3);
		bool L_27 = ARCamera_SetupCamera_m2259267036(G_B8_3, G_B8_2, G_B8_1, G_B8_0, (&V_0), /*hidden argument*/NULL);
		V_5 = L_27;
	}

IL_00b1:
	{
		bool L_28 = V_5;
		if (L_28)
		{
			goto IL_00c2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral2714561458, /*hidden argument*/NULL);
	}

IL_00c2:
	{
		int32_t L_29 = V_4;
		V_4 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_00c8:
	{
		int32_t L_30 = V_4;
		ARCameraU5BU5D_t1286580565* L_31 = V_3;
		NullCheck(L_31);
		if ((((int32_t)L_30) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_31)->max_length)))))))
		{
			goto IL_0021;
		}
	}
	{
		bool L_32 = V_0;
		ARController_set_UseVideoBackground_m3884950314(__this, (bool)((((int32_t)L_32) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean ARController::ConfigureViewports()
extern const Il2CppType* ARCamera_t431610428_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ARCameraU5BU5D_t1286580565_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var;
extern const uint32_t ARController_ConfigureViewports_m2051600675_MetadataUsageId;
extern "C"  bool ARController_ConfigureViewports_m2051600675 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_ConfigureViewports_m2051600675_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	ARCameraU5BU5D_t1286580565* V_1 = NULL;
	ARCamera_t431610428 * V_2 = NULL;
	ARCameraU5BU5D_t1286580565* V_3 = NULL;
	int32_t V_4 = 0;
	ARController_t593870669 * G_B7_0 = NULL;
	Camera_t189460977 * G_B7_1 = NULL;
	ARController_t593870669 * G_B6_0 = NULL;
	Camera_t189460977 * G_B6_1 = NULL;
	int32_t G_B8_0 = 0;
	ARController_t593870669 * G_B8_1 = NULL;
	Camera_t189460977 * G_B8_2 = NULL;
	int32_t G_B10_0 = 0;
	ARController_t593870669 * G_B10_1 = NULL;
	Camera_t189460977 * G_B10_2 = NULL;
	int32_t G_B9_0 = 0;
	ARController_t593870669 * G_B9_1 = NULL;
	Camera_t189460977 * G_B9_2 = NULL;
	int32_t G_B11_0 = 0;
	int32_t G_B11_1 = 0;
	ARController_t593870669 * G_B11_2 = NULL;
	Camera_t189460977 * G_B11_3 = NULL;
	ARController_t593870669 * G_B18_0 = NULL;
	Camera_t189460977 * G_B18_1 = NULL;
	ARController_t593870669 * G_B17_0 = NULL;
	Camera_t189460977 * G_B17_1 = NULL;
	int32_t G_B19_0 = 0;
	ARController_t593870669 * G_B19_1 = NULL;
	Camera_t189460977 * G_B19_2 = NULL;
	int32_t G_B21_0 = 0;
	ARController_t593870669 * G_B21_1 = NULL;
	Camera_t189460977 * G_B21_2 = NULL;
	int32_t G_B20_0 = 0;
	ARController_t593870669 * G_B20_1 = NULL;
	Camera_t189460977 * G_B20_2 = NULL;
	int32_t G_B22_0 = 0;
	int32_t G_B22_1 = 0;
	ARController_t593870669 * G_B22_2 = NULL;
	Camera_t189460977 * G_B22_3 = NULL;
	{
		V_0 = (bool)0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ARCamera_t431610428_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_1 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_1 = ((ARCameraU5BU5D_t1286580565*)IsInst(L_1, ARCameraU5BU5D_t1286580565_il2cpp_TypeInfo_var));
		ARCameraU5BU5D_t1286580565* L_2 = V_1;
		V_3 = L_2;
		V_4 = 0;
		goto IL_00e7;
	}

IL_0021:
	{
		ARCameraU5BU5D_t1286580565* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		ARCamera_t431610428 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		ARCamera_t431610428 * L_7 = V_2;
		NullCheck(L_7);
		bool L_8 = L_7->get_Stereo_11();
		if (L_8)
		{
			goto IL_005a;
		}
	}
	{
		ARCamera_t431610428 * L_9 = V_2;
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Camera_t189460977 * L_11 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_10, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var);
		int32_t L_12 = __this->get__videoWidth0_26();
		int32_t L_13 = __this->get__videoHeight0_27();
		Rect_t3681755626  L_14 = ARController_getViewport_m115664776(__this, L_12, L_13, (bool)0, 1, /*hidden argument*/NULL);
		NullCheck(L_11);
		Camera_set_pixelRect_m1366013782(L_11, L_14, /*hidden argument*/NULL);
		goto IL_00e1;
	}

IL_005a:
	{
		V_0 = (bool)1;
		ARCamera_t431610428 * L_15 = V_2;
		NullCheck(L_15);
		int32_t L_16 = L_15->get_StereoEye_12();
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			goto IL_0091;
		}
	}
	{
		ARCamera_t431610428 * L_17 = V_2;
		NullCheck(L_17);
		GameObject_t1756533147 * L_18 = Component_get_gameObject_m3105766835(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Camera_t189460977 * L_19 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_18, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var);
		int32_t L_20 = __this->get__videoWidth0_26();
		int32_t L_21 = __this->get__videoHeight0_27();
		Rect_t3681755626  L_22 = ARController_getViewport_m115664776(__this, L_20, L_21, (bool)1, 1, /*hidden argument*/NULL);
		NullCheck(L_19);
		Camera_set_pixelRect_m1366013782(L_19, L_22, /*hidden argument*/NULL);
		goto IL_00e1;
	}

IL_0091:
	{
		ARCamera_t431610428 * L_23 = V_2;
		NullCheck(L_23);
		GameObject_t1756533147 * L_24 = Component_get_gameObject_m3105766835(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Camera_t189460977 * L_25 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_24, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var);
		bool L_26 = __this->get_VideoIsStereo_36();
		G_B6_0 = __this;
		G_B6_1 = L_25;
		if (!L_26)
		{
			G_B7_0 = __this;
			G_B7_1 = L_25;
			goto IL_00b3;
		}
	}
	{
		int32_t L_27 = __this->get__videoWidth1_46();
		G_B8_0 = L_27;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		goto IL_00b9;
	}

IL_00b3:
	{
		int32_t L_28 = __this->get__videoWidth0_26();
		G_B8_0 = L_28;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
	}

IL_00b9:
	{
		bool L_29 = __this->get_VideoIsStereo_36();
		G_B9_0 = G_B8_0;
		G_B9_1 = G_B8_1;
		G_B9_2 = G_B8_2;
		if (!L_29)
		{
			G_B10_0 = G_B8_0;
			G_B10_1 = G_B8_1;
			G_B10_2 = G_B8_2;
			goto IL_00cf;
		}
	}
	{
		int32_t L_30 = __this->get__videoHeight1_47();
		G_B11_0 = L_30;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		goto IL_00d5;
	}

IL_00cf:
	{
		int32_t L_31 = __this->get__videoHeight0_27();
		G_B11_0 = L_31;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
	}

IL_00d5:
	{
		NullCheck(G_B11_2);
		Rect_t3681755626  L_32 = ARController_getViewport_m115664776(G_B11_2, G_B11_1, G_B11_0, (bool)1, 2, /*hidden argument*/NULL);
		NullCheck(G_B11_3);
		Camera_set_pixelRect_m1366013782(G_B11_3, L_32, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		int32_t L_33 = V_4;
		V_4 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00e7:
	{
		int32_t L_34 = V_4;
		ARCameraU5BU5D_t1286580565* L_35 = V_3;
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length)))))))
		{
			goto IL_0021;
		}
	}
	{
		bool L_36 = V_0;
		if (L_36)
		{
			goto IL_011b;
		}
	}
	{
		Camera_t189460977 * L_37 = __this->get__videoBackgroundCamera0_58();
		int32_t L_38 = __this->get__videoWidth0_26();
		int32_t L_39 = __this->get__videoHeight0_27();
		Rect_t3681755626  L_40 = ARController_getViewport_m115664776(__this, L_38, L_39, (bool)0, 1, /*hidden argument*/NULL);
		NullCheck(L_37);
		Camera_set_pixelRect_m1366013782(L_37, L_40, /*hidden argument*/NULL);
		goto IL_0185;
	}

IL_011b:
	{
		Camera_t189460977 * L_41 = __this->get__videoBackgroundCamera0_58();
		int32_t L_42 = __this->get__videoWidth0_26();
		int32_t L_43 = __this->get__videoHeight0_27();
		Rect_t3681755626  L_44 = ARController_getViewport_m115664776(__this, L_42, L_43, (bool)1, 1, /*hidden argument*/NULL);
		NullCheck(L_41);
		Camera_set_pixelRect_m1366013782(L_41, L_44, /*hidden argument*/NULL);
		Camera_t189460977 * L_45 = __this->get__videoBackgroundCamera1_60();
		bool L_46 = __this->get_VideoIsStereo_36();
		G_B17_0 = __this;
		G_B17_1 = L_45;
		if (!L_46)
		{
			G_B18_0 = __this;
			G_B18_1 = L_45;
			goto IL_0157;
		}
	}
	{
		int32_t L_47 = __this->get__videoWidth1_46();
		G_B19_0 = L_47;
		G_B19_1 = G_B17_0;
		G_B19_2 = G_B17_1;
		goto IL_015d;
	}

IL_0157:
	{
		int32_t L_48 = __this->get__videoWidth0_26();
		G_B19_0 = L_48;
		G_B19_1 = G_B18_0;
		G_B19_2 = G_B18_1;
	}

IL_015d:
	{
		bool L_49 = __this->get_VideoIsStereo_36();
		G_B20_0 = G_B19_0;
		G_B20_1 = G_B19_1;
		G_B20_2 = G_B19_2;
		if (!L_49)
		{
			G_B21_0 = G_B19_0;
			G_B21_1 = G_B19_1;
			G_B21_2 = G_B19_2;
			goto IL_0173;
		}
	}
	{
		int32_t L_50 = __this->get__videoHeight1_47();
		G_B22_0 = L_50;
		G_B22_1 = G_B20_0;
		G_B22_2 = G_B20_1;
		G_B22_3 = G_B20_2;
		goto IL_0179;
	}

IL_0173:
	{
		int32_t L_51 = __this->get__videoHeight0_27();
		G_B22_0 = L_51;
		G_B22_1 = G_B21_0;
		G_B22_2 = G_B21_1;
		G_B22_3 = G_B21_2;
	}

IL_0179:
	{
		NullCheck(G_B22_2);
		Rect_t3681755626  L_52 = ARController_getViewport_m115664776(G_B22_2, G_B22_1, G_B22_0, (bool)1, 2, /*hidden argument*/NULL);
		NullCheck(G_B22_3);
		Camera_set_pixelRect_m1366013782(G_B22_3, L_52, /*hidden argument*/NULL);
	}

IL_0185:
	{
		return (bool)1;
	}
}
// UnityEngine.Mesh ARController::newVideoMesh(System.Boolean,System.Boolean,System.Single,System.Single)
extern Il2CppClass* Mesh_t1356156583_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305143____U24fieldU2D7CDC43E92004F6C21BFB0E695E5C42C85155527E_0_FieldInfo_var;
extern const uint32_t ARController_newVideoMesh_m1266137681_MetadataUsageId;
extern "C"  Mesh_t1356156583 * ARController_newVideoMesh_m1266137681 (ARController_t593870669 * __this, bool ___flipX0, bool ___flipY1, float ___textureScaleU2, float ___textureScaleV3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_newVideoMesh_m1266137681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Mesh_t1356156583 * V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B12_0 = 0.0f;
	{
		Mesh_t1356156583 * L_0 = (Mesh_t1356156583 *)il2cpp_codegen_object_new(Mesh_t1356156583_il2cpp_TypeInfo_var);
		Mesh__ctor_m2975981674(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Mesh_t1356156583 * L_1 = V_0;
		NullCheck(L_1);
		Mesh_Clear_m231813403(L_1, /*hidden argument*/NULL);
		V_1 = (1.0f);
		Mesh_t1356156583 * L_2 = V_0;
		Vector3U5BU5D_t1172311765* L_3 = ((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_3);
		float L_4 = V_1;
		float L_5 = V_1;
		Vector3_t2243707580  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2638739322(&L_6, ((-L_4)), ((-L_5)), (0.5f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_6;
		Vector3U5BU5D_t1172311765* L_7 = L_3;
		NullCheck(L_7);
		float L_8 = V_1;
		float L_9 = V_1;
		Vector3_t2243707580  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2638739322(&L_10, L_8, ((-L_9)), (0.5f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_10;
		Vector3U5BU5D_t1172311765* L_11 = L_7;
		NullCheck(L_11);
		float L_12 = V_1;
		float L_13 = V_1;
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, L_12, L_13, (0.5f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_14;
		Vector3U5BU5D_t1172311765* L_15 = L_11;
		NullCheck(L_15);
		float L_16 = V_1;
		float L_17 = V_1;
		Vector3_t2243707580  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2638739322(&L_18, ((-L_16)), L_17, (0.5f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_18;
		NullCheck(L_2);
		Mesh_set_vertices_m2936804213(L_2, L_15, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_19 = V_0;
		Vector3U5BU5D_t1172311765* L_20 = ((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_20);
		Vector3_t2243707580  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m2638739322(&L_21, (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_21;
		Vector3U5BU5D_t1172311765* L_22 = L_20;
		NullCheck(L_22);
		Vector3_t2243707580  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector3__ctor_m2638739322(&L_23, (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_23;
		Vector3U5BU5D_t1172311765* L_24 = L_22;
		NullCheck(L_24);
		Vector3_t2243707580  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector3__ctor_m2638739322(&L_25, (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_25;
		Vector3U5BU5D_t1172311765* L_26 = L_24;
		NullCheck(L_26);
		Vector3_t2243707580  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector3__ctor_m2638739322(&L_27, (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_27;
		NullCheck(L_19);
		Mesh_set_normals_m2674236682(L_19, L_26, /*hidden argument*/NULL);
		bool L_28 = ___flipX0;
		if (!L_28)
		{
			goto IL_011a;
		}
	}
	{
		float L_29 = ___textureScaleU2;
		G_B3_0 = L_29;
		goto IL_011f;
	}

IL_011a:
	{
		G_B3_0 = (0.0f);
	}

IL_011f:
	{
		V_2 = G_B3_0;
		bool L_30 = ___flipX0;
		if (!L_30)
		{
			goto IL_0130;
		}
	}
	{
		G_B6_0 = (0.0f);
		goto IL_0131;
	}

IL_0130:
	{
		float L_31 = ___textureScaleU2;
		G_B6_0 = L_31;
	}

IL_0131:
	{
		V_3 = G_B6_0;
		bool L_32 = ___flipY1;
		if (!L_32)
		{
			goto IL_013f;
		}
	}
	{
		float L_33 = ___textureScaleV3;
		G_B9_0 = L_33;
		goto IL_0144;
	}

IL_013f:
	{
		G_B9_0 = (0.0f);
	}

IL_0144:
	{
		V_4 = G_B9_0;
		bool L_34 = ___flipY1;
		if (!L_34)
		{
			goto IL_0156;
		}
	}
	{
		G_B12_0 = (0.0f);
		goto IL_0158;
	}

IL_0156:
	{
		float L_35 = ___textureScaleV3;
		G_B12_0 = L_35;
	}

IL_0158:
	{
		V_5 = G_B12_0;
		Mesh_t1356156583 * L_36 = V_0;
		Vector2U5BU5D_t686124026* L_37 = ((Vector2U5BU5D_t686124026*)SZArrayNew(Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_37);
		float L_38 = V_2;
		float L_39 = V_4;
		Vector2_t2243707579  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Vector2__ctor_m3067419446(&L_40, L_38, L_39, /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)((L_37)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_40;
		Vector2U5BU5D_t686124026* L_41 = L_37;
		NullCheck(L_41);
		float L_42 = V_3;
		float L_43 = V_4;
		Vector2_t2243707579  L_44;
		memset(&L_44, 0, sizeof(L_44));
		Vector2__ctor_m3067419446(&L_44, L_42, L_43, /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_44;
		Vector2U5BU5D_t686124026* L_45 = L_41;
		NullCheck(L_45);
		float L_46 = V_3;
		float L_47 = V_5;
		Vector2_t2243707579  L_48;
		memset(&L_48, 0, sizeof(L_48));
		Vector2__ctor_m3067419446(&L_48, L_46, L_47, /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_48;
		Vector2U5BU5D_t686124026* L_49 = L_45;
		NullCheck(L_49);
		float L_50 = V_2;
		float L_51 = V_5;
		Vector2_t2243707579  L_52;
		memset(&L_52, 0, sizeof(L_52));
		Vector2__ctor_m3067419446(&L_52, L_50, L_51, /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)((L_49)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_52;
		NullCheck(L_36);
		Mesh_set_uv_m1497318906(L_36, L_49, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_53 = V_0;
		Int32U5BU5D_t3030399641* L_54 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)6));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_54, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305143____U24fieldU2D7CDC43E92004F6C21BFB0E695E5C42C85155527E_0_FieldInfo_var), /*hidden argument*/NULL);
		NullCheck(L_53);
		Mesh_set_triangles_m3244966865(L_53, L_54, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_55 = V_0;
		return L_55;
	}
}
// System.Void ARController::Log(System.String)
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m1040667509_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m780127360_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m338810690_MethodInfo_var;
extern const uint32_t ARController_Log_m1311826870_MetadataUsageId;
extern "C"  void ARController_Log_m1311826870 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_Log_m1311826870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		List_1_t1398341365 * L_0 = ((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->get_logMessages_4();
		String_t* L_1 = ___msg0;
		NullCheck(L_0);
		List_1_Add_m4061286785(L_0, L_1, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		goto IL_001b;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		List_1_t1398341365 * L_2 = ((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->get_logMessages_4();
		NullCheck(L_2);
		List_1_RemoveAt_m1040667509(L_2, 0, /*hidden argument*/List_1_RemoveAt_m1040667509_MethodInfo_var);
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		List_1_t1398341365 * L_3 = ((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->get_logMessages_4();
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m780127360(L_3, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		if ((((int32_t)L_4) > ((int32_t)((int32_t)1000))))
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		Action_1_t1831019615 * L_5 = ARController_get_logCallback_m620268047(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0049;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		Action_1_t1831019615 * L_6 = ARController_get_logCallback_m620268047(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = ___msg0;
		NullCheck(L_6);
		Action_1_Invoke_m338810690(L_6, L_7, /*hidden argument*/Action_1_Invoke_m338810690_MethodInfo_var);
		goto IL_004f;
	}

IL_0049:
	{
		String_t* L_8 = ___msg0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_004f:
	{
		return;
	}
}
// System.Void ARController::CalculateFPS()
extern "C"  void ARController_CalculateFPS_m1910495181 (ARController_t593870669 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_timeCounter_69();
		float L_1 = __this->get_refreshTime_71();
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_0036;
		}
	}
	{
		float L_2 = __this->get_timeCounter_69();
		float L_3 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timeCounter_69(((float)((float)L_2+(float)L_3)));
		int32_t L_4 = __this->get_frameCounter_68();
		__this->set_frameCounter_68(((int32_t)((int32_t)L_4+(int32_t)1)));
		goto IL_005c;
	}

IL_0036:
	{
		int32_t L_5 = __this->get_frameCounter_68();
		float L_6 = __this->get_timeCounter_69();
		__this->set_lastFramerate_70(((float)((float)(((float)((float)L_5)))/(float)L_6)));
		__this->set_frameCounter_68(0);
		__this->set_timeCounter_69((0.0f));
	}

IL_005c:
	{
		return;
	}
}
// System.Void ARController::SetupGUI()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern const uint32_t ARController_SetupGUI_m2678073940_MetadataUsageId;
extern "C"  void ARController_SetupGUI_m2678073940 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_SetupGUI_m2678073940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIStyleU5BU5D_t2497716199* L_0 = __this->get_style_86();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_1 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_t1799908754 * L_2 = GUISkin_get_label_m2703078986(L_1, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_3 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2210993436(L_3, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUIStyle_t1799908754 *)L_3);
		GUIStyleU5BU5D_t2497716199* L_4 = __this->get_style_86();
		NullCheck(L_4);
		int32_t L_5 = 0;
		GUIStyle_t1799908754 * L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		GUIStyleState_t3801000545 * L_7 = GUIStyle_get_normal_m2789468942(L_6, /*hidden argument*/NULL);
		Color_t2020392075  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m1909920690(&L_8, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		GUIStyleState_set_textColor_m3970174237(L_7, L_8, /*hidden argument*/NULL);
		GUIStyleU5BU5D_t2497716199* L_9 = __this->get_style_86();
		GUISkin_t1436893342 * L_10 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		GUIStyle_t1799908754 * L_11 = GUISkin_get_label_m2703078986(L_10, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_12 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2210993436(L_12, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUIStyle_t1799908754 *)L_12);
		GUIStyleU5BU5D_t2497716199* L_13 = __this->get_style_86();
		NullCheck(L_13);
		int32_t L_14 = 1;
		GUIStyle_t1799908754 * L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_15);
		GUIStyleState_t3801000545 * L_16 = GUIStyle_get_normal_m2789468942(L_15, /*hidden argument*/NULL);
		Color_t2020392075  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Color__ctor_m1909920690(&L_17, (0.0f), (0.5f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_16);
		GUIStyleState_set_textColor_m3970174237(L_16, L_17, /*hidden argument*/NULL);
		GUIStyleU5BU5D_t2497716199* L_18 = __this->get_style_86();
		GUISkin_t1436893342 * L_19 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		GUIStyle_t1799908754 * L_20 = GUISkin_get_label_m2703078986(L_19, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_21 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2210993436(L_21, L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(2), (GUIStyle_t1799908754 *)L_21);
		GUIStyleU5BU5D_t2497716199* L_22 = __this->get_style_86();
		NullCheck(L_22);
		int32_t L_23 = 2;
		GUIStyle_t1799908754 * L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_24);
		GUIStyleState_t3801000545 * L_25 = GUIStyle_get_normal_m2789468942(L_24, /*hidden argument*/NULL);
		Color_t2020392075  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Color__ctor_m1909920690(&L_26, (0.5f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_25);
		GUIStyleState_set_textColor_m3970174237(L_25, L_26, /*hidden argument*/NULL);
		__this->set_guiSetup_87((bool)1);
		return;
	}
}
// System.Void ARController::OnGUI()
extern Il2CppClass* WindowFunction_t3486805455_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* ARToolKitThresholdMode_t2161580787_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const MethodInfo* ARController_DrawErrorDialog_m2233680885_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m3547152182_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral22442200;
extern Il2CppCodeGenString* _stringLiteral2792113374;
extern Il2CppCodeGenString* _stringLiteral1502599712;
extern Il2CppCodeGenString* _stringLiteral2159913612;
extern Il2CppCodeGenString* _stringLiteral984013445;
extern Il2CppCodeGenString* _stringLiteral2018101074;
extern Il2CppCodeGenString* _stringLiteral2619141926;
extern Il2CppCodeGenString* _stringLiteral1884668475;
extern const uint32_t ARController_OnGUI_m3978310686_MetadataUsageId;
extern "C"  void ARController_OnGUI_m3978310686 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_OnGUI_m3978310686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	{
		bool L_0 = __this->get_guiSetup_87();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ARController_SetupGUI_m2678073940(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		bool L_1 = __this->get_showGUIErrorDialog_88();
		if (!L_1)
		{
			goto IL_00af;
		}
	}
	{
		Rect_t3681755626  L_2 = __this->get_showGUIErrorDialogWinRect_90();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)ARController_DrawErrorDialog_m2233680885_MethodInfo_var);
		WindowFunction_t3486805455 * L_4 = (WindowFunction_t3486805455 *)il2cpp_codegen_object_new(WindowFunction_t3486805455_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m977095815(L_4, __this, L_3, /*hidden argument*/NULL);
		Rect_t3681755626  L_5 = GUILayout_Window_m478317676(NULL /*static, unused*/, 0, L_2, L_4, _stringLiteral22442200, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		__this->set_showGUIErrorDialogWinRect_90(L_5);
		Rect_t3681755626 * L_6 = __this->get_address_of_showGUIErrorDialogWinRect_90();
		int32_t L_7 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626 * L_8 = __this->get_address_of_showGUIErrorDialogWinRect_90();
		float L_9 = Rect_get_width_m1138015702(L_8, /*hidden argument*/NULL);
		Rect_set_x_m3783700513(L_6, ((float)((float)((float)((float)(((float)((float)L_7)))-(float)L_9))*(float)(0.5f))), /*hidden argument*/NULL);
		Rect_t3681755626 * L_10 = __this->get_address_of_showGUIErrorDialogWinRect_90();
		int32_t L_11 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626 * L_12 = __this->get_address_of_showGUIErrorDialogWinRect_90();
		float L_13 = Rect_get_height_m3128694305(L_12, /*hidden argument*/NULL);
		Rect_set_y_m4294916608(L_10, ((float)((float)((float)((float)(((float)((float)L_11)))-(float)L_13))*(float)(0.5f))), /*hidden argument*/NULL);
		Rect_t3681755626  L_14 = __this->get_showGUIErrorDialogWinRect_90();
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)ARController_DrawErrorDialog_m2233680885_MethodInfo_var);
		WindowFunction_t3486805455 * L_16 = (WindowFunction_t3486805455 *)il2cpp_codegen_object_new(WindowFunction_t3486805455_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m977095815(L_16, __this, L_15, /*hidden argument*/NULL);
		GUILayout_Window_m478317676(NULL /*static, unused*/, 0, L_14, L_16, _stringLiteral22442200, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_00af:
	{
		bool L_17 = __this->get_showGUIDebug_91();
		if (!L_17)
		{
			goto IL_02fc;
		}
	}
	{
		Rect_t3681755626  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Rect__ctor_m1220545469(&L_18, (570.0f), (250.0f), (150.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_19 = GUI_Button_m3054448581(NULL /*static, unused*/, L_18, _stringLiteral2792113374, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f1;
		}
	}
	{
		bool L_20 = __this->get_showGUIDebugInfo_92();
		__this->set_showGUIDebugInfo_92((bool)((((int32_t)L_20) == ((int32_t)0))? 1 : 0));
	}

IL_00f1:
	{
		bool L_21 = __this->get_showGUIDebugInfo_92();
		if (!L_21)
		{
			goto IL_0102;
		}
	}
	{
		ARController_DrawInfoGUI_m2085452537(__this, /*hidden argument*/NULL);
	}

IL_0102:
	{
		Rect_t3681755626  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Rect__ctor_m1220545469(&L_22, (570.0f), (320.0f), (150.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_23 = GUI_Button_m3054448581(NULL /*static, unused*/, L_22, _stringLiteral1502599712, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0139;
		}
	}
	{
		bool L_24 = __this->get_showGUIDebugLogConsole_93();
		__this->set_showGUIDebugLogConsole_93((bool)((((int32_t)L_24) == ((int32_t)0))? 1 : 0));
	}

IL_0139:
	{
		bool L_25 = __this->get_showGUIDebugLogConsole_93();
		if (!L_25)
		{
			goto IL_014a;
		}
	}
	{
		ARController_DrawLogConsole_m2350776051(__this, /*hidden argument*/NULL);
	}

IL_014a:
	{
		Rect_t3681755626  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Rect__ctor_m1220545469(&L_26, (570.0f), (390.0f), (150.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		Dictionary_2_t3757846578 * L_27 = ((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->get_ContentModeNames_67();
		int32_t L_28 = ARController_get_ContentMode_m862413810(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		String_t* L_29 = Dictionary_2_get_Item_m3547152182(L_27, L_28, /*hidden argument*/Dictionary_2_get_Item_m3547152182_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2159913612, L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_31 = GUI_Button_m3054448581(NULL /*static, unused*/, L_26, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_018d;
		}
	}
	{
		ARController_CycleContentMode_m220124596(__this, /*hidden argument*/NULL);
	}

IL_018d:
	{
		Rect_t3681755626  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Rect__ctor_m1220545469(&L_32, (400.0f), (320.0f), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_33 = ARController_get_UseVideoBackground_m1201911505(__this, /*hidden argument*/NULL);
		bool L_34 = L_33;
		Il2CppObject * L_35 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_34);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral984013445, L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_37 = GUI_Button_m3054448581(NULL /*static, unused*/, L_32, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_01d4;
		}
	}
	{
		bool L_38 = ARController_get_UseVideoBackground_m1201911505(__this, /*hidden argument*/NULL);
		ARController_set_UseVideoBackground_m3884950314(__this, (bool)((((int32_t)L_38) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_01d4:
	{
		Rect_t3681755626  L_39;
		memset(&L_39, 0, sizeof(L_39));
		Rect__ctor_m1220545469(&L_39, (400.0f), (390.0f), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_40 = ARController_get_DebugVideo_m3800461305(__this, /*hidden argument*/NULL);
		bool L_41 = L_40;
		Il2CppObject * L_42 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2018101074, L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_44 = GUI_Button_m3054448581(NULL /*static, unused*/, L_39, L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_021b;
		}
	}
	{
		bool L_45 = ARController_get_DebugVideo_m3800461305(__this, /*hidden argument*/NULL);
		ARController_set_DebugVideo_m539547094(__this, (bool)((((int32_t)L_45) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_021b:
	{
		int32_t L_46 = ARController_get_VideoThresholdMode_m1767546950(__this, /*hidden argument*/NULL);
		V_0 = L_46;
		Rect_t3681755626  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Rect__ctor_m1220545469(&L_47, (400.0f), (460.0f), (320.0f), (25.0f), /*hidden argument*/NULL);
		int32_t L_48 = V_0;
		int32_t L_49 = L_48;
		Il2CppObject * L_50 = Box(ARToolKitThresholdMode_t2161580787_il2cpp_TypeInfo_var, &L_49);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_51 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2619141926, L_50, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2412846501(NULL /*static, unused*/, L_47, L_51, /*hidden argument*/NULL);
		int32_t L_52 = V_0;
		if (L_52)
		{
			goto IL_02c9;
		}
	}
	{
		int32_t L_53 = ARController_get_VideoThreshold_m208759273(__this, /*hidden argument*/NULL);
		V_1 = (((float)((float)L_53)));
		Rect_t3681755626  L_54;
		memset(&L_54, 0, sizeof(L_54));
		Rect__ctor_m1220545469(&L_54, (400.0f), (495.0f), (270.0f), (25.0f), /*hidden argument*/NULL);
		float L_55 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		float L_56 = GUI_HorizontalSlider_m2209330157(NULL /*static, unused*/, L_54, L_55, (0.0f), (255.0f), /*hidden argument*/NULL);
		V_2 = L_56;
		float L_57 = V_2;
		float L_58 = V_1;
		if ((((float)L_57) == ((float)L_58)))
		{
			goto IL_0297;
		}
	}
	{
		float L_59 = V_2;
		ARController_set_VideoThreshold_m2326990118(__this, (((int32_t)((int32_t)L_59))), /*hidden argument*/NULL);
	}

IL_0297:
	{
		Rect_t3681755626  L_60;
		memset(&L_60, 0, sizeof(L_60));
		Rect__ctor_m1220545469(&L_60, (680.0f), (495.0f), (50.0f), (25.0f), /*hidden argument*/NULL);
		int32_t L_61 = ARController_get_VideoThreshold_m208759273(__this, /*hidden argument*/NULL);
		V_3 = L_61;
		String_t* L_62 = Int32_ToString_m2960866144((&V_3), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2412846501(NULL /*static, unused*/, L_60, L_62, /*hidden argument*/NULL);
	}

IL_02c9:
	{
		Rect_t3681755626  L_63;
		memset(&L_63, 0, sizeof(L_63));
		Rect__ctor_m1220545469(&L_63, (700.0f), (20.0f), (100.0f), (25.0f), /*hidden argument*/NULL);
		float L_64 = __this->get_lastFramerate_70();
		float L_65 = L_64;
		Il2CppObject * L_66 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_65);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_67 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1884668475, L_66, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2412846501(NULL /*static, unused*/, L_63, L_67, /*hidden argument*/NULL);
	}

IL_02fc:
	{
		return;
	}
}
// System.Void ARController::DrawErrorDialog(System.Int32)
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2041362128;
extern const uint32_t ARController_DrawErrorDialog_m2233680885_MetadataUsageId;
extern "C"  void ARController_DrawErrorDialog_m2233680885 (ARController_t593870669 * __this, int32_t ___winID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_DrawErrorDialog_m2233680885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		String_t* L_0 = __this->get_showGUIErrorDialogContent_89();
		GUILayout_Label_m3466110979(NULL /*static, unused*/, L_0, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m2019304577(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral2041362128, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003d;
		}
	}
	{
		__this->set_showGUIErrorDialog_88((bool)0);
	}

IL_003d:
	{
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARController::DrawInfoGUI()
extern const Il2CppType* ARMarker_t1554260723_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ARMarkerU5BU5D_t3553995746_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1462269521;
extern Il2CppCodeGenString* _stringLiteral1004096485;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029406;
extern Il2CppCodeGenString* _stringLiteral1023885896;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern Il2CppCodeGenString* _stringLiteral2732467087;
extern Il2CppCodeGenString* _stringLiteral323081392;
extern Il2CppCodeGenString* _stringLiteral2086057683;
extern Il2CppCodeGenString* _stringLiteral3189642649;
extern Il2CppCodeGenString* _stringLiteral1708049723;
extern Il2CppCodeGenString* _stringLiteral3963676427;
extern Il2CppCodeGenString* _stringLiteral1564159062;
extern Il2CppCodeGenString* _stringLiteral24939496;
extern Il2CppCodeGenString* _stringLiteral3862154260;
extern Il2CppCodeGenString* _stringLiteral2955510294;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral3411282102;
extern const uint32_t ARController_DrawInfoGUI_m2085452537_MetadataUsageId;
extern "C"  void ARController_DrawInfoGUI_m2085452537 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_DrawInfoGUI_m2085452537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Resolution_t3693662728  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Resolution_t3693662728  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Resolution_t3693662728  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t3681755626  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t3681755626  V_6;
	memset(&V_6, 0, sizeof(V_6));
	int32_t V_7 = 0;
	ARMarkerU5BU5D_t3553995746* V_8 = NULL;
	ARMarker_t1554260723 * V_9 = NULL;
	ARMarkerU5BU5D_t3553995746* V_10 = NULL;
	int32_t V_11 = 0;
	{
		Rect_t3681755626  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Rect__ctor_m1220545469(&L_0, (10.0f), (10.0f), (500.0f), (25.0f), /*hidden argument*/NULL);
		String_t* L_1 = ARController_get_Version_m2150833958(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1462269521, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2412846501(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		Rect_t3681755626  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m1220545469(&L_3, (10.0f), (30.0f), (500.0f), (25.0f), /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_4 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9)));
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral1004096485);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1004096485);
		ObjectU5BU5D_t3614634134* L_5 = L_4;
		int32_t L_6 = __this->get__videoWidth0_26();
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_8);
		ObjectU5BU5D_t3614634134* L_9 = L_5;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral372029398);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029398);
		ObjectU5BU5D_t3614634134* L_10 = L_9;
		int32_t L_11 = __this->get__videoHeight0_27();
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_13);
		ObjectU5BU5D_t3614634134* L_14 = L_10;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral372029406);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029406);
		ObjectU5BU5D_t3614634134* L_15 = L_14;
		int32_t L_16 = __this->get__videoPixelSize0_28();
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_18);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_18);
		ObjectU5BU5D_t3614634134* L_19 = L_15;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral1023885896);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral1023885896);
		ObjectU5BU5D_t3614634134* L_20 = L_19;
		String_t* L_21 = __this->get__videoPixelFormatString0_29();
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_21);
		ObjectU5BU5D_t3614634134* L_22 = L_20;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral372029317);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral372029317);
		String_t* L_23 = String_Concat_m3881798623(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		GUI_Label_m2412846501(NULL /*static, unused*/, L_3, L_23, /*hidden argument*/NULL);
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, (10.0f), (90.0f), (500.0f), (25.0f), /*hidden argument*/NULL);
		String_t* L_25 = SystemInfo_get_graphicsDeviceName_m273312700(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_26 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2732467087, L_25, /*hidden argument*/NULL);
		GUI_Label_m2412846501(NULL /*static, unused*/, L_24, L_26, /*hidden argument*/NULL);
		Rect_t3681755626  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Rect__ctor_m1220545469(&L_27, (10.0f), (110.0f), (500.0f), (25.0f), /*hidden argument*/NULL);
		String_t* L_28 = SystemInfo_get_operatingSystem_m2575097876(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_29 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral323081392, L_28, /*hidden argument*/NULL);
		GUI_Label_m2412846501(NULL /*static, unused*/, L_27, L_29, /*hidden argument*/NULL);
		Rect_t3681755626  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Rect__ctor_m1220545469(&L_30, (10.0f), (130.0f), (500.0f), (25.0f), /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_31 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, _stringLiteral2086057683);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2086057683);
		ObjectU5BU5D_t3614634134* L_32 = L_31;
		int32_t L_33 = SystemInfo_get_processorCount_m2278036310(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_34 = L_33;
		Il2CppObject * L_35 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, L_35);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_35);
		ObjectU5BU5D_t3614634134* L_36 = L_32;
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, _stringLiteral3189642649);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3189642649);
		ObjectU5BU5D_t3614634134* L_37 = L_36;
		String_t* L_38 = SystemInfo_get_processorType_m2237532974(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_38);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_38);
		String_t* L_39 = String_Concat_m3881798623(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		GUI_Label_m2412846501(NULL /*static, unused*/, L_30, L_39, /*hidden argument*/NULL);
		Rect_t3681755626  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Rect__ctor_m1220545469(&L_40, (10.0f), (150.0f), (500.0f), (25.0f), /*hidden argument*/NULL);
		int32_t L_41 = SystemInfo_get_systemMemorySize_m2483631730(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_42 = L_41;
		Il2CppObject * L_43 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_42);
		String_t* L_44 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral1708049723, L_43, _stringLiteral3963676427, /*hidden argument*/NULL);
		GUI_Label_m2412846501(NULL /*static, unused*/, L_40, L_44, /*hidden argument*/NULL);
		Rect_t3681755626  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Rect__ctor_m1220545469(&L_45, (10.0f), (170.0f), (500.0f), (25.0f), /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_46 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_46);
		ArrayElementTypeCheck (L_46, _stringLiteral1564159062);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1564159062);
		ObjectU5BU5D_t3614634134* L_47 = L_46;
		Resolution_t3693662728  L_48 = Screen_get_currentResolution_m2361090437(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_48;
		int32_t L_49 = Resolution_get_width_m1438273472((&V_0), /*hidden argument*/NULL);
		int32_t L_50 = L_49;
		Il2CppObject * L_51 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_47);
		ArrayElementTypeCheck (L_47, L_51);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_51);
		ObjectU5BU5D_t3614634134* L_52 = L_47;
		NullCheck(L_52);
		ArrayElementTypeCheck (L_52, _stringLiteral372029398);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029398);
		ObjectU5BU5D_t3614634134* L_53 = L_52;
		Resolution_t3693662728  L_54 = Screen_get_currentResolution_m2361090437(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_54;
		int32_t L_55 = Resolution_get_height_m882683003((&V_1), /*hidden argument*/NULL);
		int32_t L_56 = L_55;
		Il2CppObject * L_57 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_56);
		NullCheck(L_53);
		ArrayElementTypeCheck (L_53, L_57);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_57);
		ObjectU5BU5D_t3614634134* L_58 = L_53;
		NullCheck(L_58);
		ArrayElementTypeCheck (L_58, _stringLiteral372029406);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029406);
		ObjectU5BU5D_t3614634134* L_59 = L_58;
		Resolution_t3693662728  L_60 = Screen_get_currentResolution_m2361090437(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_60;
		int32_t L_61 = Resolution_get_refreshRate_m1509667735((&V_2), /*hidden argument*/NULL);
		int32_t L_62 = L_61;
		Il2CppObject * L_63 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_59);
		ArrayElementTypeCheck (L_59, L_63);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_63);
		ObjectU5BU5D_t3614634134* L_64 = L_59;
		NullCheck(L_64);
		ArrayElementTypeCheck (L_64, _stringLiteral24939496);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral24939496);
		String_t* L_65 = String_Concat_m3881798623(NULL /*static, unused*/, L_64, /*hidden argument*/NULL);
		GUI_Label_m2412846501(NULL /*static, unused*/, L_45, L_65, /*hidden argument*/NULL);
		Rect_t3681755626  L_66;
		memset(&L_66, 0, sizeof(L_66));
		Rect__ctor_m1220545469(&L_66, (10.0f), (190.0f), (500.0f), (25.0f), /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_67 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_67);
		ArrayElementTypeCheck (L_67, _stringLiteral3862154260);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3862154260);
		ObjectU5BU5D_t3614634134* L_68 = L_67;
		int32_t L_69 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_70 = L_69;
		Il2CppObject * L_71 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_70);
		NullCheck(L_68);
		ArrayElementTypeCheck (L_68, L_71);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_71);
		ObjectU5BU5D_t3614634134* L_72 = L_68;
		NullCheck(L_72);
		ArrayElementTypeCheck (L_72, _stringLiteral372029398);
		(L_72)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029398);
		ObjectU5BU5D_t3614634134* L_73 = L_72;
		int32_t L_74 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_75 = L_74;
		Il2CppObject * L_76 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_75);
		NullCheck(L_73);
		ArrayElementTypeCheck (L_73, L_76);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_76);
		String_t* L_77 = String_Concat_m3881798623(NULL /*static, unused*/, L_73, /*hidden argument*/NULL);
		GUI_Label_m2412846501(NULL /*static, unused*/, L_66, L_77, /*hidden argument*/NULL);
		Rect_t3681755626  L_78;
		memset(&L_78, 0, sizeof(L_78));
		Rect__ctor_m1220545469(&L_78, (10.0f), (210.0f), (500.0f), (25.0f), /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_79 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_79);
		ArrayElementTypeCheck (L_79, _stringLiteral2955510294);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2955510294);
		ObjectU5BU5D_t3614634134* L_80 = L_79;
		Camera_t189460977 * L_81 = __this->get__videoBackgroundCamera0_58();
		NullCheck(L_81);
		Rect_t3681755626  L_82 = Camera_get_pixelRect_m2084185953(L_81, /*hidden argument*/NULL);
		V_3 = L_82;
		float L_83 = Rect_get_xMin_m1161102488((&V_3), /*hidden argument*/NULL);
		float L_84 = L_83;
		Il2CppObject * L_85 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_84);
		NullCheck(L_80);
		ArrayElementTypeCheck (L_80, L_85);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_85);
		ObjectU5BU5D_t3614634134* L_86 = L_80;
		NullCheck(L_86);
		ArrayElementTypeCheck (L_86, _stringLiteral372029314);
		(L_86)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029314);
		ObjectU5BU5D_t3614634134* L_87 = L_86;
		Camera_t189460977 * L_88 = __this->get__videoBackgroundCamera0_58();
		NullCheck(L_88);
		Rect_t3681755626  L_89 = Camera_get_pixelRect_m2084185953(L_88, /*hidden argument*/NULL);
		V_4 = L_89;
		float L_90 = Rect_get_yMin_m1161103577((&V_4), /*hidden argument*/NULL);
		float L_91 = L_90;
		Il2CppObject * L_92 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_91);
		NullCheck(L_87);
		ArrayElementTypeCheck (L_87, L_92);
		(L_87)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_92);
		ObjectU5BU5D_t3614634134* L_93 = L_87;
		NullCheck(L_93);
		ArrayElementTypeCheck (L_93, _stringLiteral811305474);
		(L_93)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral811305474);
		ObjectU5BU5D_t3614634134* L_94 = L_93;
		Camera_t189460977 * L_95 = __this->get__videoBackgroundCamera0_58();
		NullCheck(L_95);
		Rect_t3681755626  L_96 = Camera_get_pixelRect_m2084185953(L_95, /*hidden argument*/NULL);
		V_5 = L_96;
		float L_97 = Rect_get_xMax_m2915145014((&V_5), /*hidden argument*/NULL);
		float L_98 = L_97;
		Il2CppObject * L_99 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_98);
		NullCheck(L_94);
		ArrayElementTypeCheck (L_94, L_99);
		(L_94)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_99);
		ObjectU5BU5D_t3614634134* L_100 = L_94;
		NullCheck(L_100);
		ArrayElementTypeCheck (L_100, _stringLiteral811305474);
		(L_100)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral811305474);
		ObjectU5BU5D_t3614634134* L_101 = L_100;
		Camera_t189460977 * L_102 = __this->get__videoBackgroundCamera0_58();
		NullCheck(L_102);
		Rect_t3681755626  L_103 = Camera_get_pixelRect_m2084185953(L_102, /*hidden argument*/NULL);
		V_6 = L_103;
		float L_104 = Rect_get_yMax_m2915146103((&V_6), /*hidden argument*/NULL);
		float L_105 = L_104;
		Il2CppObject * L_106 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_105);
		NullCheck(L_101);
		ArrayElementTypeCheck (L_101, L_106);
		(L_101)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_106);
		String_t* L_107 = String_Concat_m3881798623(NULL /*static, unused*/, L_101, /*hidden argument*/NULL);
		GUI_Label_m2412846501(NULL /*static, unused*/, L_78, L_107, /*hidden argument*/NULL);
		V_7 = ((int32_t)350);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_108 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ARMarker_t1554260723_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_109 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_108, /*hidden argument*/NULL);
		V_8 = ((ARMarkerU5BU5D_t3553995746*)IsInst(L_109, ARMarkerU5BU5D_t3553995746_il2cpp_TypeInfo_var));
		ARMarkerU5BU5D_t3553995746* L_110 = V_8;
		V_10 = L_110;
		V_11 = 0;
		goto IL_03b7;
	}

IL_034e:
	{
		ARMarkerU5BU5D_t3553995746* L_111 = V_10;
		int32_t L_112 = V_11;
		NullCheck(L_111);
		int32_t L_113 = L_112;
		ARMarker_t1554260723 * L_114 = (L_111)->GetAt(static_cast<il2cpp_array_size_t>(L_113));
		V_9 = L_114;
		int32_t L_115 = V_7;
		Rect_t3681755626  L_116;
		memset(&L_116, 0, sizeof(L_116));
		Rect__ctor_m1220545469(&L_116, (10.0f), (((float)((float)L_115))), (500.0f), (25.0f), /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_117 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_117);
		ArrayElementTypeCheck (L_117, _stringLiteral3411282102);
		(L_117)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3411282102);
		ObjectU5BU5D_t3614634134* L_118 = L_117;
		ARMarker_t1554260723 * L_119 = V_9;
		NullCheck(L_119);
		int32_t L_120 = L_119->get_UID_5();
		int32_t L_121 = L_120;
		Il2CppObject * L_122 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_121);
		NullCheck(L_118);
		ArrayElementTypeCheck (L_118, L_122);
		(L_118)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_122);
		ObjectU5BU5D_t3614634134* L_123 = L_118;
		NullCheck(L_123);
		ArrayElementTypeCheck (L_123, _stringLiteral811305474);
		(L_123)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral811305474);
		ObjectU5BU5D_t3614634134* L_124 = L_123;
		ARMarker_t1554260723 * L_125 = V_9;
		NullCheck(L_125);
		bool L_126 = ARMarker_get_Visible_m4014965141(L_125, /*hidden argument*/NULL);
		bool L_127 = L_126;
		Il2CppObject * L_128 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_127);
		NullCheck(L_124);
		ArrayElementTypeCheck (L_124, L_128);
		(L_124)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_128);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_129 = String_Concat_m3881798623(NULL /*static, unused*/, L_124, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2412846501(NULL /*static, unused*/, L_116, L_129, /*hidden argument*/NULL);
		int32_t L_130 = V_7;
		V_7 = ((int32_t)((int32_t)L_130+(int32_t)((int32_t)25)));
		int32_t L_131 = V_11;
		V_11 = ((int32_t)((int32_t)L_131+(int32_t)1));
	}

IL_03b7:
	{
		int32_t L_132 = V_11;
		ARMarkerU5BU5D_t3553995746* L_133 = V_10;
		NullCheck(L_133);
		if ((((int32_t)L_132) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_133)->max_length)))))))
		{
			goto IL_034e;
		}
	}
	{
		return;
	}
}
// System.Void ARController::DrawLogConsole()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t ARController_DrawLogConsole_m2350776051_MetadataUsageId;
extern "C"  void ARController_DrawLogConsole_m2350776051 (ARController_t593870669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_DrawLogConsole_m2350776051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	GUIStyle_t1799908754 * V_1 = NULL;
	{
		int32_t L_0 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect__ctor_m1220545469((&V_0), (0.0f), (0.0f), (((float)((float)L_0))), (200.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_1 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_t1799908754 * L_2 = GUISkin_get_box_m533626169(L_1, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_3 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2210993436(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		GUIStyle_t1799908754 * L_4 = V_1;
		NullCheck(L_4);
		GUIStyleState_t3801000545 * L_5 = GUIStyle_get_normal_m2789468942(L_4, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_6 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_6, 1, 1, 5, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIStyleState_set_background_m3931679679(L_5, L_6, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_7 = V_1;
		NullCheck(L_7);
		GUIStyleState_t3801000545 * L_8 = GUIStyle_get_normal_m2789468942(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Texture2D_t3542995729 * L_9 = GUIStyleState_get_background_m502205442(L_8, /*hidden argument*/NULL);
		Color_t2020392075  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Color__ctor_m1909920690(&L_10, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Texture2D_SetPixel_m609991514(L_9, 0, 0, L_10, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_11 = V_1;
		NullCheck(L_11);
		GUIStyleState_t3801000545 * L_12 = GUIStyle_get_normal_m2789468942(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Texture2D_t3542995729 * L_13 = GUIStyleState_get_background_m502205442(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Texture2D_Apply_m3543341930(L_13, /*hidden argument*/NULL);
		Rect_t3681755626  L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_16 = V_1;
		GUI_Box_m2239404897(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/NULL);
		Rect_t3681755626  L_17 = V_0;
		ARController_DrawLogEntries_m2326935398(__this, L_17, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARController::DrawLogEntries(UnityEngine.Rect,System.Boolean)
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t4210063000_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t2321347278_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3799711356_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m804483696_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m870713862_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4175023932_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2205157096_MethodInfo_var;
extern const MethodInfo* Enumerable_Reverse_TisString_t_m4073804421_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3544696025;
extern Il2CppCodeGenString* _stringLiteral460283084;
extern const uint32_t ARController_DrawLogEntries_m2326935398_MetadataUsageId;
extern "C"  void ARController_DrawLogEntries_m2326935398 (ARController_t593870669 * __this, Rect_t3681755626  ___container0, bool ___reverse1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController_DrawLogEntries_m2326935398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	String_t* V_3 = NULL;
	Enumerator_t933071039  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Rect_t3681755626  V_6;
	memset(&V_6, 0, sizeof(V_6));
	float V_7 = 0.0f;
	Il2CppObject* V_8 = NULL;
	int32_t V_9 = 0;
	String_t* V_10 = NULL;
	Il2CppObject* V_11 = NULL;
	float V_12 = 0.0f;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		float L_0 = Rect_get_width_m1138015702((&___container0), /*hidden argument*/NULL);
		float L_1 = Rect_get_height_m3128694305((&___container0), /*hidden argument*/NULL);
		Rect__ctor_m1220545469((&V_0), (5.0f), (5.0f), ((float)((float)L_0-(float)(10.0f))), ((float)((float)L_1-(float)(10.0f))), /*hidden argument*/NULL);
		V_1 = (0.0f);
		float L_2 = Rect_get_width_m1138015702((&V_0), /*hidden argument*/NULL);
		V_2 = ((float)((float)L_2-(float)(30.0f)));
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		List_1_t1398341365 * L_3 = ((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->get_logMessages_4();
		NullCheck(L_3);
		Enumerator_t933071039  L_4 = List_1_GetEnumerator_m804483696(L_3, /*hidden argument*/List_1_GetEnumerator_m804483696_MethodInfo_var);
		V_4 = L_4;
	}

IL_004b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0075;
		}

IL_0050:
		{
			String_t* L_5 = Enumerator_get_Current_m870713862((&V_4), /*hidden argument*/Enumerator_get_Current_m870713862_MethodInfo_var);
			V_3 = L_5;
			IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
			GUISkin_t1436893342 * L_6 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_6);
			GUIStyle_t1799908754 * L_7 = GUISkin_get_label_m2703078986(L_6, /*hidden argument*/NULL);
			String_t* L_8 = V_3;
			GUIContent_t4210063000 * L_9 = (GUIContent_t4210063000 *)il2cpp_codegen_object_new(GUIContent_t4210063000_il2cpp_TypeInfo_var);
			GUIContent__ctor_m845353549(L_9, L_8, /*hidden argument*/NULL);
			float L_10 = V_2;
			NullCheck(L_7);
			float L_11 = GUIStyle_CalcHeight_m1685124037(L_7, L_9, L_10, /*hidden argument*/NULL);
			V_5 = L_11;
			float L_12 = V_1;
			float L_13 = V_5;
			V_1 = ((float)((float)L_12+(float)L_13));
		}

IL_0075:
		{
			bool L_14 = Enumerator_MoveNext_m4175023932((&V_4), /*hidden argument*/Enumerator_MoveNext_m4175023932_MethodInfo_var);
			if (L_14)
			{
				goto IL_0050;
			}
		}

IL_0081:
		{
			IL2CPP_LEAVE(0x94, FINALLY_0086);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0086;
	}

FINALLY_0086:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2205157096((&V_4), /*hidden argument*/Enumerator_Dispose_m2205157096_MethodInfo_var);
		IL2CPP_END_FINALLY(134)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(134)
	{
		IL2CPP_JUMP_TBL(0x94, IL_0094)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0094:
	{
		float L_15 = V_2;
		float L_16 = V_1;
		Rect__ctor_m1220545469((&V_6), (0.0f), (0.0f), L_15, L_16, /*hidden argument*/NULL);
		Rect_t3681755626  L_17 = V_0;
		Vector2_t2243707579  L_18 = __this->get_scrollPosition_94();
		Rect_t3681755626  L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_20 = GUI_BeginScrollView_m1724936596(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		__this->set_scrollPosition_94(L_20);
		V_7 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		List_1_t1398341365 * L_21 = ((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->get_logMessages_4();
		V_8 = (Il2CppObject*)L_21;
		bool L_22 = ___reverse1;
		if (!L_22)
		{
			goto IL_00d8;
		}
	}
	{
		Il2CppObject* L_23 = V_8;
		Il2CppObject* L_24 = Enumerable_Reverse_TisString_t_m4073804421(NULL /*static, unused*/, L_23, /*hidden argument*/Enumerable_Reverse_TisString_t_m4073804421_MethodInfo_var);
		V_8 = L_24;
	}

IL_00d8:
	{
		V_9 = 0;
		Il2CppObject* L_25 = V_8;
		NullCheck(L_25);
		Il2CppObject* L_26 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.String>::GetEnumerator() */, IEnumerable_1_t2321347278_il2cpp_TypeInfo_var, L_25);
		V_11 = L_26;
	}

IL_00e4:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0161;
		}

IL_00e9:
		{
			Il2CppObject* L_27 = V_11;
			NullCheck(L_27);
			String_t* L_28 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.String>::get_Current() */, IEnumerator_1_t3799711356_il2cpp_TypeInfo_var, L_27);
			V_10 = L_28;
			V_9 = 0;
			String_t* L_29 = V_10;
			NullCheck(L_29);
			bool L_30 = String_StartsWith_m1841920685(L_29, _stringLiteral3544696025, /*hidden argument*/NULL);
			if (!L_30)
			{
				goto IL_010e;
			}
		}

IL_0106:
		{
			V_9 = 1;
			goto IL_0122;
		}

IL_010e:
		{
			String_t* L_31 = V_10;
			NullCheck(L_31);
			bool L_32 = String_StartsWith_m1841920685(L_31, _stringLiteral460283084, /*hidden argument*/NULL);
			if (!L_32)
			{
				goto IL_0122;
			}
		}

IL_011f:
		{
			V_9 = 2;
		}

IL_0122:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
			GUISkin_t1436893342 * L_33 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_33);
			GUIStyle_t1799908754 * L_34 = GUISkin_get_label_m2703078986(L_33, /*hidden argument*/NULL);
			String_t* L_35 = V_10;
			GUIContent_t4210063000 * L_36 = (GUIContent_t4210063000 *)il2cpp_codegen_object_new(GUIContent_t4210063000_il2cpp_TypeInfo_var);
			GUIContent__ctor_m845353549(L_36, L_35, /*hidden argument*/NULL);
			float L_37 = V_2;
			NullCheck(L_34);
			float L_38 = GUIStyle_CalcHeight_m1685124037(L_34, L_36, L_37, /*hidden argument*/NULL);
			V_12 = L_38;
			float L_39 = V_7;
			float L_40 = V_2;
			float L_41 = V_12;
			Rect_t3681755626  L_42;
			memset(&L_42, 0, sizeof(L_42));
			Rect__ctor_m1220545469(&L_42, (0.0f), L_39, L_40, L_41, /*hidden argument*/NULL);
			String_t* L_43 = V_10;
			GUIStyleU5BU5D_t2497716199* L_44 = __this->get_style_86();
			int32_t L_45 = V_9;
			NullCheck(L_44);
			int32_t L_46 = L_45;
			GUIStyle_t1799908754 * L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
			GUI_Label_m2231582000(NULL /*static, unused*/, L_42, L_43, L_47, /*hidden argument*/NULL);
			float L_48 = V_7;
			float L_49 = V_12;
			V_7 = ((float)((float)L_48+(float)L_49));
		}

IL_0161:
		{
			Il2CppObject* L_50 = V_11;
			NullCheck(L_50);
			bool L_51 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_50);
			if (L_51)
			{
				goto IL_00e9;
			}
		}

IL_016d:
		{
			IL2CPP_LEAVE(0x181, FINALLY_0172);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0172;
	}

FINALLY_0172:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_52 = V_11;
			if (!L_52)
			{
				goto IL_0180;
			}
		}

IL_0179:
		{
			Il2CppObject* L_53 = V_11;
			NullCheck(L_53);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_53);
		}

IL_0180:
		{
			IL2CPP_END_FINALLY(370)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(370)
	{
		IL2CPP_JUMP_TBL(0x181, IL_0181)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0181:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndScrollView_m895410001(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARController::.cctor()
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3757846578_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3314526645_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2407024680_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m2603114416_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m295025071_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m4146865527_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2672961867;
extern Il2CppCodeGenString* _stringLiteral339799987;
extern Il2CppCodeGenString* _stringLiteral2247666127;
extern Il2CppCodeGenString* _stringLiteral2880464096;
extern Il2CppCodeGenString* _stringLiteral2398951807;
extern Il2CppCodeGenString* _stringLiteral623170596;
extern Il2CppCodeGenString* _stringLiteral2760134940;
extern Il2CppCodeGenString* _stringLiteral3625334991;
extern Il2CppCodeGenString* _stringLiteral813739692;
extern const uint32_t ARController__cctor_m1717663591_MetadataUsageId;
extern "C"  void ARController__cctor_m1717663591 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARController__cctor_m1717663591_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t3757846578 * V_0 = NULL;
	Dictionary_2_t3314526645 * V_1 = NULL;
	{
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->set_logMessages_4(L_0);
		Dictionary_2_t3757846578 * L_1 = (Dictionary_2_t3757846578 *)il2cpp_codegen_object_new(Dictionary_2_t3757846578_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2407024680(L_1, /*hidden argument*/Dictionary_2__ctor_m2407024680_MethodInfo_var);
		V_0 = L_1;
		Dictionary_2_t3757846578 * L_2 = V_0;
		NullCheck(L_2);
		Dictionary_2_Add_m2603114416(L_2, 0, _stringLiteral2672961867, /*hidden argument*/Dictionary_2_Add_m2603114416_MethodInfo_var);
		Dictionary_2_t3757846578 * L_3 = V_0;
		NullCheck(L_3);
		Dictionary_2_Add_m2603114416(L_3, 1, _stringLiteral339799987, /*hidden argument*/Dictionary_2_Add_m2603114416_MethodInfo_var);
		Dictionary_2_t3757846578 * L_4 = V_0;
		NullCheck(L_4);
		Dictionary_2_Add_m2603114416(L_4, 2, _stringLiteral2247666127, /*hidden argument*/Dictionary_2_Add_m2603114416_MethodInfo_var);
		Dictionary_2_t3757846578 * L_5 = V_0;
		NullCheck(L_5);
		Dictionary_2_Add_m2603114416(L_5, 3, _stringLiteral2880464096, /*hidden argument*/Dictionary_2_Add_m2603114416_MethodInfo_var);
		Dictionary_2_t3757846578 * L_6 = V_0;
		((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->set_ContentModeNames_67(L_6);
		Dictionary_2_t3314526645 * L_7 = (Dictionary_2_t3314526645 *)il2cpp_codegen_object_new(Dictionary_2_t3314526645_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m295025071(L_7, /*hidden argument*/Dictionary_2__ctor_m295025071_MethodInfo_var);
		V_1 = L_7;
		Dictionary_2_t3314526645 * L_8 = V_1;
		NullCheck(L_8);
		Dictionary_2_Add_m4146865527(L_8, 0, _stringLiteral2398951807, /*hidden argument*/Dictionary_2_Add_m4146865527_MethodInfo_var);
		Dictionary_2_t3314526645 * L_9 = V_1;
		NullCheck(L_9);
		Dictionary_2_Add_m4146865527(L_9, 1, _stringLiteral623170596, /*hidden argument*/Dictionary_2_Add_m4146865527_MethodInfo_var);
		Dictionary_2_t3314526645 * L_10 = V_1;
		NullCheck(L_10);
		Dictionary_2_Add_m4146865527(L_10, 2, _stringLiteral2760134940, /*hidden argument*/Dictionary_2_Add_m4146865527_MethodInfo_var);
		Dictionary_2_t3314526645 * L_11 = V_1;
		NullCheck(L_11);
		Dictionary_2_Add_m4146865527(L_11, 3, _stringLiteral3625334991, /*hidden argument*/Dictionary_2_Add_m4146865527_MethodInfo_var);
		Dictionary_2_t3314526645 * L_12 = V_1;
		NullCheck(L_12);
		Dictionary_2_Add_m4146865527(L_12, 4, _stringLiteral813739692, /*hidden argument*/Dictionary_2_Add_m4146865527_MethodInfo_var);
		Dictionary_2_t3314526645 * L_13 = V_1;
		((ARController_t593870669_StaticFields*)ARController_t593870669_il2cpp_TypeInfo_var->static_fields)->set_ThresholdModeDescriptions_72(L_13);
		return;
	}
}
// System.Void ARMarker::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2610802259;
extern Il2CppCodeGenString* _stringLiteral2610802032;
extern Il2CppCodeGenString* _stringLiteral2798351967;
extern const uint32_t ARMarker__ctor_m1510980660_MetadataUsageId;
extern "C"  void ARMarker__ctor_m1510980660 (ARMarker_t1554260723 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARMarker__ctor_m1510980660_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_UID_5((-1));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_Tag_7(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_PatternFilename_9(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_PatternContents_10(L_2);
		__this->set_PatternWidth_11((0.08f));
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_MultiConfigFile_13(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_NFTDataName_14(L_4);
		StringU5BU5D_t1642385972* L_5 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral2610802259);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2610802259);
		StringU5BU5D_t1642385972* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral2610802032);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2610802032);
		StringU5BU5D_t1642385972* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2798351967);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2798351967);
		__this->set_NFTDataExts_15(L_7);
		__this->set_currentFilterSampleRate_21((30.0f));
		__this->set_currentFilterCutoffFreq_22((15.0f));
		__this->set_currentNFTScale_23((1.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARMarker::Awake()
extern "C"  void ARMarker_Awake_m3919975599 (ARMarker_t1554260723 * __this, const MethodInfo* method)
{
	{
		__this->set_UID_5((-1));
		return;
	}
}
// System.Void ARMarker::OnEnable()
extern "C"  void ARMarker_OnEnable_m2896502880 (ARMarker_t1554260723 * __this, const MethodInfo* method)
{
	{
		ARMarker_Load_m2962895258(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARMarker::OnDisable()
extern "C"  void ARMarker_OnDisable_m3224112171 (ARMarker_t1554260723 * __this, const MethodInfo* method)
{
	{
		ARMarker_Unload_m3142779339(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ARMarker::unpackStreamingAssetToCacheDir(System.String)
extern Il2CppClass* Path_t41728875_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral968771974;
extern Il2CppCodeGenString* _stringLiteral372029307;
extern const uint32_t ARMarker_unpackStreamingAssetToCacheDir_m2370257800_MetadataUsageId;
extern "C"  bool ARMarker_unpackStreamingAssetToCacheDir_m2370257800 (ARMarker_t1554260723 * __this, String_t* ___basename0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARMarker_unpackStreamingAssetToCacheDir_m2370257800_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	WWW_t2919945039 * V_1 = NULL;
	{
		String_t* L_0 = Application_get_temporaryCachePath_m1120802989(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___basename0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
		String_t* L_2 = Path_Combine_m3185811654(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		bool L_3 = File_Exists_m1685968367(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0075;
		}
	}
	{
		String_t* L_4 = Application_get_streamingAssetsPath_m8890645(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = ___basename0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
		String_t* L_6 = Path_Combine_m3185811654(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		String_t* L_7 = V_0;
		WWW_t2919945039 * L_8 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_8, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		goto IL_002d;
	}

IL_002d:
	{
		WWW_t2919945039 * L_9 = V_1;
		NullCheck(L_9);
		bool L_10 = WWW_get_isDone_m3240254121(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_002d;
		}
	}
	{
		WWW_t2919945039 * L_11 = V_1;
		NullCheck(L_11);
		String_t* L_12 = WWW_get_error_m3092701216(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral968771974, L_14, _stringLiteral372029307, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_005f:
	{
		String_t* L_16 = Application_get_temporaryCachePath_m1120802989(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_17 = ___basename0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
		String_t* L_18 = Path_Combine_m3185811654(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		WWW_t2919945039 * L_19 = V_1;
		NullCheck(L_19);
		ByteU5BU5D_t3397334013* L_20 = WWW_get_bytes_m420718112(L_19, /*hidden argument*/NULL);
		File_WriteAllBytes_m677793349(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
	}

IL_0075:
	{
		return (bool)1;
	}
}
// System.Void ARMarker::Load()
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t41728875_il2cpp_TypeInfo_var;
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppClass* ARPatternU5BU5D_t59458152_il2cpp_TypeInfo_var;
extern Il2CppClass* ARPattern_t3906840165_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3796119598;
extern Il2CppCodeGenString* _stringLiteral566593916;
extern Il2CppCodeGenString* _stringLiteral1089730906;
extern Il2CppCodeGenString* _stringLiteral372029335;
extern Il2CppCodeGenString* _stringLiteral57472706;
extern Il2CppCodeGenString* _stringLiteral3371604104;
extern Il2CppCodeGenString* _stringLiteral372029316;
extern Il2CppCodeGenString* _stringLiteral1118365399;
extern Il2CppCodeGenString* _stringLiteral2781741057;
extern const uint32_t ARMarker_Load_m2962895258_MetadataUsageId;
extern "C"  void ARMarker_Load_m2962895258 (ARMarker_t1554260723 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARMarker_Load_m2962895258_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	StringU5BU5D_t1642385972* V_4 = NULL;
	int32_t V_5 = 0;
	String_t* V_6 = NULL;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	{
		int32_t L_0 = __this->get_UID_5();
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_1 = ((PluginFunctions_t2887339240_StaticFields*)PluginFunctions_t2887339240_il2cpp_TypeInfo_var->static_fields)->get_inited_0();
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return;
	}

IL_0018:
	{
		String_t* L_2 = Application_get_streamingAssetsPath_m8890645(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_1 = L_3;
		int32_t L_4 = __this->get_MarkerType_6();
		V_2 = L_4;
		int32_t L_5 = V_2;
		if (L_5 == 0)
		{
			goto IL_0046;
		}
		if (L_5 == 1)
		{
			goto IL_0084;
		}
		if (L_5 == 2)
		{
			goto IL_00c7;
		}
		if (L_5 == 3)
		{
			goto IL_0130;
		}
	}
	{
		goto IL_01cf;
	}

IL_0046:
	{
		ObjectU5BU5D_t3614634134* L_6 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3796119598);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3796119598);
		ObjectU5BU5D_t3614634134* L_7 = L_6;
		float L_8 = __this->get_PatternWidth_11();
		float L_9 = ((float)((float)L_8*(float)(1000.0f)));
		Il2CppObject * L_10 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_10);
		ObjectU5BU5D_t3614634134* L_11 = L_7;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral566593916);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral566593916);
		ObjectU5BU5D_t3614634134* L_12 = L_11;
		String_t* L_13 = __this->get_PatternContents_10();
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_1 = L_14;
		goto IL_01d4;
	}

IL_0084:
	{
		ObjectU5BU5D_t3614634134* L_15 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral1089730906);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1089730906);
		ObjectU5BU5D_t3614634134* L_16 = L_15;
		int32_t L_17 = __this->get_BarcodeID_12();
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_19);
		ObjectU5BU5D_t3614634134* L_20 = L_16;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral372029335);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029335);
		ObjectU5BU5D_t3614634134* L_21 = L_20;
		float L_22 = __this->get_PatternWidth_11();
		float L_23 = ((float)((float)L_22*(float)(1000.0f)));
		Il2CppObject * L_24 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_24);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_24);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m3881798623(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		V_1 = L_25;
		goto IL_01d4;
	}

IL_00c7:
	{
		String_t* L_26 = V_0;
		NullCheck(L_26);
		bool L_27 = String_Contains_m4017059963(L_26, _stringLiteral57472706, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00f9;
		}
	}
	{
		String_t* L_28 = Application_get_temporaryCachePath_m1120802989(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_28;
		String_t* L_29 = __this->get_MultiConfigFile_13();
		bool L_30 = ARMarker_unpackStreamingAssetToCacheDir_m2370257800(__this, L_29, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_00f9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_31;
		goto IL_00f9;
	}

IL_00f9:
	{
		String_t* L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_33 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_012b;
		}
	}
	{
		String_t* L_34 = __this->get_MultiConfigFile_13();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_35 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (L_35)
		{
			goto IL_012b;
		}
	}
	{
		String_t* L_36 = V_0;
		String_t* L_37 = __this->get_MultiConfigFile_13();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
		String_t* L_38 = Path_Combine_m3185811654(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3371604104, L_38, /*hidden argument*/NULL);
		V_1 = L_39;
	}

IL_012b:
	{
		goto IL_01d4;
	}

IL_0130:
	{
		String_t* L_40 = V_0;
		NullCheck(L_40);
		bool L_41 = String_Contains_m4017059963(L_40, _stringLiteral57472706, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0198;
		}
	}
	{
		String_t* L_42 = Application_get_temporaryCachePath_m1120802989(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_42;
		StringU5BU5D_t1642385972* L_43 = __this->get_NFTDataExts_15();
		V_4 = L_43;
		V_5 = 0;
		goto IL_018d;
	}

IL_0156:
	{
		StringU5BU5D_t1642385972* L_44 = V_4;
		int32_t L_45 = V_5;
		NullCheck(L_44);
		int32_t L_46 = L_45;
		String_t* L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		V_3 = L_47;
		String_t* L_48 = __this->get_NFTDataName_14();
		String_t* L_49 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = String_Concat_m612901809(NULL /*static, unused*/, L_48, _stringLiteral372029316, L_49, /*hidden argument*/NULL);
		V_6 = L_50;
		String_t* L_51 = V_6;
		bool L_52 = ARMarker_unpackStreamingAssetToCacheDir_m2370257800(__this, L_51, /*hidden argument*/NULL);
		if (L_52)
		{
			goto IL_0187;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_53 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_53;
		goto IL_0198;
	}

IL_0187:
	{
		int32_t L_54 = V_5;
		V_5 = ((int32_t)((int32_t)L_54+(int32_t)1));
	}

IL_018d:
	{
		int32_t L_55 = V_5;
		StringU5BU5D_t1642385972* L_56 = V_4;
		NullCheck(L_56);
		if ((((int32_t)L_55) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_56)->max_length)))))))
		{
			goto IL_0156;
		}
	}

IL_0198:
	{
		String_t* L_57 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_58 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		if (L_58)
		{
			goto IL_01ca;
		}
	}
	{
		String_t* L_59 = __this->get_NFTDataName_14();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_60 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		if (L_60)
		{
			goto IL_01ca;
		}
	}
	{
		String_t* L_61 = V_0;
		String_t* L_62 = __this->get_NFTDataName_14();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
		String_t* L_63 = Path_Combine_m3185811654(NULL /*static, unused*/, L_61, L_62, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_64 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1118365399, L_63, /*hidden argument*/NULL);
		V_1 = L_64;
	}

IL_01ca:
	{
		goto IL_01d4;
	}

IL_01cf:
	{
		goto IL_01d4;
	}

IL_01d4:
	{
		String_t* L_65 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_66 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		if (L_66)
		{
			goto IL_0307;
		}
	}
	{
		String_t* L_67 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		int32_t L_68 = PluginFunctions_arwAddMarker_m2914794118(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		__this->set_UID_5(L_68);
		int32_t L_69 = __this->get_UID_5();
		if ((!(((uint32_t)L_69) == ((uint32_t)(-1)))))
		{
			goto IL_0206;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral2781741057, /*hidden argument*/NULL);
		goto IL_0307;
	}

IL_0206:
	{
		int32_t L_70 = __this->get_MarkerType_6();
		if (!L_70)
		{
			goto IL_021d;
		}
	}
	{
		int32_t L_71 = __this->get_MarkerType_6();
		if ((!(((uint32_t)L_71) == ((uint32_t)1))))
		{
			goto IL_0229;
		}
	}

IL_021d:
	{
		bool L_72 = __this->get_currentUseContPoseEstimation_19();
		ARMarker_set_UseContPoseEstimation_m2883712299(__this, L_72, /*hidden argument*/NULL);
	}

IL_0229:
	{
		bool L_73 = __this->get_currentFiltered_20();
		ARMarker_set_Filtered_m2334347853(__this, L_73, /*hidden argument*/NULL);
		float L_74 = __this->get_currentFilterSampleRate_21();
		ARMarker_set_FilterSampleRate_m3330025658(__this, L_74, /*hidden argument*/NULL);
		float L_75 = __this->get_currentFilterCutoffFreq_22();
		ARMarker_set_FilterCutoffFreq_m2633862823(__this, L_75, /*hidden argument*/NULL);
		int32_t L_76 = __this->get_MarkerType_6();
		if ((!(((uint32_t)L_76) == ((uint32_t)3))))
		{
			goto IL_0265;
		}
	}
	{
		float L_77 = __this->get_currentNFTScale_23();
		ARMarker_set_NFTScale_m4109098010(__this, L_77, /*hidden argument*/NULL);
	}

IL_0265:
	{
		int32_t L_78 = __this->get_MarkerType_6();
		if ((!(((uint32_t)L_78) == ((uint32_t)3))))
		{
			goto IL_02b8;
		}
	}
	{
		int32_t L_79 = __this->get_UID_5();
		float* L_80 = __this->get_address_of_NFTWidth_16();
		float* L_81 = __this->get_address_of_NFTHeight_17();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwGetMarkerPatternConfig_m3210735196(NULL /*static, unused*/, L_79, 0, (SingleU5BU5D_t577127397*)(SingleU5BU5D_t577127397*)NULL, L_80, L_81, (&V_7), (&V_8), /*hidden argument*/NULL);
		float L_82 = __this->get_NFTWidth_16();
		__this->set_NFTWidth_16(((float)((float)L_82*(float)(0.001f))));
		float L_83 = __this->get_NFTHeight_17();
		__this->set_NFTHeight_17(((float)((float)L_83*(float)(0.001f))));
		goto IL_0307;
	}

IL_02b8:
	{
		int32_t L_84 = __this->get_UID_5();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		int32_t L_85 = PluginFunctions_arwGetMarkerPatternCount_m3403487639(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		V_9 = L_85;
		int32_t L_86 = V_9;
		if ((((int32_t)L_86) <= ((int32_t)0)))
		{
			goto IL_0307;
		}
	}
	{
		int32_t L_87 = V_9;
		__this->set_patterns_18(((ARPatternU5BU5D_t59458152*)SZArrayNew(ARPatternU5BU5D_t59458152_il2cpp_TypeInfo_var, (uint32_t)L_87)));
		V_10 = 0;
		goto IL_02fe;
	}

IL_02e2:
	{
		ARPatternU5BU5D_t59458152* L_88 = __this->get_patterns_18();
		int32_t L_89 = V_10;
		int32_t L_90 = __this->get_UID_5();
		int32_t L_91 = V_10;
		ARPattern_t3906840165 * L_92 = (ARPattern_t3906840165 *)il2cpp_codegen_object_new(ARPattern_t3906840165_il2cpp_TypeInfo_var);
		ARPattern__ctor_m3048001238(L_92, L_90, L_91, /*hidden argument*/NULL);
		NullCheck(L_88);
		ArrayElementTypeCheck (L_88, L_92);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(L_89), (ARPattern_t3906840165 *)L_92);
		int32_t L_93 = V_10;
		V_10 = ((int32_t)((int32_t)L_93+(int32_t)1));
	}

IL_02fe:
	{
		int32_t L_94 = V_10;
		int32_t L_95 = V_9;
		if ((((int32_t)L_94) < ((int32_t)L_95)))
		{
			goto IL_02e2;
		}
	}

IL_0307:
	{
		return;
	}
}
// System.Void ARMarker::Update()
extern Il2CppClass* SingleU5BU5D_t577127397_il2cpp_TypeInfo_var;
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARMarker_Update_m4009774547_MetadataUsageId;
extern "C"  void ARMarker_Update_m4009774547 (ARMarker_t1554260723 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARMarker_Update_m4009774547_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t577127397* V_0 = NULL;
	Matrix4x4_t2933234003  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		V_0 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		int32_t L_0 = __this->get_UID_5();
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_1 = ((PluginFunctions_t2887339240_StaticFields*)PluginFunctions_t2887339240_il2cpp_TypeInfo_var->static_fields)->get_inited_0();
		if (L_1)
		{
			goto IL_0026;
		}
	}

IL_001e:
	{
		__this->set_visible_24((bool)0);
		return;
	}

IL_0026:
	{
		bool L_2 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0093;
		}
	}
	{
		int32_t L_3 = __this->get_UID_5();
		SingleU5BU5D_t577127397* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_5 = PluginFunctions_arwQueryMarkerTransformation_m3138795604(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		__this->set_visible_24(L_5);
		bool L_6 = __this->get_visible_24();
		if (!L_6)
		{
			goto IL_0093;
		}
	}
	{
		SingleU5BU5D_t577127397* L_7 = V_0;
		NullCheck(L_7);
		float* L_8 = ((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)12))));
		*((float*)(L_8)) = (float)((float)((float)(*((float*)L_8))*(float)(0.001f)));
		SingleU5BU5D_t577127397* L_9 = V_0;
		NullCheck(L_9);
		float* L_10 = ((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)13))));
		*((float*)(L_10)) = (float)((float)((float)(*((float*)L_10))*(float)(0.001f)));
		SingleU5BU5D_t577127397* L_11 = V_0;
		NullCheck(L_11);
		float* L_12 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)14))));
		*((float*)(L_12)) = (float)((float)((float)(*((float*)L_12))*(float)(0.001f)));
		SingleU5BU5D_t577127397* L_13 = V_0;
		Matrix4x4_t2933234003  L_14 = ARUtilityFunctions_MatrixFromFloatArray_m3542072061(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		Matrix4x4_t2933234003  L_15 = V_1;
		Matrix4x4_t2933234003  L_16 = ARUtilityFunctions_LHMatrixFromRHMatrix_m2988276788(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		__this->set_transformationMatrix_25(L_16);
	}

IL_0093:
	{
		return;
	}
}
// System.Void ARMarker::Unload()
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARMarker_Unload_m3142779339_MetadataUsageId;
extern "C"  void ARMarker_Unload_m3142779339 (ARMarker_t1554260723 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARMarker_Unload_m3142779339_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_UID_5();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_1 = ((PluginFunctions_t2887339240_StaticFields*)PluginFunctions_t2887339240_il2cpp_TypeInfo_var->static_fields)->get_inited_0();
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_2 = __this->get_UID_5();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwRemoveMarker_m1199729578(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0023:
	{
		__this->set_UID_5((-1));
		__this->set_patterns_18((ARPatternU5BU5D_t59458152*)NULL);
		return;
	}
}
// UnityEngine.Matrix4x4 ARMarker::get_TransformationMatrix()
extern "C"  Matrix4x4_t2933234003  ARMarker_get_TransformationMatrix_m3960336120 (ARMarker_t1554260723 * __this, const MethodInfo* method)
{
	{
		Matrix4x4_t2933234003  L_0 = __this->get_transformationMatrix_25();
		return L_0;
	}
}
// System.Boolean ARMarker::get_Visible()
extern "C"  bool ARMarker_get_Visible_m4014965141 (ARMarker_t1554260723 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_visible_24();
		return L_0;
	}
}
// ARPattern[] ARMarker::get_Patterns()
extern "C"  ARPatternU5BU5D_t59458152* ARMarker_get_Patterns_m100568928 (ARMarker_t1554260723 * __this, const MethodInfo* method)
{
	{
		ARPatternU5BU5D_t59458152* L_0 = __this->get_patterns_18();
		return L_0;
	}
}
// System.Boolean ARMarker::get_Filtered()
extern "C"  bool ARMarker_get_Filtered_m2782988530 (ARMarker_t1554260723 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_currentFiltered_20();
		return L_0;
	}
}
// System.Void ARMarker::set_Filtered(System.Boolean)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARMarker_set_Filtered_m2334347853_MetadataUsageId;
extern "C"  void ARMarker_set_Filtered_m2334347853 (ARMarker_t1554260723 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARMarker_set_Filtered_m2334347853_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		__this->set_currentFiltered_20(L_0);
		int32_t L_1 = __this->get_UID_5();
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = __this->get_UID_5();
		bool L_3 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetMarkerOptionBool_m1349018745(NULL /*static, unused*/, L_2, 1, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Single ARMarker::get_FilterSampleRate()
extern "C"  float ARMarker_get_FilterSampleRate_m2217235765 (ARMarker_t1554260723 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_currentFilterSampleRate_21();
		return L_0;
	}
}
// System.Void ARMarker::set_FilterSampleRate(System.Single)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARMarker_set_FilterSampleRate_m3330025658_MetadataUsageId;
extern "C"  void ARMarker_set_FilterSampleRate_m3330025658 (ARMarker_t1554260723 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARMarker_set_FilterSampleRate_m3330025658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___value0;
		__this->set_currentFilterSampleRate_21(L_0);
		int32_t L_1 = __this->get_UID_5();
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = __this->get_UID_5();
		float L_3 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetMarkerOptionFloat_m35491069(NULL /*static, unused*/, L_2, 2, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Single ARMarker::get_FilterCutoffFreq()
extern "C"  float ARMarker_get_FilterCutoffFreq_m1734975516 (ARMarker_t1554260723 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_currentFilterCutoffFreq_22();
		return L_0;
	}
}
// System.Void ARMarker::set_FilterCutoffFreq(System.Single)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARMarker_set_FilterCutoffFreq_m2633862823_MetadataUsageId;
extern "C"  void ARMarker_set_FilterCutoffFreq_m2633862823 (ARMarker_t1554260723 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARMarker_set_FilterCutoffFreq_m2633862823_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___value0;
		__this->set_currentFilterCutoffFreq_22(L_0);
		int32_t L_1 = __this->get_UID_5();
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = __this->get_UID_5();
		float L_3 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetMarkerOptionFloat_m35491069(NULL /*static, unused*/, L_2, 3, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Boolean ARMarker::get_UseContPoseEstimation()
extern "C"  bool ARMarker_get_UseContPoseEstimation_m153213980 (ARMarker_t1554260723 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_currentUseContPoseEstimation_19();
		return L_0;
	}
}
// System.Void ARMarker::set_UseContPoseEstimation(System.Boolean)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARMarker_set_UseContPoseEstimation_m2883712299_MetadataUsageId;
extern "C"  void ARMarker_set_UseContPoseEstimation_m2883712299 (ARMarker_t1554260723 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARMarker_set_UseContPoseEstimation_m2883712299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		__this->set_currentUseContPoseEstimation_19(L_0);
		int32_t L_1 = __this->get_UID_5();
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_2 = __this->get_MarkerType_6();
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_3 = __this->get_MarkerType_6();
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0037;
		}
	}

IL_002a:
	{
		int32_t L_4 = __this->get_UID_5();
		bool L_5 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetMarkerOptionBool_m1349018745(NULL /*static, unused*/, L_4, 4, L_5, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
// System.Single ARMarker::get_NFTScale()
extern "C"  float ARMarker_get_NFTScale_m2738392489 (ARMarker_t1554260723 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_currentNFTScale_23();
		return L_0;
	}
}
// System.Void ARMarker::set_NFTScale(System.Single)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t ARMarker_set_NFTScale_m4109098010_MetadataUsageId;
extern "C"  void ARMarker_set_NFTScale_m4109098010 (ARMarker_t1554260723 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARMarker_set_NFTScale_m4109098010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___value0;
		__this->set_currentNFTScale_23(L_0);
		int32_t L_1 = __this->get_UID_5();
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_2 = __this->get_MarkerType_6();
		if ((!(((uint32_t)L_2) == ((uint32_t)3))))
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_3 = __this->get_UID_5();
		float L_4 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		PluginFunctions_arwSetMarkerOptionFloat_m35491069(NULL /*static, unused*/, L_3, 7, L_4, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void ARMarker::.cctor()
extern Il2CppClass* Dictionary_2_t1569850338_il2cpp_TypeInfo_var;
extern Il2CppClass* ARMarker_t1554260723_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3098768542_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m595126646_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4054626545;
extern Il2CppCodeGenString* _stringLiteral4114227977;
extern Il2CppCodeGenString* _stringLiteral1249926906;
extern Il2CppCodeGenString* _stringLiteral4073742278;
extern const uint32_t ARMarker__cctor_m3893367429_MetadataUsageId;
extern "C"  void ARMarker__cctor_m3893367429 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARMarker__cctor_m3893367429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t1569850338 * V_0 = NULL;
	{
		Dictionary_2_t1569850338 * L_0 = (Dictionary_2_t1569850338 *)il2cpp_codegen_object_new(Dictionary_2_t1569850338_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3098768542(L_0, /*hidden argument*/Dictionary_2__ctor_m3098768542_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t1569850338 * L_1 = V_0;
		NullCheck(L_1);
		Dictionary_2_Add_m595126646(L_1, 0, _stringLiteral4054626545, /*hidden argument*/Dictionary_2_Add_m595126646_MethodInfo_var);
		Dictionary_2_t1569850338 * L_2 = V_0;
		NullCheck(L_2);
		Dictionary_2_Add_m595126646(L_2, 1, _stringLiteral4114227977, /*hidden argument*/Dictionary_2_Add_m595126646_MethodInfo_var);
		Dictionary_2_t1569850338 * L_3 = V_0;
		NullCheck(L_3);
		Dictionary_2_Add_m595126646(L_3, 2, _stringLiteral1249926906, /*hidden argument*/Dictionary_2_Add_m595126646_MethodInfo_var);
		Dictionary_2_t1569850338 * L_4 = V_0;
		NullCheck(L_4);
		Dictionary_2_Add_m595126646(L_4, 3, _stringLiteral4073742278, /*hidden argument*/Dictionary_2_Add_m595126646_MethodInfo_var);
		Dictionary_2_t1569850338 * L_5 = V_0;
		((ARMarker_t1554260723_StaticFields*)ARMarker_t1554260723_il2cpp_TypeInfo_var->static_fields)->set_MarkerTypeNames_2(L_5);
		return;
	}
}
// System.Void ARNativePlugin::arwRegisterLogCallback(PluginFunctions/LogCallback)
extern "C"  void ARNativePlugin_arwRegisterLogCallback_m2470376146 (Il2CppObject * __this /* static, unused */, LogCallback_t2143553514 * ___callback0, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (Il2CppMethodPointer);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwRegisterLogCallback", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwRegisterLogCallback'"));
		}
	}

	// Marshaling of parameter '___callback0' to native representation
	Il2CppMethodPointer ____callback0_marshaled = NULL;
	____callback0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback0));

	// Native function invocation
	il2cppPInvokeFunc(____callback0_marshaled);

}
// System.Void ARNativePlugin::arwSetLogLevel(System.Int32)
extern "C"  void ARNativePlugin_arwSetLogLevel_m4002955093 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwSetLogLevel", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwSetLogLevel'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___logLevel0);

}
// System.Boolean ARNativePlugin::arwInitialiseAR()
extern "C"  bool ARNativePlugin_arwInitialiseAR_m2286360556 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwInitialiseAR", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwInitialiseAR'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc();

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwInitialiseARWithOptions(System.Int32,System.Int32)
extern "C"  bool ARNativePlugin_arwInitialiseARWithOptions_m2231924264 (Il2CppObject * __this /* static, unused */, int32_t ___pattSize0, int32_t ___pattCountMax1, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (int32_t, int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwInitialiseARWithOptions", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwInitialiseARWithOptions'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(___pattSize0, ___pattCountMax1);

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwGetARToolKitVersion(System.Text.StringBuilder,System.Int32)
extern "C"  bool ARNativePlugin_arwGetARToolKitVersion_m465574986 (Il2CppObject * __this /* static, unused */, StringBuilder_t1221177846 * ___buffer0, int32_t ___length1, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (char*, int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetARToolKitVersion", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetARToolKitVersion'"));
		}
	}

	// Marshaling of parameter '___buffer0' to native representation
	char* ____buffer0_marshaled = NULL;
	____buffer0_marshaled = il2cpp_codegen_marshal_string_builder(___buffer0);

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(____buffer0_marshaled, ___length1);

	// Marshaling of parameter '___buffer0' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___buffer0, ____buffer0_marshaled);

	// Marshaling cleanup of parameter '___buffer0' native representation
	il2cpp_codegen_marshal_free(____buffer0_marshaled);
	____buffer0_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
// System.Int32 ARNativePlugin::arwGetError()
extern "C"  int32_t ARNativePlugin_arwGetError_m4256659126 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetError", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetError'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Boolean ARNativePlugin::arwShutdownAR()
extern "C"  bool ARNativePlugin_arwShutdownAR_m1555258779 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwShutdownAR", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwShutdownAR'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc();

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwStartRunningB(System.String,System.Byte[],System.Int32,System.Single,System.Single)
extern "C"  bool ARNativePlugin_arwStartRunningB_m307445325 (Il2CppObject * __this /* static, unused */, String_t* ___vconf0, ByteU5BU5D_t3397334013* ___cparaBuff1, int32_t ___cparaBuffLen2, float ___nearPlane3, float ___farPlane4, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (char*, uint8_t*, int32_t, float, float);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(char*) + sizeof(void*) + sizeof(int32_t) + sizeof(float) + sizeof(float);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwStartRunningB", IL2CPP_CALL_C, CHARSET_ANSI, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwStartRunningB'"));
		}
	}

	// Marshaling of parameter '___vconf0' to native representation
	char* ____vconf0_marshaled = NULL;
	____vconf0_marshaled = il2cpp_codegen_marshal_string(___vconf0);

	// Marshaling of parameter '___cparaBuff1' to native representation
	uint8_t* ____cparaBuff1_marshaled = NULL;
	if (___cparaBuff1 != NULL)
	{
		____cparaBuff1_marshaled = reinterpret_cast<uint8_t*>((___cparaBuff1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(____vconf0_marshaled, ____cparaBuff1_marshaled, ___cparaBuffLen2, ___nearPlane3, ___farPlane4);

	// Marshaling cleanup of parameter '___vconf0' native representation
	il2cpp_codegen_marshal_free(____vconf0_marshaled);
	____vconf0_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwStartRunningStereoB(System.String,System.Byte[],System.Int32,System.String,System.Byte[],System.Int32,System.Byte[],System.Int32,System.Single,System.Single)
extern "C"  bool ARNativePlugin_arwStartRunningStereoB_m1235119585 (Il2CppObject * __this /* static, unused */, String_t* ___vconfL0, ByteU5BU5D_t3397334013* ___cparaBuffL1, int32_t ___cparaBuffLenL2, String_t* ___vconfR3, ByteU5BU5D_t3397334013* ___cparaBuffR4, int32_t ___cparaBuffLenR5, ByteU5BU5D_t3397334013* ___transL2RBuff6, int32_t ___transL2RBuffLen7, float ___nearPlane8, float ___farPlane9, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (char*, uint8_t*, int32_t, char*, uint8_t*, int32_t, uint8_t*, int32_t, float, float);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(char*) + sizeof(void*) + sizeof(int32_t) + sizeof(char*) + sizeof(void*) + sizeof(int32_t) + sizeof(void*) + sizeof(int32_t) + sizeof(float) + sizeof(float);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwStartRunningStereoB", IL2CPP_CALL_C, CHARSET_ANSI, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwStartRunningStereoB'"));
		}
	}

	// Marshaling of parameter '___vconfL0' to native representation
	char* ____vconfL0_marshaled = NULL;
	____vconfL0_marshaled = il2cpp_codegen_marshal_string(___vconfL0);

	// Marshaling of parameter '___cparaBuffL1' to native representation
	uint8_t* ____cparaBuffL1_marshaled = NULL;
	if (___cparaBuffL1 != NULL)
	{
		____cparaBuffL1_marshaled = reinterpret_cast<uint8_t*>((___cparaBuffL1)->GetAddressAtUnchecked(0));
	}

	// Marshaling of parameter '___vconfR3' to native representation
	char* ____vconfR3_marshaled = NULL;
	____vconfR3_marshaled = il2cpp_codegen_marshal_string(___vconfR3);

	// Marshaling of parameter '___cparaBuffR4' to native representation
	uint8_t* ____cparaBuffR4_marshaled = NULL;
	if (___cparaBuffR4 != NULL)
	{
		____cparaBuffR4_marshaled = reinterpret_cast<uint8_t*>((___cparaBuffR4)->GetAddressAtUnchecked(0));
	}

	// Marshaling of parameter '___transL2RBuff6' to native representation
	uint8_t* ____transL2RBuff6_marshaled = NULL;
	if (___transL2RBuff6 != NULL)
	{
		____transL2RBuff6_marshaled = reinterpret_cast<uint8_t*>((___transL2RBuff6)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(____vconfL0_marshaled, ____cparaBuffL1_marshaled, ___cparaBuffLenL2, ____vconfR3_marshaled, ____cparaBuffR4_marshaled, ___cparaBuffLenR5, ____transL2RBuff6_marshaled, ___transL2RBuffLen7, ___nearPlane8, ___farPlane9);

	// Marshaling cleanup of parameter '___vconfL0' native representation
	il2cpp_codegen_marshal_free(____vconfL0_marshaled);
	____vconfL0_marshaled = NULL;

	// Marshaling cleanup of parameter '___vconfR3' native representation
	il2cpp_codegen_marshal_free(____vconfR3_marshaled);
	____vconfR3_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwIsRunning()
extern "C"  bool ARNativePlugin_arwIsRunning_m3033062181 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwIsRunning", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwIsRunning'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc();

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwStopRunning()
extern "C"  bool ARNativePlugin_arwStopRunning_m622006595 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwStopRunning", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwStopRunning'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc();

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwGetProjectionMatrix(System.Single[])
extern "C"  bool ARNativePlugin_arwGetProjectionMatrix_m3243091243 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___matrix0, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (float*);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetProjectionMatrix", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetProjectionMatrix'"));
		}
	}

	// Marshaling of parameter '___matrix0' to native representation
	float* ____matrix0_marshaled = NULL;
	if (___matrix0 != NULL)
	{
		int32_t ____matrix0_Length = (___matrix0)->max_length;
		____matrix0_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____matrix0_Length);
		for (int32_t i = 0; i < ____matrix0_Length; i++)
		{
			(____matrix0_marshaled)[i] = (___matrix0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____matrix0_marshaled = NULL;
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(____matrix0_marshaled);

	// Marshaling cleanup of parameter '___matrix0' native representation
	if (____matrix0_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____matrix0_marshaled);
		____matrix0_marshaled = NULL;
	}

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwGetProjectionMatrixStereo(System.Single[],System.Single[])
extern "C"  bool ARNativePlugin_arwGetProjectionMatrixStereo_m427191080 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___matrixL0, SingleU5BU5D_t577127397* ___matrixR1, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (float*, float*);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*) + sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetProjectionMatrixStereo", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetProjectionMatrixStereo'"));
		}
	}

	// Marshaling of parameter '___matrixL0' to native representation
	float* ____matrixL0_marshaled = NULL;
	if (___matrixL0 != NULL)
	{
		int32_t ____matrixL0_Length = (___matrixL0)->max_length;
		____matrixL0_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____matrixL0_Length);
		for (int32_t i = 0; i < ____matrixL0_Length; i++)
		{
			(____matrixL0_marshaled)[i] = (___matrixL0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____matrixL0_marshaled = NULL;
	}

	// Marshaling of parameter '___matrixR1' to native representation
	float* ____matrixR1_marshaled = NULL;
	if (___matrixR1 != NULL)
	{
		int32_t ____matrixR1_Length = (___matrixR1)->max_length;
		____matrixR1_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____matrixR1_Length);
		for (int32_t i = 0; i < ____matrixR1_Length; i++)
		{
			(____matrixR1_marshaled)[i] = (___matrixR1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____matrixR1_marshaled = NULL;
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(____matrixL0_marshaled, ____matrixR1_marshaled);

	// Marshaling cleanup of parameter '___matrixL0' native representation
	if (____matrixL0_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____matrixL0_marshaled);
		____matrixL0_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___matrixR1' native representation
	if (____matrixR1_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____matrixR1_marshaled);
		____matrixR1_marshaled = NULL;
	}

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwGetVideoParams(System.Int32&,System.Int32&,System.Int32&,System.Text.StringBuilder,System.Int32)
extern "C"  bool ARNativePlugin_arwGetVideoParams_m982330381 (Il2CppObject * __this /* static, unused */, int32_t* ___width0, int32_t* ___height1, int32_t* ___pixelSize2, StringBuilder_t1221177846 * ___pixelFormatBuffer3, int32_t ___pixelFormatBufferLen4, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (int32_t*, int32_t*, int32_t*, char*, int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t*) + sizeof(int32_t*) + sizeof(int32_t*) + sizeof(void*) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetVideoParams", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetVideoParams'"));
		}
	}

	// Marshaling of parameter '___width0' to native representation
	int32_t ____width0_empty = 0;

	// Marshaling of parameter '___height1' to native representation
	int32_t ____height1_empty = 0;

	// Marshaling of parameter '___pixelSize2' to native representation
	int32_t ____pixelSize2_empty = 0;

	// Marshaling of parameter '___pixelFormatBuffer3' to native representation
	char* ____pixelFormatBuffer3_marshaled = NULL;
	____pixelFormatBuffer3_marshaled = il2cpp_codegen_marshal_string_builder(___pixelFormatBuffer3);

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(&____width0_empty, &____height1_empty, &____pixelSize2_empty, ____pixelFormatBuffer3_marshaled, ___pixelFormatBufferLen4);

	// Marshaling of parameter '___width0' back from native representation
	*___width0 = ____width0_empty;

	// Marshaling of parameter '___height1' back from native representation
	*___height1 = ____height1_empty;

	// Marshaling of parameter '___pixelSize2' back from native representation
	*___pixelSize2 = ____pixelSize2_empty;

	// Marshaling of parameter '___pixelFormatBuffer3' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___pixelFormatBuffer3, ____pixelFormatBuffer3_marshaled);

	// Marshaling cleanup of parameter '___pixelFormatBuffer3' native representation
	il2cpp_codegen_marshal_free(____pixelFormatBuffer3_marshaled);
	____pixelFormatBuffer3_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwGetVideoParamsStereo(System.Int32&,System.Int32&,System.Int32&,System.Text.StringBuilder,System.Int32,System.Int32&,System.Int32&,System.Int32&,System.Text.StringBuilder,System.Int32)
extern "C"  bool ARNativePlugin_arwGetVideoParamsStereo_m4115500785 (Il2CppObject * __this /* static, unused */, int32_t* ___widthL0, int32_t* ___heightL1, int32_t* ___pixelSizeL2, StringBuilder_t1221177846 * ___pixelFormatBufferL3, int32_t ___pixelFormatBufferLenL4, int32_t* ___widthR5, int32_t* ___heightR6, int32_t* ___pixelSizeR7, StringBuilder_t1221177846 * ___pixelFormatBufferR8, int32_t ___pixelFormatBufferLenR9, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (int32_t*, int32_t*, int32_t*, char*, int32_t, int32_t*, int32_t*, int32_t*, char*, int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t*) + sizeof(int32_t*) + sizeof(int32_t*) + sizeof(void*) + sizeof(int32_t) + sizeof(int32_t*) + sizeof(int32_t*) + sizeof(int32_t*) + sizeof(void*) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetVideoParamsStereo", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetVideoParamsStereo'"));
		}
	}

	// Marshaling of parameter '___widthL0' to native representation
	int32_t ____widthL0_empty = 0;

	// Marshaling of parameter '___heightL1' to native representation
	int32_t ____heightL1_empty = 0;

	// Marshaling of parameter '___pixelSizeL2' to native representation
	int32_t ____pixelSizeL2_empty = 0;

	// Marshaling of parameter '___pixelFormatBufferL3' to native representation
	char* ____pixelFormatBufferL3_marshaled = NULL;
	____pixelFormatBufferL3_marshaled = il2cpp_codegen_marshal_string_builder(___pixelFormatBufferL3);

	// Marshaling of parameter '___widthR5' to native representation
	int32_t ____widthR5_empty = 0;

	// Marshaling of parameter '___heightR6' to native representation
	int32_t ____heightR6_empty = 0;

	// Marshaling of parameter '___pixelSizeR7' to native representation
	int32_t ____pixelSizeR7_empty = 0;

	// Marshaling of parameter '___pixelFormatBufferR8' to native representation
	char* ____pixelFormatBufferR8_marshaled = NULL;
	____pixelFormatBufferR8_marshaled = il2cpp_codegen_marshal_string_builder(___pixelFormatBufferR8);

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(&____widthL0_empty, &____heightL1_empty, &____pixelSizeL2_empty, ____pixelFormatBufferL3_marshaled, ___pixelFormatBufferLenL4, &____widthR5_empty, &____heightR6_empty, &____pixelSizeR7_empty, ____pixelFormatBufferR8_marshaled, ___pixelFormatBufferLenR9);

	// Marshaling of parameter '___widthL0' back from native representation
	*___widthL0 = ____widthL0_empty;

	// Marshaling of parameter '___heightL1' back from native representation
	*___heightL1 = ____heightL1_empty;

	// Marshaling of parameter '___pixelSizeL2' back from native representation
	*___pixelSizeL2 = ____pixelSizeL2_empty;

	// Marshaling of parameter '___pixelFormatBufferL3' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___pixelFormatBufferL3, ____pixelFormatBufferL3_marshaled);

	// Marshaling cleanup of parameter '___pixelFormatBufferL3' native representation
	il2cpp_codegen_marshal_free(____pixelFormatBufferL3_marshaled);
	____pixelFormatBufferL3_marshaled = NULL;

	// Marshaling of parameter '___widthR5' back from native representation
	*___widthR5 = ____widthR5_empty;

	// Marshaling of parameter '___heightR6' back from native representation
	*___heightR6 = ____heightR6_empty;

	// Marshaling of parameter '___pixelSizeR7' back from native representation
	*___pixelSizeR7 = ____pixelSizeR7_empty;

	// Marshaling of parameter '___pixelFormatBufferR8' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___pixelFormatBufferR8, ____pixelFormatBufferR8_marshaled);

	// Marshaling cleanup of parameter '___pixelFormatBufferR8' native representation
	il2cpp_codegen_marshal_free(____pixelFormatBufferR8_marshaled);
	____pixelFormatBufferR8_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwCapture()
extern "C"  bool ARNativePlugin_arwCapture_m808157064 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwCapture", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwCapture'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc();

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwUpdateAR()
extern "C"  bool ARNativePlugin_arwUpdateAR_m3219639630 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwUpdateAR", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwUpdateAR'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc();

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwUpdateTexture(System.IntPtr)
extern "C"  bool ARNativePlugin_arwUpdateTexture_m938831980 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colors0, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (intptr_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwUpdateTexture", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwUpdateTexture'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___colors0).get_m_value_0()));

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwUpdateTextureStereo(System.IntPtr,System.IntPtr)
extern "C"  bool ARNativePlugin_arwUpdateTextureStereo_m4292622656 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colorsL0, IntPtr_t ___colorsR1, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (intptr_t, intptr_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwUpdateTextureStereo", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwUpdateTextureStereo'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___colorsL0).get_m_value_0()), reinterpret_cast<intptr_t>((___colorsR1).get_m_value_0()));

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwUpdateTexture32(System.IntPtr)
extern "C"  bool ARNativePlugin_arwUpdateTexture32_m443923741 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colors320, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (intptr_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwUpdateTexture32", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwUpdateTexture32'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___colors320).get_m_value_0()));

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwUpdateTexture32Stereo(System.IntPtr,System.IntPtr)
extern "C"  bool ARNativePlugin_arwUpdateTexture32Stereo_m993054649 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colors32L0, IntPtr_t ___colors32R1, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (intptr_t, intptr_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwUpdateTexture32Stereo", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwUpdateTexture32Stereo'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___colors32L0).get_m_value_0()), reinterpret_cast<intptr_t>((___colors32R1).get_m_value_0()));

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwUpdateTextureGL(System.Int32)
extern "C"  bool ARNativePlugin_arwUpdateTextureGL_m2994090928 (Il2CppObject * __this /* static, unused */, int32_t ___textureID0, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwUpdateTextureGL", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwUpdateTextureGL'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(___textureID0);

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwUpdateTextureGLStereo(System.Int32,System.Int32)
extern "C"  bool ARNativePlugin_arwUpdateTextureGLStereo_m2235959663 (Il2CppObject * __this /* static, unused */, int32_t ___textureID_L0, int32_t ___textureID_R1, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (int32_t, int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwUpdateTextureGLStereo", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwUpdateTextureGLStereo'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(___textureID_L0, ___textureID_R1);

	return static_cast<bool>(returnValue);
}
// System.Void ARNativePlugin::arwSetUnityRenderEventUpdateTextureGLTextureID(System.Int32)
extern "C"  void ARNativePlugin_arwSetUnityRenderEventUpdateTextureGLTextureID_m1975041325 (Il2CppObject * __this /* static, unused */, int32_t ___textureID0, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwSetUnityRenderEventUpdateTextureGLTextureID", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwSetUnityRenderEventUpdateTextureGLTextureID'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___textureID0);

}
// System.Void ARNativePlugin::arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs(System.Int32,System.Int32)
extern "C"  void ARNativePlugin_arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs_m1459928577 (Il2CppObject * __this /* static, unused */, int32_t ___textureID_L0, int32_t ___textureID_R1, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (int32_t, int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___textureID_L0, ___textureID_R1);

}
// System.Int32 ARNativePlugin::arwGetMarkerPatternCount(System.Int32)
extern "C"  int32_t ARNativePlugin_arwGetMarkerPatternCount_m1314450108 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetMarkerPatternCount", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetMarkerPatternCount'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___markerID0);

	return returnValue;
}
// System.Boolean ARNativePlugin::arwGetMarkerPatternConfig(System.Int32,System.Int32,System.Single[],System.Single&,System.Single&,System.Int32&,System.Int32&)
extern "C"  bool ARNativePlugin_arwGetMarkerPatternConfig_m4089949209 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___patternID1, SingleU5BU5D_t577127397* ___matrix2, float* ___width3, float* ___height4, int32_t* ___imageSizeX5, int32_t* ___imageSizeY6, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (int32_t, int32_t, float*, float*, float*, int32_t*, int32_t*);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t) + sizeof(int32_t) + sizeof(void*) + sizeof(float*) + sizeof(float*) + sizeof(int32_t*) + sizeof(int32_t*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetMarkerPatternConfig", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetMarkerPatternConfig'"));
		}
	}

	// Marshaling of parameter '___matrix2' to native representation
	float* ____matrix2_marshaled = NULL;
	if (___matrix2 != NULL)
	{
		int32_t ____matrix2_Length = (___matrix2)->max_length;
		____matrix2_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____matrix2_Length);
		for (int32_t i = 0; i < ____matrix2_Length; i++)
		{
			(____matrix2_marshaled)[i] = (___matrix2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____matrix2_marshaled = NULL;
	}

	// Marshaling of parameter '___width3' to native representation
	float ____width3_empty = 0.0f;

	// Marshaling of parameter '___height4' to native representation
	float ____height4_empty = 0.0f;

	// Marshaling of parameter '___imageSizeX5' to native representation
	int32_t ____imageSizeX5_empty = 0;

	// Marshaling of parameter '___imageSizeY6' to native representation
	int32_t ____imageSizeY6_empty = 0;

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(___markerID0, ___patternID1, ____matrix2_marshaled, &____width3_empty, &____height4_empty, &____imageSizeX5_empty, &____imageSizeY6_empty);

	// Marshaling cleanup of parameter '___matrix2' native representation
	if (____matrix2_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____matrix2_marshaled);
		____matrix2_marshaled = NULL;
	}

	// Marshaling of parameter '___width3' back from native representation
	*___width3 = ____width3_empty;

	// Marshaling of parameter '___height4' back from native representation
	*___height4 = ____height4_empty;

	// Marshaling of parameter '___imageSizeX5' back from native representation
	*___imageSizeX5 = ____imageSizeX5_empty;

	// Marshaling of parameter '___imageSizeY6' back from native representation
	*___imageSizeY6 = ____imageSizeY6_empty;

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwGetMarkerPatternImage(System.Int32,System.Int32,UnityEngine.Color[])
extern "C"  bool ARNativePlugin_arwGetMarkerPatternImage_m769298731 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___patternID1, ColorU5BU5D_t672350442* ___colors2, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (int32_t, int32_t, Color_t2020392075 *);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t) + sizeof(int32_t) + sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetMarkerPatternImage", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetMarkerPatternImage'"));
		}
	}

	// Marshaling of parameter '___colors2' to native representation
	Color_t2020392075 * ____colors2_marshaled = NULL;
	if (___colors2 != NULL)
	{
		____colors2_marshaled = reinterpret_cast<Color_t2020392075 *>((___colors2)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(___markerID0, ___patternID1, ____colors2_marshaled);

	// Marshaling of parameter '___colors2' back from native representation
	if (____colors2_marshaled != NULL)
	{
		int32_t ____colors2_Length = (___colors2)->max_length;
		for (int32_t i = 0; i < ____colors2_Length; i++)
		{
			(___colors2)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____colors2_marshaled)[i]);
		}
	}

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwGetMarkerOptionBool(System.Int32,System.Int32)
extern "C"  bool ARNativePlugin_arwGetMarkerOptionBool_m2354836317 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (int32_t, int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetMarkerOptionBool", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetMarkerOptionBool'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(___markerID0, ___option1);

	return static_cast<bool>(returnValue);
}
// System.Void ARNativePlugin::arwSetMarkerOptionBool(System.Int32,System.Int32,System.Boolean)
extern "C"  void ARNativePlugin_arwSetMarkerOptionBool_m1342859260 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, bool ___value2, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (int32_t, int32_t, int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t) + sizeof(int32_t) + 4;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwSetMarkerOptionBool", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwSetMarkerOptionBool'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___markerID0, ___option1, static_cast<int32_t>(___value2));

}
// System.Int32 ARNativePlugin::arwGetMarkerOptionInt(System.Int32,System.Int32)
extern "C"  int32_t ARNativePlugin_arwGetMarkerOptionInt_m1863724448 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) (int32_t, int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetMarkerOptionInt", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetMarkerOptionInt'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___markerID0, ___option1);

	return returnValue;
}
// System.Void ARNativePlugin::arwSetMarkerOptionInt(System.Int32,System.Int32,System.Int32)
extern "C"  void ARNativePlugin_arwSetMarkerOptionInt_m1852067113 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, int32_t ___value2, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (int32_t, int32_t, int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwSetMarkerOptionInt", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwSetMarkerOptionInt'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___markerID0, ___option1, ___value2);

}
// System.Single ARNativePlugin::arwGetMarkerOptionFloat(System.Int32,System.Int32)
extern "C"  float ARNativePlugin_arwGetMarkerOptionFloat_m1337828381 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method)
{
	typedef float (CDECL *PInvokeFunc) (int32_t, int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetMarkerOptionFloat", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetMarkerOptionFloat'"));
		}
	}

	// Native function invocation
	float returnValue = il2cppPInvokeFunc(___markerID0, ___option1);

	return returnValue;
}
// System.Void ARNativePlugin::arwSetMarkerOptionFloat(System.Int32,System.Int32,System.Single)
extern "C"  void ARNativePlugin_arwSetMarkerOptionFloat_m3494403524 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, float ___value2, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (int32_t, int32_t, float);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t) + sizeof(int32_t) + sizeof(float);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwSetMarkerOptionFloat", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwSetMarkerOptionFloat'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___markerID0, ___option1, ___value2);

}
// System.Void ARNativePlugin::arwSetVideoDebugMode(System.Boolean)
extern "C"  void ARNativePlugin_arwSetVideoDebugMode_m1349895608 (Il2CppObject * __this /* static, unused */, bool ___debug0, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 4;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwSetVideoDebugMode", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwSetVideoDebugMode'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(static_cast<int32_t>(___debug0));

}
// System.Boolean ARNativePlugin::arwGetVideoDebugMode()
extern "C"  bool ARNativePlugin_arwGetVideoDebugMode_m2178590269 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetVideoDebugMode", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetVideoDebugMode'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc();

	return static_cast<bool>(returnValue);
}
// System.Void ARNativePlugin::arwSetVideoThreshold(System.Int32)
extern "C"  void ARNativePlugin_arwSetVideoThreshold_m2002595265 (Il2CppObject * __this /* static, unused */, int32_t ___threshold0, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwSetVideoThreshold", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwSetVideoThreshold'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___threshold0);

}
// System.Int32 ARNativePlugin::arwGetVideoThreshold()
extern "C"  int32_t ARNativePlugin_arwGetVideoThreshold_m2841997392 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetVideoThreshold", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetVideoThreshold'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Void ARNativePlugin::arwSetVideoThresholdMode(System.Int32)
extern "C"  void ARNativePlugin_arwSetVideoThresholdMode_m1677852552 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwSetVideoThresholdMode", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwSetVideoThresholdMode'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___mode0);

}
// System.Int32 ARNativePlugin::arwGetVideoThresholdMode()
extern "C"  int32_t ARNativePlugin_arwGetVideoThresholdMode_m1626559629 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetVideoThresholdMode", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetVideoThresholdMode'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Void ARNativePlugin::arwSetLabelingMode(System.Int32)
extern "C"  void ARNativePlugin_arwSetLabelingMode_m801205656 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwSetLabelingMode", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwSetLabelingMode'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___mode0);

}
// System.Int32 ARNativePlugin::arwGetLabelingMode()
extern "C"  int32_t ARNativePlugin_arwGetLabelingMode_m3294800397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetLabelingMode", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetLabelingMode'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Void ARNativePlugin::arwSetBorderSize(System.Single)
extern "C"  void ARNativePlugin_arwSetBorderSize_m3785932980 (Il2CppObject * __this /* static, unused */, float ___mode0, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (float);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(float);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwSetBorderSize", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwSetBorderSize'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___mode0);

}
// System.Single ARNativePlugin::arwGetBorderSize()
extern "C"  float ARNativePlugin_arwGetBorderSize_m1115123341 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetBorderSize", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetBorderSize'"));
		}
	}

	// Native function invocation
	float returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Void ARNativePlugin::arwSetPatternDetectionMode(System.Int32)
extern "C"  void ARNativePlugin_arwSetPatternDetectionMode_m2117597091 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwSetPatternDetectionMode", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwSetPatternDetectionMode'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___mode0);

}
// System.Int32 ARNativePlugin::arwGetPatternDetectionMode()
extern "C"  int32_t ARNativePlugin_arwGetPatternDetectionMode_m1233416524 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetPatternDetectionMode", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetPatternDetectionMode'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Void ARNativePlugin::arwSetMatrixCodeType(System.Int32)
extern "C"  void ARNativePlugin_arwSetMatrixCodeType_m675665857 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwSetMatrixCodeType", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwSetMatrixCodeType'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___type0);

}
// System.Int32 ARNativePlugin::arwGetMatrixCodeType()
extern "C"  int32_t ARNativePlugin_arwGetMatrixCodeType_m176152962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetMatrixCodeType", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetMatrixCodeType'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Void ARNativePlugin::arwSetImageProcMode(System.Int32)
extern "C"  void ARNativePlugin_arwSetImageProcMode_m302197153 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwSetImageProcMode", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwSetImageProcMode'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___mode0);

}
// System.Int32 ARNativePlugin::arwGetImageProcMode()
extern "C"  int32_t ARNativePlugin_arwGetImageProcMode_m676231894 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetImageProcMode", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetImageProcMode'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Void ARNativePlugin::arwSetNFTMultiMode(System.Boolean)
extern "C"  void ARNativePlugin_arwSetNFTMultiMode_m733908047 (Il2CppObject * __this /* static, unused */, bool ___on0, const MethodInfo* method)
{
	typedef void (CDECL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 4;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwSetNFTMultiMode", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwSetNFTMultiMode'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(static_cast<int32_t>(___on0));

}
// System.Boolean ARNativePlugin::arwGetNFTMultiMode()
extern "C"  bool ARNativePlugin_arwGetNFTMultiMode_m1076663180 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwGetNFTMultiMode", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwGetNFTMultiMode'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc();

	return static_cast<bool>(returnValue);
}
// System.Int32 ARNativePlugin::arwAddMarker(System.String)
extern "C"  int32_t ARNativePlugin_arwAddMarker_m1259082831 (Il2CppObject * __this /* static, unused */, String_t* ___cfg0, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) (char*);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(char*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwAddMarker", IL2CPP_CALL_C, CHARSET_ANSI, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwAddMarker'"));
		}
	}

	// Marshaling of parameter '___cfg0' to native representation
	char* ____cfg0_marshaled = NULL;
	____cfg0_marshaled = il2cpp_codegen_marshal_string(___cfg0);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____cfg0_marshaled);

	// Marshaling cleanup of parameter '___cfg0' native representation
	il2cpp_codegen_marshal_free(____cfg0_marshaled);
	____cfg0_marshaled = NULL;

	return returnValue;
}
// System.Boolean ARNativePlugin::arwRemoveMarker(System.Int32)
extern "C"  bool ARNativePlugin_arwRemoveMarker_m78809961 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwRemoveMarker", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwRemoveMarker'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(___markerID0);

	return static_cast<bool>(returnValue);
}
// System.Int32 ARNativePlugin::arwRemoveAllMarkers()
extern "C"  int32_t ARNativePlugin_arwRemoveAllMarkers_m3455426882 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwRemoveAllMarkers", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwRemoveAllMarkers'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Boolean ARNativePlugin::arwQueryMarkerVisibility(System.Int32)
extern "C"  bool ARNativePlugin_arwQueryMarkerVisibility_m3920274723 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwQueryMarkerVisibility", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwQueryMarkerVisibility'"));
		}
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(___markerID0);

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwQueryMarkerTransformation(System.Int32,System.Single[])
extern "C"  bool ARNativePlugin_arwQueryMarkerTransformation_m3853733251 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, SingleU5BU5D_t577127397* ___matrix1, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (int32_t, float*);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t) + sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwQueryMarkerTransformation", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwQueryMarkerTransformation'"));
		}
	}

	// Marshaling of parameter '___matrix1' to native representation
	float* ____matrix1_marshaled = NULL;
	if (___matrix1 != NULL)
	{
		int32_t ____matrix1_Length = (___matrix1)->max_length;
		____matrix1_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____matrix1_Length);
		for (int32_t i = 0; i < ____matrix1_Length; i++)
		{
			(____matrix1_marshaled)[i] = (___matrix1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____matrix1_marshaled = NULL;
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(___markerID0, ____matrix1_marshaled);

	// Marshaling cleanup of parameter '___matrix1' native representation
	if (____matrix1_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____matrix1_marshaled);
		____matrix1_marshaled = NULL;
	}

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwQueryMarkerTransformationStereo(System.Int32,System.Single[],System.Single[])
extern "C"  bool ARNativePlugin_arwQueryMarkerTransformationStereo_m3906452060 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, SingleU5BU5D_t577127397* ___matrixL1, SingleU5BU5D_t577127397* ___matrixR2, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (int32_t, float*, float*);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t) + sizeof(void*) + sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwQueryMarkerTransformationStereo", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwQueryMarkerTransformationStereo'"));
		}
	}

	// Marshaling of parameter '___matrixL1' to native representation
	float* ____matrixL1_marshaled = NULL;
	if (___matrixL1 != NULL)
	{
		int32_t ____matrixL1_Length = (___matrixL1)->max_length;
		____matrixL1_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____matrixL1_Length);
		for (int32_t i = 0; i < ____matrixL1_Length; i++)
		{
			(____matrixL1_marshaled)[i] = (___matrixL1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____matrixL1_marshaled = NULL;
	}

	// Marshaling of parameter '___matrixR2' to native representation
	float* ____matrixR2_marshaled = NULL;
	if (___matrixR2 != NULL)
	{
		int32_t ____matrixR2_Length = (___matrixR2)->max_length;
		____matrixR2_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____matrixR2_Length);
		for (int32_t i = 0; i < ____matrixR2_Length; i++)
		{
			(____matrixR2_marshaled)[i] = (___matrixR2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____matrixR2_marshaled = NULL;
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(___markerID0, ____matrixL1_marshaled, ____matrixR2_marshaled);

	// Marshaling cleanup of parameter '___matrixL1' native representation
	if (____matrixL1_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____matrixL1_marshaled);
		____matrixL1_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___matrixR2' native representation
	if (____matrixR2_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____matrixR2_marshaled);
		____matrixR2_marshaled = NULL;
	}

	return static_cast<bool>(returnValue);
}
// System.Boolean ARNativePlugin::arwLoadOpticalParams(System.String,System.Byte[],System.Int32,System.Single&,System.Single&,System.Single[],System.Single[])
extern "C"  bool ARNativePlugin_arwLoadOpticalParams_m1652819160 (Il2CppObject * __this /* static, unused */, String_t* ___optical_param_name0, ByteU5BU5D_t3397334013* ___optical_param_buff1, int32_t ___optical_param_buffLen2, float* ___fovy_p3, float* ___aspect_p4, SingleU5BU5D_t577127397* ___m5, SingleU5BU5D_t577127397* ___p6, const MethodInfo* method)
{
	typedef int8_t (CDECL *PInvokeFunc) (char*, uint8_t*, int32_t, float*, float*, float*, float*);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(char*) + sizeof(void*) + sizeof(int32_t) + sizeof(float*) + sizeof(float*) + sizeof(void*) + sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("ARWrapper"), "arwLoadOpticalParams", IL2CPP_CALL_C, CHARSET_ANSI, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'arwLoadOpticalParams'"));
		}
	}

	// Marshaling of parameter '___optical_param_name0' to native representation
	char* ____optical_param_name0_marshaled = NULL;
	____optical_param_name0_marshaled = il2cpp_codegen_marshal_string(___optical_param_name0);

	// Marshaling of parameter '___optical_param_buff1' to native representation
	uint8_t* ____optical_param_buff1_marshaled = NULL;
	if (___optical_param_buff1 != NULL)
	{
		____optical_param_buff1_marshaled = reinterpret_cast<uint8_t*>((___optical_param_buff1)->GetAddressAtUnchecked(0));
	}

	// Marshaling of parameter '___fovy_p3' to native representation
	float ____fovy_p3_empty = 0.0f;

	// Marshaling of parameter '___aspect_p4' to native representation
	float ____aspect_p4_empty = 0.0f;

	// Marshaling of parameter '___m5' to native representation
	float* ____m5_marshaled = NULL;
	if (___m5 != NULL)
	{
		int32_t ____m5_Length = (___m5)->max_length;
		____m5_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____m5_Length);
		for (int32_t i = 0; i < ____m5_Length; i++)
		{
			(____m5_marshaled)[i] = (___m5)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____m5_marshaled = NULL;
	}

	// Marshaling of parameter '___p6' to native representation
	float* ____p6_marshaled = NULL;
	if (___p6 != NULL)
	{
		int32_t ____p6_Length = (___p6)->max_length;
		____p6_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____p6_Length);
		for (int32_t i = 0; i < ____p6_Length; i++)
		{
			(____p6_marshaled)[i] = (___p6)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____p6_marshaled = NULL;
	}

	// Native function invocation
	int8_t returnValue = il2cppPInvokeFunc(____optical_param_name0_marshaled, ____optical_param_buff1_marshaled, ___optical_param_buffLen2, &____fovy_p3_empty, &____aspect_p4_empty, ____m5_marshaled, ____p6_marshaled);

	// Marshaling cleanup of parameter '___optical_param_name0' native representation
	il2cpp_codegen_marshal_free(____optical_param_name0_marshaled);
	____optical_param_name0_marshaled = NULL;

	// Marshaling of parameter '___fovy_p3' back from native representation
	*___fovy_p3 = ____fovy_p3_empty;

	// Marshaling of parameter '___aspect_p4' back from native representation
	*___aspect_p4 = ____aspect_p4_empty;

	// Marshaling cleanup of parameter '___m5' native representation
	if (____m5_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____m5_marshaled);
		____m5_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___p6' native representation
	if (____p6_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____p6_marshaled);
		____p6_marshaled = NULL;
	}

	return static_cast<bool>(returnValue);
}
extern "C" void DEFAULT_CALL aruRequestCamera();
// System.Void ARNativePluginStatic::aruRequestCamera()
extern "C"  void ARNativePluginStatic_aruRequestCamera_m4285056894 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(aruRequestCamera)();

}
extern "C" void DEFAULT_CALL arwRegisterLogCallback(Il2CppMethodPointer);
// System.Void ARNativePluginStatic::arwRegisterLogCallback(PluginFunctions/LogCallback)
extern "C"  void ARNativePluginStatic_arwRegisterLogCallback_m2696023928 (Il2CppObject * __this /* static, unused */, LogCallback_t2143553514 * ___callback0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer);

	// Marshaling of parameter '___callback0' to native representation
	Il2CppMethodPointer ____callback0_marshaled = NULL;
	____callback0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback0));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwRegisterLogCallback)(____callback0_marshaled);

}
extern "C" void DEFAULT_CALL arwSetLogLevel(int32_t);
// System.Void ARNativePluginStatic::arwSetLogLevel(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetLogLevel_m3738585053 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwSetLogLevel)(___logLevel0);

}
extern "C" int8_t DEFAULT_CALL arwInitialiseAR();
// System.Boolean ARNativePluginStatic::arwInitialiseAR()
extern "C"  bool ARNativePluginStatic_arwInitialiseAR_m3292125414 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwInitialiseAR)();

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwInitialiseARWithOptions(int32_t, int32_t);
// System.Boolean ARNativePluginStatic::arwInitialiseARWithOptions(System.Int32,System.Int32)
extern "C"  bool ARNativePluginStatic_arwInitialiseARWithOptions_m4273179010 (Il2CppObject * __this /* static, unused */, int32_t ___pattSize0, int32_t ___pattCountMax1, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwInitialiseARWithOptions)(___pattSize0, ___pattCountMax1);

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwGetARToolKitVersion(char*, int32_t);
// System.Boolean ARNativePluginStatic::arwGetARToolKitVersion(System.Text.StringBuilder,System.Int32)
extern "C"  bool ARNativePluginStatic_arwGetARToolKitVersion_m3283193640 (Il2CppObject * __this /* static, unused */, StringBuilder_t1221177846 * ___buffer0, int32_t ___length1, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (char*, int32_t);

	// Marshaling of parameter '___buffer0' to native representation
	char* ____buffer0_marshaled = NULL;
	____buffer0_marshaled = il2cpp_codegen_marshal_string_builder(___buffer0);

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetARToolKitVersion)(____buffer0_marshaled, ___length1);

	// Marshaling of parameter '___buffer0' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___buffer0, ____buffer0_marshaled);

	// Marshaling cleanup of parameter '___buffer0' native representation
	il2cpp_codegen_marshal_free(____buffer0_marshaled);
	____buffer0_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
extern "C" int32_t DEFAULT_CALL arwGetError();
// System.Int32 ARNativePluginStatic::arwGetError()
extern "C"  int32_t ARNativePluginStatic_arwGetError_m1131212540 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetError)();

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL arwShutdownAR();
// System.Boolean ARNativePluginStatic::arwShutdownAR()
extern "C"  bool ARNativePluginStatic_arwShutdownAR_m3238479991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwShutdownAR)();

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwStartRunningB(char*, uint8_t*, int32_t, float, float);
// System.Boolean ARNativePluginStatic::arwStartRunningB(System.String,System.Byte[],System.Int32,System.Single,System.Single)
extern "C"  bool ARNativePluginStatic_arwStartRunningB_m1273829661 (Il2CppObject * __this /* static, unused */, String_t* ___vconf0, ByteU5BU5D_t3397334013* ___cparaBuff1, int32_t ___cparaBuffLen2, float ___nearPlane3, float ___farPlane4, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (char*, uint8_t*, int32_t, float, float);

	// Marshaling of parameter '___vconf0' to native representation
	char* ____vconf0_marshaled = NULL;
	____vconf0_marshaled = il2cpp_codegen_marshal_string(___vconf0);

	// Marshaling of parameter '___cparaBuff1' to native representation
	uint8_t* ____cparaBuff1_marshaled = NULL;
	if (___cparaBuff1 != NULL)
	{
		____cparaBuff1_marshaled = reinterpret_cast<uint8_t*>((___cparaBuff1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwStartRunningB)(____vconf0_marshaled, ____cparaBuff1_marshaled, ___cparaBuffLen2, ___nearPlane3, ___farPlane4);

	// Marshaling cleanup of parameter '___vconf0' native representation
	il2cpp_codegen_marshal_free(____vconf0_marshaled);
	____vconf0_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwStartRunningStereoB(char*, uint8_t*, int32_t, char*, uint8_t*, int32_t, uint8_t*, int32_t, float, float);
// System.Boolean ARNativePluginStatic::arwStartRunningStereoB(System.String,System.Byte[],System.Int32,System.String,System.Byte[],System.Int32,System.Byte[],System.Int32,System.Single,System.Single)
extern "C"  bool ARNativePluginStatic_arwStartRunningStereoB_m4213855497 (Il2CppObject * __this /* static, unused */, String_t* ___vconfL0, ByteU5BU5D_t3397334013* ___cparaBuffL1, int32_t ___cparaBuffLenL2, String_t* ___vconfR3, ByteU5BU5D_t3397334013* ___cparaBuffR4, int32_t ___cparaBuffLenR5, ByteU5BU5D_t3397334013* ___transL2RBuff6, int32_t ___transL2RBuffLen7, float ___nearPlane8, float ___farPlane9, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (char*, uint8_t*, int32_t, char*, uint8_t*, int32_t, uint8_t*, int32_t, float, float);

	// Marshaling of parameter '___vconfL0' to native representation
	char* ____vconfL0_marshaled = NULL;
	____vconfL0_marshaled = il2cpp_codegen_marshal_string(___vconfL0);

	// Marshaling of parameter '___cparaBuffL1' to native representation
	uint8_t* ____cparaBuffL1_marshaled = NULL;
	if (___cparaBuffL1 != NULL)
	{
		____cparaBuffL1_marshaled = reinterpret_cast<uint8_t*>((___cparaBuffL1)->GetAddressAtUnchecked(0));
	}

	// Marshaling of parameter '___vconfR3' to native representation
	char* ____vconfR3_marshaled = NULL;
	____vconfR3_marshaled = il2cpp_codegen_marshal_string(___vconfR3);

	// Marshaling of parameter '___cparaBuffR4' to native representation
	uint8_t* ____cparaBuffR4_marshaled = NULL;
	if (___cparaBuffR4 != NULL)
	{
		____cparaBuffR4_marshaled = reinterpret_cast<uint8_t*>((___cparaBuffR4)->GetAddressAtUnchecked(0));
	}

	// Marshaling of parameter '___transL2RBuff6' to native representation
	uint8_t* ____transL2RBuff6_marshaled = NULL;
	if (___transL2RBuff6 != NULL)
	{
		____transL2RBuff6_marshaled = reinterpret_cast<uint8_t*>((___transL2RBuff6)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwStartRunningStereoB)(____vconfL0_marshaled, ____cparaBuffL1_marshaled, ___cparaBuffLenL2, ____vconfR3_marshaled, ____cparaBuffR4_marshaled, ___cparaBuffLenR5, ____transL2RBuff6_marshaled, ___transL2RBuffLen7, ___nearPlane8, ___farPlane9);

	// Marshaling cleanup of parameter '___vconfL0' native representation
	il2cpp_codegen_marshal_free(____vconfL0_marshaled);
	____vconfL0_marshaled = NULL;

	// Marshaling cleanup of parameter '___vconfR3' native representation
	il2cpp_codegen_marshal_free(____vconfR3_marshaled);
	____vconfR3_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwIsRunning();
// System.Boolean ARNativePluginStatic::arwIsRunning()
extern "C"  bool ARNativePluginStatic_arwIsRunning_m3525939861 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwIsRunning)();

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwStopRunning();
// System.Boolean ARNativePluginStatic::arwStopRunning()
extern "C"  bool ARNativePluginStatic_arwStopRunning_m3225889991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwStopRunning)();

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwGetProjectionMatrix(float*);
// System.Boolean ARNativePluginStatic::arwGetProjectionMatrix(System.Single[])
extern "C"  bool ARNativePluginStatic_arwGetProjectionMatrix_m1707584415 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___matrix0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (float*);

	// Marshaling of parameter '___matrix0' to native representation
	float* ____matrix0_marshaled = NULL;
	if (___matrix0 != NULL)
	{
		int32_t ____matrix0_Length = (___matrix0)->max_length;
		____matrix0_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____matrix0_Length);
		for (int32_t i = 0; i < ____matrix0_Length; i++)
		{
			(____matrix0_marshaled)[i] = (___matrix0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____matrix0_marshaled = NULL;
	}

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetProjectionMatrix)(____matrix0_marshaled);

	// Marshaling cleanup of parameter '___matrix0' native representation
	if (____matrix0_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____matrix0_marshaled);
		____matrix0_marshaled = NULL;
	}

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwGetProjectionMatrixStereo(float*, float*);
// System.Boolean ARNativePluginStatic::arwGetProjectionMatrixStereo(System.Single[],System.Single[])
extern "C"  bool ARNativePluginStatic_arwGetProjectionMatrixStereo_m1572723186 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___matrixL0, SingleU5BU5D_t577127397* ___matrixR1, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (float*, float*);

	// Marshaling of parameter '___matrixL0' to native representation
	float* ____matrixL0_marshaled = NULL;
	if (___matrixL0 != NULL)
	{
		int32_t ____matrixL0_Length = (___matrixL0)->max_length;
		____matrixL0_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____matrixL0_Length);
		for (int32_t i = 0; i < ____matrixL0_Length; i++)
		{
			(____matrixL0_marshaled)[i] = (___matrixL0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____matrixL0_marshaled = NULL;
	}

	// Marshaling of parameter '___matrixR1' to native representation
	float* ____matrixR1_marshaled = NULL;
	if (___matrixR1 != NULL)
	{
		int32_t ____matrixR1_Length = (___matrixR1)->max_length;
		____matrixR1_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____matrixR1_Length);
		for (int32_t i = 0; i < ____matrixR1_Length; i++)
		{
			(____matrixR1_marshaled)[i] = (___matrixR1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____matrixR1_marshaled = NULL;
	}

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetProjectionMatrixStereo)(____matrixL0_marshaled, ____matrixR1_marshaled);

	// Marshaling cleanup of parameter '___matrixL0' native representation
	if (____matrixL0_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____matrixL0_marshaled);
		____matrixL0_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___matrixR1' native representation
	if (____matrixR1_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____matrixR1_marshaled);
		____matrixR1_marshaled = NULL;
	}

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwGetVideoParams(int32_t*, int32_t*, int32_t*, char*, int32_t);
// System.Boolean ARNativePluginStatic::arwGetVideoParams(System.Int32&,System.Int32&,System.Int32&,System.Text.StringBuilder,System.Int32)
extern "C"  bool ARNativePluginStatic_arwGetVideoParams_m804941317 (Il2CppObject * __this /* static, unused */, int32_t* ___width0, int32_t* ___height1, int32_t* ___pixelSize2, StringBuilder_t1221177846 * ___pixelFormatBuffer3, int32_t ___pixelFormatBufferLen4, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (int32_t*, int32_t*, int32_t*, char*, int32_t);

	// Marshaling of parameter '___width0' to native representation
	int32_t ____width0_empty = 0;

	// Marshaling of parameter '___height1' to native representation
	int32_t ____height1_empty = 0;

	// Marshaling of parameter '___pixelSize2' to native representation
	int32_t ____pixelSize2_empty = 0;

	// Marshaling of parameter '___pixelFormatBuffer3' to native representation
	char* ____pixelFormatBuffer3_marshaled = NULL;
	____pixelFormatBuffer3_marshaled = il2cpp_codegen_marshal_string_builder(___pixelFormatBuffer3);

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetVideoParams)(&____width0_empty, &____height1_empty, &____pixelSize2_empty, ____pixelFormatBuffer3_marshaled, ___pixelFormatBufferLen4);

	// Marshaling of parameter '___width0' back from native representation
	*___width0 = ____width0_empty;

	// Marshaling of parameter '___height1' back from native representation
	*___height1 = ____height1_empty;

	// Marshaling of parameter '___pixelSize2' back from native representation
	*___pixelSize2 = ____pixelSize2_empty;

	// Marshaling of parameter '___pixelFormatBuffer3' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___pixelFormatBuffer3, ____pixelFormatBuffer3_marshaled);

	// Marshaling cleanup of parameter '___pixelFormatBuffer3' native representation
	il2cpp_codegen_marshal_free(____pixelFormatBuffer3_marshaled);
	____pixelFormatBuffer3_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwGetVideoParamsStereo(int32_t*, int32_t*, int32_t*, char*, int32_t, int32_t*, int32_t*, int32_t*, char*, int32_t);
// System.Boolean ARNativePluginStatic::arwGetVideoParamsStereo(System.Int32&,System.Int32&,System.Int32&,System.Text.StringBuilder,System.Int32,System.Int32&,System.Int32&,System.Int32&,System.Text.StringBuilder,System.Int32)
extern "C"  bool ARNativePluginStatic_arwGetVideoParamsStereo_m3904472601 (Il2CppObject * __this /* static, unused */, int32_t* ___widthL0, int32_t* ___heightL1, int32_t* ___pixelSizeL2, StringBuilder_t1221177846 * ___pixelFormatBufferL3, int32_t ___pixelFormatBufferLenL4, int32_t* ___widthR5, int32_t* ___heightR6, int32_t* ___pixelSizeR7, StringBuilder_t1221177846 * ___pixelFormatBufferR8, int32_t ___pixelFormatBufferLenR9, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (int32_t*, int32_t*, int32_t*, char*, int32_t, int32_t*, int32_t*, int32_t*, char*, int32_t);

	// Marshaling of parameter '___widthL0' to native representation
	int32_t ____widthL0_empty = 0;

	// Marshaling of parameter '___heightL1' to native representation
	int32_t ____heightL1_empty = 0;

	// Marshaling of parameter '___pixelSizeL2' to native representation
	int32_t ____pixelSizeL2_empty = 0;

	// Marshaling of parameter '___pixelFormatBufferL3' to native representation
	char* ____pixelFormatBufferL3_marshaled = NULL;
	____pixelFormatBufferL3_marshaled = il2cpp_codegen_marshal_string_builder(___pixelFormatBufferL3);

	// Marshaling of parameter '___widthR5' to native representation
	int32_t ____widthR5_empty = 0;

	// Marshaling of parameter '___heightR6' to native representation
	int32_t ____heightR6_empty = 0;

	// Marshaling of parameter '___pixelSizeR7' to native representation
	int32_t ____pixelSizeR7_empty = 0;

	// Marshaling of parameter '___pixelFormatBufferR8' to native representation
	char* ____pixelFormatBufferR8_marshaled = NULL;
	____pixelFormatBufferR8_marshaled = il2cpp_codegen_marshal_string_builder(___pixelFormatBufferR8);

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetVideoParamsStereo)(&____widthL0_empty, &____heightL1_empty, &____pixelSizeL2_empty, ____pixelFormatBufferL3_marshaled, ___pixelFormatBufferLenL4, &____widthR5_empty, &____heightR6_empty, &____pixelSizeR7_empty, ____pixelFormatBufferR8_marshaled, ___pixelFormatBufferLenR9);

	// Marshaling of parameter '___widthL0' back from native representation
	*___widthL0 = ____widthL0_empty;

	// Marshaling of parameter '___heightL1' back from native representation
	*___heightL1 = ____heightL1_empty;

	// Marshaling of parameter '___pixelSizeL2' back from native representation
	*___pixelSizeL2 = ____pixelSizeL2_empty;

	// Marshaling of parameter '___pixelFormatBufferL3' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___pixelFormatBufferL3, ____pixelFormatBufferL3_marshaled);

	// Marshaling cleanup of parameter '___pixelFormatBufferL3' native representation
	il2cpp_codegen_marshal_free(____pixelFormatBufferL3_marshaled);
	____pixelFormatBufferL3_marshaled = NULL;

	// Marshaling of parameter '___widthR5' back from native representation
	*___widthR5 = ____widthR5_empty;

	// Marshaling of parameter '___heightR6' back from native representation
	*___heightR6 = ____heightR6_empty;

	// Marshaling of parameter '___pixelSizeR7' back from native representation
	*___pixelSizeR7 = ____pixelSizeR7_empty;

	// Marshaling of parameter '___pixelFormatBufferR8' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___pixelFormatBufferR8, ____pixelFormatBufferR8_marshaled);

	// Marshaling cleanup of parameter '___pixelFormatBufferR8' native representation
	il2cpp_codegen_marshal_free(____pixelFormatBufferR8_marshaled);
	____pixelFormatBufferR8_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwCapture();
// System.Boolean ARNativePluginStatic::arwCapture()
extern "C"  bool ARNativePluginStatic_arwCapture_m3910463490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwCapture)();

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwUpdateAR();
// System.Boolean ARNativePluginStatic::arwUpdateAR()
extern "C"  bool ARNativePluginStatic_arwUpdateAR_m1910658788 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwUpdateAR)();

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwUpdateTexture(intptr_t);
// System.Boolean ARNativePluginStatic::arwUpdateTexture(System.IntPtr)
extern "C"  bool ARNativePluginStatic_arwUpdateTexture_m1158379942 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colors0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwUpdateTexture)(reinterpret_cast<intptr_t>((___colors0).get_m_value_0()));

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwUpdateTextureStereo(intptr_t, intptr_t);
// System.Boolean ARNativePluginStatic::arwUpdateTextureStereo(System.IntPtr,System.IntPtr)
extern "C"  bool ARNativePluginStatic_arwUpdateTextureStereo_m693222146 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colorsL0, IntPtr_t ___colorsR1, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t);

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwUpdateTextureStereo)(reinterpret_cast<intptr_t>((___colorsL0).get_m_value_0()), reinterpret_cast<intptr_t>((___colorsR1).get_m_value_0()));

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwUpdateTexture32(intptr_t);
// System.Boolean ARNativePluginStatic::arwUpdateTexture32(System.IntPtr)
extern "C"  bool ARNativePluginStatic_arwUpdateTexture32_m185359189 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colors320, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwUpdateTexture32)(reinterpret_cast<intptr_t>((___colors320).get_m_value_0()));

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwUpdateTexture32Stereo(intptr_t, intptr_t);
// System.Boolean ARNativePluginStatic::arwUpdateTexture32Stereo(System.IntPtr,System.IntPtr)
extern "C"  bool ARNativePluginStatic_arwUpdateTexture32Stereo_m813074921 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colors32L0, IntPtr_t ___colors32R1, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t);

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwUpdateTexture32Stereo)(reinterpret_cast<intptr_t>((___colors32L0).get_m_value_0()), reinterpret_cast<intptr_t>((___colors32R1).get_m_value_0()));

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwUpdateTextureGL(int32_t);
// System.Boolean ARNativePluginStatic::arwUpdateTextureGL(System.Int32)
extern "C"  bool ARNativePluginStatic_arwUpdateTextureGL_m562093018 (Il2CppObject * __this /* static, unused */, int32_t ___textureID0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwUpdateTextureGL)(___textureID0);

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwUpdateTextureGLStereo(int32_t, int32_t);
// System.Boolean ARNativePluginStatic::arwUpdateTextureGLStereo(System.Int32,System.Int32)
extern "C"  bool ARNativePluginStatic_arwUpdateTextureGLStereo_m3176194339 (Il2CppObject * __this /* static, unused */, int32_t ___textureID_L0, int32_t ___textureID_R1, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwUpdateTextureGLStereo)(___textureID_L0, ___textureID_R1);

	return static_cast<bool>(returnValue);
}
extern "C" void DEFAULT_CALL arwSetUnityRenderEventUpdateTextureGLTextureID(int32_t);
// System.Void ARNativePluginStatic::arwSetUnityRenderEventUpdateTextureGLTextureID(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetUnityRenderEventUpdateTextureGLTextureID_m3208451437 (Il2CppObject * __this /* static, unused */, int32_t ___textureID0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwSetUnityRenderEventUpdateTextureGLTextureID)(___textureID0);

}
extern "C" void DEFAULT_CALL arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs(int32_t, int32_t);
// System.Void ARNativePluginStatic::arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs(System.Int32,System.Int32)
extern "C"  void ARNativePluginStatic_arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs_m2332439057 (Il2CppObject * __this /* static, unused */, int32_t ___textureID_L0, int32_t ___textureID_R1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs)(___textureID_L0, ___textureID_R1);

}
extern "C" int32_t DEFAULT_CALL arwGetMarkerPatternCount(int32_t);
// System.Int32 ARNativePluginStatic::arwGetMarkerPatternCount(System.Int32)
extern "C"  int32_t ARNativePluginStatic_arwGetMarkerPatternCount_m2534201878 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetMarkerPatternCount)(___markerID0);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL arwGetMarkerPatternConfig(int32_t, int32_t, float*, float*, float*, int32_t*, int32_t*);
// System.Boolean ARNativePluginStatic::arwGetMarkerPatternConfig(System.Int32,System.Int32,System.Single[],System.Single&,System.Single&,System.Int32&,System.Int32&)
extern "C"  bool ARNativePluginStatic_arwGetMarkerPatternConfig_m978360953 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___patternID1, SingleU5BU5D_t577127397* ___matrix2, float* ___width3, float* ___height4, int32_t* ___imageSizeX5, int32_t* ___imageSizeY6, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, float*, float*, float*, int32_t*, int32_t*);

	// Marshaling of parameter '___matrix2' to native representation
	float* ____matrix2_marshaled = NULL;
	if (___matrix2 != NULL)
	{
		int32_t ____matrix2_Length = (___matrix2)->max_length;
		____matrix2_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____matrix2_Length);
		for (int32_t i = 0; i < ____matrix2_Length; i++)
		{
			(____matrix2_marshaled)[i] = (___matrix2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____matrix2_marshaled = NULL;
	}

	// Marshaling of parameter '___width3' to native representation
	float ____width3_empty = 0.0f;

	// Marshaling of parameter '___height4' to native representation
	float ____height4_empty = 0.0f;

	// Marshaling of parameter '___imageSizeX5' to native representation
	int32_t ____imageSizeX5_empty = 0;

	// Marshaling of parameter '___imageSizeY6' to native representation
	int32_t ____imageSizeY6_empty = 0;

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetMarkerPatternConfig)(___markerID0, ___patternID1, ____matrix2_marshaled, &____width3_empty, &____height4_empty, &____imageSizeX5_empty, &____imageSizeY6_empty);

	// Marshaling cleanup of parameter '___matrix2' native representation
	if (____matrix2_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____matrix2_marshaled);
		____matrix2_marshaled = NULL;
	}

	// Marshaling of parameter '___width3' back from native representation
	*___width3 = ____width3_empty;

	// Marshaling of parameter '___height4' back from native representation
	*___height4 = ____height4_empty;

	// Marshaling of parameter '___imageSizeX5' back from native representation
	*___imageSizeX5 = ____imageSizeX5_empty;

	// Marshaling of parameter '___imageSizeY6' back from native representation
	*___imageSizeY6 = ____imageSizeY6_empty;

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwGetMarkerPatternImage(int32_t, int32_t, Color_t2020392075 *);
// System.Boolean ARNativePluginStatic::arwGetMarkerPatternImage(System.Int32,System.Int32,UnityEngine.Color[])
extern "C"  bool ARNativePluginStatic_arwGetMarkerPatternImage_m2239266479 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___patternID1, ColorU5BU5D_t672350442* ___colors2, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, Color_t2020392075 *);

	// Marshaling of parameter '___colors2' to native representation
	Color_t2020392075 * ____colors2_marshaled = NULL;
	if (___colors2 != NULL)
	{
		____colors2_marshaled = reinterpret_cast<Color_t2020392075 *>((___colors2)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetMarkerPatternImage)(___markerID0, ___patternID1, ____colors2_marshaled);

	// Marshaling of parameter '___colors2' back from native representation
	if (____colors2_marshaled != NULL)
	{
		int32_t ____colors2_Length = (___colors2)->max_length;
		for (int32_t i = 0; i < ____colors2_Length; i++)
		{
			(___colors2)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____colors2_marshaled)[i]);
		}
	}

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwGetMarkerOptionBool(int32_t, int32_t);
// System.Boolean ARNativePluginStatic::arwGetMarkerOptionBool(System.Int32,System.Int32)
extern "C"  bool ARNativePluginStatic_arwGetMarkerOptionBool_m535783117 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetMarkerOptionBool)(___markerID0, ___option1);

	return static_cast<bool>(returnValue);
}
extern "C" void DEFAULT_CALL arwSetMarkerOptionBool(int32_t, int32_t, int32_t);
// System.Void ARNativePluginStatic::arwSetMarkerOptionBool(System.Int32,System.Int32,System.Boolean)
extern "C"  void ARNativePluginStatic_arwSetMarkerOptionBool_m2682973062 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, bool ___value2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwSetMarkerOptionBool)(___markerID0, ___option1, static_cast<int32_t>(___value2));

}
extern "C" int32_t DEFAULT_CALL arwGetMarkerOptionInt(int32_t, int32_t);
// System.Int32 ARNativePluginStatic::arwGetMarkerOptionInt(System.Int32,System.Int32)
extern "C"  int32_t ARNativePluginStatic_arwGetMarkerOptionInt_m4131469642 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetMarkerOptionInt)(___markerID0, ___option1);

	return returnValue;
}
extern "C" void DEFAULT_CALL arwSetMarkerOptionInt(int32_t, int32_t, int32_t);
// System.Void ARNativePluginStatic::arwSetMarkerOptionInt(System.Int32,System.Int32,System.Int32)
extern "C"  void ARNativePluginStatic_arwSetMarkerOptionInt_m4268478505 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, int32_t ___value2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwSetMarkerOptionInt)(___markerID0, ___option1, ___value2);

}
extern "C" float DEFAULT_CALL arwGetMarkerOptionFloat(int32_t, int32_t);
// System.Single ARNativePluginStatic::arwGetMarkerOptionFloat(System.Int32,System.Int32)
extern "C"  float ARNativePluginStatic_arwGetMarkerOptionFloat_m2202747965 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(arwGetMarkerOptionFloat)(___markerID0, ___option1);

	return returnValue;
}
extern "C" void DEFAULT_CALL arwSetMarkerOptionFloat(int32_t, int32_t, float);
// System.Void ARNativePluginStatic::arwSetMarkerOptionFloat(System.Int32,System.Int32,System.Single)
extern "C"  void ARNativePluginStatic_arwSetMarkerOptionFloat_m591864046 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, float ___value2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, float);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwSetMarkerOptionFloat)(___markerID0, ___option1, ___value2);

}
extern "C" void DEFAULT_CALL arwSetVideoDebugMode(int32_t);
// System.Void ARNativePluginStatic::arwSetVideoDebugMode(System.Boolean)
extern "C"  void ARNativePluginStatic_arwSetVideoDebugMode_m2725814322 (Il2CppObject * __this /* static, unused */, bool ___debug0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwSetVideoDebugMode)(static_cast<int32_t>(___debug0));

}
extern "C" int8_t DEFAULT_CALL arwGetVideoDebugMode();
// System.Boolean ARNativePluginStatic::arwGetVideoDebugMode()
extern "C"  bool ARNativePluginStatic_arwGetVideoDebugMode_m4059797661 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetVideoDebugMode)();

	return static_cast<bool>(returnValue);
}
extern "C" void DEFAULT_CALL arwSetVideoThreshold(int32_t);
// System.Void ARNativePluginStatic::arwSetVideoThreshold(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetVideoThreshold_m2572009209 (Il2CppObject * __this /* static, unused */, int32_t ___threshold0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwSetVideoThreshold)(___threshold0);

}
extern "C" int32_t DEFAULT_CALL arwGetVideoThreshold();
// System.Int32 ARNativePluginStatic::arwGetVideoThreshold()
extern "C"  int32_t ARNativePluginStatic_arwGetVideoThreshold_m2851652890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetVideoThreshold)();

	return returnValue;
}
extern "C" void DEFAULT_CALL arwSetVideoThresholdMode(int32_t);
// System.Void ARNativePluginStatic::arwSetVideoThresholdMode(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetVideoThresholdMode_m2501069538 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwSetVideoThresholdMode)(___mode0);

}
extern "C" int32_t DEFAULT_CALL arwGetVideoThresholdMode();
// System.Int32 ARNativePluginStatic::arwGetVideoThresholdMode()
extern "C"  int32_t ARNativePluginStatic_arwGetVideoThresholdMode_m3923466573 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetVideoThresholdMode)();

	return returnValue;
}
extern "C" void DEFAULT_CALL arwSetLabelingMode(int32_t);
// System.Void ARNativePluginStatic::arwSetLabelingMode(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetLabelingMode_m1320145026 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwSetLabelingMode)(___mode0);

}
extern "C" int32_t DEFAULT_CALL arwGetLabelingMode();
// System.Int32 ARNativePluginStatic::arwGetLabelingMode()
extern "C"  int32_t ARNativePluginStatic_arwGetLabelingMode_m2403718909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetLabelingMode)();

	return returnValue;
}
extern "C" void DEFAULT_CALL arwSetBorderSize(float);
// System.Void ARNativePluginStatic::arwSetBorderSize(System.Single)
extern "C"  void ARNativePluginStatic_arwSetBorderSize_m3985598462 (Il2CppObject * __this /* static, unused */, float ___mode0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwSetBorderSize)(___mode0);

}
extern "C" float DEFAULT_CALL arwGetBorderSize();
// System.Single ARNativePluginStatic::arwGetBorderSize()
extern "C"  float ARNativePluginStatic_arwGetBorderSize_m402710309 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(arwGetBorderSize)();

	return returnValue;
}
extern "C" void DEFAULT_CALL arwSetPatternDetectionMode(int32_t);
// System.Void ARNativePluginStatic::arwSetPatternDetectionMode(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetPatternDetectionMode_m981787463 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwSetPatternDetectionMode)(___mode0);

}
extern "C" int32_t DEFAULT_CALL arwGetPatternDetectionMode();
// System.Int32 ARNativePluginStatic::arwGetPatternDetectionMode()
extern "C"  int32_t ARNativePluginStatic_arwGetPatternDetectionMode_m2165003022 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetPatternDetectionMode)();

	return returnValue;
}
extern "C" void DEFAULT_CALL arwSetMatrixCodeType(int32_t);
// System.Void ARNativePluginStatic::arwSetMatrixCodeType(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetMatrixCodeType_m1755799257 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwSetMatrixCodeType)(___type0);

}
extern "C" int32_t DEFAULT_CALL arwGetMatrixCodeType();
// System.Int32 ARNativePluginStatic::arwGetMatrixCodeType()
extern "C"  int32_t ARNativePluginStatic_arwGetMatrixCodeType_m1128356520 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetMatrixCodeType)();

	return returnValue;
}
extern "C" void DEFAULT_CALL arwSetImageProcMode(int32_t);
// System.Void ARNativePluginStatic::arwSetImageProcMode(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetImageProcMode_m1469690497 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwSetImageProcMode)(___mode0);

}
extern "C" int32_t DEFAULT_CALL arwGetImageProcMode();
// System.Int32 ARNativePluginStatic::arwGetImageProcMode()
extern "C"  int32_t ARNativePluginStatic_arwGetImageProcMode_m855251164 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetImageProcMode)();

	return returnValue;
}
extern "C" void DEFAULT_CALL arwSetNFTMultiMode(int32_t);
// System.Void ARNativePluginStatic::arwSetNFTMultiMode(System.Boolean)
extern "C"  void ARNativePluginStatic_arwSetNFTMultiMode_m4123606051 (Il2CppObject * __this /* static, unused */, bool ___on0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(arwSetNFTMultiMode)(static_cast<int32_t>(___on0));

}
extern "C" int8_t DEFAULT_CALL arwGetNFTMultiMode();
// System.Boolean ARNativePluginStatic::arwGetNFTMultiMode()
extern "C"  bool ARNativePluginStatic_arwGetNFTMultiMode_m44684326 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwGetNFTMultiMode)();

	return static_cast<bool>(returnValue);
}
extern "C" int32_t DEFAULT_CALL arwAddMarker(char*);
// System.Int32 ARNativePluginStatic::arwAddMarker(System.String)
extern "C"  int32_t ARNativePluginStatic_arwAddMarker_m4198366323 (Il2CppObject * __this /* static, unused */, String_t* ___cfg0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___cfg0' to native representation
	char* ____cfg0_marshaled = NULL;
	____cfg0_marshaled = il2cpp_codegen_marshal_string(___cfg0);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(arwAddMarker)(____cfg0_marshaled);

	// Marshaling cleanup of parameter '___cfg0' native representation
	il2cpp_codegen_marshal_free(____cfg0_marshaled);
	____cfg0_marshaled = NULL;

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL arwRemoveMarker(int32_t);
// System.Boolean ARNativePluginStatic::arwRemoveMarker(System.Int32)
extern "C"  bool ARNativePluginStatic_arwRemoveMarker_m3792023969 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwRemoveMarker)(___markerID0);

	return static_cast<bool>(returnValue);
}
extern "C" int32_t DEFAULT_CALL arwRemoveAllMarkers();
// System.Int32 ARNativePluginStatic::arwRemoveAllMarkers()
extern "C"  int32_t ARNativePluginStatic_arwRemoveAllMarkers_m1950709840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(arwRemoveAllMarkers)();

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL arwQueryMarkerVisibility(int32_t);
// System.Boolean ARNativePluginStatic::arwQueryMarkerVisibility(System.Int32)
extern "C"  bool ARNativePluginStatic_arwQueryMarkerVisibility_m3506306455 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwQueryMarkerVisibility)(___markerID0);

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwQueryMarkerTransformation(int32_t, float*);
// System.Boolean ARNativePluginStatic::arwQueryMarkerTransformation(System.Int32,System.Single[])
extern "C"  bool ARNativePluginStatic_arwQueryMarkerTransformation_m1782821311 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, SingleU5BU5D_t577127397* ___matrix1, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (int32_t, float*);

	// Marshaling of parameter '___matrix1' to native representation
	float* ____matrix1_marshaled = NULL;
	if (___matrix1 != NULL)
	{
		int32_t ____matrix1_Length = (___matrix1)->max_length;
		____matrix1_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____matrix1_Length);
		for (int32_t i = 0; i < ____matrix1_Length; i++)
		{
			(____matrix1_marshaled)[i] = (___matrix1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____matrix1_marshaled = NULL;
	}

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwQueryMarkerTransformation)(___markerID0, ____matrix1_marshaled);

	// Marshaling cleanup of parameter '___matrix1' native representation
	if (____matrix1_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____matrix1_marshaled);
		____matrix1_marshaled = NULL;
	}

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwQueryMarkerTransformationStereo(int32_t, float*, float*);
// System.Boolean ARNativePluginStatic::arwQueryMarkerTransformationStereo(System.Int32,System.Single[],System.Single[])
extern "C"  bool ARNativePluginStatic_arwQueryMarkerTransformationStereo_m2434214006 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, SingleU5BU5D_t577127397* ___matrixL1, SingleU5BU5D_t577127397* ___matrixR2, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (int32_t, float*, float*);

	// Marshaling of parameter '___matrixL1' to native representation
	float* ____matrixL1_marshaled = NULL;
	if (___matrixL1 != NULL)
	{
		int32_t ____matrixL1_Length = (___matrixL1)->max_length;
		____matrixL1_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____matrixL1_Length);
		for (int32_t i = 0; i < ____matrixL1_Length; i++)
		{
			(____matrixL1_marshaled)[i] = (___matrixL1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____matrixL1_marshaled = NULL;
	}

	// Marshaling of parameter '___matrixR2' to native representation
	float* ____matrixR2_marshaled = NULL;
	if (___matrixR2 != NULL)
	{
		int32_t ____matrixR2_Length = (___matrixR2)->max_length;
		____matrixR2_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____matrixR2_Length);
		for (int32_t i = 0; i < ____matrixR2_Length; i++)
		{
			(____matrixR2_marshaled)[i] = (___matrixR2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____matrixR2_marshaled = NULL;
	}

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwQueryMarkerTransformationStereo)(___markerID0, ____matrixL1_marshaled, ____matrixR2_marshaled);

	// Marshaling cleanup of parameter '___matrixL1' native representation
	if (____matrixL1_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____matrixL1_marshaled);
		____matrixL1_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___matrixR2' native representation
	if (____matrixR2_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____matrixR2_marshaled);
		____matrixR2_marshaled = NULL;
	}

	return static_cast<bool>(returnValue);
}
extern "C" int8_t DEFAULT_CALL arwLoadOpticalParams(char*, uint8_t*, int32_t, float*, float*, float*, float*);
// System.Boolean ARNativePluginStatic::arwLoadOpticalParams(System.String,System.Byte[],System.Int32,System.Single&,System.Single&,System.Single[],System.Single[])
extern "C"  bool ARNativePluginStatic_arwLoadOpticalParams_m1348665490 (Il2CppObject * __this /* static, unused */, String_t* ___optical_param_name0, ByteU5BU5D_t3397334013* ___optical_param_buff1, int32_t ___optical_param_buffLen2, float* ___fovy_p3, float* ___aspect_p4, SingleU5BU5D_t577127397* ___m5, SingleU5BU5D_t577127397* ___p6, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (char*, uint8_t*, int32_t, float*, float*, float*, float*);

	// Marshaling of parameter '___optical_param_name0' to native representation
	char* ____optical_param_name0_marshaled = NULL;
	____optical_param_name0_marshaled = il2cpp_codegen_marshal_string(___optical_param_name0);

	// Marshaling of parameter '___optical_param_buff1' to native representation
	uint8_t* ____optical_param_buff1_marshaled = NULL;
	if (___optical_param_buff1 != NULL)
	{
		____optical_param_buff1_marshaled = reinterpret_cast<uint8_t*>((___optical_param_buff1)->GetAddressAtUnchecked(0));
	}

	// Marshaling of parameter '___fovy_p3' to native representation
	float ____fovy_p3_empty = 0.0f;

	// Marshaling of parameter '___aspect_p4' to native representation
	float ____aspect_p4_empty = 0.0f;

	// Marshaling of parameter '___m5' to native representation
	float* ____m5_marshaled = NULL;
	if (___m5 != NULL)
	{
		int32_t ____m5_Length = (___m5)->max_length;
		____m5_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____m5_Length);
		for (int32_t i = 0; i < ____m5_Length; i++)
		{
			(____m5_marshaled)[i] = (___m5)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____m5_marshaled = NULL;
	}

	// Marshaling of parameter '___p6' to native representation
	float* ____p6_marshaled = NULL;
	if (___p6 != NULL)
	{
		int32_t ____p6_Length = (___p6)->max_length;
		____p6_marshaled = il2cpp_codegen_marshal_allocate_array<float>(____p6_Length);
		for (int32_t i = 0; i < ____p6_Length; i++)
		{
			(____p6_marshaled)[i] = (___p6)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		____p6_marshaled = NULL;
	}

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(arwLoadOpticalParams)(____optical_param_name0_marshaled, ____optical_param_buff1_marshaled, ___optical_param_buffLen2, &____fovy_p3_empty, &____aspect_p4_empty, ____m5_marshaled, ____p6_marshaled);

	// Marshaling cleanup of parameter '___optical_param_name0' native representation
	il2cpp_codegen_marshal_free(____optical_param_name0_marshaled);
	____optical_param_name0_marshaled = NULL;

	// Marshaling of parameter '___fovy_p3' back from native representation
	*___fovy_p3 = ____fovy_p3_empty;

	// Marshaling of parameter '___aspect_p4' back from native representation
	*___aspect_p4 = ____aspect_p4_empty;

	// Marshaling cleanup of parameter '___m5' native representation
	if (____m5_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____m5_marshaled);
		____m5_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___p6' native representation
	if (____p6_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____p6_marshaled);
		____p6_marshaled = NULL;
	}

	return static_cast<bool>(returnValue);
}
// System.Void AROrigin::.ctor()
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t923381855_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m871114666_MethodInfo_var;
extern const uint32_t AROrigin__ctor_m3064099192_MetadataUsageId;
extern "C"  void AROrigin__ctor_m3064099192 (AROrigin_t3335349585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AROrigin__ctor_m3064099192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_findMarkerTags_3(L_0);
		List_1_t923381855 * L_1 = (List_1_t923381855 *)il2cpp_codegen_object_new(List_1_t923381855_il2cpp_TypeInfo_var);
		List_1__ctor_m871114666(L_1, /*hidden argument*/List_1__ctor_m871114666_MethodInfo_var);
		__this->set_markersEligibleForBaseMarker_5(L_1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// AROrigin/FindMode AROrigin::get_findMarkerMode()
extern "C"  int32_t AROrigin_get_findMarkerMode_m3132928938 (AROrigin_t3335349585 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__findMarkerMode_6();
		return L_0;
	}
}
// System.Void AROrigin::set_findMarkerMode(AROrigin/FindMode)
extern "C"  void AROrigin_set_findMarkerMode_m3601119679 (AROrigin_t3335349585 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__findMarkerMode_6();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_2 = ___value0;
		__this->set__findMarkerMode_6(L_2);
		AROrigin_FindMarkers_m3022729598(__this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void AROrigin::AddMarker(ARMarker,System.Boolean)
extern const MethodInfo* List_1_Add_m16401390_MethodInfo_var;
extern const MethodInfo* List_1_Insert_m1110070819_MethodInfo_var;
extern const uint32_t AROrigin_AddMarker_m1409079797_MetadataUsageId;
extern "C"  void AROrigin_AddMarker_m1409079797 (AROrigin_t3335349585 * __this, ARMarker_t1554260723 * ___marker0, bool ___atHeadOfList1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AROrigin_AddMarker_m1409079797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___atHeadOfList1;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		List_1_t923381855 * L_1 = __this->get_markersEligibleForBaseMarker_5();
		ARMarker_t1554260723 * L_2 = ___marker0;
		NullCheck(L_1);
		List_1_Add_m16401390(L_1, L_2, /*hidden argument*/List_1_Add_m16401390_MethodInfo_var);
		goto IL_0024;
	}

IL_0017:
	{
		List_1_t923381855 * L_3 = __this->get_markersEligibleForBaseMarker_5();
		ARMarker_t1554260723 * L_4 = ___marker0;
		NullCheck(L_3);
		List_1_Insert_m1110070819(L_3, 0, L_4, /*hidden argument*/List_1_Insert_m1110070819_MethodInfo_var);
	}

IL_0024:
	{
		return;
	}
}
// System.Boolean AROrigin::RemoveMarker(ARMarker)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Remove_m2621825223_MethodInfo_var;
extern const uint32_t AROrigin_RemoveMarker_m1452674393_MetadataUsageId;
extern "C"  bool AROrigin_RemoveMarker_m1452674393 (AROrigin_t3335349585 * __this, ARMarker_t1554260723 * ___marker0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AROrigin_RemoveMarker_m1452674393_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ARMarker_t1554260723 * L_0 = __this->get_baseMarker_4();
		ARMarker_t1554260723 * L_1 = ___marker0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		__this->set_baseMarker_4((ARMarker_t1554260723 *)NULL);
	}

IL_0018:
	{
		List_1_t923381855 * L_3 = __this->get_markersEligibleForBaseMarker_5();
		ARMarker_t1554260723 * L_4 = ___marker0;
		NullCheck(L_3);
		bool L_5 = List_1_Remove_m2621825223(L_3, L_4, /*hidden argument*/List_1_Remove_m2621825223_MethodInfo_var);
		return L_5;
	}
}
// System.Void AROrigin::RemoveAllMarkers()
extern const MethodInfo* List_1_Clear_m4072441733_MethodInfo_var;
extern const uint32_t AROrigin_RemoveAllMarkers_m3158628672_MetadataUsageId;
extern "C"  void AROrigin_RemoveAllMarkers_m3158628672 (AROrigin_t3335349585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AROrigin_RemoveAllMarkers_m3158628672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_baseMarker_4((ARMarker_t1554260723 *)NULL);
		List_1_t923381855 * L_0 = __this->get_markersEligibleForBaseMarker_5();
		NullCheck(L_0);
		List_1_Clear_m4072441733(L_0, /*hidden argument*/List_1_Clear_m4072441733_MethodInfo_var);
		return;
	}
}
// System.Void AROrigin::FindMarkers()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectsOfType_TisARMarker_t1554260723_m2341901897_MethodInfo_var;
extern const MethodInfo* List_1_Contains_m2369280605_MethodInfo_var;
extern const MethodInfo* List_1_Add_m16401390_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m586089396_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2977222119;
extern Il2CppCodeGenString* _stringLiteral2206127825;
extern const uint32_t AROrigin_FindMarkers_m3022729598_MetadataUsageId;
extern "C"  void AROrigin_FindMarkers_m3022729598 (AROrigin_t3335349585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AROrigin_FindMarkers_m3022729598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARMarkerU5BU5D_t3553995746* V_0 = NULL;
	ARMarker_t1554260723 * V_1 = NULL;
	ARMarkerU5BU5D_t3553995746* V_2 = NULL;
	int32_t V_3 = 0;
	{
		AROrigin_RemoveAllMarkers_m3158628672(__this, /*hidden argument*/NULL);
		int32_t L_0 = AROrigin_get_findMarkerMode_m3132928938(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_008f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ARMarkerU5BU5D_t3553995746* L_1 = Object_FindObjectsOfType_TisARMarker_t1554260723_m2341901897(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectsOfType_TisARMarker_t1554260723_m2341901897_MethodInfo_var);
		V_0 = L_1;
		ARMarkerU5BU5D_t3553995746* L_2 = V_0;
		V_2 = L_2;
		V_3 = 0;
		goto IL_0062;
	}

IL_0021:
	{
		ARMarkerU5BU5D_t3553995746* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		ARMarker_t1554260723 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		int32_t L_7 = AROrigin_get_findMarkerMode_m3132928938(__this, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_8 = AROrigin_get_findMarkerMode_m3132928938(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)1))))
		{
			goto IL_005e;
		}
	}
	{
		List_1_t1398341365 * L_9 = __this->get_findMarkerTags_3();
		ARMarker_t1554260723 * L_10 = V_1;
		NullCheck(L_10);
		String_t* L_11 = L_10->get_Tag_7();
		NullCheck(L_9);
		bool L_12 = List_1_Contains_m2369280605(L_9, L_11, /*hidden argument*/List_1_Contains_m2369280605_MethodInfo_var);
		if (!L_12)
		{
			goto IL_005e;
		}
	}

IL_0052:
	{
		List_1_t923381855 * L_13 = __this->get_markersEligibleForBaseMarker_5();
		ARMarker_t1554260723 * L_14 = V_1;
		NullCheck(L_13);
		List_1_Add_m16401390(L_13, L_14, /*hidden argument*/List_1_Add_m16401390_MethodInfo_var);
	}

IL_005e:
	{
		int32_t L_15 = V_3;
		V_3 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0062:
	{
		int32_t L_16 = V_3;
		ARMarkerU5BU5D_t3553995746* L_17 = V_2;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_0021;
		}
	}
	{
		List_1_t923381855 * L_18 = __this->get_markersEligibleForBaseMarker_5();
		NullCheck(L_18);
		int32_t L_19 = List_1_get_Count_m586089396(L_18, /*hidden argument*/List_1_get_Count_m586089396_MethodInfo_var);
		int32_t L_20 = L_19;
		Il2CppObject * L_21 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral2977222119, L_21, _stringLiteral2206127825, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
	}

IL_008f:
	{
		return;
	}
}
// System.Void AROrigin::Start()
extern "C"  void AROrigin_Start_m3396998224 (AROrigin_t3335349585 * __this, const MethodInfo* method)
{
	{
		AROrigin_FindMarkers_m3022729598(__this, /*hidden argument*/NULL);
		return;
	}
}
// ARMarker AROrigin::GetBaseMarker()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4219110609_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4012250305_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2323311869_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m555758363_MethodInfo_var;
extern const uint32_t AROrigin_GetBaseMarker_m4103772195_MetadataUsageId;
extern "C"  ARMarker_t1554260723 * AROrigin_GetBaseMarker_m4103772195 (AROrigin_t3335349585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AROrigin_GetBaseMarker_m4103772195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARMarker_t1554260723 * V_0 = NULL;
	Enumerator_t458111529  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ARMarker_t1554260723 * L_0 = __this->get_baseMarker_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		ARMarker_t1554260723 * L_2 = __this->get_baseMarker_4();
		NullCheck(L_2);
		bool L_3 = ARMarker_get_Visible_m4014965141(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		ARMarker_t1554260723 * L_4 = __this->get_baseMarker_4();
		return L_4;
	}

IL_0028:
	{
		__this->set_baseMarker_4((ARMarker_t1554260723 *)NULL);
	}

IL_002f:
	{
		List_1_t923381855 * L_5 = __this->get_markersEligibleForBaseMarker_5();
		NullCheck(L_5);
		Enumerator_t458111529  L_6 = List_1_GetEnumerator_m4219110609(L_5, /*hidden argument*/List_1_GetEnumerator_m4219110609_MethodInfo_var);
		V_1 = L_6;
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005f;
		}

IL_0040:
		{
			ARMarker_t1554260723 * L_7 = Enumerator_get_Current_m4012250305((&V_1), /*hidden argument*/Enumerator_get_Current_m4012250305_MethodInfo_var);
			V_0 = L_7;
			ARMarker_t1554260723 * L_8 = V_0;
			NullCheck(L_8);
			bool L_9 = ARMarker_get_Visible_m4014965141(L_8, /*hidden argument*/NULL);
			if (!L_9)
			{
				goto IL_005f;
			}
		}

IL_0053:
		{
			ARMarker_t1554260723 * L_10 = V_0;
			__this->set_baseMarker_4(L_10);
			goto IL_006b;
		}

IL_005f:
		{
			bool L_11 = Enumerator_MoveNext_m2323311869((&V_1), /*hidden argument*/Enumerator_MoveNext_m2323311869_MethodInfo_var);
			if (L_11)
			{
				goto IL_0040;
			}
		}

IL_006b:
		{
			IL2CPP_LEAVE(0x7E, FINALLY_0070);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0070;
	}

FINALLY_0070:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m555758363((&V_1), /*hidden argument*/Enumerator_Dispose_m555758363_MethodInfo_var);
		IL2CPP_END_FINALLY(112)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(112)
	{
		IL2CPP_JUMP_TBL(0x7E, IL_007e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_007e:
	{
		ARMarker_t1554260723 * L_12 = __this->get_baseMarker_4();
		return L_12;
	}
}
// System.Void AROrigin::OnApplicationQuit()
extern "C"  void AROrigin_OnApplicationQuit_m3423715038 (AROrigin_t3335349585 * __this, const MethodInfo* method)
{
	{
		AROrigin_RemoveAllMarkers_m3158628672(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARPattern::.ctor(System.Int32,System.Int32)
extern Il2CppClass* SingleU5BU5D_t577127397_il2cpp_TypeInfo_var;
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppClass* ColorU5BU5D_t672350442_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral516110880;
extern Il2CppCodeGenString* _stringLiteral4200425294;
extern const uint32_t ARPattern__ctor_m3048001238_MetadataUsageId;
extern "C"  void ARPattern__ctor_m3048001238 (ARPattern_t3906840165 * __this, int32_t ___markerID0, int32_t ___patternID1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARPattern__ctor_m3048001238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t577127397* V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Matrix4x4_t2933234003  V_3;
	memset(&V_3, 0, sizeof(V_3));
	ColorU5BU5D_t672350442* V_4 = NULL;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		V_0 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		V_1 = (0.0f);
		V_2 = (0.0f);
		int32_t L_0 = ___markerID0;
		int32_t L_1 = ___patternID1;
		SingleU5BU5D_t577127397* L_2 = V_0;
		int32_t* L_3 = __this->get_address_of_imageSizeX_4();
		int32_t* L_4 = __this->get_address_of_imageSizeY_5();
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_5 = PluginFunctions_arwGetMarkerPatternConfig_m3210735196(NULL /*static, unused*/, L_0, L_1, L_2, (&V_1), (&V_2), L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0047;
		}
	}
	{
		ArgumentException_t3259014390 * L_6 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_6, _stringLiteral516110880, _stringLiteral4200425294, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0047:
	{
		float L_7 = V_1;
		__this->set_width_2(((float)((float)L_7*(float)(0.001f))));
		float L_8 = V_2;
		__this->set_height_3(((float)((float)L_8*(float)(0.001f))));
		SingleU5BU5D_t577127397* L_9 = V_0;
		NullCheck(L_9);
		float* L_10 = ((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)12))));
		*((float*)(L_10)) = (float)((float)((float)(*((float*)L_10))*(float)(0.001f)));
		SingleU5BU5D_t577127397* L_11 = V_0;
		NullCheck(L_11);
		float* L_12 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)13))));
		*((float*)(L_12)) = (float)((float)((float)(*((float*)L_12))*(float)(0.001f)));
		SingleU5BU5D_t577127397* L_13 = V_0;
		NullCheck(L_13);
		float* L_14 = ((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)14))));
		*((float*)(L_14)) = (float)((float)((float)(*((float*)L_14))*(float)(0.001f)));
		SingleU5BU5D_t577127397* L_15 = V_0;
		Matrix4x4_t2933234003  L_16 = ARUtilityFunctions_MatrixFromFloatArray_m3542072061(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		Matrix4x4_t2933234003  L_17 = V_3;
		Matrix4x4_t2933234003  L_18 = ARUtilityFunctions_LHMatrixFromRHMatrix_m2988276788(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		__this->set_matrix_1(L_18);
		int32_t L_19 = __this->get_imageSizeX_4();
		if ((((int32_t)L_19) <= ((int32_t)0)))
		{
			goto IL_0136;
		}
	}
	{
		int32_t L_20 = __this->get_imageSizeY_5();
		if ((((int32_t)L_20) <= ((int32_t)0)))
		{
			goto IL_0136;
		}
	}
	{
		int32_t L_21 = __this->get_imageSizeX_4();
		int32_t L_22 = __this->get_imageSizeY_5();
		Texture2D_t3542995729 * L_23 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_23, L_21, L_22, 4, (bool)0, /*hidden argument*/NULL);
		__this->set_texture_0(L_23);
		Texture2D_t3542995729 * L_24 = __this->get_texture_0();
		NullCheck(L_24);
		Texture_set_filterMode_m3838996656(L_24, 0, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_25 = __this->get_texture_0();
		NullCheck(L_25);
		Texture_set_wrapMode_m333956747(L_25, 1, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_26 = __this->get_texture_0();
		NullCheck(L_26);
		Texture_set_anisoLevel_m4242988344(L_26, 0, /*hidden argument*/NULL);
		int32_t L_27 = __this->get_imageSizeX_4();
		int32_t L_28 = __this->get_imageSizeY_5();
		V_4 = ((ColorU5BU5D_t672350442*)SZArrayNew(ColorU5BU5D_t672350442_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_27*(int32_t)L_28))));
		int32_t L_29 = ___markerID0;
		int32_t L_30 = ___patternID1;
		ColorU5BU5D_t672350442* L_31 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		bool L_32 = PluginFunctions_arwGetMarkerPatternImage_m1110189182(NULL /*static, unused*/, L_29, L_30, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0136;
		}
	}
	{
		Texture2D_t3542995729 * L_33 = __this->get_texture_0();
		ColorU5BU5D_t672350442* L_34 = V_4;
		NullCheck(L_33);
		Texture2D_SetPixels_m2799169789(L_33, L_34, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_35 = __this->get_texture_0();
		NullCheck(L_35);
		Texture2D_Apply_m3543341930(L_35, /*hidden argument*/NULL);
	}

IL_0136:
	{
		return;
	}
}
// System.Void ARTrackedCamera::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t ARTrackedCamera__ctor_m3960348331_MetadataUsageId;
extern "C"  void ARTrackedCamera__ctor_m3960348331 (ARTrackedCamera_t3950879424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARTrackedCamera__ctor_m3960348331_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_cullingMask_22((-1));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__markerTag_24(L_0);
		ARCamera__ctor_m4072460825(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String ARTrackedCamera::get_MarkerTag()
extern "C"  String_t* ARTrackedCamera_get_MarkerTag_m1116666943 (ARTrackedCamera_t3950879424 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__markerTag_24();
		return L_0;
	}
}
// System.Void ARTrackedCamera::set_MarkerTag(System.String)
extern "C"  void ARTrackedCamera_set_MarkerTag_m3168639924 (ARTrackedCamera_t3950879424 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__markerTag_24(L_0);
		((ARCamera_t431610428 *)__this)->set__marker_4((ARMarker_t1554260723 *)NULL);
		return;
	}
}
// ARMarker ARTrackedCamera::GetMarker()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectsOfType_TisARMarker_t1554260723_m2341901897_MethodInfo_var;
extern const uint32_t ARTrackedCamera_GetMarker_m3016132977_MetadataUsageId;
extern "C"  ARMarker_t1554260723 * ARTrackedCamera_GetMarker_m3016132977 (ARTrackedCamera_t3950879424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARTrackedCamera_GetMarker_m3016132977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARMarkerU5BU5D_t3553995746* V_0 = NULL;
	ARMarker_t1554260723 * V_1 = NULL;
	ARMarkerU5BU5D_t3553995746* V_2 = NULL;
	int32_t V_3 = 0;
	{
		ARMarker_t1554260723 * L_0 = ((ARCamera_t431610428 *)__this)->get__marker_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ARMarkerU5BU5D_t3553995746* L_2 = Object_FindObjectsOfType_TisARMarker_t1554260723_m2341901897(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectsOfType_TisARMarker_t1554260723_m2341901897_MethodInfo_var);
		V_0 = L_2;
		ARMarkerU5BU5D_t3553995746* L_3 = V_0;
		V_2 = L_3;
		V_3 = 0;
		goto IL_004a;
	}

IL_0020:
	{
		ARMarkerU5BU5D_t3553995746* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		ARMarker_t1554260723 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = L_7;
		ARMarker_t1554260723 * L_8 = V_1;
		NullCheck(L_8);
		String_t* L_9 = L_8->get_Tag_7();
		String_t* L_10 = __this->get__markerTag_24();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0046;
		}
	}
	{
		ARMarker_t1554260723 * L_12 = V_1;
		((ARCamera_t431610428 *)__this)->set__marker_4(L_12);
		goto IL_0053;
	}

IL_0046:
	{
		int32_t L_13 = V_3;
		V_3 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_14 = V_3;
		ARMarkerU5BU5D_t3553995746* L_15 = V_2;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0020;
		}
	}

IL_0053:
	{
		ARMarker_t1554260723 * L_16 = ((ARCamera_t431610428 *)__this)->get__marker_4();
		return L_16;
	}
}
// System.Void ARTrackedCamera::Start()
extern const MethodInfo* GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var;
extern const uint32_t ARTrackedCamera_Start_m3877372487_MetadataUsageId;
extern "C"  void ARTrackedCamera_Start_m3877372487 (ARTrackedCamera_t3950879424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARTrackedCamera_Start_m3877372487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_cullingMask_22();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Camera_t189460977 * L_2 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_1, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var);
		NullCheck(L_2);
		int32_t L_3 = Camera_get_cullingMask_m73686965(L_2, /*hidden argument*/NULL);
		__this->set_cullingMask_22(L_3);
	}

IL_0022:
	{
		return;
	}
}
// System.Void ARTrackedCamera::ApplyTracking()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral775922081;
extern Il2CppCodeGenString* _stringLiteral3236695341;
extern Il2CppCodeGenString* _stringLiteral3854481271;
extern const uint32_t ARTrackedCamera_ApplyTracking_m369285298_MetadataUsageId;
extern "C"  void ARTrackedCamera_ApplyTracking_m369285298 (ARTrackedCamera_t3950879424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARTrackedCamera_ApplyTracking_m369285298_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((ARCamera_t431610428 *)__this)->get_arVisible_7();
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		float L_1 = ((ARCamera_t431610428 *)__this)->get_timeLastUpdate_8();
		float L_2 = ((ARCamera_t431610428 *)__this)->get_timeTrackingLost_9();
		float L_3 = __this->get_secondsToRemainVisible_21();
		if ((!(((float)((float)((float)L_1-(float)L_2))) < ((float)L_3))))
		{
			goto IL_00c1;
		}
	}

IL_0023:
	{
		bool L_4 = ((ARCamera_t431610428 *)__this)->get_arVisible_7();
		bool L_5 = __this->get_lastArVisible_23();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0072;
		}
	}
	{
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Camera_t189460977 * L_7 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_6, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var);
		int32_t L_8 = __this->get_cullingMask_22();
		NullCheck(L_7);
		Camera_set_cullingMask_m2396665826(L_7, L_8, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_9 = ((ARCamera_t431610428 *)__this)->get_eventReceiver_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0072;
		}
	}
	{
		GameObject_t1756533147 * L_11 = ((ARCamera_t431610428 *)__this)->get_eventReceiver_10();
		ARMarker_t1554260723 * L_12 = VirtFuncInvoker0< ARMarker_t1554260723 * >::Invoke(5 /* ARMarker ARCamera::GetMarker() */, __this);
		NullCheck(L_11);
		GameObject_BroadcastMessage_m3472370570(L_11, _stringLiteral775922081, L_12, 1, /*hidden argument*/NULL);
	}

IL_0072:
	{
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = ((ARCamera_t431610428 *)__this)->get_arPosition_5();
		NullCheck(L_13);
		Transform_set_localPosition_m1026930133(L_13, L_14, /*hidden argument*/NULL);
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_16 = ((ARCamera_t431610428 *)__this)->get_arRotation_6();
		NullCheck(L_15);
		Transform_set_localRotation_m2055111962(L_15, L_16, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_17 = ((ARCamera_t431610428 *)__this)->get_eventReceiver_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_17, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00bc;
		}
	}
	{
		GameObject_t1756533147 * L_19 = ((ARCamera_t431610428 *)__this)->get_eventReceiver_10();
		ARMarker_t1554260723 * L_20 = VirtFuncInvoker0< ARMarker_t1554260723 * >::Invoke(5 /* ARMarker ARCamera::GetMarker() */, __this);
		NullCheck(L_19);
		GameObject_BroadcastMessage_m3472370570(L_19, _stringLiteral3236695341, L_20, 1, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		goto IL_010b;
	}

IL_00c1:
	{
		bool L_21 = ((ARCamera_t431610428 *)__this)->get_arVisible_7();
		bool L_22 = __this->get_lastArVisible_23();
		if ((((int32_t)L_21) == ((int32_t)L_22)))
		{
			goto IL_010b;
		}
	}
	{
		GameObject_t1756533147 * L_23 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Camera_t189460977 * L_24 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_23, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var);
		NullCheck(L_24);
		Camera_set_cullingMask_m2396665826(L_24, 0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_25 = ((ARCamera_t431610428 *)__this)->get_eventReceiver_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_25, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_010b;
		}
	}
	{
		GameObject_t1756533147 * L_27 = ((ARCamera_t431610428 *)__this)->get_eventReceiver_10();
		ARMarker_t1554260723 * L_28 = VirtFuncInvoker0< ARMarker_t1554260723 * >::Invoke(5 /* ARMarker ARCamera::GetMarker() */, __this);
		NullCheck(L_27);
		GameObject_BroadcastMessage_m3472370570(L_27, _stringLiteral3854481271, L_28, 1, /*hidden argument*/NULL);
	}

IL_010b:
	{
		bool L_29 = ((ARCamera_t431610428 *)__this)->get_arVisible_7();
		__this->set_lastArVisible_23(L_29);
		return;
	}
}
// System.Void ARTrackedObject::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t ARTrackedObject__ctor_m2314920655_MetadataUsageId;
extern "C"  void ARTrackedObject__ctor_m2314920655 (ARTrackedObject_t1684152848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARTrackedObject__ctor_m2314920655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__markerTag_10(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String ARTrackedObject::get_MarkerTag()
extern "C"  String_t* ARTrackedObject_get_MarkerTag_m2984654219 (ARTrackedObject_t1684152848 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__markerTag_10();
		return L_0;
	}
}
// System.Void ARTrackedObject::set_MarkerTag(System.String)
extern "C"  void ARTrackedObject_set_MarkerTag_m3948804028 (ARTrackedObject_t1684152848 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__markerTag_10(L_0);
		__this->set__marker_4((ARMarker_t1554260723 *)NULL);
		return;
	}
}
// ARMarker ARTrackedObject::GetMarker()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectsOfType_TisARMarker_t1554260723_m2341901897_MethodInfo_var;
extern const uint32_t ARTrackedObject_GetMarker_m343782273_MetadataUsageId;
extern "C"  ARMarker_t1554260723 * ARTrackedObject_GetMarker_m343782273 (ARTrackedObject_t1684152848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARTrackedObject_GetMarker_m343782273_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARMarkerU5BU5D_t3553995746* V_0 = NULL;
	ARMarker_t1554260723 * V_1 = NULL;
	ARMarkerU5BU5D_t3553995746* V_2 = NULL;
	int32_t V_3 = 0;
	{
		ARMarker_t1554260723 * L_0 = __this->get__marker_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ARMarkerU5BU5D_t3553995746* L_2 = Object_FindObjectsOfType_TisARMarker_t1554260723_m2341901897(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectsOfType_TisARMarker_t1554260723_m2341901897_MethodInfo_var);
		V_0 = L_2;
		ARMarkerU5BU5D_t3553995746* L_3 = V_0;
		V_2 = L_3;
		V_3 = 0;
		goto IL_004a;
	}

IL_0020:
	{
		ARMarkerU5BU5D_t3553995746* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		ARMarker_t1554260723 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = L_7;
		ARMarker_t1554260723 * L_8 = V_1;
		NullCheck(L_8);
		String_t* L_9 = L_8->get_Tag_7();
		String_t* L_10 = __this->get__markerTag_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0046;
		}
	}
	{
		ARMarker_t1554260723 * L_12 = V_1;
		__this->set__marker_4(L_12);
		goto IL_0053;
	}

IL_0046:
	{
		int32_t L_13 = V_3;
		V_3 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_14 = V_3;
		ARMarkerU5BU5D_t3553995746* L_15 = V_2;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0020;
		}
	}

IL_0053:
	{
		ARMarker_t1554260723 * L_16 = __this->get__marker_4();
		return L_16;
	}
}
// AROrigin ARTrackedObject::GetOrigin()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentInParent_TisAROrigin_t3335349585_m1108684983_MethodInfo_var;
extern const uint32_t ARTrackedObject_GetOrigin_m1492473453_MetadataUsageId;
extern "C"  AROrigin_t3335349585 * ARTrackedObject_GetOrigin_m1492473453 (ARTrackedObject_t1684152848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARTrackedObject_GetOrigin_m1492473453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AROrigin_t3335349585 * L_0 = __this->get__origin_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		AROrigin_t3335349585 * L_3 = GameObject_GetComponentInParent_TisAROrigin_t3335349585_m1108684983(L_2, /*hidden argument*/GameObject_GetComponentInParent_TisAROrigin_t3335349585_m1108684983_MethodInfo_var);
		__this->set__origin_3(L_3);
	}

IL_0022:
	{
		AROrigin_t3335349585 * L_4 = __this->get__origin_3();
		return L_4;
	}
}
// System.Void ARTrackedObject::Start()
extern "C"  void ARTrackedObject_Start_m1585862763 (ARTrackedObject_t1684152848 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		V_0 = 0;
		goto IL_002c;
	}

IL_0011:
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Transform_t3275118058 * L_3 = Transform_GetChild_m3838588184(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_6 = V_0;
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = Transform_get_childCount_m881385315(L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_0011;
		}
	}
	{
		goto IL_0075;
	}

IL_0042:
	{
		V_1 = 0;
		goto IL_0064;
	}

IL_0049:
	{
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		Transform_t3275118058 * L_11 = Transform_GetChild_m3838588184(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		GameObject_SetActive_m2887581199(L_12, (bool)1, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0064:
	{
		int32_t L_14 = V_1;
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = Transform_get_childCount_m881385315(L_15, /*hidden argument*/NULL);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0049;
		}
	}

IL_0075:
	{
		return;
	}
}
// System.Void ARTrackedObject::LateUpdate()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral775922081;
extern Il2CppCodeGenString* _stringLiteral3236695341;
extern Il2CppCodeGenString* _stringLiteral3854481271;
extern const uint32_t ARTrackedObject_LateUpdate_m3261816006_MetadataUsageId;
extern "C"  void ARTrackedObject_LateUpdate_m3261816006 (ARTrackedObject_t1684152848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARTrackedObject_LateUpdate_m3261816006_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AROrigin_t3335349585 * V_0 = NULL;
	ARMarker_t1554260723 * V_1 = NULL;
	float V_2 = 0.0f;
	ARMarker_t1554260723 * V_3 = NULL;
	bool V_4 = false;
	int32_t V_5 = 0;
	Matrix4x4_t2933234003  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Matrix4x4_t2933234003  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t V_8 = 0;
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localScale_m2325460848(L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0215;
		}
	}
	{
		AROrigin_t3335349585 * L_3 = VirtFuncInvoker0< AROrigin_t3335349585 * >::Invoke(5 /* AROrigin ARTrackedObject::GetOrigin() */, __this);
		V_0 = L_3;
		AROrigin_t3335349585 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_0215;
	}

IL_0032:
	{
		ARMarker_t1554260723 * L_6 = VirtFuncInvoker0< ARMarker_t1554260723 * >::Invoke(4 /* ARMarker ARTrackedObject::GetMarker() */, __this);
		V_1 = L_6;
		ARMarker_t1554260723 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_7, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004a;
		}
	}
	{
		goto IL_0215;
	}

IL_004a:
	{
		float L_9 = Time_get_realtimeSinceStartup_m357614587(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_9;
		AROrigin_t3335349585 * L_10 = V_0;
		NullCheck(L_10);
		ARMarker_t1554260723 * L_11 = AROrigin_GetBaseMarker_m4103772195(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		ARMarker_t1554260723 * L_12 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_12, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_017c;
		}
	}
	{
		ARMarker_t1554260723 * L_14 = V_1;
		NullCheck(L_14);
		bool L_15 = ARMarker_get_Visible_m4014965141(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_017c;
		}
	}
	{
		bool L_16 = __this->get_visible_5();
		if (L_16)
		{
			goto IL_00e6;
		}
	}
	{
		int32_t L_17 = 1;
		V_4 = (bool)L_17;
		__this->set_visibleOrRemain_8((bool)L_17);
		bool L_18 = V_4;
		__this->set_visible_5(L_18);
		GameObject_t1756533147 * L_19 = __this->get_eventReceiver_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_19, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00ae;
		}
	}
	{
		GameObject_t1756533147 * L_21 = __this->get_eventReceiver_9();
		ARMarker_t1554260723 * L_22 = V_1;
		NullCheck(L_21);
		GameObject_BroadcastMessage_m3472370570(L_21, _stringLiteral775922081, L_22, 1, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		V_5 = 0;
		goto IL_00d4;
	}

IL_00b6:
	{
		Transform_t3275118058 * L_23 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		int32_t L_24 = V_5;
		NullCheck(L_23);
		Transform_t3275118058 * L_25 = Transform_GetChild_m3838588184(L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		GameObject_t1756533147 * L_26 = Component_get_gameObject_m3105766835(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		GameObject_SetActive_m2887581199(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_5;
		V_5 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00d4:
	{
		int32_t L_28 = V_5;
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		int32_t L_30 = Transform_get_childCount_m881385315(L_29, /*hidden argument*/NULL);
		if ((((int32_t)L_28) < ((int32_t)L_30)))
		{
			goto IL_00b6;
		}
	}

IL_00e6:
	{
		ARMarker_t1554260723 * L_31 = V_1;
		ARMarker_t1554260723 * L_32 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_33 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0104;
		}
	}
	{
		AROrigin_t3335349585 * L_34 = V_0;
		NullCheck(L_34);
		Transform_t3275118058 * L_35 = Component_get_transform_m2697483695(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Matrix4x4_t2933234003  L_36 = Transform_get_localToWorldMatrix_m2868579006(L_35, /*hidden argument*/NULL);
		V_6 = L_36;
		goto IL_0130;
	}

IL_0104:
	{
		AROrigin_t3335349585 * L_37 = V_0;
		NullCheck(L_37);
		Transform_t3275118058 * L_38 = Component_get_transform_m2697483695(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		Matrix4x4_t2933234003  L_39 = Transform_get_localToWorldMatrix_m2868579006(L_38, /*hidden argument*/NULL);
		ARMarker_t1554260723 * L_40 = V_3;
		NullCheck(L_40);
		Matrix4x4_t2933234003  L_41 = ARMarker_get_TransformationMatrix_m3960336120(L_40, /*hidden argument*/NULL);
		V_7 = L_41;
		Matrix4x4_t2933234003  L_42 = Matrix4x4_get_inverse_m2479387736((&V_7), /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_43 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_39, L_42, /*hidden argument*/NULL);
		ARMarker_t1554260723 * L_44 = V_1;
		NullCheck(L_44);
		Matrix4x4_t2933234003  L_45 = ARMarker_get_TransformationMatrix_m3960336120(L_44, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_46 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_43, L_45, /*hidden argument*/NULL);
		V_6 = L_46;
	}

IL_0130:
	{
		Transform_t3275118058 * L_47 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_48 = V_6;
		Vector3_t2243707580  L_49 = ARUtilityFunctions_PositionFromMatrix_m2391904295(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		NullCheck(L_47);
		Transform_set_position_m2469242620(L_47, L_49, /*hidden argument*/NULL);
		Transform_t3275118058 * L_50 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_51 = V_6;
		Quaternion_t4030073918  L_52 = ARUtilityFunctions_QuaternionFromMatrix_m2564935062(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		NullCheck(L_50);
		Transform_set_rotation_m3411284563(L_50, L_52, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_53 = __this->get_eventReceiver_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_54 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_53, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_0177;
		}
	}
	{
		GameObject_t1756533147 * L_55 = __this->get_eventReceiver_9();
		ARMarker_t1554260723 * L_56 = V_1;
		NullCheck(L_55);
		GameObject_BroadcastMessage_m3472370570(L_55, _stringLiteral3236695341, L_56, 1, /*hidden argument*/NULL);
	}

IL_0177:
	{
		goto IL_0215;
	}

IL_017c:
	{
		bool L_57 = __this->get_visible_5();
		if (!L_57)
		{
			goto IL_0195;
		}
	}
	{
		__this->set_visible_5((bool)0);
		float L_58 = V_2;
		__this->set_timeTrackingLost_6(L_58);
	}

IL_0195:
	{
		bool L_59 = __this->get_visibleOrRemain_8();
		if (!L_59)
		{
			goto IL_0215;
		}
	}
	{
		float L_60 = V_2;
		float L_61 = __this->get_timeTrackingLost_6();
		float L_62 = __this->get_secondsToRemainVisible_7();
		if ((!(((float)((float)((float)L_60-(float)L_61))) >= ((float)L_62))))
		{
			goto IL_0215;
		}
	}
	{
		__this->set_visibleOrRemain_8((bool)0);
		GameObject_t1756533147 * L_63 = __this->get_eventReceiver_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_64 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_63, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_01dd;
		}
	}
	{
		GameObject_t1756533147 * L_65 = __this->get_eventReceiver_9();
		ARMarker_t1554260723 * L_66 = V_1;
		NullCheck(L_65);
		GameObject_BroadcastMessage_m3472370570(L_65, _stringLiteral3854481271, L_66, 1, /*hidden argument*/NULL);
	}

IL_01dd:
	{
		V_8 = 0;
		goto IL_0203;
	}

IL_01e5:
	{
		Transform_t3275118058 * L_67 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		int32_t L_68 = V_8;
		NullCheck(L_67);
		Transform_t3275118058 * L_69 = Transform_GetChild_m3838588184(L_67, L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		GameObject_t1756533147 * L_70 = Component_get_gameObject_m3105766835(L_69, /*hidden argument*/NULL);
		NullCheck(L_70);
		GameObject_SetActive_m2887581199(L_70, (bool)0, /*hidden argument*/NULL);
		int32_t L_71 = V_8;
		V_8 = ((int32_t)((int32_t)L_71+(int32_t)1));
	}

IL_0203:
	{
		int32_t L_72 = V_8;
		Transform_t3275118058 * L_73 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_73);
		int32_t L_74 = Transform_get_childCount_m881385315(L_73, /*hidden argument*/NULL);
		if ((((int32_t)L_72) < ((int32_t)L_74)))
		{
			goto IL_01e5;
		}
	}

IL_0215:
	{
		return;
	}
}
// System.Void ARTransitionalCamera::.ctor()
extern "C"  void ARTransitionalCamera__ctor_m2916672927 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method)
{
	{
		__this->set_movementRate_29((1.389f));
		Vector3_t2243707580  L_0 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_vrObserverOffset_32(L_0);
		__this->set_automaticTransitionDistance_34((0.3f));
		ARTrackedCamera__ctor_m3960348331(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ARTransitionalCamera::DoTransition(System.Boolean)
extern Il2CppClass* U3CDoTransitionU3Ec__Iterator0_t340230560_il2cpp_TypeInfo_var;
extern const uint32_t ARTransitionalCamera_DoTransition_m1186448882_MetadataUsageId;
extern "C"  Il2CppObject * ARTransitionalCamera_DoTransition_m1186448882 (ARTransitionalCamera_t3582277450 * __this, bool ___flyIn0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARTransitionalCamera_DoTransition_m1186448882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CDoTransitionU3Ec__Iterator0_t340230560 * V_0 = NULL;
	{
		U3CDoTransitionU3Ec__Iterator0_t340230560 * L_0 = (U3CDoTransitionU3Ec__Iterator0_t340230560 *)il2cpp_codegen_object_new(U3CDoTransitionU3Ec__Iterator0_t340230560_il2cpp_TypeInfo_var);
		U3CDoTransitionU3Ec__Iterator0__ctor_m3676305879(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDoTransitionU3Ec__Iterator0_t340230560 * L_1 = V_0;
		bool L_2 = ___flyIn0;
		NullCheck(L_1);
		L_1->set_flyIn_1(L_2);
		U3CDoTransitionU3Ec__Iterator0_t340230560 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_4(__this);
		U3CDoTransitionU3Ec__Iterator0_t340230560 * L_4 = V_0;
		return L_4;
	}
}
// System.Void ARTransitionalCamera::transitionIn()
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3248206644;
extern const uint32_t ARTransitionalCamera_transitionIn_m3162725215_MetadataUsageId;
extern "C"  void ARTransitionalCamera_transitionIn_m3162725215 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARTransitionalCamera_transitionIn_m3162725215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_StopCoroutine_m987450539(__this, _stringLiteral3248206644, /*hidden argument*/NULL);
		bool L_0 = ((bool)1);
		Il2CppObject * L_1 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_0);
		MonoBehaviour_StartCoroutine_m296997955(__this, _stringLiteral3248206644, L_1, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_2 = Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var);
		NullCheck(L_2);
		AudioSource_Play_m353744792(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARTransitionalCamera::transitionOut()
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3248206644;
extern const uint32_t ARTransitionalCamera_transitionOut_m4140032048_MetadataUsageId;
extern "C"  void ARTransitionalCamera_transitionOut_m4140032048 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARTransitionalCamera_transitionOut_m4140032048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_StopCoroutine_m987450539(__this, _stringLiteral3248206644, /*hidden argument*/NULL);
		bool L_0 = ((bool)0);
		Il2CppObject * L_1 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_0);
		MonoBehaviour_StartCoroutine_m296997955(__this, _stringLiteral3248206644, L_1, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_2 = Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var);
		NullCheck(L_2);
		AudioSource_Play_m353744792(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARTransitionalCamera::Start()
extern const MethodInfo* GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var;
extern const uint32_t ARTransitionalCamera_Start_m1838311147_MetadataUsageId;
extern "C"  void ARTransitionalCamera_Start_m1838311147 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARTransitionalCamera_Start_m1838311147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t2933234003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t2933234003  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		ARTrackedCamera_Start_m3877372487(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = __this->get_targetObject_27();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Matrix4x4_t2933234003  L_2 = Transform_get_localToWorldMatrix_m2868579006(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Camera_t189460977 * L_4 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_3, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = Transform_get_parent_m147407266(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Matrix4x4_t2933234003  L_7 = Transform_get_worldToLocalMatrix_m3299477436(L_6, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_8 = V_0;
		Matrix4x4_t2933234003  L_9 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Matrix4x4_t2933234003  L_10 = V_1;
		Vector3_t2243707580  L_11 = ARUtilityFunctions_PositionFromMatrix_m2391904295(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		__this->set_vrTargetPosition_25(L_11);
		Matrix4x4_t2933234003  L_12 = V_1;
		Quaternion_t4030073918  L_13 = ARUtilityFunctions_QuaternionFromMatrix_m2564935062(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		__this->set_vrTargetRotation_26(L_13);
		float L_14 = (0.0f);
		V_2 = L_14;
		__this->set_vrObserverElevation_31(L_14);
		float L_15 = V_2;
		__this->set_vrObserverAzimuth_30(L_15);
		Vector3_t2243707580  L_16 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_vrObserverOffset_32(L_16);
		return;
	}
}
// System.Void ARTransitionalCamera::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t ARTransitionalCamera_Update_m1478680_MetadataUsageId;
extern "C"  void ARTransitionalCamera_Update_m1478680 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARTransitionalCamera_Update_m1478680_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)105), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		ARTransitionalCamera_transitionIn_m3162725215(__this, /*hidden argument*/NULL);
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)111), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		ARTransitionalCamera_transitionOut_m4140032048(__this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		bool L_2 = __this->get_automaticTransition_33();
		if (!L_2)
		{
			goto IL_0066;
		}
	}
	{
		bool L_3 = ((ARCamera_t431610428 *)__this)->get_arVisible_7();
		if (!L_3)
		{
			goto IL_0066;
		}
	}
	{
		Vector3_t2243707580 * L_4 = ((ARCamera_t431610428 *)__this)->get_address_of_arPosition_5();
		float L_5 = Vector3_get_magnitude_m860342598(L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_automaticTransitionDistance_34();
		if ((!(((float)L_5) < ((float)L_6))))
		{
			goto IL_0066;
		}
	}
	{
		float L_7 = __this->get_transitionAmount_28();
		if ((((float)L_7) == ((float)(1.0f))))
		{
			goto IL_0066;
		}
	}
	{
		ARTransitionalCamera_transitionIn_m3162725215(__this, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void ARTransitionalCamera::OnPreRender()
extern const MethodInfo* GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisSkybox_t2033495038_m3670937650_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4022078178;
extern const uint32_t ARTransitionalCamera_OnPreRender_m1212776491_MetadataUsageId;
extern "C"  void ARTransitionalCamera_OnPreRender_m1212776491 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARTransitionalCamera_OnPreRender_m1212776491_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Camera_t189460977 * L_1 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_0, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var);
		GL_ClearWithSkybox_m1456702260(NULL /*static, unused*/, (bool)0, L_1, /*hidden argument*/NULL);
		Skybox_t2033495038 * L_2 = Component_GetComponent_TisSkybox_t2033495038_m3670937650(__this, /*hidden argument*/Component_GetComponent_TisSkybox_t2033495038_m3670937650_MethodInfo_var);
		NullCheck(L_2);
		Material_t193706927 * L_3 = Skybox_get_material_m2038447772(L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_transitionAmount_28();
		Color_t2020392075  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Color__ctor_m1909920690(&L_5, (1.0f), (1.0f), (1.0f), L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Material_SetColor_m650857509(L_3, _stringLiteral4022078178, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARTransitionalCamera::ApplyTracking()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1307534077;
extern Il2CppCodeGenString* _stringLiteral1307534078;
extern Il2CppCodeGenString* _stringLiteral855845486;
extern Il2CppCodeGenString* _stringLiteral1635882288;
extern const uint32_t ARTransitionalCamera_ApplyTracking_m3447604840_MetadataUsageId;
extern "C"  void ARTransitionalCamera_ApplyTracking_m3447604840 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARTransitionalCamera_ApplyTracking_m3447604840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Touch_t407273883  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Quaternion_t4030073918  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Quaternion_t4030073918  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Vector2_t2243707579  L_0 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = SystemInfo_get_deviceType_m3581258207(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_3 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0036;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_4 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_4;
		Vector2_t2243707579  L_5 = Touch_get_deltaPosition_m97688791((&V_2), /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_006d;
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_6 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_006d;
		}
	}
	{
		float L_7 = __this->get_transitionAmount_28();
		if ((!(((float)L_7) <= ((float)(0.0f)))))
		{
			goto IL_0057;
		}
	}
	{
		ARTransitionalCamera_transitionIn_m3162725215(__this, /*hidden argument*/NULL);
	}

IL_0057:
	{
		float L_8 = __this->get_transitionAmount_28();
		if ((!(((float)L_8) >= ((float)(1.0f)))))
		{
			goto IL_006d;
		}
	}
	{
		ARTransitionalCamera_transitionOut_m4140032048(__this, /*hidden argument*/NULL);
	}

IL_006d:
	{
		goto IL_0100;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_9 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534077, /*hidden argument*/NULL);
		(&V_0)->set_x_0(((-L_9)));
		float L_10 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534078, /*hidden argument*/NULL);
		(&V_0)->set_y_1(((-L_10)));
		float L_11 = __this->get_movementRate_29();
		float L_12 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		(&V_1)->set_x_1(((float)((float)((float)((float)L_11*(float)L_12))*(float)L_13)));
		(&V_1)->set_y_2((0.0f));
		float L_14 = __this->get_movementRate_29();
		float L_15 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_16 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1635882288, /*hidden argument*/NULL);
		(&V_1)->set_z_3(((float)((float)((float)((float)L_14*(float)L_15))*(float)L_16)));
		bool L_17 = Input_GetMouseButton_m464100923(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00ef;
		}
	}
	{
		ARTransitionalCamera_transitionIn_m3162725215(__this, /*hidden argument*/NULL);
	}

IL_00ef:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_18 = Input_GetMouseButton_m464100923(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0100;
		}
	}
	{
		ARTransitionalCamera_transitionOut_m4140032048(__this, /*hidden argument*/NULL);
	}

IL_0100:
	{
		float L_19 = __this->get_vrObserverAzimuth_30();
		float L_20 = (&V_0)->get_x_0();
		__this->set_vrObserverAzimuth_30(((float)((float)L_19-(float)((float)((float)L_20*(float)(0.5f))))));
		float L_21 = __this->get_vrObserverAzimuth_30();
		if ((!(((float)L_21) >= ((float)(360.0f)))))
		{
			goto IL_0141;
		}
	}
	{
		float L_22 = __this->get_vrObserverAzimuth_30();
		__this->set_vrObserverAzimuth_30(((float)((float)L_22-(float)(360.0f))));
		goto IL_0163;
	}

IL_0141:
	{
		float L_23 = __this->get_vrObserverAzimuth_30();
		if ((!(((float)L_23) < ((float)(0.0f)))))
		{
			goto IL_0163;
		}
	}
	{
		float L_24 = __this->get_vrObserverAzimuth_30();
		__this->set_vrObserverAzimuth_30(((float)((float)L_24+(float)(360.0f))));
	}

IL_0163:
	{
		float L_25 = __this->get_vrObserverElevation_31();
		float L_26 = (&V_0)->get_y_1();
		__this->set_vrObserverElevation_31(((float)((float)L_25+(float)((float)((float)L_26*(float)(0.5f))))));
		float L_27 = __this->get_vrObserverElevation_31();
		if ((!(((float)L_27) > ((float)(90.0f)))))
		{
			goto IL_019d;
		}
	}
	{
		__this->set_vrObserverElevation_31((90.0f));
		goto IL_01b8;
	}

IL_019d:
	{
		float L_28 = __this->get_vrObserverElevation_31();
		if ((!(((float)L_28) < ((float)(-90.0f)))))
		{
			goto IL_01b8;
		}
	}
	{
		__this->set_vrObserverElevation_31((-90.0f));
	}

IL_01b8:
	{
		float L_29 = __this->get_vrObserverElevation_31();
		float L_30 = __this->get_vrObserverAzimuth_30();
		Quaternion_t4030073918  L_31 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, L_29, L_30, (0.0f), /*hidden argument*/NULL);
		V_3 = L_31;
		Vector3_t2243707580  L_32 = __this->get_vrObserverOffset_32();
		Quaternion_t4030073918  L_33 = V_3;
		Vector3_t2243707580  L_34 = V_1;
		Vector3_t2243707580  L_35 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		Vector3_t2243707580  L_36 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_32, L_35, /*hidden argument*/NULL);
		__this->set_vrObserverOffset_32(L_36);
		Vector3_t2243707580  L_37 = __this->get_vrTargetPosition_25();
		Quaternion_t4030073918  L_38 = __this->get_vrTargetRotation_26();
		Vector3_t2243707580  L_39 = __this->get_vrObserverOffset_32();
		Vector3_t2243707580  L_40 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_37, L_40, /*hidden argument*/NULL);
		V_4 = L_41;
		Quaternion_t4030073918  L_42 = __this->get_vrTargetRotation_26();
		Quaternion_t4030073918  L_43 = V_3;
		Quaternion_t4030073918  L_44 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_42, L_43, /*hidden argument*/NULL);
		V_5 = L_44;
		float L_45 = __this->get_transitionAmount_28();
		if ((!(((float)L_45) < ((float)(1.0f)))))
		{
			goto IL_029b;
		}
	}
	{
		bool L_46 = ((ARCamera_t431610428 *)__this)->get_arVisible_7();
		if (!L_46)
		{
			goto IL_0285;
		}
	}
	{
		Transform_t3275118058 * L_47 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_48 = ((ARCamera_t431610428 *)__this)->get_arPosition_5();
		Vector3_t2243707580  L_49 = V_4;
		float L_50 = __this->get_transitionAmount_28();
		Vector3_t2243707580  L_51 = Vector3_Lerp_m2935648359(NULL /*static, unused*/, L_48, L_49, L_50, /*hidden argument*/NULL);
		NullCheck(L_47);
		Transform_set_localPosition_m1026930133(L_47, L_51, /*hidden argument*/NULL);
		Transform_t3275118058 * L_52 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_53 = ((ARCamera_t431610428 *)__this)->get_arRotation_6();
		Quaternion_t4030073918  L_54 = V_5;
		float L_55 = __this->get_transitionAmount_28();
		Quaternion_t4030073918  L_56 = Quaternion_Slerp_m1992855400(NULL /*static, unused*/, L_53, L_54, L_55, /*hidden argument*/NULL);
		NullCheck(L_52);
		Transform_set_localRotation_m2055111962(L_52, L_56, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_57 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_57);
		Camera_t189460977 * L_58 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_57, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var);
		int32_t L_59 = ((ARTrackedCamera_t3950879424 *)__this)->get_cullingMask_22();
		NullCheck(L_58);
		Camera_set_cullingMask_m2396665826(L_58, L_59, /*hidden argument*/NULL);
		goto IL_0296;
	}

IL_0285:
	{
		GameObject_t1756533147 * L_60 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_60);
		Camera_t189460977 * L_61 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_60, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var);
		NullCheck(L_61);
		Camera_set_cullingMask_m2396665826(L_61, 0, /*hidden argument*/NULL);
	}

IL_0296:
	{
		goto IL_02cb;
	}

IL_029b:
	{
		GameObject_t1756533147 * L_62 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_62);
		Camera_t189460977 * L_63 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_62, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var);
		int32_t L_64 = ((ARTrackedCamera_t3950879424 *)__this)->get_cullingMask_22();
		NullCheck(L_63);
		Camera_set_cullingMask_m2396665826(L_63, L_64, /*hidden argument*/NULL);
		Transform_t3275118058 * L_65 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_66 = V_4;
		NullCheck(L_65);
		Transform_set_localPosition_m1026930133(L_65, L_66, /*hidden argument*/NULL);
		Transform_t3275118058 * L_67 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_68 = V_5;
		NullCheck(L_67);
		Transform_set_localRotation_m2055111962(L_67, L_68, /*hidden argument*/NULL);
	}

IL_02cb:
	{
		return;
	}
}
// System.Void ARTransitionalCamera::OnGUI()
extern "C"  void ARTransitionalCamera_OnGUI_m997302209 (ARTransitionalCamera_t3582277450 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = SystemInfo_get_deviceType_m3581258207(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0010;
		}
	}
	{
		goto IL_0010;
	}

IL_0010:
	{
		return;
	}
}
// System.Void ARTransitionalCamera/<DoTransition>c__Iterator0::.ctor()
extern "C"  void U3CDoTransitionU3Ec__Iterator0__ctor_m3676305879 (U3CDoTransitionU3Ec__Iterator0_t340230560 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ARTransitionalCamera/<DoTransition>c__Iterator0::MoveNext()
extern const Il2CppType* ARController_t593870669_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral960552972;
extern const uint32_t U3CDoTransitionU3Ec__Iterator0_MoveNext_m995060305_MetadataUsageId;
extern "C"  bool U3CDoTransitionU3Ec__Iterator0_MoveNext_m995060305 (U3CDoTransitionU3Ec__Iterator0_t340230560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDoTransitionU3Ec__Iterator0_MoveNext_m995060305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	U3CDoTransitionU3Ec__Iterator0_t340230560 * G_B4_0 = NULL;
	U3CDoTransitionU3Ec__Iterator0_t340230560 * G_B3_0 = NULL;
	float G_B5_0 = 0.0f;
	U3CDoTransitionU3Ec__Iterator0_t340230560 * G_B5_1 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0125;
		}
	}
	{
		goto IL_0141;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ARController_t593870669_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_3 = Object_FindObjectOfType_m2330404063(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_U3CartoolkitU3E__0_0(((ARController_t593870669 *)IsInstClass(L_3, ARController_t593870669_il2cpp_TypeInfo_var)));
		bool L_4 = __this->get_flyIn_1();
		G_B3_0 = __this;
		if (!L_4)
		{
			G_B4_0 = __this;
			goto IL_0051;
		}
	}
	{
		G_B5_0 = (1.0f);
		G_B5_1 = G_B3_0;
		goto IL_0056;
	}

IL_0051:
	{
		G_B5_0 = (-1.0f);
		G_B5_1 = G_B4_0;
	}

IL_0056:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_U3CtransitionSpeedU3E__1_2(G_B5_0);
		__this->set_U3CtransitioningU3E__2_3((bool)1);
		goto IL_0125;
	}

IL_0067:
	{
		ARTransitionalCamera_t3582277450 * L_5 = __this->get_U24this_4();
		ARTransitionalCamera_t3582277450 * L_6 = L_5;
		NullCheck(L_6);
		float L_7 = L_6->get_transitionAmount_28();
		float L_8 = __this->get_U3CtransitionSpeedU3E__1_2();
		float L_9 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->set_transitionAmount_28(((float)((float)L_7+(float)((float)((float)L_8*(float)L_9)))));
		ARTransitionalCamera_t3582277450 * L_10 = __this->get_U24this_4();
		NullCheck(L_10);
		float L_11 = L_10->get_transitionAmount_28();
		if ((!(((float)L_11) > ((float)(1.0f)))))
		{
			goto IL_00b1;
		}
	}
	{
		ARTransitionalCamera_t3582277450 * L_12 = __this->get_U24this_4();
		NullCheck(L_12);
		L_12->set_transitionAmount_28((1.0f));
		__this->set_U3CtransitioningU3E__2_3((bool)0);
	}

IL_00b1:
	{
		ARTransitionalCamera_t3582277450 * L_13 = __this->get_U24this_4();
		NullCheck(L_13);
		float L_14 = L_13->get_transitionAmount_28();
		if ((!(((float)L_14) < ((float)(0.0f)))))
		{
			goto IL_00dd;
		}
	}
	{
		ARTransitionalCamera_t3582277450 * L_15 = __this->get_U24this_4();
		NullCheck(L_15);
		L_15->set_transitionAmount_28((0.0f));
		__this->set_U3CtransitioningU3E__2_3((bool)0);
	}

IL_00dd:
	{
		ARController_t593870669 * L_16 = __this->get_U3CartoolkitU3E__0_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_16, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_010a;
		}
	}
	{
		ARController_t593870669 * L_18 = __this->get_U3CartoolkitU3E__0_0();
		ARTransitionalCamera_t3582277450 * L_19 = __this->get_U24this_4();
		NullCheck(L_19);
		float L_20 = L_19->get_transitionAmount_28();
		NullCheck(L_18);
		ARController_SetVideoAlpha_m3263902264(L_18, ((float)((float)(1.0f)-(float)L_20)), /*hidden argument*/NULL);
	}

IL_010a:
	{
		__this->set_U24current_5(NULL);
		bool L_21 = __this->get_U24disposing_6();
		if (L_21)
		{
			goto IL_0120;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_0120:
	{
		goto IL_0143;
	}

IL_0125:
	{
		bool L_22 = __this->get_U3CtransitioningU3E__2_3();
		if (L_22)
		{
			goto IL_0067;
		}
	}
	{
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral960552972, /*hidden argument*/NULL);
		__this->set_U24PC_7((-1));
	}

IL_0141:
	{
		return (bool)0;
	}

IL_0143:
	{
		return (bool)1;
	}
}
// System.Object ARTransitionalCamera/<DoTransition>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoTransitionU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3081689489 (U3CDoTransitionU3Ec__Iterator0_t340230560 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object ARTransitionalCamera/<DoTransition>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoTransitionU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m100713465 (U3CDoTransitionU3Ec__Iterator0_t340230560 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Void ARTransitionalCamera/<DoTransition>c__Iterator0::Dispose()
extern "C"  void U3CDoTransitionU3Ec__Iterator0_Dispose_m3270519898 (U3CDoTransitionU3Ec__Iterator0_t340230560 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void ARTransitionalCamera/<DoTransition>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CDoTransitionU3Ec__Iterator0_Reset_m3338190028_MetadataUsageId;
extern "C"  void U3CDoTransitionU3Ec__Iterator0_Reset_m3338190028 (U3CDoTransitionU3Ec__Iterator0_t340230560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDoTransitionU3Ec__Iterator0_Reset_m3338190028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// UnityEngine.Camera ARUtilityFunctions::FindCameraByName(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t ARUtilityFunctions_FindCameraByName_m1687395814_MetadataUsageId;
extern "C"  Camera_t189460977 * ARUtilityFunctions_FindCameraByName_m1687395814 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARUtilityFunctions_FindCameraByName_m1687395814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Camera_t189460977 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	int32_t V_2 = 0;
	{
		CameraU5BU5D_t3079764780* L_0 = Camera_get_allCameras_m1343394249(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_0;
		V_2 = 0;
		goto IL_002d;
	}

IL_000d:
	{
		CameraU5BU5D_t3079764780* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Camera_t189460977 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		Camera_t189460977 * L_5 = V_0;
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = Object_get_name_m2079638459(L_6, /*hidden argument*/NULL);
		String_t* L_8 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0029;
		}
	}
	{
		Camera_t189460977 * L_10 = V_0;
		return L_10;
	}

IL_0029:
	{
		int32_t L_11 = V_2;
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_12 = V_2;
		CameraU5BU5D_t3079764780* L_13 = V_1;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		return (Camera_t189460977 *)NULL;
	}
}
// UnityEngine.Matrix4x4 ARUtilityFunctions::MatrixFromFloatArray(System.Single[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* Matrix4x4_t2933234003_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2616084920;
extern Il2CppCodeGenString* _stringLiteral1188922308;
extern const uint32_t ARUtilityFunctions_MatrixFromFloatArray_m3542072061_MetadataUsageId;
extern "C"  Matrix4x4_t2933234003  ARUtilityFunctions_MatrixFromFloatArray_m3542072061 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___values0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARUtilityFunctions_MatrixFromFloatArray_m3542072061_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t2933234003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		SingleU5BU5D_t577127397* L_0 = ___values0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		SingleU5BU5D_t577127397* L_1 = ___values0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)((int32_t)16))))
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_2, _stringLiteral2616084920, _stringLiteral1188922308, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0020:
	{
		Initobj (Matrix4x4_t2933234003_il2cpp_TypeInfo_var, (&V_0));
		V_1 = 0;
		goto IL_003e;
	}

IL_002f:
	{
		int32_t L_3 = V_1;
		SingleU5BU5D_t577127397* L_4 = ___values0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		float L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		Matrix4x4_set_Item_m870949794((&V_0), L_3, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)16))))
		{
			goto IL_002f;
		}
	}
	{
		Matrix4x4_t2933234003  L_10 = V_0;
		return L_10;
	}
}
// UnityEngine.Quaternion ARUtilityFunctions::QuaternionFromMatrix(UnityEngine.Matrix4x4)
extern Il2CppClass* ARController_t593870669_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1010803964;
extern const uint32_t ARUtilityFunctions_QuaternionFromMatrix_m2564935062_MetadataUsageId;
extern "C"  Quaternion_t4030073918  ARUtilityFunctions_QuaternionFromMatrix_m2564935062 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  ___m0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARUtilityFunctions_QuaternionFromMatrix_m2564935062_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector4_t2243707581  L_0 = Matrix4x4_GetColumn_m1367096730((&___m0), 2, /*hidden argument*/NULL);
		Vector4_t2243707581  L_1 = Vector4_get_zero_m3810945132(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = Vector4_op_Equality_m1825453464(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARController_t593870669_il2cpp_TypeInfo_var);
		ARController_Log_m1311826870(NULL /*static, unused*/, _stringLiteral1010803964, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_3 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0027:
	{
		Vector4_t2243707581  L_4 = Matrix4x4_GetColumn_m1367096730((&___m0), 2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Vector4_op_Implicit_m1902992875(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Vector4_t2243707581  L_6 = Matrix4x4_GetColumn_m1367096730((&___m0), 1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector4_op_Implicit_m1902992875(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_8 = Quaternion_LookRotation_m700700634(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector3 ARUtilityFunctions::PositionFromMatrix(UnityEngine.Matrix4x4)
extern "C"  Vector3_t2243707580  ARUtilityFunctions_PositionFromMatrix_m2391904295 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  ___m0, const MethodInfo* method)
{
	{
		Vector4_t2243707581  L_0 = Matrix4x4_GetColumn_m1367096730((&___m0), 3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Vector4_op_Implicit_m1902992875(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Matrix4x4 ARUtilityFunctions::LHMatrixFromRHMatrix(UnityEngine.Matrix4x4)
extern Il2CppClass* Matrix4x4_t2933234003_il2cpp_TypeInfo_var;
extern const uint32_t ARUtilityFunctions_LHMatrixFromRHMatrix_m2988276788_MetadataUsageId;
extern "C"  Matrix4x4_t2933234003  ARUtilityFunctions_LHMatrixFromRHMatrix_m2988276788 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  ___rhm0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARUtilityFunctions_LHMatrixFromRHMatrix_m2988276788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t2933234003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t2933234003_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = Matrix4x4_get_Item_m312280350((&___rhm0), 0, 0, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 0, 0, L_0, /*hidden argument*/NULL);
		float L_1 = Matrix4x4_get_Item_m312280350((&___rhm0), 1, 0, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 1, 0, L_1, /*hidden argument*/NULL);
		float L_2 = Matrix4x4_get_Item_m312280350((&___rhm0), 2, 0, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 2, 0, ((-L_2)), /*hidden argument*/NULL);
		float L_3 = Matrix4x4_get_Item_m312280350((&___rhm0), 3, 0, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 3, 0, L_3, /*hidden argument*/NULL);
		float L_4 = Matrix4x4_get_Item_m312280350((&___rhm0), 0, 1, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 0, 1, L_4, /*hidden argument*/NULL);
		float L_5 = Matrix4x4_get_Item_m312280350((&___rhm0), 1, 1, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 1, 1, L_5, /*hidden argument*/NULL);
		float L_6 = Matrix4x4_get_Item_m312280350((&___rhm0), 2, 1, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 2, 1, ((-L_6)), /*hidden argument*/NULL);
		float L_7 = Matrix4x4_get_Item_m312280350((&___rhm0), 3, 1, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 3, 1, L_7, /*hidden argument*/NULL);
		float L_8 = Matrix4x4_get_Item_m312280350((&___rhm0), 0, 2, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 0, 2, ((-L_8)), /*hidden argument*/NULL);
		float L_9 = Matrix4x4_get_Item_m312280350((&___rhm0), 1, 2, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 1, 2, ((-L_9)), /*hidden argument*/NULL);
		float L_10 = Matrix4x4_get_Item_m312280350((&___rhm0), 2, 2, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 2, 2, L_10, /*hidden argument*/NULL);
		float L_11 = Matrix4x4_get_Item_m312280350((&___rhm0), 3, 2, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 3, 2, ((-L_11)), /*hidden argument*/NULL);
		float L_12 = Matrix4x4_get_Item_m312280350((&___rhm0), 0, 3, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 0, 3, L_12, /*hidden argument*/NULL);
		float L_13 = Matrix4x4_get_Item_m312280350((&___rhm0), 1, 3, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 1, 3, L_13, /*hidden argument*/NULL);
		float L_14 = Matrix4x4_get_Item_m312280350((&___rhm0), 2, 3, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 2, 3, ((-L_14)), /*hidden argument*/NULL);
		float L_15 = Matrix4x4_get_Item_m312280350((&___rhm0), 3, 3, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1035113911((&V_0), 3, 3, L_15, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_16 = V_0;
		return L_16;
	}
}
// System.Void CharacterBehaviors::.ctor()
extern "C"  void CharacterBehaviors__ctor_m1152876165 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	{
		__this->set_maxPunchTime_11((0.65f));
		__this->set_glovesShouldRetract_13((bool)1);
		__this->set_totalHealthPoints_18((100.0f));
		__this->set_startLocalPosY_37((-100.0f));
		__this->set_damageGivenPerPunch_40((5.0f));
		__this->set_attackTimeIncrement_41((1.25f));
		__this->set_punchSpeedModifier_42((0.05f));
		__this->set_defenseModifier_43((0.5f));
		__this->set_chanceOfMassiveHit_44(5);
		__this->set_allowTouch_46((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterBehaviors::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisAnimation_t2068071072_m2090634241_MethodInfo_var;
extern const uint32_t CharacterBehaviors_Awake_m3037885346_MetadataUsageId;
extern "C"  void CharacterBehaviors_Awake_m3037885346 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterBehaviors_Awake_m3037885346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Transform_get_parent_m147407266(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Animation_t2068071072 * L_3 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_2, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		__this->set_characterAnimation_50(L_3);
		Animation_t2068071072 * L_4 = __this->get_characterAnimation_50();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0047;
		}
	}
	{
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = Transform_get_parent_m147407266(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Animation_t2068071072 * L_9 = GameObject_AddComponent_TisAnimation_t2068071072_m2090634241(L_8, /*hidden argument*/GameObject_AddComponent_TisAnimation_t2068071072_m2090634241_MethodInfo_var);
		__this->set_characterAnimation_50(L_9);
	}

IL_0047:
	{
		return;
	}
}
// System.Void CharacterBehaviors::Start()
extern Il2CppClass* CharacterBehaviors_t674834278_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisAnimation_t2068071072_m2090634241_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2115673192;
extern Il2CppCodeGenString* _stringLiteral2328219685;
extern const uint32_t CharacterBehaviors_Start_m1108797477_MetadataUsageId;
extern "C"  void CharacterBehaviors_Start_m1108797477 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterBehaviors_Start_m1108797477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CharacterBehaviors_t674834278_il2cpp_TypeInfo_var);
		Camera_t189460977 * L_0 = ((CharacterBehaviors_t674834278_StaticFields*)CharacterBehaviors_t674834278_il2cpp_TypeInfo_var->static_fields)->get_arCamera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		GameObject_t1756533147 * L_2 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral2115673192, /*hidden argument*/NULL);
		NullCheck(L_2);
		Camera_t189460977 * L_3 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_2, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(CharacterBehaviors_t674834278_il2cpp_TypeInfo_var);
		((CharacterBehaviors_t674834278_StaticFields*)CharacterBehaviors_t674834278_il2cpp_TypeInfo_var->static_fields)->set_arCamera_2(L_3);
	}

IL_0024:
	{
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Transform_FindChild_m2677714886(L_4, _stringLiteral2328219685, /*hidden argument*/NULL);
		V_0 = L_5;
		Transform_t3275118058 * L_6 = V_0;
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(L_6, /*hidden argument*/NULL);
		__this->set_myFace_19(L_7);
		GameObject_t1756533147 * L_8 = __this->get_myFace_19();
		NullCheck(L_8);
		Animation_t2068071072 * L_9 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_8, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		__this->set_faceAnimation_51(L_9);
		Animation_t2068071072 * L_10 = __this->get_faceAnimation_51();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0074;
		}
	}
	{
		GameObject_t1756533147 * L_12 = __this->get_myFace_19();
		NullCheck(L_12);
		Animation_t2068071072 * L_13 = GameObject_AddComponent_TisAnimation_t2068071072_m2090634241(L_12, /*hidden argument*/GameObject_AddComponent_TisAnimation_t2068071072_m2090634241_MethodInfo_var);
		__this->set_faceAnimation_51(L_13);
	}

IL_0074:
	{
		int32_t L_14 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0092;
		}
	}
	{
		float L_15 = __this->get_punchSpeedModifier_42();
		__this->set_punchSpeedModifier_42(((float)((float)L_15*(float)(0.5f))));
	}

IL_0092:
	{
		return;
	}
}
// System.Void CharacterBehaviors::OnEnable()
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern const MethodInfo* List_1_Contains_m2391088647_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3441471442_MethodInfo_var;
extern const uint32_t CharacterBehaviors_OnEnable_m3487677605_MetadataUsageId;
extern "C"  void CharacterBehaviors_OnEnable_m3487677605 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterBehaviors_OnEnable_m3487677605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharacterBehaviors_ResetMe_m3493009400(__this, /*hidden argument*/NULL);
		__this->set_okToAnimate_39((bool)0);
		CharacterBehaviors_StartPopUp_m3438804919(__this, /*hidden argument*/NULL);
		FisticuffsController_t308090938 * L_0 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_t1125654279 * L_1 = L_0->get_cardsInPlay_15();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m2764296230(L_1, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		FisticuffsController_t308090938 * L_3 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_maxNumberOfCardsInPlay_3();
		if ((((int32_t)L_2) >= ((int32_t)L_4)))
		{
			goto IL_0060;
		}
	}
	{
		FisticuffsController_t308090938 * L_5 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		List_1_t1125654279 * L_6 = L_5->get_cardsInPlay_15();
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_8 = List_1_Contains_m2391088647(L_6, L_7, /*hidden argument*/List_1_Contains_m2391088647_MethodInfo_var);
		if (L_8)
		{
			goto IL_0060;
		}
	}
	{
		FisticuffsController_t308090938 * L_9 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		List_1_t1125654279 * L_10 = L_9->get_cardsInPlay_15();
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		List_1_Add_m3441471442(L_10, L_11, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
	}

IL_0060:
	{
		return;
	}
}
// System.Void CharacterBehaviors::OnDisable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Remove_m2287078133_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2400499566;
extern const uint32_t CharacterBehaviors_OnDisable_m1321403450_MetadataUsageId;
extern "C"  void CharacterBehaviors_OnDisable_m1321403450 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterBehaviors_OnDisable_m1321403450_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FisticuffsController_t308090938 * L_0 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral2400499566, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		FisticuffsController_t308090938 * L_2 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		List_1_t1125654279 * L_3 = L_2->get_cardsInPlay_15();
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		List_1_Remove_m2287078133(L_3, L_4, /*hidden argument*/List_1_Remove_m2287078133_MethodInfo_var);
		return;
	}
}
// System.Void CharacterBehaviors::ResetMe()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCollider_t3497673348_m2914479322_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2758188231;
extern Il2CppCodeGenString* _stringLiteral1639376636;
extern const uint32_t CharacterBehaviors_ResetMe_m3493009400_MetadataUsageId;
extern "C"  void CharacterBehaviors_ResetMe_m3493009400 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterBehaviors_ResetMe_m3493009400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FisticuffsController_t308090938 * L_0 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral2758188231, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		Animation_t2068071072 * L_2 = __this->get_characterAnimation_50();
		NullCheck(L_2);
		Animation_Play_m976361057(L_2, _stringLiteral1639376636, /*hidden argument*/NULL);
		CharacterBehaviors_SetHealthScaleFactor_m2141324510(__this, /*hidden argument*/NULL);
		CharacterBehaviors_SetHealth_m3563463624(__this, (100.0f), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_myAttributes_25();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_myHealthHolder_26();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = __this->get_leftGlove_33();
		Vector3_t2243707580  L_6 = __this->get_origLeftGloveLocalPos_35();
		NullCheck(L_5);
		Transform_set_localPosition_m1026930133(L_5, L_6, /*hidden argument*/NULL);
		Transform_t3275118058 * L_7 = __this->get_rightGlove_34();
		Vector3_t2243707580  L_8 = __this->get_origRightGloveLocalPos_36();
		NullCheck(L_7);
		Transform_set_localPosition_m1026930133(L_7, L_8, /*hidden argument*/NULL);
		__this->set_punchTimerHasStarted_12((bool)0);
		__this->set_glovesShouldRetract_13((bool)1);
		FisticuffsController_t308090938 * L_9 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_gameIsDone_22((bool)0);
		__this->set_okToAnimate_39((bool)1);
		__this->set_iLost_8((bool)0);
		__this->set_punchPhase_38(0);
		__this->set_allowTouch_46((bool)1);
		__this->set_loseAnimPlayed_47((bool)0);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Collider_t3497673348 * L_11 = GameObject_GetComponent_TisCollider_t3497673348_m2914479322(L_10, /*hidden argument*/GameObject_GetComponent_TisCollider_t3497673348_m2914479322_MethodInfo_var);
		NullCheck(L_11);
		Collider_set_enabled_m3489100454(L_11, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterBehaviors::Update()
extern "C"  void CharacterBehaviors_Update_m1593411844 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	{
		CharacterBehaviors_CheckForGameOver_m3978069034(__this, /*hidden argument*/NULL);
		CharacterBehaviors_TouchTracker_m4253395824(__this, /*hidden argument*/NULL);
		CharacterBehaviors_GlovesRetract_m3584609958(__this, /*hidden argument*/NULL);
		CharacterBehaviors_PopUp_m2295277643(__this, /*hidden argument*/NULL);
		CharacterBehaviors_SetUpPunch_m426768826(__this, /*hidden argument*/NULL);
		CharacterBehaviors_Punch_m1922326469(__this, /*hidden argument*/NULL);
		CharacterBehaviors_Hover_m2887359879(__this, /*hidden argument*/NULL);
		CharacterBehaviors_ResetCheck_m2350668060(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterBehaviors::TouchTracker()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* CharacterBehaviors_t674834278_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3925875275;
extern const uint32_t CharacterBehaviors_TouchTracker_m4253395824_MetadataUsageId;
extern "C"  void CharacterBehaviors_TouchTracker_m4253395824 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterBehaviors_TouchTracker_m4253395824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	Touch_t407273883  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Touch_t407273883  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Touch_t407273883  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Touch_t407273883  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Touch_t407273883  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Ray_t2469606224  V_11;
	memset(&V_11, 0, sizeof(V_11));
	RaycastHit_t87180320  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector3_t2243707580  V_15;
	memset(&V_15, 0, sizeof(V_15));
	int32_t G_B9_0 = 0;
	int32_t G_B16_0 = 0;
	{
		FisticuffsController_t308090938 * L_0 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3925875275, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		Vector2_t2243707579  L_2 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector2_t2243707579  L_3 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		V_3 = (bool)0;
		V_4 = (bool)0;
		V_5 = (bool)0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_4 = Input_get_touchSupported_m3352846145(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00a8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_5 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_5) > ((int32_t)0))? 1 : 0);
		bool L_6 = V_2;
		if (!L_6)
		{
			goto IL_00a3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_7 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_6 = L_7;
		Vector2_t2243707579  L_8 = Touch_get_position_m2079703643((&V_6), /*hidden argument*/NULL);
		V_0 = L_8;
		Touch_t407273883  L_9 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_7 = L_9;
		Vector2_t2243707579  L_10 = Touch_get_deltaPosition_m97688791((&V_7), /*hidden argument*/NULL);
		V_1 = L_10;
		Touch_t407273883  L_11 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_8 = L_11;
		int32_t L_12 = Touch_get_phase_m196706494((&V_8), /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)L_12) == ((int32_t)0))? 1 : 0);
		Touch_t407273883  L_13 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_9 = L_13;
		int32_t L_14 = Touch_get_phase_m196706494((&V_9), /*hidden argument*/NULL);
		V_4 = (bool)((((int32_t)L_14) == ((int32_t)1))? 1 : 0);
		Touch_t407273883  L_15 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = L_15;
		int32_t L_16 = Touch_get_phase_m196706494((&V_10), /*hidden argument*/NULL);
		V_5 = (bool)((((int32_t)L_16) == ((int32_t)3))? 1 : 0);
	}

IL_00a3:
	{
		goto IL_0110;
	}

IL_00a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_17 = Input_GetMouseButton_m464100923(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00bb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_18 = Input_GetMouseButtonUp_m1275967966(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		G_B9_0 = ((int32_t)(L_18));
		goto IL_00bc;
	}

IL_00bb:
	{
		G_B9_0 = 1;
	}

IL_00bc:
	{
		V_2 = (bool)G_B9_0;
		bool L_19 = V_2;
		if (!L_19)
		{
			goto IL_0110;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_20 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_21 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		V_0 = L_21;
		bool L_22 = __this->get_lastTouchValid_49();
		if (!L_22)
		{
			goto IL_00eb;
		}
	}
	{
		Vector2_t2243707579  L_23 = V_0;
		Vector2_t2243707579  L_24 = __this->get_lastTouchPos_48();
		Vector2_t2243707579  L_25 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		V_1 = L_25;
		goto IL_00f1;
	}

IL_00eb:
	{
		Vector2_t2243707579  L_26 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_26;
	}

IL_00f1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_27 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_3 = L_27;
		bool L_28 = Input_GetMouseButtonUp_m1275967966(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_5 = L_28;
		bool L_29 = V_3;
		if (L_29)
		{
			goto IL_010d;
		}
	}
	{
		bool L_30 = V_5;
		G_B16_0 = ((((int32_t)L_30) == ((int32_t)0))? 1 : 0);
		goto IL_010e;
	}

IL_010d:
	{
		G_B16_0 = 0;
	}

IL_010e:
	{
		V_4 = (bool)G_B16_0;
	}

IL_0110:
	{
		bool L_31 = V_2;
		if (!L_31)
		{
			goto IL_028d;
		}
	}
	{
		bool L_32 = __this->get_allowTouch_46();
		if (!L_32)
		{
			goto IL_028d;
		}
	}
	{
		bool L_33 = __this->get_loseAnimPlayed_47();
		if (L_33)
		{
			goto IL_028d;
		}
	}
	{
		bool L_34 = __this->get_iLost_8();
		if (!L_34)
		{
			goto IL_014c;
		}
	}
	{
		__this->set_imTouched_6((bool)0);
		__this->set_okToAnimate_39((bool)1);
		__this->set_allowTouch_46((bool)0);
	}

IL_014c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CharacterBehaviors_t674834278_il2cpp_TypeInfo_var);
		Camera_t189460977 * L_35 = ((CharacterBehaviors_t674834278_StaticFields*)CharacterBehaviors_t674834278_il2cpp_TypeInfo_var->static_fields)->get_arCamera_2();
		Vector2_t2243707579  L_36 = V_0;
		Vector3_t2243707580  L_37 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		Ray_t2469606224  L_38 = Camera_ScreenPointToRay_m614889538(L_35, L_37, /*hidden argument*/NULL);
		V_11 = L_38;
		FisticuffsController_t308090938 * L_39 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_39);
		bool L_40 = L_39->get_gameIsDone_22();
		if (L_40)
		{
			goto IL_028d;
		}
	}
	{
		Ray_t2469606224  L_41 = V_11;
		bool L_42 = Physics_Raycast_m2308457076(NULL /*static, unused*/, L_41, (&V_12), (1000.0f), /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_01bd;
		}
	}
	{
		bool L_43 = V_3;
		if (!L_43)
		{
			goto IL_01bd;
		}
	}
	{
		Collider_t3497673348 * L_44 = RaycastHit_get_collider_m301198172((&V_12), /*hidden argument*/NULL);
		NullCheck(L_44);
		GameObject_t1756533147 * L_45 = Component_get_gameObject_m3105766835(L_44, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_46 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_47 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_01bd;
		}
	}
	{
		__this->set_imTouched_6((bool)1);
		__this->set_okToAnimate_39((bool)0);
		float L_48 = (&V_0)->get_y_1();
		__this->set_originalTouchPosY_3(L_48);
	}

IL_01bd:
	{
		bool L_49 = V_4;
		if (!L_49)
		{
			goto IL_0240;
		}
	}
	{
		bool L_50 = __this->get_imTouched_6();
		if (!L_50)
		{
			goto IL_0240;
		}
	}
	{
		float L_51 = (&V_0)->get_y_1();
		float L_52 = __this->get_originalTouchPosY_3();
		if ((!(((float)L_51) < ((float)L_52))))
		{
			goto IL_0240;
		}
	}
	{
		Transform_t3275118058 * L_53 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_53);
		Vector3_t2243707580  L_54 = Transform_get_localPosition_m2533925116(L_53, /*hidden argument*/NULL);
		V_14 = L_54;
		float L_55 = (&V_14)->get_y_2();
		float L_56 = (&V_1)->get_y_1();
		Vector3__ctor_m2638739322((&V_13), (0.0f), ((float)((float)L_55+(float)((float)((float)L_56*(float)(0.8f))))), (6.0f), /*hidden argument*/NULL);
		float L_57 = (&V_13)->get_y_2();
		float L_58 = __this->get_startLocalPosY_37();
		if ((!(((float)L_57) < ((float)L_58))))
		{
			goto IL_0233;
		}
	}
	{
		float L_59 = __this->get_startLocalPosY_37();
		(&V_13)->set_y_2(L_59);
	}

IL_0233:
	{
		Transform_t3275118058 * L_60 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_61 = V_13;
		NullCheck(L_60);
		Transform_set_localPosition_m1026930133(L_60, L_61, /*hidden argument*/NULL);
	}

IL_0240:
	{
		bool L_62 = V_5;
		if (!L_62)
		{
			goto IL_028d;
		}
	}
	{
		bool L_63 = __this->get_imTouched_6();
		if (!L_63)
		{
			goto IL_028d;
		}
	}
	{
		__this->set_imTouched_6((bool)0);
		Transform_t3275118058 * L_64 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_64);
		Vector3_t2243707580  L_65 = Transform_get_localPosition_m2533925116(L_64, /*hidden argument*/NULL);
		V_15 = L_65;
		float L_66 = (&V_15)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_67 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_66, (30.0f), /*hidden argument*/NULL);
		if (L_67)
		{
			goto IL_0287;
		}
	}
	{
		CharacterBehaviors_StartPopUp_m3438804919(__this, /*hidden argument*/NULL);
		goto IL_028d;
	}

IL_0287:
	{
		CharacterBehaviors_StopPopUp_m2532184579(__this, /*hidden argument*/NULL);
	}

IL_028d:
	{
		bool L_68 = V_2;
		__this->set_lastTouchValid_49(L_68);
		Vector2_t2243707579  L_69 = V_0;
		__this->set_lastTouchPos_48(L_69);
		return;
	}
}
// System.Void CharacterBehaviors::PopUp()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t CharacterBehaviors_PopUp_m2295277643_MetadataUsageId;
extern "C"  void CharacterBehaviors_PopUp_m2295277643 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterBehaviors_PopUp_m2295277643_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		bool L_0 = __this->get_iLost_8();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_localPosition_m2533925116(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = __this->get_popUpMode_5();
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0045;
		}
	}
	{
		float L_4 = (&V_0)->get_y_2();
		if ((!(((float)L_4) < ((float)(50.0f)))))
		{
			goto IL_0045;
		}
	}
	{
		__this->set_popTargetHeight_4((50.0f));
		goto IL_007f;
	}

IL_0045:
	{
		int32_t L_5 = __this->get_popUpMode_5();
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_007f;
		}
	}
	{
		float L_6 = (&V_0)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_7 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_6, (50.0f), /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0078;
		}
	}
	{
		float L_8 = (&V_0)->get_y_2();
		if ((!(((float)L_8) > ((float)(50.0f)))))
		{
			goto IL_007f;
		}
	}

IL_0078:
	{
		__this->set_popUpMode_5(2);
	}

IL_007f:
	{
		int32_t L_9 = __this->get_popUpMode_5();
		if ((!(((uint32_t)L_9) == ((uint32_t)2))))
		{
			goto IL_00ac;
		}
	}
	{
		float L_10 = (&V_0)->get_y_2();
		if ((!(((float)L_10) > ((float)(15.0f)))))
		{
			goto IL_00ac;
		}
	}
	{
		__this->set_popTargetHeight_4((15.0f));
		goto IL_00e6;
	}

IL_00ac:
	{
		int32_t L_11 = __this->get_popUpMode_5();
		if ((!(((uint32_t)L_11) == ((uint32_t)2))))
		{
			goto IL_00e6;
		}
	}
	{
		float L_12 = (&V_0)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_13 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_12, (15.0f), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_00df;
		}
	}
	{
		float L_14 = (&V_0)->get_y_2();
		if ((!(((float)L_14) < ((float)(15.0f)))))
		{
			goto IL_00e6;
		}
	}

IL_00df:
	{
		__this->set_popUpMode_5(3);
	}

IL_00e6:
	{
		int32_t L_15 = __this->get_popUpMode_5();
		if ((!(((uint32_t)L_15) == ((uint32_t)3))))
		{
			goto IL_0113;
		}
	}
	{
		float L_16 = (&V_0)->get_y_2();
		if ((!(((float)L_16) < ((float)(30.0f)))))
		{
			goto IL_0113;
		}
	}
	{
		__this->set_popTargetHeight_4((30.0f));
		goto IL_014c;
	}

IL_0113:
	{
		int32_t L_17 = __this->get_popUpMode_5();
		if ((!(((uint32_t)L_17) == ((uint32_t)3))))
		{
			goto IL_014c;
		}
	}
	{
		float L_18 = (&V_0)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_19 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_18, (30.0f), /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0146;
		}
	}
	{
		float L_20 = (&V_0)->get_y_2();
		if ((!(((float)L_20) > ((float)(30.0f)))))
		{
			goto IL_014c;
		}
	}

IL_0146:
	{
		CharacterBehaviors_StopPopUp_m2532184579(__this, /*hidden argument*/NULL);
	}

IL_014c:
	{
		int32_t L_21 = __this->get_popUpMode_5();
		if (!L_21)
		{
			goto IL_018d;
		}
	}
	{
		float L_22 = (&V_0)->get_y_2();
		float L_23 = __this->get_popTargetHeight_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_24 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_22, L_23, (1.0f), /*hidden argument*/NULL);
		V_1 = L_24;
		float L_25 = V_1;
		Vector3__ctor_m2638739322((&V_2), (0.0f), L_25, (6.0f), /*hidden argument*/NULL);
		Transform_t3275118058 * L_26 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_27 = V_2;
		NullCheck(L_26);
		Transform_set_localPosition_m1026930133(L_26, L_27, /*hidden argument*/NULL);
	}

IL_018d:
	{
		return;
	}
}
// System.Void CharacterBehaviors::StartPopUp()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3048238950;
extern const uint32_t CharacterBehaviors_StartPopUp_m3438804919_MetadataUsageId;
extern "C"  void CharacterBehaviors_StartPopUp_m3438804919 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterBehaviors_StartPopUp_m3438804919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FisticuffsController_t308090938 * L_0 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3048238950, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		__this->set_popUpMode_5(1);
		bool L_2 = __this->get_okToPlaySounds_7();
		if (!L_2)
		{
			goto IL_0046;
		}
	}
	{
		FisticuffsController_t308090938 * L_3 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		AudioSource_t1135106623 * L_4 = L_3->get_oneShotAudio_18();
		FisticuffsController_t308090938 * L_5 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		AudioClip_t1932558630 * L_6 = L_5->get_pop_8();
		NullCheck(L_4);
		AudioSource_PlayOneShot_m286472761(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0046:
	{
		__this->set_okToPlaySounds_7((bool)1);
		return;
	}
}
// System.Void CharacterBehaviors::StopPopUp()
extern "C"  void CharacterBehaviors_StopPopUp_m2532184579 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	{
		__this->set_popUpMode_5(0);
		__this->set_okToAnimate_39((bool)1);
		return;
	}
}
// System.Void CharacterBehaviors::GlovesRetract()
extern "C"  void CharacterBehaviors_GlovesRetract_m3584609958 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = __this->get_glovesShouldRetract_13();
		if (!L_0)
		{
			goto IL_0096;
		}
	}
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_localPosition_m2533925116(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = (&V_1)->get_y_2();
		V_0 = ((float)((float)((float)((float)L_3-(float)(30.0f)))*(float)(0.6923077f)));
		GameObject_t1756533147 * L_4 = __this->get_myLeftGloveHolder_31();
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_localEulerAngles_m4231787854(L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		GameObject_t1756533147 * L_7 = __this->get_myLeftGloveHolder_31();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		float L_9 = (&V_2)->get_x_1();
		float L_10 = (&V_2)->get_y_2();
		float L_11 = V_0;
		Vector3_t2243707580  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2638739322(&L_12, L_9, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_localEulerAngles_m2927195985(L_8, L_12, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_13 = __this->get_myRightGloveHolder_32();
		NullCheck(L_13);
		Transform_t3275118058 * L_14 = GameObject_get_transform_m909382139(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = Transform_get_localEulerAngles_m4231787854(L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		GameObject_t1756533147 * L_16 = __this->get_myRightGloveHolder_32();
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = GameObject_get_transform_m909382139(L_16, /*hidden argument*/NULL);
		float L_18 = (&V_3)->get_x_1();
		float L_19 = (&V_3)->get_y_2();
		float L_20 = V_0;
		Vector3_t2243707580  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m2638739322(&L_21, L_18, L_19, ((-L_20)), /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_localEulerAngles_m2927195985(L_17, L_21, /*hidden argument*/NULL);
	}

IL_0096:
	{
		return;
	}
}
// System.Void CharacterBehaviors::SetUpPunch()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1764411785;
extern Il2CppCodeGenString* _stringLiteral1081293081;
extern const uint32_t CharacterBehaviors_SetUpPunch_m426768826_MetadataUsageId;
extern "C"  void CharacterBehaviors_SetUpPunch_m426768826 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterBehaviors_SetUpPunch_m426768826_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		FisticuffsController_t308090938 * L_0 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1764411785, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		bool L_2 = __this->get_okToAnimate_39();
		if (!L_2)
		{
			goto IL_0159;
		}
	}
	{
		FisticuffsController_t308090938 * L_3 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = L_3->get_gameIsDone_22();
		if (L_4)
		{
			goto IL_0159;
		}
	}
	{
		FisticuffsController_t308090938 * L_5 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = L_5->get_gameHasStarted_21();
		if (!L_6)
		{
			goto IL_0061;
		}
	}
	{
		bool L_7 = __this->get_punchTimerHasStarted_12();
		if (L_7)
		{
			goto IL_0061;
		}
	}
	{
		__this->set_punchTimerHasStarted_12((bool)1);
		float L_8 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_punchTimer_9(L_8);
	}

IL_0061:
	{
		FisticuffsController_t308090938 * L_9 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		bool L_10 = L_9->get_gameHasStarted_21();
		if (L_10)
		{
			goto IL_0077;
		}
	}
	{
		__this->set_punchTimerHasStarted_12((bool)0);
	}

IL_0077:
	{
		FisticuffsController_t308090938 * L_11 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		bool L_12 = L_11->get_gameHasStarted_21();
		if (!L_12)
		{
			goto IL_0159;
		}
	}
	{
		float L_13 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = __this->get_punchTimer_9();
		float L_15 = __this->get_modifiedAttackTimeIncrement_45();
		if ((!(((float)L_13) > ((float)((float)((float)L_14+(float)L_15))))))
		{
			goto IL_0159;
		}
	}
	{
		float L_16 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_punchTimer_9(L_16);
		GameObject_t1756533147 * L_17 = __this->get_targetPointColliderObj_28();
		Transform_t3275118058 * L_18 = __this->get_opponentTargetPoint_30();
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = Transform_get_position_m1104419803(L_18, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_20 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_21 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_17, L_19, L_20, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_0 = L_21;
		GameObject_t1756533147 * L_22 = V_0;
		NullCheck(L_22);
		GameObject_set_tag_m717375123(L_22, _stringLiteral1081293081, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_23 = V_0;
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = GameObject_get_transform_m909382139(L_23, /*hidden argument*/NULL);
		Transform_t3275118058 * L_25 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_parent_m3281327839(L_24, L_25, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_26 = V_0;
		__this->set_myTempTarget_29(L_26);
		__this->set_punchPhase_38(1);
		float L_27 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_punchStartTime_10(L_27);
		int32_t L_28 = Random_Range_m694320887(NULL /*static, unused*/, 1, 3, /*hidden argument*/NULL);
		V_1 = L_28;
		int32_t L_29 = V_1;
		if ((!(((uint32_t)L_29) == ((uint32_t)1))))
		{
			goto IL_0125;
		}
	}
	{
		Transform_t3275118058 * L_30 = __this->get_leftGlove_33();
		__this->set_punchingGlove_14(L_30);
		Vector3_t2243707580  L_31 = __this->get_origLeftGloveLocalPos_35();
		__this->set_punchingGloveStartPosition_15(L_31);
		goto IL_013d;
	}

IL_0125:
	{
		Transform_t3275118058 * L_32 = __this->get_rightGlove_34();
		__this->set_punchingGlove_14(L_32);
		Vector3_t2243707580  L_33 = __this->get_origRightGloveLocalPos_36();
		__this->set_punchingGloveStartPosition_15(L_33);
	}

IL_013d:
	{
		float L_34 = __this->get_attackTimeIncrement_41();
		float L_35 = Random_Range_m2884721203(NULL /*static, unused*/, (-0.5f), (0.5f), /*hidden argument*/NULL);
		__this->set_modifiedAttackTimeIncrement_45(((float)((float)L_34+(float)L_35)));
	}

IL_0159:
	{
		return;
	}
}
// System.Void CharacterBehaviors::Punch()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t CharacterBehaviors_Punch_m1922326469_MetadataUsageId;
extern "C"  void CharacterBehaviors_Punch_m1922326469 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterBehaviors_Punch_m1922326469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = __this->get_punchPhase_38();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_002a;
		}
	}
	{
		float L_1 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_punchStartTime_10();
		float L_3 = __this->get_maxPunchTime_11();
		if ((!(((float)L_1) > ((float)((float)((float)L_2+(float)L_3))))))
		{
			goto IL_002a;
		}
	}
	{
		__this->set_punchPhase_38(2);
	}

IL_002a:
	{
		Vector3_t2243707580  L_4 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = __this->get_punchPhase_38();
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_0094;
		}
	}
	{
		Transform_t3275118058 * L_6 = __this->get_punchingGlove_14();
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = __this->get_myTempTarget_29();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = Transform_get_position_m1104419803(L_9, /*hidden argument*/NULL);
		bool L_11 = Vector3_op_Inequality_m799191452(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0094;
		}
	}
	{
		Transform_t3275118058 * L_12 = __this->get_punchingGlove_14();
		Transform_t3275118058 * L_13 = __this->get_punchingGlove_14();
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = Transform_get_position_m1104419803(L_13, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_15 = __this->get_myTempTarget_29();
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = GameObject_get_transform_m909382139(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t2243707580  L_17 = Transform_get_position_m1104419803(L_16, /*hidden argument*/NULL);
		float L_18 = __this->get_punchSpeedModifier_42();
		Vector3_t2243707580  L_19 = Vector3_SmoothDamp_m3087890513(NULL /*static, unused*/, L_14, L_17, (&V_0), L_18, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_position_m2469242620(L_12, L_19, /*hidden argument*/NULL);
	}

IL_0094:
	{
		int32_t L_20 = __this->get_punchPhase_38();
		if ((!(((uint32_t)L_20) == ((uint32_t)2))))
		{
			goto IL_0125;
		}
	}
	{
		Transform_t3275118058 * L_21 = __this->get_punchingGlove_14();
		NullCheck(L_21);
		Vector3_t2243707580  L_22 = Transform_get_localPosition_m2533925116(L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		float L_23 = (&V_1)->get_x_1();
		Vector3_t2243707580 * L_24 = __this->get_address_of_punchingGloveStartPosition_15();
		float L_25 = L_24->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_26 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_23, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00f0;
		}
	}
	{
		Transform_t3275118058 * L_27 = __this->get_punchingGlove_14();
		NullCheck(L_27);
		Vector3_t2243707580  L_28 = Transform_get_localPosition_m2533925116(L_27, /*hidden argument*/NULL);
		V_2 = L_28;
		float L_29 = (&V_2)->get_z_3();
		Vector3_t2243707580 * L_30 = __this->get_address_of_punchingGloveStartPosition_15();
		float L_31 = L_30->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_32 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_29, L_31, /*hidden argument*/NULL);
		if (L_32)
		{
			goto IL_011e;
		}
	}

IL_00f0:
	{
		Transform_t3275118058 * L_33 = __this->get_punchingGlove_14();
		Transform_t3275118058 * L_34 = __this->get_punchingGlove_14();
		NullCheck(L_34);
		Vector3_t2243707580  L_35 = Transform_get_localPosition_m2533925116(L_34, /*hidden argument*/NULL);
		Vector3_t2243707580  L_36 = __this->get_punchingGloveStartPosition_15();
		float L_37 = __this->get_punchSpeedModifier_42();
		Vector3_t2243707580  L_38 = Vector3_SmoothDamp_m3087890513(NULL /*static, unused*/, L_35, L_36, (&V_0), L_37, /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_set_localPosition_m1026930133(L_33, L_38, /*hidden argument*/NULL);
		goto IL_0125;
	}

IL_011e:
	{
		__this->set_punchPhase_38(0);
	}

IL_0125:
	{
		return;
	}
}
// System.Void CharacterBehaviors::Hover()
extern Il2CppCodeGenString* _stringLiteral302432435;
extern Il2CppCodeGenString* _stringLiteral919667940;
extern Il2CppCodeGenString* _stringLiteral3927388421;
extern const uint32_t CharacterBehaviors_Hover_m2887359879_MetadataUsageId;
extern "C"  void CharacterBehaviors_Hover_m2887359879 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterBehaviors_Hover_m2887359879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_okToAnimate_39();
		if (!L_0)
		{
			goto IL_005c;
		}
	}
	{
		Animation_t2068071072 * L_1 = __this->get_characterAnimation_50();
		NullCheck(L_1);
		bool L_2 = Animation_IsPlaying_m1305767247(L_1, _stringLiteral302432435, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		Animation_t2068071072 * L_3 = __this->get_characterAnimation_50();
		NullCheck(L_3);
		Animation_Play_m976361057(L_3, _stringLiteral302432435, /*hidden argument*/NULL);
	}

IL_0031:
	{
		Animation_t2068071072 * L_4 = __this->get_faceAnimation_51();
		NullCheck(L_4);
		bool L_5 = Animation_IsPlaying_m1305767247(L_4, _stringLiteral919667940, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0057;
		}
	}
	{
		Animation_t2068071072 * L_6 = __this->get_faceAnimation_51();
		NullCheck(L_6);
		Animation_Play_m976361057(L_6, _stringLiteral3927388421, /*hidden argument*/NULL);
	}

IL_0057:
	{
		goto IL_007c;
	}

IL_005c:
	{
		Animation_t2068071072 * L_7 = __this->get_characterAnimation_50();
		NullCheck(L_7);
		bool L_8 = Animation_IsPlaying_m1305767247(L_7, _stringLiteral302432435, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_007c;
		}
	}
	{
		Animation_t2068071072 * L_9 = __this->get_characterAnimation_50();
		NullCheck(L_9);
		Animation_Stop_m1726655695(L_9, /*hidden argument*/NULL);
	}

IL_007c:
	{
		return;
	}
}
// System.Void CharacterBehaviors::ResetCheck()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral14041493;
extern const uint32_t CharacterBehaviors_ResetCheck_m2350668060_MetadataUsageId;
extern "C"  void CharacterBehaviors_ResetCheck_m2350668060 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterBehaviors_ResetCheck_m2350668060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FisticuffsController_t308090938 * L_0 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral14041493, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		GameObject_t1756533147 * L_2 = __this->get_myHealthHolder_26();
		NullCheck(L_2);
		bool L_3 = GameObject_get_activeInHierarchy_m4242915935(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004f;
		}
	}
	{
		FisticuffsController_t308090938 * L_4 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		List_1_t1125654279 * L_5 = L_4->get_cardsInPlay_15();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m2764296230(L_5, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		FisticuffsController_t308090938 * L_7 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = L_7->get_cardsNeededForGameToStart_4();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_004f;
		}
	}
	{
		CharacterBehaviors_ResetMe_m3493009400(__this, /*hidden argument*/NULL);
	}

IL_004f:
	{
		return;
	}
}
// System.Void CharacterBehaviors::CalculateDamageToOpponent()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m939767277_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCharacterBehaviors_t674834278_m289165397_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3576867463;
extern const uint32_t CharacterBehaviors_CalculateDamageToOpponent_m1939745900_MetadataUsageId;
extern "C"  void CharacterBehaviors_CalculateDamageToOpponent_m1939745900 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterBehaviors_CalculateDamageToOpponent_m1939745900_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Transform_t3275118058 * V_1 = NULL;
	CharacterBehaviors_t674834278 * V_2 = NULL;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	{
		FisticuffsController_t308090938 * L_0 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3576867463, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		V_0 = 0;
		int32_t L_2 = __this->get_myPositionInControllerList_24();
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		V_0 = 1;
	}

IL_002a:
	{
		FisticuffsController_t308090938 * L_3 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		List_1_t1125654279 * L_4 = L_3->get_cardsInPlay_15();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		GameObject_t1756533147 * L_6 = List_1_get_Item_m939767277(L_4, L_5, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Transform_t3275118058 * L_8 = V_1;
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		CharacterBehaviors_t674834278 * L_10 = GameObject_GetComponent_TisCharacterBehaviors_t674834278_m289165397(L_9, /*hidden argument*/GameObject_GetComponent_TisCharacterBehaviors_t674834278_m289165397_MethodInfo_var);
		V_2 = L_10;
		float L_11 = Random_Range_m2884721203(NULL /*static, unused*/, (-1.0f), (1.0f), /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = Random_Range_m2884721203(NULL /*static, unused*/, (-0.1f), (0.1f), /*hidden argument*/NULL);
		V_4 = L_12;
		float L_13 = __this->get_damageGivenPerPunch_40();
		float L_14 = V_3;
		V_5 = ((float)((float)L_13+(float)L_14));
		float L_15 = V_5;
		CharacterBehaviors_t674834278 * L_16 = V_2;
		NullCheck(L_16);
		float L_17 = L_16->get_defenseModifier_43();
		float L_18 = V_5;
		V_6 = ((float)((float)L_15-(float)((float)((float)L_17*(float)L_18))));
		int32_t L_19 = Random_Range_m694320887(NULL /*static, unused*/, 0, ((int32_t)1001), /*hidden argument*/NULL);
		V_8 = L_19;
		int32_t L_20 = V_8;
		int32_t L_21 = __this->get_chanceOfMassiveHit_44();
		if ((((int32_t)L_20) > ((int32_t)L_21)))
		{
			goto IL_00b0;
		}
	}
	{
		FisticuffsController_t308090938 * L_22 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_22);
		float L_23 = L_22->get_megaDamageAmount_20();
		V_7 = L_23;
		goto IL_00b4;
	}

IL_00b0:
	{
		float L_24 = V_6;
		V_7 = L_24;
	}

IL_00b4:
	{
		CharacterBehaviors_t674834278 * L_25 = V_2;
		float L_26 = V_7;
		NullCheck(L_25);
		CharacterBehaviors_ReceiveDamage_m2006513562(L_25, L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterBehaviors::ReceiveDamage(System.Single)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral610373448;
extern Il2CppCodeGenString* _stringLiteral919667940;
extern const uint32_t CharacterBehaviors_ReceiveDamage_m2006513562_MetadataUsageId;
extern "C"  void CharacterBehaviors_ReceiveDamage_m2006513562 (CharacterBehaviors_t674834278 * __this, float ___howMuchDamage0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterBehaviors_ReceiveDamage_m2006513562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	GameObject_t1756533147 * V_4 = NULL;
	float V_5 = 0.0f;
	{
		FisticuffsController_t308090938 * L_0 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral610373448, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		Animation_t2068071072 * L_2 = __this->get_faceAnimation_51();
		NullCheck(L_2);
		Animation_Play_m976361057(L_2, _stringLiteral919667940, /*hidden argument*/NULL);
		float L_3 = ___howMuchDamage0;
		FisticuffsController_t308090938 * L_4 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		float L_5 = L_4->get_megaDamageAmount_20();
		if ((!(((float)L_3) == ((float)L_5))))
		{
			goto IL_00c2;
		}
	}
	{
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = (&V_1)->get_x_1();
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = Transform_get_position_m1104419803(L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = (&V_2)->get_y_2();
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = (&V_3)->get_z_3();
		Vector3__ctor_m2638739322((&V_0), L_8, ((float)((float)L_11+(float)(50.0f))), L_14, /*hidden argument*/NULL);
		FisticuffsController_t308090938 * L_15 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		GameObject_t1756533147 * L_16 = L_15->get_megaHit_6();
		NullCheck(L_16);
		GameObject_SetActive_m2887581199(L_16, (bool)1, /*hidden argument*/NULL);
		FisticuffsController_t308090938 * L_17 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		AudioSource_t1135106623 * L_18 = L_17->get_oneShotAudio_18();
		FisticuffsController_t308090938 * L_19 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		AudioClip_t1932558630 * L_20 = L_19->get_hitExplosion_14();
		NullCheck(L_18);
		AudioSource_PlayOneShot_m286472761(L_18, L_20, /*hidden argument*/NULL);
		FisticuffsController_t308090938 * L_21 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		GameObject_t1756533147 * L_22 = L_21->get_megaHitParticles_7();
		Vector3_t2243707580  L_23 = V_0;
		Quaternion_t4030073918  L_24 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_25 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_22, L_23, L_24, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_4 = L_25;
	}

IL_00c2:
	{
		float L_26 = __this->get_totalHealthPoints_18();
		float L_27 = ___howMuchDamage0;
		V_5 = ((float)((float)L_26-(float)L_27));
		float L_28 = V_5;
		if ((!(((float)L_28) <= ((float)(0.0f)))))
		{
			goto IL_010a;
		}
	}
	{
		V_5 = (0.0f);
		FisticuffsController_t308090938 * L_29 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_29);
		AudioSource_t1135106623 * L_30 = L_29->get_oneShotAudio_18();
		FisticuffsController_t308090938 * L_31 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_31);
		AudioClip_t1932558630 * L_32 = L_31->get_hitExplosion_14();
		NullCheck(L_30);
		AudioSource_PlayOneShot_m286472761(L_30, L_32, /*hidden argument*/NULL);
		FisticuffsController_t308090938 * L_33 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_33);
		L_33->set_gameIsDone_22((bool)1);
		__this->set_iLost_8((bool)1);
	}

IL_010a:
	{
		float L_34 = V_5;
		CharacterBehaviors_SetHealth_m3563463624(__this, L_34, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterBehaviors::SetHealth(System.Single)
extern "C"  void CharacterBehaviors_SetHealth_m3563463624 (CharacterBehaviors_t674834278 * __this, float ___newHealthPoints0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___newHealthPoints0;
		__this->set_totalHealthPoints_18(L_0);
		float L_1 = __this->get_totalHealthPoints_18();
		float L_2 = __this->get_healthScaleFactor_16();
		V_0 = ((float)((float)L_1/(float)L_2));
		GameObject_t1756533147 * L_3 = __this->get_myHealthScaler_27();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		float L_5 = V_0;
		Vector3_t2243707580 * L_6 = __this->get_address_of_healthOriginalScale_17();
		float L_7 = L_6->get_y_2();
		Vector3_t2243707580 * L_8 = __this->get_address_of_healthOriginalScale_17();
		float L_9 = L_8->get_z_3();
		Vector3_t2243707580  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2638739322(&L_10, L_5, L_7, L_9, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localScale_m2325460848(L_4, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterBehaviors::SetHealthScaleFactor()
extern "C"  void CharacterBehaviors_SetHealthScaleFactor_m2141324510 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_healthScaleFactor_16();
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_003d;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_myHealthScaler_27();
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_localScale_m3074381503(L_2, /*hidden argument*/NULL);
		__this->set_healthOriginalScale_17(L_3);
		Vector3_t2243707580 * L_4 = __this->get_address_of_healthOriginalScale_17();
		float L_5 = L_4->get_x_1();
		__this->set_healthScaleFactor_16(((float)((float)(100.0f)/(float)L_5)));
	}

IL_003d:
	{
		return;
	}
}
// System.Void CharacterBehaviors::CheckForGameOver()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCollider_t3497673348_m2914479322_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral610373448;
extern Il2CppCodeGenString* _stringLiteral3641632200;
extern Il2CppCodeGenString* _stringLiteral3240152713;
extern const uint32_t CharacterBehaviors_CheckForGameOver_m3978069034_MetadataUsageId;
extern "C"  void CharacterBehaviors_CheckForGameOver_m3978069034 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterBehaviors_CheckForGameOver_m3978069034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FisticuffsController_t308090938 * L_0 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral610373448, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		FisticuffsController_t308090938 * L_2 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = L_2->get_gameIsDone_22();
		if (!L_3)
		{
			goto IL_00ba;
		}
	}
	{
		bool L_4 = __this->get_okToAnimate_39();
		if (!L_4)
		{
			goto IL_00ba;
		}
	}
	{
		bool L_5 = __this->get_loseAnimPlayed_47();
		if (L_5)
		{
			goto IL_00ba;
		}
	}
	{
		__this->set_okToAnimate_39((bool)0);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m2638739322(&L_7, (0.0f), (30.0f), (6.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_localPosition_m1026930133(L_6, L_7, /*hidden argument*/NULL);
		bool L_8 = __this->get_iLost_8();
		if (!L_8)
		{
			goto IL_00a9;
		}
	}
	{
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Collider_t3497673348 * L_10 = GameObject_GetComponent_TisCollider_t3497673348_m2914479322(L_9, /*hidden argument*/GameObject_GetComponent_TisCollider_t3497673348_m2914479322_MethodInfo_var);
		NullCheck(L_10);
		Collider_set_enabled_m3489100454(L_10, (bool)0, /*hidden argument*/NULL);
		__this->set_loseAnimPlayed_47((bool)1);
		FisticuffsController_t308090938 * L_11 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		FisticuffsController_GameEnd_m456451874(L_11, /*hidden argument*/NULL);
		Animation_t2068071072 * L_12 = __this->get_characterAnimation_50();
		NullCheck(L_12);
		Animation_Play_m976361057(L_12, _stringLiteral3641632200, /*hidden argument*/NULL);
		goto IL_00ba;
	}

IL_00a9:
	{
		Animation_t2068071072 * L_13 = __this->get_characterAnimation_50();
		NullCheck(L_13);
		Animation_Play_m976361057(L_13, _stringLiteral3240152713, /*hidden argument*/NULL);
	}

IL_00ba:
	{
		return;
	}
}
// System.Void CharacterBehaviors::.cctor()
extern "C"  void CharacterBehaviors__cctor_m4292261400 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FisticuffsController::.ctor()
extern Il2CppClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m704351054_MethodInfo_var;
extern const uint32_t FisticuffsController__ctor_m19705551_MetadataUsageId;
extern "C"  void FisticuffsController__ctor_m19705551 (FisticuffsController_t308090938 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FisticuffsController__ctor_m19705551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_maxNumberOfCardsInPlay_3(2);
		__this->set_cardsNeededForGameToStart_4(2);
		List_1_t1125654279 * L_0 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_0, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		__this->set_cardsInPlay_15(L_0);
		__this->set_timeAllowedBeforeReset_17((5.0f));
		__this->set_megaDamageAmount_20((20.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// FisticuffsController FisticuffsController::get_Instance()
extern Il2CppClass* FisticuffsController_t308090938_il2cpp_TypeInfo_var;
extern const uint32_t FisticuffsController_get_Instance_m3869543890_MetadataUsageId;
extern "C"  FisticuffsController_t308090938 * FisticuffsController_get_Instance_m3869543890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FisticuffsController_get_Instance_m3869543890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FisticuffsController_t308090938 * L_0 = ((FisticuffsController_t308090938_StaticFields*)FisticuffsController_t308090938_il2cpp_TypeInfo_var->static_fields)->get_U3CInstanceU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void FisticuffsController::set_Instance(FisticuffsController)
extern Il2CppClass* FisticuffsController_t308090938_il2cpp_TypeInfo_var;
extern const uint32_t FisticuffsController_set_Instance_m2765316899_MetadataUsageId;
extern "C"  void FisticuffsController_set_Instance_m2765316899 (Il2CppObject * __this /* static, unused */, FisticuffsController_t308090938 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FisticuffsController_set_Instance_m2765316899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FisticuffsController_t308090938 * L_0 = ___value0;
		((FisticuffsController_t308090938_StaticFields*)FisticuffsController_t308090938_il2cpp_TypeInfo_var->static_fields)->set_U3CInstanceU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void FisticuffsController::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3961728767;
extern const uint32_t FisticuffsController_Awake_m2424235722_MetadataUsageId;
extern "C"  void FisticuffsController_Awake_m2424235722 (FisticuffsController_t308090938 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FisticuffsController_Awake_m2424235722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FisticuffsController_t308090938 * L_0 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		FisticuffsController_set_Instance_m2765316899(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		goto IL_0025;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3961728767, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void FisticuffsController::Start()
extern "C"  void FisticuffsController_Start_m2623175131 (FisticuffsController_t308090938 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_bell_5();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_megaHit_6();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FisticuffsController::Update()
extern "C"  void FisticuffsController_Update_m1668360200 (FisticuffsController_t308090938 * __this, const MethodInfo* method)
{
	{
		FisticuffsController_CheckIfTwoCards_m1789851537(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_ready_16();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		FisticuffsController_LookAtEachOther_m2746055666(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void FisticuffsController::CheckIfTwoCards()
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m939767277_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCharacterBehaviors_t674834278_m152693849_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var;
extern const uint32_t FisticuffsController_CheckIfTwoCards_m1789851537_MetadataUsageId;
extern "C"  void FisticuffsController_CheckIfTwoCards_m1789851537 (FisticuffsController_t308090938 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FisticuffsController_CheckIfTwoCards_m1789851537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1125654279 * L_0 = __this->get_cardsInPlay_15();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m2764296230(L_0, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		int32_t L_2 = __this->get_cardsNeededForGameToStart_4();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_009d;
		}
	}
	{
		bool L_3 = __this->get_ready_16();
		if (L_3)
		{
			goto IL_009d;
		}
	}
	{
		List_1_t1125654279 * L_4 = __this->get_cardsInPlay_15();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = List_1_get_Item_m939767277(L_4, 0, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		__this->set_character1_23(L_6);
		List_1_t1125654279 * L_7 = __this->get_cardsInPlay_15();
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = List_1_get_Item_m939767277(L_7, 1, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		__this->set_character2_24(L_9);
		Transform_t3275118058 * L_10 = __this->get_character1_23();
		NullCheck(L_10);
		CharacterBehaviors_t674834278 * L_11 = Component_GetComponent_TisCharacterBehaviors_t674834278_m152693849(L_10, /*hidden argument*/Component_GetComponent_TisCharacterBehaviors_t674834278_m152693849_MethodInfo_var);
		__this->set_char1behaviors_25(L_11);
		Transform_t3275118058 * L_12 = __this->get_character2_24();
		NullCheck(L_12);
		CharacterBehaviors_t674834278 * L_13 = Component_GetComponent_TisCharacterBehaviors_t674834278_m152693849(L_12, /*hidden argument*/Component_GetComponent_TisCharacterBehaviors_t674834278_m152693849_MethodInfo_var);
		__this->set_char2behaviors_26(L_13);
		CharacterBehaviors_t674834278 * L_14 = __this->get_char1behaviors_25();
		NullCheck(L_14);
		L_14->set_myPositionInControllerList_24(0);
		CharacterBehaviors_t674834278 * L_15 = __this->get_char2behaviors_26();
		NullCheck(L_15);
		L_15->set_myPositionInControllerList_24(1);
		__this->set_ready_16((bool)1);
		Il2CppObject * L_16 = FisticuffsController_IntroStart_m1902157099(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_16, /*hidden argument*/NULL);
	}

IL_009d:
	{
		List_1_t1125654279 * L_17 = __this->get_cardsInPlay_15();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2764296230(L_17, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		int32_t L_19 = __this->get_cardsNeededForGameToStart_4();
		if ((((int32_t)L_18) == ((int32_t)L_19)))
		{
			goto IL_00f3;
		}
	}
	{
		bool L_20 = __this->get_ready_16();
		if (!L_20)
		{
			goto IL_00f3;
		}
	}
	{
		__this->set_ready_16((bool)0);
		__this->set_gameHasStarted_21((bool)0);
		GameObject_t1756533147 * L_21 = __this->get_bell_5();
		NullCheck(L_21);
		Animation_t2068071072 * L_22 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_21, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_22);
		Animation_Stop_m1726655695(L_22, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_23 = __this->get_bell_5();
		NullCheck(L_23);
		GameObject_SetActive_m2887581199(L_23, (bool)0, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_24 = __this->get_crowdAudio_19();
		NullCheck(L_24);
		AudioSource_Stop_m3452679614(L_24, /*hidden argument*/NULL);
	}

IL_00f3:
	{
		return;
	}
}
// System.Collections.IEnumerator FisticuffsController::IntroStart()
extern Il2CppClass* U3CIntroStartU3Ec__Iterator0_t3871887700_il2cpp_TypeInfo_var;
extern const uint32_t FisticuffsController_IntroStart_m1902157099_MetadataUsageId;
extern "C"  Il2CppObject * FisticuffsController_IntroStart_m1902157099 (FisticuffsController_t308090938 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FisticuffsController_IntroStart_m1902157099_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CIntroStartU3Ec__Iterator0_t3871887700 * V_0 = NULL;
	{
		U3CIntroStartU3Ec__Iterator0_t3871887700 * L_0 = (U3CIntroStartU3Ec__Iterator0_t3871887700 *)il2cpp_codegen_object_new(U3CIntroStartU3Ec__Iterator0_t3871887700_il2cpp_TypeInfo_var);
		U3CIntroStartU3Ec__Iterator0__ctor_m3149942269(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CIntroStartU3Ec__Iterator0_t3871887700 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CIntroStartU3Ec__Iterator0_t3871887700 * L_2 = V_0;
		return L_2;
	}
}
// System.Void FisticuffsController::GameStart()
extern "C"  void FisticuffsController_GameStart_m702030753 (FisticuffsController_t308090938 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1135106623 * L_0 = __this->get_crowdAudio_19();
		NullCheck(L_0);
		AudioSource_Play_m353744792(L_0, /*hidden argument*/NULL);
		__this->set_gameHasStarted_21((bool)1);
		CharacterBehaviors_t674834278 * L_1 = __this->get_char1behaviors_25();
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = L_1->get_myHealthHolder_26();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)1, /*hidden argument*/NULL);
		CharacterBehaviors_t674834278 * L_3 = __this->get_char2behaviors_26();
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = L_3->get_myHealthHolder_26();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FisticuffsController::LookAtEachOther()
extern "C"  void FisticuffsController_LookAtEachOther_m2746055666 (FisticuffsController_t308090938 * __this, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t3275118058 * L_0 = __this->get_character1_23();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = __this->get_character2_24();
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		float L_4 = Vector3_Distance_m1859670022(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		if ((!(((double)(((double)((double)L_4)))) > ((double)(0.01)))))
		{
			goto IL_012c;
		}
	}
	{
		CharacterBehaviors_t674834278 * L_5 = __this->get_char1behaviors_25();
		NullCheck(L_5);
		bool L_6 = L_5->get_okToAnimate_39();
		if (!L_6)
		{
			goto IL_00ab;
		}
	}
	{
		Transform_t3275118058 * L_7 = __this->get_character2_24();
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_position_m1104419803(L_7, /*hidden argument*/NULL);
		Transform_t3275118058 * L_9 = __this->get_character1_23();
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = Transform_get_parent_m147407266(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t2243707580  L_12 = Transform_get_position_m1104419803(L_11, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_8, L_12, /*hidden argument*/NULL);
		Transform_t3275118058 * L_14 = __this->get_character1_23();
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = Transform_get_parent_m147407266(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t2243707580  L_17 = Transform_get_up_m1603627763(L_16, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_18 = Quaternion_LookRotation_m700700634(NULL /*static, unused*/, L_13, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		Transform_t3275118058 * L_19 = __this->get_character1_23();
		NullCheck(L_19);
		Transform_t3275118058 * L_20 = Component_get_transform_m2697483695(L_19, /*hidden argument*/NULL);
		Transform_t3275118058 * L_21 = __this->get_character1_23();
		NullCheck(L_21);
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Quaternion_t4030073918  L_23 = Transform_get_rotation_m1033555130(L_22, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_24 = V_0;
		float L_25 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_26 = Quaternion_Slerp_m1992855400(NULL /*static, unused*/, L_23, L_24, ((float)((float)L_25*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_rotation_m3411284563(L_20, L_26, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		CharacterBehaviors_t674834278 * L_27 = __this->get_char2behaviors_26();
		NullCheck(L_27);
		bool L_28 = L_27->get_okToAnimate_39();
		if (!L_28)
		{
			goto IL_012c;
		}
	}
	{
		Transform_t3275118058 * L_29 = __this->get_character1_23();
		NullCheck(L_29);
		Vector3_t2243707580  L_30 = Transform_get_position_m1104419803(L_29, /*hidden argument*/NULL);
		Transform_t3275118058 * L_31 = __this->get_character2_24();
		NullCheck(L_31);
		Transform_t3275118058 * L_32 = Transform_get_parent_m147407266(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Transform_t3275118058 * L_33 = Component_get_transform_m2697483695(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t2243707580  L_34 = Transform_get_position_m1104419803(L_33, /*hidden argument*/NULL);
		Vector3_t2243707580  L_35 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_30, L_34, /*hidden argument*/NULL);
		Transform_t3275118058 * L_36 = __this->get_character2_24();
		NullCheck(L_36);
		Transform_t3275118058 * L_37 = Transform_get_parent_m147407266(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_t3275118058 * L_38 = Component_get_transform_m2697483695(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		Vector3_t2243707580  L_39 = Transform_get_up_m1603627763(L_38, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_40 = Quaternion_LookRotation_m700700634(NULL /*static, unused*/, L_35, L_39, /*hidden argument*/NULL);
		V_1 = L_40;
		Transform_t3275118058 * L_41 = __this->get_character2_24();
		NullCheck(L_41);
		Transform_t3275118058 * L_42 = Component_get_transform_m2697483695(L_41, /*hidden argument*/NULL);
		Transform_t3275118058 * L_43 = __this->get_character2_24();
		NullCheck(L_43);
		Transform_t3275118058 * L_44 = Component_get_transform_m2697483695(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		Quaternion_t4030073918  L_45 = Transform_get_rotation_m1033555130(L_44, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_46 = V_1;
		float L_47 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_48 = Quaternion_Slerp_m1992855400(NULL /*static, unused*/, L_45, L_46, ((float)((float)L_47*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_42);
		Transform_set_rotation_m3411284563(L_42, L_48, /*hidden argument*/NULL);
	}

IL_012c:
	{
		return;
	}
}
// System.Void FisticuffsController::TakeAim()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3339230585;
extern const uint32_t FisticuffsController_TakeAim_m3709832649_MetadataUsageId;
extern "C"  void FisticuffsController_TakeAim_m3709832649 (FisticuffsController_t308090938 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FisticuffsController_TakeAim_m3709832649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharacterBehaviors_t674834278 * L_0 = __this->get_char1behaviors_25();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = L_0->get_opponentTargetPoint_30();
		Transform_t3275118058 * L_2 = __this->get_character2_24();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Transform_FindChild_m2677714886(L_2, _stringLiteral3339230585, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		CharacterBehaviors_t674834278 * L_5 = __this->get_char1behaviors_25();
		Transform_t3275118058 * L_6 = __this->get_character2_24();
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = Transform_FindChild_m2677714886(L_6, _stringLiteral3339230585, /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->set_opponentTargetPoint_30(L_7);
	}

IL_0040:
	{
		CharacterBehaviors_t674834278 * L_8 = __this->get_char2behaviors_26();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = L_8->get_opponentTargetPoint_30();
		Transform_t3275118058 * L_10 = __this->get_character1_23();
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Transform_FindChild_m2677714886(L_10, _stringLiteral3339230585, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0080;
		}
	}
	{
		CharacterBehaviors_t674834278 * L_13 = __this->get_char2behaviors_26();
		Transform_t3275118058 * L_14 = __this->get_character1_23();
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = Transform_FindChild_m2677714886(L_14, _stringLiteral3339230585, /*hidden argument*/NULL);
		NullCheck(L_13);
		L_13->set_opponentTargetPoint_30(L_15);
	}

IL_0080:
	{
		return;
	}
}
// System.Void FisticuffsController::GameEnd()
extern "C"  void FisticuffsController_GameEnd_m456451874 (FisticuffsController_t308090938 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1135106623 * L_0 = __this->get_oneShotAudio_18();
		AudioClip_t1932558630 * L_1 = __this->get_crash_12();
		NullCheck(L_0);
		AudioSource_PlayOneShot_m286472761(L_0, L_1, /*hidden argument*/NULL);
		Il2CppObject * L_2 = FisticuffsController_EndBell_m1572967765(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator FisticuffsController::EndBell()
extern Il2CppClass* U3CEndBellU3Ec__Iterator1_t868562403_il2cpp_TypeInfo_var;
extern const uint32_t FisticuffsController_EndBell_m1572967765_MetadataUsageId;
extern "C"  Il2CppObject * FisticuffsController_EndBell_m1572967765 (FisticuffsController_t308090938 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FisticuffsController_EndBell_m1572967765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CEndBellU3Ec__Iterator1_t868562403 * V_0 = NULL;
	{
		U3CEndBellU3Ec__Iterator1_t868562403 * L_0 = (U3CEndBellU3Ec__Iterator1_t868562403 *)il2cpp_codegen_object_new(U3CEndBellU3Ec__Iterator1_t868562403_il2cpp_TypeInfo_var);
		U3CEndBellU3Ec__Iterator1__ctor_m572293778(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEndBellU3Ec__Iterator1_t868562403 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CEndBellU3Ec__Iterator1_t868562403 * L_2 = V_0;
		return L_2;
	}
}
// System.Void FisticuffsController/<EndBell>c__Iterator1::.ctor()
extern "C"  void U3CEndBellU3Ec__Iterator1__ctor_m572293778 (U3CEndBellU3Ec__Iterator1_t868562403 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean FisticuffsController/<EndBell>c__Iterator1::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1591714047;
extern const uint32_t U3CEndBellU3Ec__Iterator1_MoveNext_m372940338_MetadataUsageId;
extern "C"  bool U3CEndBellU3Ec__Iterator1_MoveNext_m372940338 (U3CEndBellU3Ec__Iterator1_t868562403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEndBellU3Ec__Iterator1_MoveNext_m372940338_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_0093;
	}

IL_0021:
	{
		WaitForSeconds_t3839502067 * L_2 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_2, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0040:
	{
		goto IL_0095;
	}

IL_0045:
	{
		FisticuffsController_t308090938 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = L_4->get_bell_5();
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)1, /*hidden argument*/NULL);
		FisticuffsController_t308090938 * L_6 = __this->get_U24this_0();
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = L_6->get_bell_5();
		NullCheck(L_7);
		Animation_t2068071072 * L_8 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_7, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_8);
		Animation_Play_m976361057(L_8, _stringLiteral1591714047, /*hidden argument*/NULL);
		FisticuffsController_t308090938 * L_9 = __this->get_U24this_0();
		NullCheck(L_9);
		AudioSource_t1135106623 * L_10 = L_9->get_oneShotAudio_18();
		FisticuffsController_t308090938 * L_11 = __this->get_U24this_0();
		NullCheck(L_11);
		AudioClip_t1932558630 * L_12 = L_11->get_victory_13();
		NullCheck(L_10);
		AudioSource_PlayOneShot_m286472761(L_10, L_12, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_0093:
	{
		return (bool)0;
	}

IL_0095:
	{
		return (bool)1;
	}
}
// System.Object FisticuffsController/<EndBell>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEndBellU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m626065106 (U3CEndBellU3Ec__Iterator1_t868562403 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object FisticuffsController/<EndBell>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEndBellU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m466185690 (U3CEndBellU3Ec__Iterator1_t868562403 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void FisticuffsController/<EndBell>c__Iterator1::Dispose()
extern "C"  void U3CEndBellU3Ec__Iterator1_Dispose_m2051336465 (U3CEndBellU3Ec__Iterator1_t868562403 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void FisticuffsController/<EndBell>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CEndBellU3Ec__Iterator1_Reset_m4036382267_MetadataUsageId;
extern "C"  void U3CEndBellU3Ec__Iterator1_Reset_m4036382267 (U3CEndBellU3Ec__Iterator1_t868562403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEndBellU3Ec__Iterator1_Reset_m4036382267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void FisticuffsController/<IntroStart>c__Iterator0::.ctor()
extern "C"  void U3CIntroStartU3Ec__Iterator0__ctor_m3149942269 (U3CIntroStartU3Ec__Iterator0_t3871887700 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean FisticuffsController/<IntroStart>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2846564031;
extern const uint32_t U3CIntroStartU3Ec__Iterator0_MoveNext_m503637435_MetadataUsageId;
extern "C"  bool U3CIntroStartU3Ec__Iterator0_MoveNext_m503637435 (U3CIntroStartU3Ec__Iterator0_t3871887700 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CIntroStartU3Ec__Iterator0_MoveNext_m503637435_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0050;
		}
	}
	{
		goto IL_00bf;
	}

IL_0021:
	{
		FisticuffsController_t308090938 * L_2 = __this->get_U24this_0();
		NullCheck(L_2);
		FisticuffsController_TakeAim_m3709832649(L_2, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, (1.7f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_3);
		bool L_4 = __this->get_U24disposing_2();
		if (L_4)
		{
			goto IL_004b;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_004b:
	{
		goto IL_00c1;
	}

IL_0050:
	{
		FisticuffsController_t308090938 * L_5 = __this->get_U24this_0();
		NullCheck(L_5);
		bool L_6 = L_5->get_ready_16();
		if (!L_6)
		{
			goto IL_00b8;
		}
	}
	{
		FisticuffsController_t308090938 * L_7 = __this->get_U24this_0();
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = L_7->get_bell_5();
		NullCheck(L_8);
		GameObject_SetActive_m2887581199(L_8, (bool)1, /*hidden argument*/NULL);
		FisticuffsController_t308090938 * L_9 = __this->get_U24this_0();
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = L_9->get_bell_5();
		NullCheck(L_10);
		Animation_t2068071072 * L_11 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_10, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_11);
		Animation_Play_m976361057(L_11, _stringLiteral2846564031, /*hidden argument*/NULL);
		FisticuffsController_t308090938 * L_12 = __this->get_U24this_0();
		NullCheck(L_12);
		CharacterBehaviors_t674834278 * L_13 = L_12->get_char1behaviors_25();
		NullCheck(L_13);
		GameObject_t1756533147 * L_14 = L_13->get_myAttributes_25();
		NullCheck(L_14);
		GameObject_SetActive_m2887581199(L_14, (bool)0, /*hidden argument*/NULL);
		FisticuffsController_t308090938 * L_15 = __this->get_U24this_0();
		NullCheck(L_15);
		CharacterBehaviors_t674834278 * L_16 = L_15->get_char2behaviors_26();
		NullCheck(L_16);
		GameObject_t1756533147 * L_17 = L_16->get_myAttributes_25();
		NullCheck(L_17);
		GameObject_SetActive_m2887581199(L_17, (bool)0, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		__this->set_U24PC_3((-1));
	}

IL_00bf:
	{
		return (bool)0;
	}

IL_00c1:
	{
		return (bool)1;
	}
}
// System.Object FisticuffsController/<IntroStart>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIntroStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2993597583 (U3CIntroStartU3Ec__Iterator0_t3871887700 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object FisticuffsController/<IntroStart>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIntroStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1169169895 (U3CIntroStartU3Ec__Iterator0_t3871887700 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void FisticuffsController/<IntroStart>c__Iterator0::Dispose()
extern "C"  void U3CIntroStartU3Ec__Iterator0_Dispose_m1828718102 (U3CIntroStartU3Ec__Iterator0_t3871887700 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void FisticuffsController/<IntroStart>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CIntroStartU3Ec__Iterator0_Reset_m1306905816_MetadataUsageId;
extern "C"  void U3CIntroStartU3Ec__Iterator0_Reset_m1306905816 (U3CIntroStartU3Ec__Iterator0_t3871887700 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CIntroStartU3Ec__Iterator0_Reset_m1306905816_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void FollowCameraPrecise::.ctor()
extern "C"  void FollowCameraPrecise__ctor_m207961366 (FollowCameraPrecise_t1971515421 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FollowCameraPrecise::LateUpdate()
extern Il2CppCodeGenString* _stringLiteral3767752037;
extern const uint32_t FollowCameraPrecise_LateUpdate_m1186855005_MetadataUsageId;
extern "C"  void FollowCameraPrecise_LateUpdate_m1186855005 (FollowCameraPrecise_t1971515421 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FollowCameraPrecise_LateUpdate_m1186855005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3767752037, /*hidden argument*/NULL);
		V_0 = L_0;
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = V_0;
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_position_m1104419803(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_m2469242620(L_1, L_4, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = V_0;
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Quaternion_t4030073918  L_8 = Transform_get_rotation_m1033555130(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_rotation_m3411284563(L_5, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GloveScript::.ctor()
extern "C"  void GloveScript__ctor_m450649515 (GloveScript_t3172908102 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GloveScript::Start()
extern const MethodInfo* GameObject_GetComponentInParent_TisCharacterBehaviors_t674834278_m2550472710_MethodInfo_var;
extern const uint32_t GloveScript_Start_m2534267511_MetadataUsageId;
extern "C"  void GloveScript_Start_m2534267511 (GloveScript_t3172908102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GloveScript_Start_m2534267511_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		CharacterBehaviors_t674834278 * L_1 = GameObject_GetComponentInParent_TisCharacterBehaviors_t674834278_m2550472710(L_0, /*hidden argument*/GameObject_GetComponentInParent_TisCharacterBehaviors_t674834278_m2550472710_MethodInfo_var);
		__this->set_characterBehvaiors_3(L_1);
		return;
	}
}
// System.Void GloveScript::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral287558685;
extern Il2CppCodeGenString* _stringLiteral4083037397;
extern Il2CppCodeGenString* _stringLiteral1776489660;
extern Il2CppCodeGenString* _stringLiteral159138813;
extern Il2CppCodeGenString* _stringLiteral1081293081;
extern const uint32_t GloveScript_OnTriggerEnter_m3362502079_MetadataUsageId;
extern "C"  void GloveScript_OnTriggerEnter_m3362502079 (GloveScript_t3172908102 * __this, Collider_t3497673348 * ___hit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GloveScript_OnTriggerEnter_m3362502079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FisticuffsController_t308090938 * L_0 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral287558685, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		Collider_t3497673348 * L_2 = ___hit0;
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = GameObject_get_tag_m1425941094(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004f;
		}
	}
	{
		Collider_t3497673348 * L_6 = ___hit0;
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m2079638459(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral4083037397, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		goto IL_0167;
	}

IL_004f:
	{
		Collider_t3497673348 * L_10 = ___hit0;
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = GameObject_get_tag_m1425941094(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_12, _stringLiteral1776489660, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00a5;
		}
	}
	{
		CharacterBehaviors_t674834278 * L_14 = __this->get_characterBehvaiors_3();
		NullCheck(L_14);
		int32_t L_15 = L_14->get_punchPhase_38();
		if ((((int32_t)L_15) <= ((int32_t)0)))
		{
			goto IL_00a5;
		}
	}
	{
		FisticuffsController_t308090938 * L_16 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		AudioSource_t1135106623 * L_17 = L_16->get_oneShotAudio_18();
		FisticuffsController_t308090938 * L_18 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		AudioClip_t1932558630 * L_19 = L_18->get_punchMiss_10();
		NullCheck(L_17);
		AudioSource_PlayOneShot_m286472761(L_17, L_19, /*hidden argument*/NULL);
		Collider_t3497673348 * L_20 = ___hit0;
		NullCheck(L_20);
		GameObject_t1756533147 * L_21 = Component_get_gameObject_m3105766835(L_20, /*hidden argument*/NULL);
		GloveScript_FinishPunch_m1990033543(__this, L_21, (bool)0, /*hidden argument*/NULL);
		goto IL_0167;
	}

IL_00a5:
	{
		Collider_t3497673348 * L_22 = ___hit0;
		NullCheck(L_22);
		GameObject_t1756533147 * L_23 = Component_get_gameObject_m3105766835(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		String_t* L_24 = GameObject_get_tag_m1425941094(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_24, _stringLiteral159138813, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0116;
		}
	}
	{
		CharacterBehaviors_t674834278 * L_26 = __this->get_characterBehvaiors_3();
		NullCheck(L_26);
		int32_t L_27 = L_26->get_punchPhase_38();
		if ((((int32_t)L_27) <= ((int32_t)0)))
		{
			goto IL_0116;
		}
	}
	{
		Collider_t3497673348 * L_28 = ___hit0;
		NullCheck(L_28);
		GameObject_t1756533147 * L_29 = Component_get_gameObject_m3105766835(L_28, /*hidden argument*/NULL);
		CharacterBehaviors_t674834278 * L_30 = __this->get_characterBehvaiors_3();
		NullCheck(L_30);
		GameObject_t1756533147 * L_31 = Component_get_gameObject_m3105766835(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_32 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_29, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0111;
		}
	}
	{
		FisticuffsController_t308090938 * L_33 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_33);
		AudioSource_t1135106623 * L_34 = L_33->get_oneShotAudio_18();
		FisticuffsController_t308090938 * L_35 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_35);
		AudioClip_t1932558630 * L_36 = L_35->get_punchHit_9();
		NullCheck(L_34);
		AudioSource_PlayOneShot_m286472761(L_34, L_36, /*hidden argument*/NULL);
		Collider_t3497673348 * L_37 = ___hit0;
		NullCheck(L_37);
		GameObject_t1756533147 * L_38 = Component_get_gameObject_m3105766835(L_37, /*hidden argument*/NULL);
		GloveScript_FinishPunch_m1990033543(__this, L_38, (bool)1, /*hidden argument*/NULL);
	}

IL_0111:
	{
		goto IL_0167;
	}

IL_0116:
	{
		Collider_t3497673348 * L_39 = ___hit0;
		NullCheck(L_39);
		GameObject_t1756533147 * L_40 = Component_get_gameObject_m3105766835(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		String_t* L_41 = GameObject_get_tag_m1425941094(L_40, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_42 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_41, _stringLiteral1081293081, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0167;
		}
	}
	{
		CharacterBehaviors_t674834278 * L_43 = __this->get_characterBehvaiors_3();
		NullCheck(L_43);
		int32_t L_44 = L_43->get_punchPhase_38();
		if ((((int32_t)L_44) <= ((int32_t)0)))
		{
			goto IL_0167;
		}
	}
	{
		FisticuffsController_t308090938 * L_45 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_45);
		AudioSource_t1135106623 * L_46 = L_45->get_oneShotAudio_18();
		FisticuffsController_t308090938 * L_47 = FisticuffsController_get_Instance_m3869543890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_47);
		AudioClip_t1932558630 * L_48 = L_47->get_punchMiss_10();
		NullCheck(L_46);
		AudioSource_PlayOneShot_m286472761(L_46, L_48, /*hidden argument*/NULL);
		Collider_t3497673348 * L_49 = ___hit0;
		NullCheck(L_49);
		GameObject_t1756533147 * L_50 = Component_get_gameObject_m3105766835(L_49, /*hidden argument*/NULL);
		GloveScript_FinishPunch_m1990033543(__this, L_50, (bool)0, /*hidden argument*/NULL);
	}

IL_0167:
	{
		return;
	}
}
// System.Void GloveScript::FinishPunch(UnityEngine.GameObject,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const uint32_t GloveScript_FinishPunch_m1990033543_MetadataUsageId;
extern "C"  void GloveScript_FinishPunch_m1990033543 (GloveScript_t3172908102 * __this, GameObject_t1756533147 * ___otherCharacter0, bool ___hit1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GloveScript_FinishPunch_m1990033543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		CharacterBehaviors_t674834278 * L_0 = __this->get_characterBehvaiors_3();
		NullCheck(L_0);
		L_0->set_punchPhase_38(2);
		bool L_1 = ___hit1;
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_hitPoof_2();
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_position_m1104419803(L_3, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_5 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_6 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_2, L_4, L_5, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_0 = L_6;
		CharacterBehaviors_t674834278 * L_7 = __this->get_characterBehvaiors_3();
		NullCheck(L_7);
		CharacterBehaviors_CalculateDamageToOpponent_m1939745900(L_7, /*hidden argument*/NULL);
	}

IL_0039:
	{
		CharacterBehaviors_t674834278 * L_8 = __this->get_characterBehvaiors_3();
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = L_8->get_myTempTarget_29();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_005f;
		}
	}
	{
		CharacterBehaviors_t674834278 * L_11 = __this->get_characterBehvaiors_3();
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = L_11->get_myTempTarget_29();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_005f:
	{
		return;
	}
}
// System.Void JsonManager::.ctor()
extern "C"  void JsonManager__ctor_m1386307564 (JsonManager_t1028810929 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JsonManager::Start()
extern "C"  void JsonManager_Start_m500403212 (JsonManager_t1028810929 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void JsonManager::Update()
extern "C"  void JsonManager_Update_m2955290545 (JsonManager_t1028810929 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 JsonManager::FakeWebServer()
extern Il2CppClass* JsonManager_t1028810929_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlDocument_t3649534162_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1414245037;
extern Il2CppCodeGenString* _stringLiteral3199101968;
extern Il2CppCodeGenString* _stringLiteral1791380807;
extern const uint32_t JsonManager_FakeWebServer_m300552568_MetadataUsageId;
extern "C"  int32_t JsonManager_FakeWebServer_m300552568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonManager_FakeWebServer_m300552568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XmlDocument_t3649534162 * V_0 = NULL;
	XmlNode_t616554813 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		Object_t1021602117 * L_0 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral1414245037, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		((JsonManager_t1028810929_StaticFields*)JsonManager_t1028810929_il2cpp_TypeInfo_var->static_fields)->set_filename_3(L_1);
		XmlDocument_t3649534162 * L_2 = (XmlDocument_t3649534162 *)il2cpp_codegen_object_new(XmlDocument_t3649534162_il2cpp_TypeInfo_var);
		XmlDocument__ctor_m3336214851(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		XmlDocument_t3649534162 * L_3 = V_0;
		String_t* L_4 = ((JsonManager_t1028810929_StaticFields*)JsonManager_t1028810929_il2cpp_TypeInfo_var->static_fields)->get_filename_3();
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(53 /* System.Void System.Xml.XmlDocument::LoadXml(System.String) */, L_3, L_4);
		XmlDocument_t3649534162 * L_5 = V_0;
		NullCheck(L_5);
		XmlElement_t2877111883 * L_6 = XmlDocument_get_DocumentElement_m898495187(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		XmlNode_t616554813 * L_7 = VirtFuncInvoker0< XmlNode_t616554813 * >::Invoke(10 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_6);
		V_1 = L_7;
		XmlNode_t616554813 * L_8 = V_1;
		NullCheck(L_8);
		XmlAttributeCollection_t3359885287 * L_9 = VirtFuncInvoker0< XmlAttributeCollection_t3359885287 * >::Invoke(6 /* System.Xml.XmlAttributeCollection System.Xml.XmlNode::get_Attributes() */, L_8);
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Xml.XmlNamedNodeMap::get_Count() */, L_9);
		((JsonManager_t1028810929_StaticFields*)JsonManager_t1028810929_il2cpp_TypeInfo_var->static_fields)->set_countPlay_4(L_10);
		int32_t L_11 = ((JsonManager_t1028810929_StaticFields*)JsonManager_t1028810929_il2cpp_TypeInfo_var->static_fields)->get_countPlay_4();
		((JsonManager_t1028810929_StaticFields*)JsonManager_t1028810929_il2cpp_TypeInfo_var->static_fields)->set_numString_5(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_11)));
		V_2 = 0;
		goto IL_009f;
	}

IL_0057:
	{
		StringU5BU5D_t1642385972* L_12 = ((JsonManager_t1028810929_StaticFields*)JsonManager_t1028810929_il2cpp_TypeInfo_var->static_fields)->get_numString_5();
		int32_t L_13 = V_2;
		XmlNode_t616554813 * L_14 = V_1;
		NullCheck(L_14);
		XmlAttributeCollection_t3359885287 * L_15 = VirtFuncInvoker0< XmlAttributeCollection_t3359885287 * >::Invoke(6 /* System.Xml.XmlAttributeCollection System.Xml.XmlNode::get_Attributes() */, L_14);
		int32_t L_16 = V_2;
		NullCheck(L_15);
		XmlAttribute_t175731005 * L_17 = XmlAttributeCollection_get_ItemOf_m177692376(L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String System.Xml.XmlNode::get_InnerText() */, L_17);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_18);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_13), (String_t*)L_18);
		StringU5BU5D_t1642385972* L_19 = ((JsonManager_t1028810929_StaticFields*)JsonManager_t1028810929_il2cpp_TypeInfo_var->static_fields)->get_numString_5();
		int32_t L_20 = V_2;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		String_t* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		StringU5BU5D_t1642385972* L_23 = ((JsonManager_t1028810929_StaticFields*)JsonManager_t1028810929_il2cpp_TypeInfo_var->static_fields)->get_numString_5();
		int32_t L_24 = V_2;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		String_t* L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_27 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_26, _stringLiteral3199101968, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_009b;
		}
	}
	{
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral1791380807, /*hidden argument*/NULL);
	}

IL_009b:
	{
		int32_t L_28 = V_2;
		V_2 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_009f:
	{
		int32_t L_29 = V_2;
		int32_t L_30 = ((JsonManager_t1028810929_StaticFields*)JsonManager_t1028810929_il2cpp_TypeInfo_var->static_fields)->get_countPlay_4();
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_31 = ((JsonManager_t1028810929_StaticFields*)JsonManager_t1028810929_il2cpp_TypeInfo_var->static_fields)->get_countPlay_4();
		return L_31;
	}
}
// System.Void Logo::.ctor()
extern "C"  void Logo__ctor_m2042197794 (Logo_t2205197457 * __this, const MethodInfo* method)
{
	{
		__this->set_speedRotate_2((0.1f));
		__this->set_desireScale_4((0.02f));
		__this->set_desirePos_5((-0.05f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Logo::Start()
extern "C"  void Logo_Start_m1689820146 (Logo_t2205197457 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_startScale_3();
		float L_2 = __this->get_startScale_3();
		float L_3 = __this->get_startScale_3();
		Vector3_t2243707580  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m2638739322(&L_4, L_1, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localScale_m2325460848(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Logo::Update()
extern Il2CppClass* Main_t2809994845_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Logo_Update_m4152945649_MetadataUsageId;
extern "C"  void Logo_Update_m4152945649 (Logo_t2205197457 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logo_Update_m4152945649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2809994845_il2cpp_TypeInfo_var);
		bool L_0 = ((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->get_bFound_19();
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		__this->set_startScale_3((0.0f));
		__this->set_desireScale_4((0.02f));
		__this->set_desirePos_5((-0.05f));
	}

IL_002b:
	{
		float L_1 = __this->get_startScale_3();
		float L_2 = __this->get_desireScale_4();
		float L_3 = __this->get_t_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set_lerpScale_6(L_4);
		float L_5 = __this->get_desirePos_5();
		float L_6 = __this->get_t_8();
		float L_7 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, (0.0f), L_5, L_6, /*hidden argument*/NULL);
		__this->set_lerpPos_7(L_7);
		float L_8 = __this->get_t_8();
		float L_9 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_t_8(((float)((float)L_8+(float)((float)((float)L_9*(float)(0.2f))))));
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_11 = __this->get_speedRotate_2();
		NullCheck(L_10);
		Transform_Rotate_m4255273365(L_10, (0.0f), L_11, (0.0f), /*hidden argument*/NULL);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_13 = __this->get_lerpScale_6();
		float L_14 = __this->get_lerpScale_6();
		float L_15 = __this->get_lerpScale_6();
		Vector3_t2243707580  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m2638739322(&L_16, L_13, L_14, L_15, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localScale_m2325460848(L_12, L_16, /*hidden argument*/NULL);
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_18 = __this->get_lerpPos_7();
		Vector3_t2243707580  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m2638739322(&L_19, (0.0f), (0.0f), L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_localPosition_m1026930133(L_17, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Logo::Reset()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisLogo_t2205197457_m4031711006_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3928224635;
extern Il2CppCodeGenString* _stringLiteral3089150521;
extern const uint32_t Logo_Reset_m359214433_MetadataUsageId;
extern "C"  void Logo_Reset_m359214433 (Logo_t2205197457 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logo_Reset_m359214433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3928224635, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3089150521, /*hidden argument*/NULL);
		V_1 = L_1;
		GameObject_t1756533147 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		GameObject_t1756533147 * L_4 = V_0;
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = GameObject_get_gameObject_m3662236595(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Logo_t2205197457 * L_6 = GameObject_GetComponent_TisLogo_t2205197457_m4031711006(L_5, /*hidden argument*/GameObject_GetComponent_TisLogo_t2205197457_m4031711006_MethodInfo_var);
		NullCheck(L_6);
		L_6->set_desireScale_4((0.001f));
	}

IL_0037:
	{
		GameObject_t1756533147 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_7, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0058;
		}
	}
	{
		GameObject_t1756533147 * L_9 = V_1;
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = GameObject_get_gameObject_m3662236595(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Logo_t2205197457 * L_11 = GameObject_GetComponent_TisLogo_t2205197457_m4031711006(L_10, /*hidden argument*/GameObject_GetComponent_TisLogo_t2205197457_m4031711006_MethodInfo_var);
		NullCheck(L_11);
		L_11->set_desireScale_4((0.001f));
	}

IL_0058:
	{
		return;
	}
}
// System.Void LookAtCameraFisticuffs::.ctor()
extern "C"  void LookAtCameraFisticuffs__ctor_m3764978938 (LookAtCameraFisticuffs_t2130711525 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LookAtCameraFisticuffs::OnEnable()
extern "C"  void LookAtCameraFisticuffs_OnEnable_m4294516114 (LookAtCameraFisticuffs_t2130711525 * __this, const MethodInfo* method)
{
	{
		LookAtCameraFisticuffs_FindCamera_m225812980(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LookAtCameraFisticuffs::FindCamera()
extern Il2CppCodeGenString* _stringLiteral2115673192;
extern const uint32_t LookAtCameraFisticuffs_FindCamera_m225812980_MetadataUsageId;
extern "C"  void LookAtCameraFisticuffs_FindCamera_m225812980 (LookAtCameraFisticuffs_t2130711525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LookAtCameraFisticuffs_FindCamera_m225812980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral2115673192, /*hidden argument*/NULL);
		__this->set_theCamera_2(L_0);
		return;
	}
}
// System.Void LookAtCameraFisticuffs::Update()
extern "C"  void LookAtCameraFisticuffs_Update_m2234155241 (LookAtCameraFisticuffs_t2130711525 * __this, const MethodInfo* method)
{
	{
		LookAtCameraFisticuffs_RotateMe_m3541710813(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LookAtCameraFisticuffs::RotateMe()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t LookAtCameraFisticuffs_RotateMe_m3541710813_MetadataUsageId;
extern "C"  void LookAtCameraFisticuffs_RotateMe_m3541710813 (LookAtCameraFisticuffs_t2130711525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LookAtCameraFisticuffs_RotateMe_m3541710813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Quaternion_t4030073918  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		GameObject_t1756533147 * L_0 = __this->get_theCamera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		LookAtCameraFisticuffs_FindCamera_m225812980(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		GameObject_t1756533147 * L_2 = __this->get_theCamera_2();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Quaternion_t4030073918  L_5 = Transform_get_rotation_m1033555130(L_4, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = V_0;
		NullCheck(L_6);
		Quaternion_t4030073918  L_7 = Transform_get_rotation_m1033555130(L_6, /*hidden argument*/NULL);
		bool L_8 = Quaternion_op_Inequality_m3629786166(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_006e;
		}
	}
	{
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		Transform_t3275118058 * L_11 = V_0;
		NullCheck(L_11);
		Quaternion_t4030073918  L_12 = Transform_get_rotation_m1033555130(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_14 = V_1;
		Quaternion_t4030073918  L_15 = V_2;
		float L_16 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_17 = Quaternion_Slerp_m1992855400(NULL /*static, unused*/, L_14, L_15, ((float)((float)L_16*(float)(5.0f))), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_rotation_m3411284563(L_13, L_17, /*hidden argument*/NULL);
	}

IL_006e:
	{
		return;
	}
}
// System.Void Main::.ctor()
extern Il2CppClass* WebClient_t1432723993_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern const uint32_t Main__ctor_m325986520_MetadataUsageId;
extern "C"  void Main__ctor_m325986520 (Main_t2809994845 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main__ctor_m325986520_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WebClient_t1432723993 * L_0 = (WebClient_t1432723993 *)il2cpp_codegen_object_new(WebClient_t1432723993_il2cpp_TypeInfo_var);
		WebClient__ctor_m660733025(L_0, /*hidden argument*/NULL);
		__this->set_web_3(L_0);
		__this->set_numPlays_17(1);
		__this->set_gameResults_18(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)0)));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Main Main::get_main()
extern Il2CppClass* Main_t2809994845_il2cpp_TypeInfo_var;
extern const uint32_t Main_get_main_m2962309062_MetadataUsageId;
extern "C"  Main_t2809994845 * Main_get_main_m2962309062 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_get_main_m2962309062_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2809994845_il2cpp_TypeInfo_var);
		Main_t2809994845 * L_0 = ((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->get_U3CmainU3Ek__BackingField_22();
		return L_0;
	}
}
// System.Void Main::set_main(Main)
extern Il2CppClass* Main_t2809994845_il2cpp_TypeInfo_var;
extern const uint32_t Main_set_main_m1382543681_MetadataUsageId;
extern "C"  void Main_set_main_m1382543681 (Il2CppObject * __this /* static, unused */, Main_t2809994845 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_set_main_m1382543681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Main_t2809994845 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2809994845_il2cpp_TypeInfo_var);
		((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->set_U3CmainU3Ek__BackingField_22(L_0);
		return;
	}
}
// System.Void Main::Awake()
extern Il2CppClass* Main_t2809994845_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisARController_t593870669_m2574969960_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisARTrackedObject_t1684152848_m1251800011_MethodInfo_var;
extern const uint32_t Main_Awake_m955704701_MetadataUsageId;
extern "C"  void Main_Awake_m955704701 (Main_t2809994845 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_Awake_m955704701_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_panelPostPlay_10();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2809994845_il2cpp_TypeInfo_var);
		Main_set_main_m1382543681(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		Main_t2809994845 * L_1 = Main_get_main_m2962309062(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		ARController_t593870669 * L_2 = Component_GetComponent_TisARController_t593870669_m2574969960(L_1, /*hidden argument*/Component_GetComponent_TisARController_t593870669_m2574969960_MethodInfo_var);
		__this->set_myARController_20(L_2);
		Main_t2809994845 * L_3 = Main_get_main_m2962309062(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		ARTrackedObject_t1684152848 * L_4 = Component_GetComponent_TisARTrackedObject_t1684152848_m1251800011(L_3, /*hidden argument*/Component_GetComponent_TisARTrackedObject_t1684152848_m1251800011_MethodInfo_var);
		__this->set_myARTtrackedObject_21(L_4);
		return;
	}
}
// System.Void Main::Start()
extern "C"  void Main_Start_m2091519880 (Main_t2809994845 * __this, const MethodInfo* method)
{
	{
		Main_goStart_m2958511914(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main::Update()
extern Il2CppClass* Main_t2809994845_il2cpp_TypeInfo_var;
extern const uint32_t Main_Update_m2467676297_MetadataUsageId;
extern "C"  void Main_Update_m2467676297 (Main_t2809994845 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_Update_m2467676297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_curState_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0051;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2809994845_il2cpp_TypeInfo_var);
		bool L_1 = ((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->get_bFound_19();
		if (L_1)
		{
			goto IL_0031;
		}
	}
	{
		ARController_t593870669 * L_2 = __this->get_myARController_20();
		NullCheck(L_2);
		bool L_3 = L_2->get_bFound_2();
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Main_onMarkerFound_m2678331393(__this, /*hidden argument*/NULL);
	}

IL_002c:
	{
		goto IL_0041;
	}

IL_0031:
	{
		ARController_t593870669 * L_4 = __this->get_myARController_20();
		NullCheck(L_4);
		bool L_5 = L_4->get_bFound_2();
		if (L_5)
		{
			goto IL_0041;
		}
	}

IL_0041:
	{
		ARController_t593870669 * L_6 = __this->get_myARController_20();
		NullCheck(L_6);
		bool L_7 = L_6->get_bFound_2();
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2809994845_il2cpp_TypeInfo_var);
		((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->set_bFound_19(L_7);
	}

IL_0051:
	{
		return;
	}
}
// System.Void Main::goStart()
extern Il2CppClass* Main_t2809994845_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Main_goStart_m2958511914_MetadataUsageId;
extern "C"  void Main_goStart_m2958511914 (Main_t2809994845 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_goStart_m2958511914_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_curState_2(0);
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2809994845_il2cpp_TypeInfo_var);
		((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->set_bWin_15((bool)0);
		Text_t356221433 * L_0 = __this->get_txtPremi_13();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		InputField_t1631627530 * L_2 = __this->get_codice_7();
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_2);
		InputField_set_text_m114077119(L_2, L_3, /*hidden argument*/NULL);
		InputField_t1631627530 * L_4 = __this->get_totale_8();
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_4);
		InputField_set_text_m114077119(L_4, L_5, /*hidden argument*/NULL);
		__this->set_curPlay_16(0);
		__this->set_numPlays_17(1);
		((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->set_bFound_19((bool)0);
		ARController_t593870669 * L_6 = __this->get_myARController_20();
		NullCheck(L_6);
		ARController_StopAR_m3264126367(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = __this->get_panel_9();
		NullCheck(L_7);
		GameObject_SetActive_m2887581199(L_7, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = __this->get_panelPostPlay_10();
		NullCheck(L_8);
		GameObject_SetActive_m2887581199(L_8, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_9 = __this->get_panelRecap_11();
		NullCheck(L_9);
		GameObject_SetActive_m2887581199(L_9, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main::goNextPlay()
extern Il2CppClass* Main_t2809994845_il2cpp_TypeInfo_var;
extern const uint32_t Main_goNextPlay_m2503892891_MetadataUsageId;
extern "C"  void Main_goNextPlay_m2503892891 (Main_t2809994845 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_goNextPlay_m2503892891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_curPlay_16();
		int32_t L_1 = __this->get_numPlays_17();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0067;
		}
	}
	{
		__this->set_curState_2(1);
		int32_t L_2 = __this->get_curPlay_16();
		__this->set_curPlay_16(((int32_t)((int32_t)L_2+(int32_t)1)));
		Main_checkWin_m3851975444(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2809994845_il2cpp_TypeInfo_var);
		((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->set_bFound_19((bool)0);
		GameObject_t1756533147 * L_3 = __this->get_panel_9();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_panelRecap_11();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = __this->get_panelPostPlay_10();
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)0, /*hidden argument*/NULL);
		ARController_t593870669 * L_6 = __this->get_myARController_20();
		NullCheck(L_6);
		ARController_StartAR_m3868647255(L_6, /*hidden argument*/NULL);
		goto IL_006d;
	}

IL_0067:
	{
		Main_goRecap_m414710553(__this, /*hidden argument*/NULL);
	}

IL_006d:
	{
		return;
	}
}
// System.Void Main::goRecap()
extern "C"  void Main_goRecap_m414710553 (Main_t2809994845 * __this, const MethodInfo* method)
{
	{
		__this->set_curState_2(2);
		Main_makeRecapText_m687811546(__this, /*hidden argument*/NULL);
		ARController_t593870669 * L_0 = __this->get_myARController_20();
		NullCheck(L_0);
		ARController_StopAR_m3264126367(L_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_panel_9();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_panelPostPlay_10();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_panelRecap_11();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main::setNumPlays(System.Int32)
extern "C"  void Main_setNumPlays_m3669947674 (Main_t2809994845 * __this, int32_t ____n0, const MethodInfo* method)
{
	{
		__this->set_curPlay_16(0);
		int32_t L_0 = ____n0;
		__this->set_numPlays_17(L_0);
		return;
	}
}
// System.Void Main::onClickAvanti()
extern "C"  void Main_onClickAvanti_m543164064 (Main_t2809994845 * __this, const MethodInfo* method)
{
	{
		ARController_t593870669 * L_0 = __this->get_myARController_20();
		NullCheck(L_0);
		ARController_StopAR_m3264126367(L_0, /*hidden argument*/NULL);
		Main_goNextPlay_m2503892891(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main::onMarkerFound()
extern "C"  void Main_onMarkerFound_m2678331393 (Main_t2809994845 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_panelPostPlay_10();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		StringU5BU5D_t1642385972* L_1 = __this->get_gameResults_18();
		NullCheck(L_1);
		int32_t L_2 = __this->get_curPlay_16();
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) < ((int32_t)((int32_t)((int32_t)L_2-(int32_t)1)))))
		{
			goto IL_003b;
		}
	}
	{
		Text_t356221433 * L_3 = __this->get_txtResult_12();
		StringU5BU5D_t1642385972* L_4 = __this->get_gameResults_18();
		int32_t L_5 = __this->get_curPlay_16();
		NullCheck(L_4);
		int32_t L_6 = ((int32_t)((int32_t)L_5-(int32_t)1));
		String_t* L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_7);
	}

IL_003b:
	{
		return;
	}
}
// System.Void Main::GetCode()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Main_t2809994845_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3490323421;
extern Il2CppCodeGenString* _stringLiteral372029315;
extern const uint32_t Main_GetCode_m2363977499_MetadataUsageId;
extern "C"  void Main_GetCode_m2363977499 (Main_t2809994845 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_GetCode_m2363977499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		InputField_t1631627530 * L_0 = __this->get_codice_7();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m409351770(L_0, /*hidden argument*/NULL);
		InputField_t1631627530 * L_2 = __this->get_totale_8();
		NullCheck(L_2);
		String_t* L_3 = InputField_get_text_m409351770(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral3490323421, L_1, _stringLiteral372029315, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		WebClient_t1432723993 * L_5 = __this->get_web_3();
		String_t* L_6 = V_0;
		NullCheck(L_5);
		String_t* L_7 = WebClient_DownloadString_m1757636243(L_5, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2809994845_il2cpp_TypeInfo_var);
		((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->set_reply_6(L_7);
		String_t* L_8 = ((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->get_reply_6();
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		String_t* L_9 = ((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->get_reply_6();
		StringU5BU5D_t1642385972* L_10 = Main_parseUserWebReply_m4126468891(__this, L_9, /*hidden argument*/NULL);
		__this->set_gameResults_18(L_10);
		StringU5BU5D_t1642385972* L_11 = __this->get_gameResults_18();
		NullCheck(L_11);
		Main_setNumPlays_m3669947674(__this, (((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))), /*hidden argument*/NULL);
		StringU5BU5D_t1642385972* L_12 = __this->get_gameResults_18();
		NullCheck(L_12);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0079;
		}
	}
	{
		Main_goNextPlay_m2503892891(__this, /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_0079:
	{
		Main_goRecap_m414710553(__this, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.String[] Main::parseUserWebReply(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1853652170;
extern Il2CppCodeGenString* _stringLiteral1139199429;
extern Il2CppCodeGenString* _stringLiteral1362767617;
extern const uint32_t Main_parseUserWebReply_m4126468891_MetadataUsageId;
extern "C"  StringU5BU5D_t1642385972* Main_parseUserWebReply_m4126468891 (Main_t2809994845 * __this, String_t* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_parseUserWebReply_m4126468891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	CharU5BU5D_t1328083999* V_1 = NULL;
	StringU5BU5D_t1642385972* V_2 = NULL;
	int32_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		String_t* L_1 = ___data0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)2)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_3 = ___data0;
		String_t* L_4 = ___data0;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1606060069(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_6 = String_Substring_m12482732(L_3, 1, ((int32_t)((int32_t)L_5-(int32_t)2)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1853652170, /*hidden argument*/NULL);
	}

IL_0031:
	{
		CharU5BU5D_t1328083999* L_7 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		V_1 = L_7;
		String_t* L_8 = V_0;
		CharU5BU5D_t1328083999* L_9 = V_1;
		NullCheck(L_8);
		StringU5BU5D_t1642385972* L_10 = String_Split_m3326265864(L_8, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		V_3 = 0;
		goto IL_008b;
	}

IL_004c:
	{
		StringU5BU5D_t1642385972* L_11 = V_2;
		int32_t L_12 = V_3;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		String_t* L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m1606060069(L_14, /*hidden argument*/NULL);
		if ((((int32_t)L_15) <= ((int32_t)4)))
		{
			goto IL_0075;
		}
	}
	{
		StringU5BU5D_t1642385972* L_16 = V_2;
		int32_t L_17 = V_3;
		StringU5BU5D_t1642385972* L_18 = V_2;
		int32_t L_19 = V_3;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		String_t* L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		StringU5BU5D_t1642385972* L_22 = V_2;
		int32_t L_23 = V_3;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		String_t* L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		NullCheck(L_25);
		int32_t L_26 = String_get_Length_m1606060069(L_25, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_27 = String_Substring_m12482732(L_21, 2, ((int32_t)((int32_t)L_26-(int32_t)4)), /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_27);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (String_t*)L_27);
		goto IL_0087;
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1139199429, /*hidden argument*/NULL);
		StringU5BU5D_t1642385972* L_28 = V_2;
		int32_t L_29 = V_3;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteral1362767617);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(L_29), (String_t*)_stringLiteral1362767617);
	}

IL_0087:
	{
		int32_t L_30 = V_3;
		V_3 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_008b:
	{
		int32_t L_31 = V_3;
		StringU5BU5D_t1642385972* L_32 = V_2;
		NullCheck(L_32);
		if ((((int32_t)L_31) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		StringU5BU5D_t1642385972* L_33 = V_2;
		return L_33;
	}
}
// System.Void Main::checkWin()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Main_t2809994845_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1362767617;
extern const uint32_t Main_checkWin_m3851975444_MetadataUsageId;
extern "C"  void Main_checkWin_m3851975444 (Main_t2809994845 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_checkWin_m3851975444_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_gameResults_18();
		NullCheck(L_0);
		int32_t L_1 = __this->get_curPlay_16();
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) < ((int32_t)((int32_t)((int32_t)L_1-(int32_t)1)))))
		{
			goto IL_0038;
		}
	}
	{
		StringU5BU5D_t1642385972* L_2 = __this->get_gameResults_18();
		int32_t L_3 = __this->get_curPlay_16();
		NullCheck(L_2);
		int32_t L_4 = ((int32_t)((int32_t)L_3-(int32_t)1));
		String_t* L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_5, _stringLiteral1362767617, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2809994845_il2cpp_TypeInfo_var);
		((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->set_bWin_15(L_6);
		goto IL_003e;
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2809994845_il2cpp_TypeInfo_var);
		((Main_t2809994845_StaticFields*)Main_t2809994845_il2cpp_TypeInfo_var->static_fields)->set_bWin_15((bool)0);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Main::makeRecapText()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t Main_makeRecapText_m687811546_MetadataUsageId;
extern "C"  void Main_makeRecapText_m687811546 (Main_t2809994845 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_makeRecapText_m687811546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		Text_t356221433 * L_0 = __this->get_txtPremi_13();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_2;
		V_1 = 0;
		goto IL_0035;
	}

IL_001d:
	{
		String_t* L_3 = V_0;
		StringU5BU5D_t1642385972* L_4 = __this->get_gameResults_18();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		String_t* L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m612901809(NULL /*static, unused*/, L_3, L_7, _stringLiteral372029352, /*hidden argument*/NULL);
		V_0 = L_8;
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0035:
	{
		int32_t L_10 = V_1;
		StringU5BU5D_t1642385972* L_11 = __this->get_gameResults_18();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_001d;
		}
	}
	{
		Text_t356221433 * L_12 = __this->get_txtPremi_13();
		String_t* L_13 = V_0;
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_13);
		return;
	}
}
// System.Void Main::hideModels()
extern "C"  void Main_hideModels_m1537942704 (Main_t2809994845 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Main::.cctor()
extern "C"  void Main__cctor_m2288302007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PluginFunctions::arwRegisterLogCallback(PluginFunctions/LogCallback)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t PluginFunctions_arwRegisterLogCallback_m1389878145_MetadataUsageId;
extern "C"  void PluginFunctions_arwRegisterLogCallback_m1389878145 (Il2CppObject * __this /* static, unused */, LogCallback_t2143553514 * ___lcb0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PluginFunctions_arwRegisterLogCallback_m1389878145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LogCallback_t2143553514 * L_0 = ___lcb0;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		LogCallback_t2143553514 * L_1 = ___lcb0;
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		((PluginFunctions_t2887339240_StaticFields*)PluginFunctions_t2887339240_il2cpp_TypeInfo_var->static_fields)->set_logCallback_1(L_1);
		LogCallback_t2143553514 * L_2 = ((PluginFunctions_t2887339240_StaticFields*)PluginFunctions_t2887339240_il2cpp_TypeInfo_var->static_fields)->get_logCallback_1();
		GCHandle_t3409268066  L_3 = GCHandle_Alloc_m3171748614(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		((PluginFunctions_t2887339240_StaticFields*)PluginFunctions_t2887339240_il2cpp_TypeInfo_var->static_fields)->set_logCallbackGCH_2(L_3);
	}

IL_001b:
	{
		int32_t L_4 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)8))))
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		LogCallback_t2143553514 * L_5 = ((PluginFunctions_t2887339240_StaticFields*)PluginFunctions_t2887339240_il2cpp_TypeInfo_var->static_fields)->get_logCallback_1();
		ARNativePluginStatic_arwRegisterLogCallback_m2696023928(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		goto IL_003f;
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		LogCallback_t2143553514 * L_6 = ((PluginFunctions_t2887339240_StaticFields*)PluginFunctions_t2887339240_il2cpp_TypeInfo_var->static_fields)->get_logCallback_1();
		ARNativePlugin_arwRegisterLogCallback_m2470376146(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_003f:
	{
		LogCallback_t2143553514 * L_7 = ___lcb0;
		if (L_7)
		{
			goto IL_0055;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		((PluginFunctions_t2887339240_StaticFields*)PluginFunctions_t2887339240_il2cpp_TypeInfo_var->static_fields)->set_logCallback_1((LogCallback_t2143553514 *)NULL);
		GCHandle_Free_m1639542352((((PluginFunctions_t2887339240_StaticFields*)PluginFunctions_t2887339240_il2cpp_TypeInfo_var->static_fields)->get_address_of_logCallbackGCH_2()), /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void PluginFunctions::arwSetLogLevel(System.Int32)
extern "C"  void PluginFunctions_arwSetLogLevel_m3400122846 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = ___logLevel0;
		ARNativePluginStatic_arwSetLogLevel_m3738585053(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		int32_t L_2 = ___logLevel0;
		ARNativePlugin_arwSetLogLevel_m4002955093(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Boolean PluginFunctions::arwInitialiseAR(System.Int32,System.Int32)
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t PluginFunctions_arwInitialiseAR_m1868305941_MetadataUsageId;
extern "C"  bool PluginFunctions_arwInitialiseAR_m1868305941 (Il2CppObject * __this /* static, unused */, int32_t ___pattSize0, int32_t ___pattCountMax1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PluginFunctions_arwInitialiseAR_m1868305941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___pattSize0;
		int32_t L_2 = ___pattCountMax1;
		bool L_3 = ARNativePluginStatic_arwInitialiseARWithOptions_m4273179010(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0020;
	}

IL_0018:
	{
		int32_t L_4 = ___pattSize0;
		int32_t L_5 = ___pattCountMax1;
		bool L_6 = ARNativePlugin_arwInitialiseARWithOptions_m2231924264(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0020:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		((PluginFunctions_t2887339240_StaticFields*)PluginFunctions_t2887339240_il2cpp_TypeInfo_var->static_fields)->set_inited_0((bool)1);
	}

IL_002c:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
// System.String PluginFunctions::arwGetARToolKitVersion()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2845190196;
extern const uint32_t PluginFunctions_arwGetARToolKitVersion_m1242319639_MetadataUsageId;
extern "C"  String_t* PluginFunctions_arwGetARToolKitVersion_m1242319639 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PluginFunctions_arwGetARToolKitVersion_m1242319639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	bool V_1 = false;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_0, ((int32_t)128), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)8))))
		{
			goto IL_0028;
		}
	}
	{
		StringBuilder_t1221177846 * L_2 = V_0;
		StringBuilder_t1221177846 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = StringBuilder_get_Capacity_m1253303803(L_3, /*hidden argument*/NULL);
		bool L_5 = ARNativePluginStatic_arwGetARToolKitVersion_m3283193640(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0035;
	}

IL_0028:
	{
		StringBuilder_t1221177846 * L_6 = V_0;
		StringBuilder_t1221177846 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = StringBuilder_get_Capacity_m1253303803(L_7, /*hidden argument*/NULL);
		bool L_9 = ARNativePlugin_arwGetARToolKitVersion_m465574986(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0035:
	{
		bool L_10 = V_1;
		if (!L_10)
		{
			goto IL_0042;
		}
	}
	{
		StringBuilder_t1221177846 * L_11 = V_0;
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_11);
		return L_12;
	}

IL_0042:
	{
		return _stringLiteral2845190196;
	}
}
// System.Int32 PluginFunctions::arwGetError()
extern "C"  int32_t PluginFunctions_arwGetError_m1333834619 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = ARNativePluginStatic_arwGetError_m1131212540(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		int32_t L_2 = ARNativePlugin_arwGetError_m4256659126(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean PluginFunctions::arwShutdownAR()
extern Il2CppClass* PluginFunctions_t2887339240_il2cpp_TypeInfo_var;
extern const uint32_t PluginFunctions_arwShutdownAR_m3422793632_MetadataUsageId;
extern "C"  bool PluginFunctions_arwShutdownAR_m3422793632 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PluginFunctions_arwShutdownAR_m3422793632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = ARNativePluginStatic_arwShutdownAR_m3238479991(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		bool L_2 = ARNativePlugin_arwShutdownAR_m1555258779(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_001c:
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PluginFunctions_t2887339240_il2cpp_TypeInfo_var);
		((PluginFunctions_t2887339240_StaticFields*)PluginFunctions_t2887339240_il2cpp_TypeInfo_var->static_fields)->set_inited_0((bool)0);
	}

IL_0028:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean PluginFunctions::arwStartRunningB(System.String,System.Byte[],System.Int32,System.Single,System.Single)
extern "C"  bool PluginFunctions_arwStartRunningB_m1651599480 (Il2CppObject * __this /* static, unused */, String_t* ___vconf0, ByteU5BU5D_t3397334013* ___cparaBuff1, int32_t ___cparaBuffLen2, float ___nearPlane3, float ___farPlane4, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0017;
		}
	}
	{
		String_t* L_1 = ___vconf0;
		ByteU5BU5D_t3397334013* L_2 = ___cparaBuff1;
		int32_t L_3 = ___cparaBuffLen2;
		float L_4 = ___nearPlane3;
		float L_5 = ___farPlane4;
		bool L_6 = ARNativePluginStatic_arwStartRunningB_m1273829661(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0017:
	{
		String_t* L_7 = ___vconf0;
		ByteU5BU5D_t3397334013* L_8 = ___cparaBuff1;
		int32_t L_9 = ___cparaBuffLen2;
		float L_10 = ___nearPlane3;
		float L_11 = ___farPlane4;
		bool L_12 = ARNativePlugin_arwStartRunningB_m307445325(NULL /*static, unused*/, L_7, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Boolean PluginFunctions::arwStartRunningStereoB(System.String,System.Byte[],System.Int32,System.String,System.Byte[],System.Int32,System.Byte[],System.Int32,System.Single,System.Single)
extern "C"  bool PluginFunctions_arwStartRunningStereoB_m1451019968 (Il2CppObject * __this /* static, unused */, String_t* ___vconfL0, ByteU5BU5D_t3397334013* ___cparaBuffL1, int32_t ___cparaBuffLenL2, String_t* ___vconfR3, ByteU5BU5D_t3397334013* ___cparaBuffR4, int32_t ___cparaBuffLenR5, ByteU5BU5D_t3397334013* ___transL2RBuff6, int32_t ___transL2RBuffLen7, float ___nearPlane8, float ___farPlane9, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_1 = ___vconfL0;
		ByteU5BU5D_t3397334013* L_2 = ___cparaBuffL1;
		int32_t L_3 = ___cparaBuffLenL2;
		String_t* L_4 = ___vconfR3;
		ByteU5BU5D_t3397334013* L_5 = ___cparaBuffR4;
		int32_t L_6 = ___cparaBuffLenR5;
		ByteU5BU5D_t3397334013* L_7 = ___transL2RBuff6;
		int32_t L_8 = ___transL2RBuffLen7;
		float L_9 = ___nearPlane8;
		float L_10 = ___farPlane9;
		bool L_11 = ARNativePluginStatic_arwStartRunningStereoB_m4213855497(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0021:
	{
		String_t* L_12 = ___vconfL0;
		ByteU5BU5D_t3397334013* L_13 = ___cparaBuffL1;
		int32_t L_14 = ___cparaBuffLenL2;
		String_t* L_15 = ___vconfR3;
		ByteU5BU5D_t3397334013* L_16 = ___cparaBuffR4;
		int32_t L_17 = ___cparaBuffLenR5;
		ByteU5BU5D_t3397334013* L_18 = ___transL2RBuff6;
		int32_t L_19 = ___transL2RBuffLen7;
		float L_20 = ___nearPlane8;
		float L_21 = ___farPlane9;
		bool L_22 = ARNativePlugin_arwStartRunningStereoB_m1235119585(NULL /*static, unused*/, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Boolean PluginFunctions::arwIsRunning()
extern "C"  bool PluginFunctions_arwIsRunning_m3605556890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0011;
		}
	}
	{
		bool L_1 = ARNativePluginStatic_arwIsRunning_m3525939861(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		bool L_2 = ARNativePlugin_arwIsRunning_m3033062181(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean PluginFunctions::arwStopRunning()
extern "C"  bool PluginFunctions_arwStopRunning_m1121675878 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0011;
		}
	}
	{
		bool L_1 = ARNativePluginStatic_arwStopRunning_m3225889991(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		bool L_2 = ARNativePlugin_arwStopRunning_m622006595(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean PluginFunctions::arwGetProjectionMatrix(System.Single[])
extern "C"  bool PluginFunctions_arwGetProjectionMatrix_m2140236978 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___matrix0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0012;
		}
	}
	{
		SingleU5BU5D_t577127397* L_1 = ___matrix0;
		bool L_2 = ARNativePluginStatic_arwGetProjectionMatrix_m1707584415(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0012:
	{
		SingleU5BU5D_t577127397* L_3 = ___matrix0;
		bool L_4 = ARNativePlugin_arwGetProjectionMatrix_m3243091243(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean PluginFunctions::arwGetProjectionMatrixStereo(System.Single[],System.Single[])
extern "C"  bool PluginFunctions_arwGetProjectionMatrixStereo_m3127493433 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___matrixL0, SingleU5BU5D_t577127397* ___matrixR1, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0013;
		}
	}
	{
		SingleU5BU5D_t577127397* L_1 = ___matrixL0;
		SingleU5BU5D_t577127397* L_2 = ___matrixR1;
		bool L_3 = ARNativePluginStatic_arwGetProjectionMatrixStereo_m1572723186(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0013:
	{
		SingleU5BU5D_t577127397* L_4 = ___matrixL0;
		SingleU5BU5D_t577127397* L_5 = ___matrixR1;
		bool L_6 = ARNativePlugin_arwGetProjectionMatrixStereo_m427191080(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean PluginFunctions::arwGetVideoParams(System.Int32&,System.Int32&,System.Int32&,System.String&)
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PluginFunctions_arwGetVideoParams_m4086655695_MetadataUsageId;
extern "C"  bool PluginFunctions_arwGetVideoParams_m4086655695 (Il2CppObject * __this /* static, unused */, int32_t* ___width0, int32_t* ___height1, int32_t* ___pixelSize2, String_t** ___pixelFormatString3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PluginFunctions_arwGetVideoParams_m4086655695_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	bool V_1 = false;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_0, ((int32_t)128), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)8))))
		{
			goto IL_002b;
		}
	}
	{
		int32_t* L_2 = ___width0;
		int32_t* L_3 = ___height1;
		int32_t* L_4 = ___pixelSize2;
		StringBuilder_t1221177846 * L_5 = V_0;
		StringBuilder_t1221177846 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = StringBuilder_get_Capacity_m1253303803(L_6, /*hidden argument*/NULL);
		bool L_8 = ARNativePluginStatic_arwGetVideoParams_m804941317(NULL /*static, unused*/, L_2, L_3, L_4, L_5, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		goto IL_003b;
	}

IL_002b:
	{
		int32_t* L_9 = ___width0;
		int32_t* L_10 = ___height1;
		int32_t* L_11 = ___pixelSize2;
		StringBuilder_t1221177846 * L_12 = V_0;
		StringBuilder_t1221177846 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = StringBuilder_get_Capacity_m1253303803(L_13, /*hidden argument*/NULL);
		bool L_15 = ARNativePlugin_arwGetVideoParams_m982330381(NULL /*static, unused*/, L_9, L_10, L_11, L_12, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
	}

IL_003b:
	{
		bool L_16 = V_1;
		if (L_16)
		{
			goto IL_004d;
		}
	}
	{
		String_t** L_17 = ___pixelFormatString3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		*((Il2CppObject **)(L_17)) = (Il2CppObject *)L_18;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_17), (Il2CppObject *)L_18);
		goto IL_0055;
	}

IL_004d:
	{
		String_t** L_19 = ___pixelFormatString3;
		StringBuilder_t1221177846 * L_20 = V_0;
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		*((Il2CppObject **)(L_19)) = (Il2CppObject *)L_21;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_19), (Il2CppObject *)L_21);
	}

IL_0055:
	{
		bool L_22 = V_1;
		return L_22;
	}
}
// System.Boolean PluginFunctions::arwGetVideoParamsStereo(System.Int32&,System.Int32&,System.Int32&,System.String&,System.Int32&,System.Int32&,System.Int32&,System.String&)
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PluginFunctions_arwGetVideoParamsStereo_m3050262472_MetadataUsageId;
extern "C"  bool PluginFunctions_arwGetVideoParamsStereo_m3050262472 (Il2CppObject * __this /* static, unused */, int32_t* ___widthL0, int32_t* ___heightL1, int32_t* ___pixelSizeL2, String_t** ___pixelFormatL3, int32_t* ___widthR4, int32_t* ___heightR5, int32_t* ___pixelSizeR6, String_t** ___pixelFormatR7, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PluginFunctions_arwGetVideoParamsStereo_m3050262472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	StringBuilder_t1221177846 * V_1 = NULL;
	bool V_2 = false;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_0, ((int32_t)128), /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1221177846 * L_1 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_1, ((int32_t)128), /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)8))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t* L_3 = ___widthL0;
		int32_t* L_4 = ___heightL1;
		int32_t* L_5 = ___pixelSizeL2;
		StringBuilder_t1221177846 * L_6 = V_0;
		StringBuilder_t1221177846 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = StringBuilder_get_Capacity_m1253303803(L_7, /*hidden argument*/NULL);
		int32_t* L_9 = ___widthR4;
		int32_t* L_10 = ___heightR5;
		int32_t* L_11 = ___pixelSizeR6;
		StringBuilder_t1221177846 * L_12 = V_1;
		StringBuilder_t1221177846 * L_13 = V_1;
		NullCheck(L_13);
		int32_t L_14 = StringBuilder_get_Capacity_m1253303803(L_13, /*hidden argument*/NULL);
		bool L_15 = ARNativePluginStatic_arwGetVideoParamsStereo_m3904472601(NULL /*static, unused*/, L_3, L_4, L_5, L_6, L_8, L_9, L_10, L_11, L_12, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		goto IL_0060;
	}

IL_0043:
	{
		int32_t* L_16 = ___widthL0;
		int32_t* L_17 = ___heightL1;
		int32_t* L_18 = ___pixelSizeL2;
		StringBuilder_t1221177846 * L_19 = V_0;
		StringBuilder_t1221177846 * L_20 = V_0;
		NullCheck(L_20);
		int32_t L_21 = StringBuilder_get_Capacity_m1253303803(L_20, /*hidden argument*/NULL);
		int32_t* L_22 = ___widthR4;
		int32_t* L_23 = ___heightR5;
		int32_t* L_24 = ___pixelSizeR6;
		StringBuilder_t1221177846 * L_25 = V_1;
		StringBuilder_t1221177846 * L_26 = V_1;
		NullCheck(L_26);
		int32_t L_27 = StringBuilder_get_Capacity_m1253303803(L_26, /*hidden argument*/NULL);
		bool L_28 = ARNativePlugin_arwGetVideoParamsStereo_m4115500785(NULL /*static, unused*/, L_16, L_17, L_18, L_19, L_21, L_22, L_23, L_24, L_25, L_27, /*hidden argument*/NULL);
		V_2 = L_28;
	}

IL_0060:
	{
		bool L_29 = V_2;
		if (L_29)
		{
			goto IL_007a;
		}
	}
	{
		String_t** L_30 = ___pixelFormatL3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		*((Il2CppObject **)(L_30)) = (Il2CppObject *)L_31;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_30), (Il2CppObject *)L_31);
		String_t** L_32 = ___pixelFormatR7;
		String_t* L_33 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		*((Il2CppObject **)(L_32)) = (Il2CppObject *)L_33;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_32), (Il2CppObject *)L_33);
		goto IL_008b;
	}

IL_007a:
	{
		String_t** L_34 = ___pixelFormatL3;
		StringBuilder_t1221177846 * L_35 = V_0;
		NullCheck(L_35);
		String_t* L_36 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		*((Il2CppObject **)(L_34)) = (Il2CppObject *)L_36;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_34), (Il2CppObject *)L_36);
		String_t** L_37 = ___pixelFormatR7;
		StringBuilder_t1221177846 * L_38 = V_1;
		NullCheck(L_38);
		String_t* L_39 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_38);
		*((Il2CppObject **)(L_37)) = (Il2CppObject *)L_39;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_37), (Il2CppObject *)L_39);
	}

IL_008b:
	{
		bool L_40 = V_2;
		return L_40;
	}
}
// System.Boolean PluginFunctions::arwCapture()
extern "C"  bool PluginFunctions_arwCapture_m650910371 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0011;
		}
	}
	{
		bool L_1 = ARNativePluginStatic_arwCapture_m3910463490(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		bool L_2 = ARNativePlugin_arwCapture_m808157064(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean PluginFunctions::arwUpdateAR()
extern "C"  bool PluginFunctions_arwUpdateAR_m598243641 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0011;
		}
	}
	{
		bool L_1 = ARNativePluginStatic_arwUpdateAR_m1910658788(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		bool L_2 = ARNativePlugin_arwUpdateAR_m3219639630(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean PluginFunctions::arwUpdateTexture(UnityEngine.Color[])
extern "C"  bool PluginFunctions_arwUpdateTexture_m210610373 (Il2CppObject * __this /* static, unused */, ColorU5BU5D_t672350442* ___colors0, const MethodInfo* method)
{
	bool V_0 = false;
	GCHandle_t3409268066  V_1;
	memset(&V_1, 0, sizeof(V_1));
	IntPtr_t V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		ColorU5BU5D_t672350442* L_0 = ___colors0;
		GCHandle_t3409268066  L_1 = GCHandle_Alloc_m1063472408(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_0, 3, /*hidden argument*/NULL);
		V_1 = L_1;
		IntPtr_t L_2 = GCHandle_AddrOfPinnedObject_m3034420542((&V_1), /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0027;
		}
	}
	{
		IntPtr_t L_4 = V_2;
		bool L_5 = ARNativePluginStatic_arwUpdateTexture_m1158379942(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_002e;
	}

IL_0027:
	{
		IntPtr_t L_6 = V_2;
		bool L_7 = ARNativePlugin_arwUpdateTexture_m938831980(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_002e:
	{
		GCHandle_Free_m1639542352((&V_1), /*hidden argument*/NULL);
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Boolean PluginFunctions::arwUpdateTextureStereo(UnityEngine.Color[],UnityEngine.Color[])
extern "C"  bool PluginFunctions_arwUpdateTextureStereo_m3642194655 (Il2CppObject * __this /* static, unused */, ColorU5BU5D_t672350442* ___colorsL0, ColorU5BU5D_t672350442* ___colorsR1, const MethodInfo* method)
{
	bool V_0 = false;
	GCHandle_t3409268066  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GCHandle_t3409268066  V_2;
	memset(&V_2, 0, sizeof(V_2));
	IntPtr_t V_3;
	memset(&V_3, 0, sizeof(V_3));
	IntPtr_t V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		ColorU5BU5D_t672350442* L_0 = ___colorsL0;
		GCHandle_t3409268066  L_1 = GCHandle_Alloc_m1063472408(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_0, 3, /*hidden argument*/NULL);
		V_1 = L_1;
		ColorU5BU5D_t672350442* L_2 = ___colorsR1;
		GCHandle_t3409268066  L_3 = GCHandle_Alloc_m1063472408(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_2, 3, /*hidden argument*/NULL);
		V_2 = L_3;
		IntPtr_t L_4 = GCHandle_AddrOfPinnedObject_m3034420542((&V_1), /*hidden argument*/NULL);
		V_3 = L_4;
		IntPtr_t L_5 = GCHandle_AddrOfPinnedObject_m3034420542((&V_2), /*hidden argument*/NULL);
		V_4 = L_5;
		int32_t L_6 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)8))))
		{
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_7 = V_3;
		IntPtr_t L_8 = V_4;
		bool L_9 = ARNativePluginStatic_arwUpdateTextureStereo_m693222146(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0043;
	}

IL_003a:
	{
		IntPtr_t L_10 = V_3;
		IntPtr_t L_11 = V_4;
		bool L_12 = ARNativePlugin_arwUpdateTextureStereo_m4292622656(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
	}

IL_0043:
	{
		GCHandle_Free_m1639542352((&V_1), /*hidden argument*/NULL);
		GCHandle_Free_m1639542352((&V_2), /*hidden argument*/NULL);
		bool L_13 = V_0;
		return L_13;
	}
}
// System.Boolean PluginFunctions::arwUpdateTexture32(UnityEngine.Color32[])
extern "C"  bool PluginFunctions_arwUpdateTexture32_m3012693829 (Il2CppObject * __this /* static, unused */, Color32U5BU5D_t30278651* ___colors320, const MethodInfo* method)
{
	bool V_0 = false;
	GCHandle_t3409268066  V_1;
	memset(&V_1, 0, sizeof(V_1));
	IntPtr_t V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Color32U5BU5D_t30278651* L_0 = ___colors320;
		GCHandle_t3409268066  L_1 = GCHandle_Alloc_m1063472408(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_0, 3, /*hidden argument*/NULL);
		V_1 = L_1;
		IntPtr_t L_2 = GCHandle_AddrOfPinnedObject_m3034420542((&V_1), /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0027;
		}
	}
	{
		IntPtr_t L_4 = V_2;
		bool L_5 = ARNativePluginStatic_arwUpdateTexture32_m185359189(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_002e;
	}

IL_0027:
	{
		IntPtr_t L_6 = V_2;
		bool L_7 = ARNativePlugin_arwUpdateTexture32_m443923741(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_002e:
	{
		GCHandle_Free_m1639542352((&V_1), /*hidden argument*/NULL);
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Boolean PluginFunctions::arwUpdateTexture32Stereo(UnityEngine.Color32[],UnityEngine.Color32[])
extern "C"  bool PluginFunctions_arwUpdateTexture32Stereo_m3306506678 (Il2CppObject * __this /* static, unused */, Color32U5BU5D_t30278651* ___colors32L0, Color32U5BU5D_t30278651* ___colors32R1, const MethodInfo* method)
{
	bool V_0 = false;
	GCHandle_t3409268066  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GCHandle_t3409268066  V_2;
	memset(&V_2, 0, sizeof(V_2));
	IntPtr_t V_3;
	memset(&V_3, 0, sizeof(V_3));
	IntPtr_t V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Color32U5BU5D_t30278651* L_0 = ___colors32L0;
		GCHandle_t3409268066  L_1 = GCHandle_Alloc_m1063472408(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_0, 3, /*hidden argument*/NULL);
		V_1 = L_1;
		Color32U5BU5D_t30278651* L_2 = ___colors32R1;
		GCHandle_t3409268066  L_3 = GCHandle_Alloc_m1063472408(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_2, 3, /*hidden argument*/NULL);
		V_2 = L_3;
		IntPtr_t L_4 = GCHandle_AddrOfPinnedObject_m3034420542((&V_1), /*hidden argument*/NULL);
		V_3 = L_4;
		IntPtr_t L_5 = GCHandle_AddrOfPinnedObject_m3034420542((&V_2), /*hidden argument*/NULL);
		V_4 = L_5;
		int32_t L_6 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)8))))
		{
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_7 = V_3;
		IntPtr_t L_8 = V_4;
		bool L_9 = ARNativePluginStatic_arwUpdateTexture32Stereo_m813074921(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0043;
	}

IL_003a:
	{
		IntPtr_t L_10 = V_3;
		IntPtr_t L_11 = V_4;
		bool L_12 = ARNativePlugin_arwUpdateTexture32Stereo_m993054649(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
	}

IL_0043:
	{
		GCHandle_Free_m1639542352((&V_1), /*hidden argument*/NULL);
		GCHandle_Free_m1639542352((&V_2), /*hidden argument*/NULL);
		bool L_13 = V_0;
		return L_13;
	}
}
// System.Boolean PluginFunctions::arwUpdateTextureGL(System.Int32)
extern "C"  bool PluginFunctions_arwUpdateTextureGL_m358316523 (Il2CppObject * __this /* static, unused */, int32_t ___textureID0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___textureID0;
		bool L_2 = ARNativePluginStatic_arwUpdateTextureGL_m562093018(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0012:
	{
		int32_t L_3 = ___textureID0;
		bool L_4 = ARNativePlugin_arwUpdateTextureGL_m2994090928(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean PluginFunctions::arwUpdateTextureGLStereo(System.Int32,System.Int32)
extern "C"  bool PluginFunctions_arwUpdateTextureGLStereo_m2096161246 (Il2CppObject * __this /* static, unused */, int32_t ___textureID_L0, int32_t ___textureID_R1, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___textureID_L0;
		int32_t L_2 = ___textureID_R1;
		bool L_3 = ARNativePluginStatic_arwUpdateTextureGLStereo_m3176194339(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0013:
	{
		int32_t L_4 = ___textureID_L0;
		int32_t L_5 = ___textureID_R1;
		bool L_6 = ARNativePlugin_arwUpdateTextureGLStereo_m2235959663(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void PluginFunctions::arwSetUnityRenderEventUpdateTextureGLTextureID(System.Int32)
extern "C"  void PluginFunctions_arwSetUnityRenderEventUpdateTextureGLTextureID_m3634330186 (Il2CppObject * __this /* static, unused */, int32_t ___textureID0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = ___textureID0;
		ARNativePluginStatic_arwSetUnityRenderEventUpdateTextureGLTextureID_m3208451437(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		int32_t L_2 = ___textureID0;
		ARNativePlugin_arwSetUnityRenderEventUpdateTextureGLTextureID_m1975041325(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void PluginFunctions::arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs(System.Int32,System.Int32)
extern "C"  void PluginFunctions_arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs_m10115510 (Il2CppObject * __this /* static, unused */, int32_t ___textureID_L0, int32_t ___textureID_R1, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = ___textureID_L0;
		int32_t L_2 = ___textureID_R1;
		ARNativePluginStatic_arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs_m2332439057(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_001e;
	}

IL_0017:
	{
		int32_t L_3 = ___textureID_L0;
		int32_t L_4 = ___textureID_R1;
		ARNativePlugin_arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs_m1459928577(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Int32 PluginFunctions::arwGetMarkerPatternCount(System.Int32)
extern "C"  int32_t PluginFunctions_arwGetMarkerPatternCount_m3403487639 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___markerID0;
		int32_t L_2 = ARNativePluginStatic_arwGetMarkerPatternCount_m2534201878(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0012:
	{
		int32_t L_3 = ___markerID0;
		int32_t L_4 = ARNativePlugin_arwGetMarkerPatternCount_m1314450108(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean PluginFunctions::arwGetMarkerPatternConfig(System.Int32,System.Int32,System.Single[],System.Single&,System.Single&,System.Int32&,System.Int32&)
extern "C"  bool PluginFunctions_arwGetMarkerPatternConfig_m3210735196 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___patternID1, SingleU5BU5D_t577127397* ___matrix2, float* ___width3, float* ___height4, int32_t* ___imageSizeX5, int32_t* ___imageSizeY6, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_1 = ___markerID0;
		int32_t L_2 = ___patternID1;
		SingleU5BU5D_t577127397* L_3 = ___matrix2;
		float* L_4 = ___width3;
		float* L_5 = ___height4;
		int32_t* L_6 = ___imageSizeX5;
		int32_t* L_7 = ___imageSizeY6;
		bool L_8 = ARNativePluginStatic_arwGetMarkerPatternConfig_m978360953(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_001b:
	{
		int32_t L_9 = ___markerID0;
		int32_t L_10 = ___patternID1;
		SingleU5BU5D_t577127397* L_11 = ___matrix2;
		float* L_12 = ___width3;
		float* L_13 = ___height4;
		int32_t* L_14 = ___imageSizeX5;
		int32_t* L_15 = ___imageSizeY6;
		bool L_16 = ARNativePlugin_arwGetMarkerPatternConfig_m4089949209(NULL /*static, unused*/, L_9, L_10, L_11, L_12, L_13, L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Boolean PluginFunctions::arwGetMarkerPatternImage(System.Int32,System.Int32,UnityEngine.Color[])
extern "C"  bool PluginFunctions_arwGetMarkerPatternImage_m1110189182 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___patternID1, ColorU5BU5D_t672350442* ___colors2, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_1 = ___markerID0;
		int32_t L_2 = ___patternID1;
		ColorU5BU5D_t672350442* L_3 = ___colors2;
		bool L_4 = ARNativePluginStatic_arwGetMarkerPatternImage_m2239266479(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0022;
	}

IL_0019:
	{
		int32_t L_5 = ___markerID0;
		int32_t L_6 = ___patternID1;
		ColorU5BU5D_t672350442* L_7 = ___colors2;
		bool L_8 = ARNativePlugin_arwGetMarkerPatternImage_m769298731(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0022:
	{
		bool L_9 = V_0;
		return L_9;
	}
}
// System.Boolean PluginFunctions::arwGetMarkerOptionBool(System.Int32,System.Int32)
extern "C"  bool PluginFunctions_arwGetMarkerOptionBool_m1227531826 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___markerID0;
		int32_t L_2 = ___option1;
		bool L_3 = ARNativePluginStatic_arwGetMarkerOptionBool_m535783117(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0013:
	{
		int32_t L_4 = ___markerID0;
		int32_t L_5 = ___option1;
		bool L_6 = ARNativePlugin_arwGetMarkerOptionBool_m2354836317(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void PluginFunctions::arwSetMarkerOptionBool(System.Int32,System.Int32,System.Boolean)
extern "C"  void PluginFunctions_arwSetMarkerOptionBool_m1349018745 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, bool ___value2, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___markerID0;
		int32_t L_2 = ___option1;
		bool L_3 = ___value2;
		ARNativePluginStatic_arwSetMarkerOptionBool_m2682973062(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		goto IL_0020;
	}

IL_0018:
	{
		int32_t L_4 = ___markerID0;
		int32_t L_5 = ___option1;
		bool L_6 = ___value2;
		ARNativePlugin_arwSetMarkerOptionBool_m1342859260(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Int32 PluginFunctions::arwGetMarkerOptionInt(System.Int32,System.Int32)
extern "C"  int32_t PluginFunctions_arwGetMarkerOptionInt_m4058957085 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___markerID0;
		int32_t L_2 = ___option1;
		int32_t L_3 = ARNativePluginStatic_arwGetMarkerOptionInt_m4131469642(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0013:
	{
		int32_t L_4 = ___markerID0;
		int32_t L_5 = ___option1;
		int32_t L_6 = ARNativePlugin_arwGetMarkerOptionInt_m1863724448(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void PluginFunctions::arwSetMarkerOptionInt(System.Int32,System.Int32,System.Int32)
extern "C"  void PluginFunctions_arwSetMarkerOptionInt_m4162112020 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, int32_t ___value2, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___markerID0;
		int32_t L_2 = ___option1;
		int32_t L_3 = ___value2;
		ARNativePluginStatic_arwSetMarkerOptionInt_m4268478505(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		goto IL_0020;
	}

IL_0018:
	{
		int32_t L_4 = ___markerID0;
		int32_t L_5 = ___option1;
		int32_t L_6 = ___value2;
		ARNativePlugin_arwSetMarkerOptionInt_m1852067113(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Single PluginFunctions::arwGetMarkerOptionFloat(System.Int32,System.Int32)
extern "C"  float PluginFunctions_arwGetMarkerOptionFloat_m3375944890 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___markerID0;
		int32_t L_2 = ___option1;
		float L_3 = ARNativePluginStatic_arwGetMarkerOptionFloat_m2202747965(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0013:
	{
		int32_t L_4 = ___markerID0;
		int32_t L_5 = ___option1;
		float L_6 = ARNativePlugin_arwGetMarkerOptionFloat_m1337828381(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void PluginFunctions::arwSetMarkerOptionFloat(System.Int32,System.Int32,System.Single)
extern "C"  void PluginFunctions_arwSetMarkerOptionFloat_m35491069 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, float ___value2, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___markerID0;
		int32_t L_2 = ___option1;
		float L_3 = ___value2;
		ARNativePluginStatic_arwSetMarkerOptionFloat_m591864046(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		goto IL_0020;
	}

IL_0018:
	{
		int32_t L_4 = ___markerID0;
		int32_t L_5 = ___option1;
		float L_6 = ___value2;
		ARNativePlugin_arwSetMarkerOptionFloat_m3494403524(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void PluginFunctions::arwSetVideoDebugMode(System.Boolean)
extern "C"  void PluginFunctions_arwSetVideoDebugMode_m3436558569 (Il2CppObject * __this /* static, unused */, bool ___debug0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = ___debug0;
		ARNativePluginStatic_arwSetVideoDebugMode_m2725814322(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		bool L_2 = ___debug0;
		ARNativePlugin_arwSetVideoDebugMode_m1349895608(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Boolean PluginFunctions::arwGetVideoDebugMode()
extern "C"  bool PluginFunctions_arwGetVideoDebugMode_m445557160 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0011;
		}
	}
	{
		bool L_1 = ARNativePluginStatic_arwGetVideoDebugMode_m4059797661(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		bool L_2 = ARNativePlugin_arwGetVideoDebugMode_m2178590269(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void PluginFunctions::arwSetVideoThreshold(System.Int32)
extern "C"  void PluginFunctions_arwSetVideoThreshold_m770268112 (Il2CppObject * __this /* static, unused */, int32_t ___threshold0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = ___threshold0;
		ARNativePluginStatic_arwSetVideoThreshold_m2572009209(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		int32_t L_2 = ___threshold0;
		ARNativePlugin_arwSetVideoThreshold_m2002595265(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 PluginFunctions::arwGetVideoThreshold()
extern "C"  int32_t PluginFunctions_arwGetVideoThreshold_m4164593841 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = ARNativePluginStatic_arwGetVideoThreshold_m2851652890(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		int32_t L_2 = ARNativePlugin_arwGetVideoThreshold_m2841997392(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void PluginFunctions::arwSetVideoThresholdMode(System.Int32)
extern "C"  void PluginFunctions_arwSetVideoThresholdMode_m3956560441 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = ___mode0;
		ARNativePluginStatic_arwSetVideoThresholdMode_m2501069538(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		int32_t L_2 = ___mode0;
		ARNativePlugin_arwSetVideoThresholdMode_m1677852552(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 PluginFunctions::arwGetVideoThresholdMode()
extern "C"  int32_t PluginFunctions_arwGetVideoThresholdMode_m1232578952 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = ARNativePluginStatic_arwGetVideoThresholdMode_m3923466573(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		int32_t L_2 = ARNativePlugin_arwGetVideoThresholdMode_m1626559629(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void PluginFunctions::arwSetLabelingMode(System.Int32)
extern "C"  void PluginFunctions_arwSetLabelingMode_m2460252585 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = ___mode0;
		ARNativePluginStatic_arwSetLabelingMode_m1320145026(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		int32_t L_2 = ___mode0;
		ARNativePlugin_arwSetLabelingMode_m801205656(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 PluginFunctions::arwGetLabelingMode()
extern "C"  int32_t PluginFunctions_arwGetLabelingMode_m2141811176 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = ARNativePluginStatic_arwGetLabelingMode_m2403718909(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		int32_t L_2 = ARNativePlugin_arwGetLabelingMode_m3294800397(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void PluginFunctions::arwSetBorderSize(System.Single)
extern "C"  void PluginFunctions_arwSetBorderSize_m3074401841 (Il2CppObject * __this /* static, unused */, float ___size0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0016;
		}
	}
	{
		float L_1 = ___size0;
		ARNativePluginStatic_arwSetBorderSize_m3985598462(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		float L_2 = ___size0;
		ARNativePlugin_arwSetBorderSize_m3785932980(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Single PluginFunctions::arwGetBorderSize()
extern "C"  float PluginFunctions_arwGetBorderSize_m756391518 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0011;
		}
	}
	{
		float L_1 = ARNativePluginStatic_arwGetBorderSize_m402710309(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		float L_2 = ARNativePlugin_arwGetBorderSize_m1115123341(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void PluginFunctions::arwSetPatternDetectionMode(System.Int32)
extern "C"  void PluginFunctions_arwSetPatternDetectionMode_m825573592 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = ___mode0;
		ARNativePluginStatic_arwSetPatternDetectionMode_m981787463(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		int32_t L_2 = ___mode0;
		ARNativePlugin_arwSetPatternDetectionMode_m2117597091(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 PluginFunctions::arwGetPatternDetectionMode()
extern "C"  int32_t PluginFunctions_arwGetPatternDetectionMode_m3294842179 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = ARNativePluginStatic_arwGetPatternDetectionMode_m2165003022(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		int32_t L_2 = ARNativePlugin_arwGetPatternDetectionMode_m1233416524(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void PluginFunctions::arwSetMatrixCodeType(System.Int32)
extern "C"  void PluginFunctions_arwSetMatrixCodeType_m4153092434 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = ___type0;
		ARNativePluginStatic_arwSetMatrixCodeType_m1755799257(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		int32_t L_2 = ___type0;
		ARNativePlugin_arwSetMatrixCodeType_m675665857(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 PluginFunctions::arwGetMatrixCodeType()
extern "C"  int32_t PluginFunctions_arwGetMatrixCodeType_m2398804801 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = ARNativePluginStatic_arwGetMatrixCodeType_m1128356520(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		int32_t L_2 = ARNativePlugin_arwGetMatrixCodeType_m176152962(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void PluginFunctions::arwSetImageProcMode(System.Int32)
extern "C"  void PluginFunctions_arwSetImageProcMode_m2102756940 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = ___mode0;
		ARNativePluginStatic_arwSetImageProcMode_m1469690497(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		int32_t L_2 = ___mode0;
		ARNativePlugin_arwSetImageProcMode_m302197153(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 PluginFunctions::arwGetImageProcMode()
extern "C"  int32_t PluginFunctions_arwGetImageProcMode_m3143829701 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = ARNativePluginStatic_arwGetImageProcMode_m855251164(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		int32_t L_2 = ARNativePlugin_arwGetImageProcMode_m676231894(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void PluginFunctions::arwSetNFTMultiMode(System.Boolean)
extern "C"  void PluginFunctions_arwSetNFTMultiMode_m2677128016 (Il2CppObject * __this /* static, unused */, bool ___on0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = ___on0;
		ARNativePluginStatic_arwSetNFTMultiMode_m4123606051(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		bool L_2 = ___on0;
		ARNativePlugin_arwSetNFTMultiMode_m733908047(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Boolean PluginFunctions::arwGetNFTMultiMode()
extern "C"  bool PluginFunctions_arwGetNFTMultiMode_m149465679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0011;
		}
	}
	{
		bool L_1 = ARNativePluginStatic_arwGetNFTMultiMode_m44684326(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		bool L_2 = ARNativePlugin_arwGetNFTMultiMode_m1076663180(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 PluginFunctions::arwAddMarker(System.String)
extern "C"  int32_t PluginFunctions_arwAddMarker_m2914794118 (Il2CppObject * __this /* static, unused */, String_t* ___cfg0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = ___cfg0;
		int32_t L_2 = ARNativePluginStatic_arwAddMarker_m4198366323(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0012:
	{
		String_t* L_3 = ___cfg0;
		int32_t L_4 = ARNativePlugin_arwAddMarker_m1259082831(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean PluginFunctions::arwRemoveMarker(System.Int32)
extern "C"  bool PluginFunctions_arwRemoveMarker_m1199729578 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___markerID0;
		bool L_2 = ARNativePluginStatic_arwRemoveMarker_m3792023969(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0012:
	{
		int32_t L_3 = ___markerID0;
		bool L_4 = ARNativePlugin_arwRemoveMarker_m78809961(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 PluginFunctions::arwRemoveAllMarkers()
extern "C"  int32_t PluginFunctions_arwRemoveAllMarkers_m3243259227 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = ARNativePluginStatic_arwRemoveAllMarkers_m1950709840(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		int32_t L_2 = ARNativePlugin_arwRemoveAllMarkers_m3455426882(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean PluginFunctions::arwQueryMarkerVisibility(System.Int32)
extern "C"  bool PluginFunctions_arwQueryMarkerVisibility_m1271849078 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___markerID0;
		bool L_2 = ARNativePluginStatic_arwQueryMarkerVisibility_m3506306455(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0012:
	{
		int32_t L_3 = ___markerID0;
		bool L_4 = ARNativePlugin_arwQueryMarkerVisibility_m3920274723(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean PluginFunctions::arwQueryMarkerTransformation(System.Int32,System.Single[])
extern "C"  bool PluginFunctions_arwQueryMarkerTransformation_m3138795604 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, SingleU5BU5D_t577127397* ___matrix1, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___markerID0;
		SingleU5BU5D_t577127397* L_2 = ___matrix1;
		bool L_3 = ARNativePluginStatic_arwQueryMarkerTransformation_m1782821311(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0013:
	{
		int32_t L_4 = ___markerID0;
		SingleU5BU5D_t577127397* L_5 = ___matrix1;
		bool L_6 = ARNativePlugin_arwQueryMarkerTransformation_m3853733251(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean PluginFunctions::arwQueryMarkerTransformationStereo(System.Int32,System.Single[],System.Single[])
extern "C"  bool PluginFunctions_arwQueryMarkerTransformationStereo_m3818845253 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, SingleU5BU5D_t577127397* ___matrixL1, SingleU5BU5D_t577127397* ___matrixR2, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = ___markerID0;
		SingleU5BU5D_t577127397* L_2 = ___matrixL1;
		SingleU5BU5D_t577127397* L_3 = ___matrixR2;
		bool L_4 = ARNativePluginStatic_arwQueryMarkerTransformationStereo_m2434214006(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0014:
	{
		int32_t L_5 = ___markerID0;
		SingleU5BU5D_t577127397* L_6 = ___matrixL1;
		SingleU5BU5D_t577127397* L_7 = ___matrixR2;
		bool L_8 = ARNativePlugin_arwQueryMarkerTransformationStereo_m3906452060(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean PluginFunctions::arwLoadOpticalParams(System.String,System.Byte[],System.Int32,System.Single&,System.Single&,System.Single[],System.Single[])
extern "C"  bool PluginFunctions_arwLoadOpticalParams_m780578185 (Il2CppObject * __this /* static, unused */, String_t* ___optical_param_name0, ByteU5BU5D_t3397334013* ___optical_param_buff1, int32_t ___optical_param_buffLen2, float* ___fovy_p3, float* ___aspect_p4, SingleU5BU5D_t577127397* ___m5, SingleU5BU5D_t577127397* ___p6, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = ___optical_param_name0;
		ByteU5BU5D_t3397334013* L_2 = ___optical_param_buff1;
		int32_t L_3 = ___optical_param_buffLen2;
		float* L_4 = ___fovy_p3;
		float* L_5 = ___aspect_p4;
		SingleU5BU5D_t577127397* L_6 = ___m5;
		SingleU5BU5D_t577127397* L_7 = ___p6;
		bool L_8 = ARNativePluginStatic_arwLoadOpticalParams_m1348665490(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_001b:
	{
		String_t* L_9 = ___optical_param_name0;
		ByteU5BU5D_t3397334013* L_10 = ___optical_param_buff1;
		int32_t L_11 = ___optical_param_buffLen2;
		float* L_12 = ___fovy_p3;
		float* L_13 = ___aspect_p4;
		SingleU5BU5D_t577127397* L_14 = ___m5;
		SingleU5BU5D_t577127397* L_15 = ___p6;
		bool L_16 = ARNativePlugin_arwLoadOpticalParams_m1652819160(NULL /*static, unused*/, L_9, L_10, L_11, L_12, L_13, L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Void PluginFunctions::.cctor()
extern "C"  void PluginFunctions__cctor_m453146558 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PluginFunctions/LogCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LogCallback__ctor_m1707937529 (LogCallback_t2143553514 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PluginFunctions/LogCallback::Invoke(System.String)
extern "C"  void LogCallback_Invoke_m1524203691 (LogCallback_t2143553514 * __this, String_t* ___msg0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LogCallback_Invoke_m1524203691((LogCallback_t2143553514 *)__this->get_prev_9(),___msg0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___msg0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___msg0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___msg0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___msg0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___msg0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_LogCallback_t2143553514 (LogCallback_t2143553514 * __this, String_t* ___msg0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___msg0' to native representation
	char* ____msg0_marshaled = NULL;
	____msg0_marshaled = il2cpp_codegen_marshal_string(___msg0);

	// Native function invocation
	il2cppPInvokeFunc(____msg0_marshaled);

	// Marshaling cleanup of parameter '___msg0' native representation
	il2cpp_codegen_marshal_free(____msg0_marshaled);
	____msg0_marshaled = NULL;

}
// System.IAsyncResult PluginFunctions/LogCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LogCallback_BeginInvoke_m696390104 (LogCallback_t2143553514 * __this, String_t* ___msg0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___msg0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void PluginFunctions/LogCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LogCallback_EndInvoke_m1985947475 (LogCallback_t2143553514 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void TimedSelfDeactivate::.ctor()
extern "C"  void TimedSelfDeactivate__ctor_m3836804226 (TimedSelfDeactivate_t3836150707 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimedSelfDeactivate::OnEnable()
extern "C"  void TimedSelfDeactivate_OnEnable_m681472206 (TimedSelfDeactivate_t3836150707 * __this, const MethodInfo* method)
{
	{
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_storedTime_2(L_0);
		return;
	}
}
// System.Void TimedSelfDeactivate::Update()
extern "C"  void TimedSelfDeactivate_Update_m1600353847 (TimedSelfDeactivate_t3836150707 * __this, const MethodInfo* method)
{
	{
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_storedTime_2();
		float L_2 = __this->get_timeBeforeDeactivate_3();
		if ((!(((float)L_0) > ((float)((float)((float)L_1+(float)L_2))))))
		{
			goto IL_0023;
		}
	}
	{
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void TimedSelfDestruct::.ctor()
extern "C"  void TimedSelfDestruct__ctor_m1407401746 (TimedSelfDestruct_t4043215813 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimedSelfDestruct::Start()
extern "C"  void TimedSelfDestruct_Start_m2725227658 (TimedSelfDestruct_t4043215813 * __this, const MethodInfo* method)
{
	{
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_storedTime_2(L_0);
		return;
	}
}
// System.Void TimedSelfDestruct::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t TimedSelfDestruct_Update_m869068077_MetadataUsageId;
extern "C"  void TimedSelfDestruct_Update_m869068077 (TimedSelfDestruct_t4043215813 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimedSelfDestruct_Update_m869068077_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_storedTime_2();
		float L_2 = __this->get_timeBeforeDestroy_3();
		if ((!(((float)L_0) > ((float)((float)((float)L_1+(float)L_2))))))
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
