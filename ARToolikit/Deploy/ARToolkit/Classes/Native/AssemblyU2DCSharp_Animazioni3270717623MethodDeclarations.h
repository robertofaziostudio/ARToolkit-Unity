﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Animazioni
struct Animazioni_t3270717623;

#include "codegen/il2cpp-codegen.h"

// System.Void Animazioni::.ctor()
extern "C"  void Animazioni__ctor_m2857345758 (Animazioni_t3270717623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Animazioni::CheckWinner()
extern "C"  void Animazioni_CheckWinner_m2031567265 (Animazioni_t3270717623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
