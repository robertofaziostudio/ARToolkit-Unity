﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FisticuffsController
struct FisticuffsController_t308090938;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FisticuffsController308090938.h"

// System.Void FisticuffsController::.ctor()
extern "C"  void FisticuffsController__ctor_m19705551 (FisticuffsController_t308090938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FisticuffsController FisticuffsController::get_Instance()
extern "C"  FisticuffsController_t308090938 * FisticuffsController_get_Instance_m3869543890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FisticuffsController::set_Instance(FisticuffsController)
extern "C"  void FisticuffsController_set_Instance_m2765316899 (Il2CppObject * __this /* static, unused */, FisticuffsController_t308090938 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FisticuffsController::Awake()
extern "C"  void FisticuffsController_Awake_m2424235722 (FisticuffsController_t308090938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FisticuffsController::Start()
extern "C"  void FisticuffsController_Start_m2623175131 (FisticuffsController_t308090938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FisticuffsController::Update()
extern "C"  void FisticuffsController_Update_m1668360200 (FisticuffsController_t308090938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FisticuffsController::CheckIfTwoCards()
extern "C"  void FisticuffsController_CheckIfTwoCards_m1789851537 (FisticuffsController_t308090938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FisticuffsController::IntroStart()
extern "C"  Il2CppObject * FisticuffsController_IntroStart_m1902157099 (FisticuffsController_t308090938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FisticuffsController::GameStart()
extern "C"  void FisticuffsController_GameStart_m702030753 (FisticuffsController_t308090938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FisticuffsController::LookAtEachOther()
extern "C"  void FisticuffsController_LookAtEachOther_m2746055666 (FisticuffsController_t308090938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FisticuffsController::TakeAim()
extern "C"  void FisticuffsController_TakeAim_m3709832649 (FisticuffsController_t308090938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FisticuffsController::GameEnd()
extern "C"  void FisticuffsController_GameEnd_m456451874 (FisticuffsController_t308090938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FisticuffsController::EndBell()
extern "C"  Il2CppObject * FisticuffsController_EndBell_m1572967765 (FisticuffsController_t308090938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
