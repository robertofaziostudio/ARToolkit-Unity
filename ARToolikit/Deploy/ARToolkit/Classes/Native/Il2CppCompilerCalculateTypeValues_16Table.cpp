﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMo857969000.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache4810721.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall2183506063.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3420894182.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall3793436469.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup339478082.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList2295673753.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"
#include "UnityEngine_UnityEngine_UnityString276356480.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime2105307154.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1808633952.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram1120735295.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1027170048.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAtt665825653.h"
#include "UnityEngine_UnityEngine_Logger3328995178.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri3673080018.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules1810425448.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1390152093.h"
#include "UnityEngine_UnityEngineInternal_GenericStack3718539591.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970.h"
#include "System_Xml_U3CModuleU3E3783534214.h"
#include "System_Xml_System_MonoTODOAttribute3487514019.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet2758097827.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType1096449895.h"
#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicType3359210273.h"
#include "System_Xml_Mono_Xml_Schema_XdtUntypedAtomic2904699188.h"
#include "System_Xml_Mono_Xml_Schema_XsdString263933896.h"
#include "System_Xml_Mono_Xml_Schema_XsdNormalizedString2132216291.h"
#include "System_Xml_Mono_Xml_Schema_XsdToken2902215462.h"
#include "System_Xml_Mono_Xml_Schema_XsdLanguage3897851897.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMToken547135877.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokens1753890866.h"
#include "System_Xml_Mono_Xml_Schema_XsdName1409945702.h"
#include "System_Xml_Mono_Xml_Schema_XsdNCName414304927.h"
#include "System_Xml_Mono_Xml_Schema_XsdID1067193160.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRef1377311049.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefs763119546.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntity2664305022.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntities1103053540.h"
#include "System_Xml_Mono_Xml_Schema_XsdNotation720379093.h"
#include "System_Xml_Mono_Xml_Schema_XsdDecimal2932251550.h"
#include "System_Xml_Mono_Xml_Schema_XsdInteger1330502157.h"
#include "System_Xml_Mono_Xml_Schema_XsdLong4179235589.h"
#include "System_Xml_Mono_Xml_Schema_XsdInt1488443894.h"
#include "System_Xml_Mono_Xml_Schema_XsdShort1778530041.h"
#include "System_Xml_Mono_Xml_Schema_XsdByte1120972221.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonNegativeInteger3587933853.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedLong137890294.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedInt2956447959.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedShort3693774826.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedByte3216355454.h"
#include "System_Xml_Mono_Xml_Schema_XsdPositiveInteger1896481288.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonPositiveInteger409343009.h"
#include "System_Xml_Mono_Xml_Schema_XsdNegativeInteger399444136.h"
#include "System_Xml_Mono_Xml_Schema_XsdFloat386143221.h"
#include "System_Xml_Mono_Xml_Schema_XsdDouble2510112208.h"
#include "System_Xml_Mono_Xml_Schema_XsdBase64Binary1094704629.h"
#include "System_Xml_Mono_Xml_Schema_XsdHexBinary3496718151.h"
#include "System_Xml_Mono_Xml_Schema_XsdQName930779123.h"
#include "System_Xml_Mono_Xml_Schema_XsdBoolean4126353587.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyURI2527983239.h"
#include "System_Xml_Mono_Xml_Schema_XsdDuration1605638443.h"
#include "System_Xml_Mono_Xml_Schema_XdtDayTimeDuration2797717973.h"
#include "System_Xml_Mono_Xml_Schema_XdtYearMonthDuration1764328599.h"
#include "System_Xml_Mono_Xml_Schema_XsdDateTime1344468684.h"
#include "System_Xml_Mono_Xml_Schema_XsdDate919459387.h"
#include "System_Xml_Mono_Xml_Schema_XsdTime4165680512.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYearMonth1363357165.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonthDay3859978294.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYear3810382607.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonth4076673358.h"
#include "System_Xml_Mono_Xml_Schema_XsdGDay1914244270.h"
#include "System_Xml_System_Xml_Schema_ValidationEventArgs1577905814.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2082486936.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttribute4015859774.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCompilationS2971213394.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (PersistentListenerMode_t857969000)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1600[8] = 
{
	PersistentListenerMode_t857969000::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (ArgumentCache_t4810721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1601[6] = 
{
	ArgumentCache_t4810721::get_offset_of_m_ObjectArgument_0(),
	ArgumentCache_t4810721::get_offset_of_m_ObjectArgumentAssemblyTypeName_1(),
	ArgumentCache_t4810721::get_offset_of_m_IntArgument_2(),
	ArgumentCache_t4810721::get_offset_of_m_FloatArgument_3(),
	ArgumentCache_t4810721::get_offset_of_m_StringArgument_4(),
	ArgumentCache_t4810721::get_offset_of_m_BoolArgument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (BaseInvokableCall_t2229564840), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (InvokableCall_t2183506063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1603[1] = 
{
	InvokableCall_t2183506063::get_offset_of_Delegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1604[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1605[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1606[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1607[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1608[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (UnityEventCallState_t3420894182)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1609[4] = 
{
	UnityEventCallState_t3420894182::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (PersistentCall_t3793436469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1610[5] = 
{
	PersistentCall_t3793436469::get_offset_of_m_Target_0(),
	PersistentCall_t3793436469::get_offset_of_m_MethodName_1(),
	PersistentCall_t3793436469::get_offset_of_m_Mode_2(),
	PersistentCall_t3793436469::get_offset_of_m_Arguments_3(),
	PersistentCall_t3793436469::get_offset_of_m_CallState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (PersistentCallGroup_t339478082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1611[1] = 
{
	PersistentCallGroup_t339478082::get_offset_of_m_Calls_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (InvokableCallList_t2295673753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1612[4] = 
{
	InvokableCallList_t2295673753::get_offset_of_m_PersistentCalls_0(),
	InvokableCallList_t2295673753::get_offset_of_m_RuntimeCalls_1(),
	InvokableCallList_t2295673753::get_offset_of_m_ExecutingCalls_2(),
	InvokableCallList_t2295673753::get_offset_of_m_NeedsUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (UnityEventBase_t828812576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1613[4] = 
{
	UnityEventBase_t828812576::get_offset_of_m_Calls_0(),
	UnityEventBase_t828812576::get_offset_of_m_PersistentCalls_1(),
	UnityEventBase_t828812576::get_offset_of_m_TypeName_2(),
	UnityEventBase_t828812576::get_offset_of_m_CallsDirty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (UnityAction_t4025899511), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (UnityEvent_t408735097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1615[1] = 
{
	UnityEvent_t408735097::get_offset_of_m_InvokeArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1617[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1619[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1621[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1623[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (UnityString_t276356480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (Vector2_t2243707579)+ sizeof (Il2CppObject), sizeof(Vector2_t2243707579 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1625[3] = 
{
	Vector2_t2243707579::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector2_t2243707579::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (Vector4_t2243707581)+ sizeof (Il2CppObject), sizeof(Vector4_t2243707581 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1626[5] = 
{
	0,
	Vector4_t2243707581::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_w_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (WaitForSecondsRealtime_t2105307154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1627[1] = 
{
	WaitForSecondsRealtime_t2105307154::get_offset_of_waitTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (AnimationPlayableUtilities_t1808633952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (FrameData_t1120735295)+ sizeof (Il2CppObject), sizeof(FrameData_t1120735295 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1629[4] = 
{
	FrameData_t1120735295::get_offset_of_m_UpdateId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_Time_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_LastTime_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_TimeScale_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (DefaultValueAttribute_t1027170048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1630[1] = 
{
	DefaultValueAttribute_t1027170048::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (ExcludeFromDocsAttribute_t665825653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (Logger_t3328995178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1634[3] = 
{
	Logger_t3328995178::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t3328995178::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t3328995178::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (UsedByNativeCodeAttribute_t3212052468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (RequiredByNativeCodeAttribute_t1913052472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (FormerlySerializedAsAttribute_t3673080018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1637[1] = 
{
	FormerlySerializedAsAttribute_t3673080018::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (TypeInferenceRules_t1810425448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1638[5] = 
{
	TypeInferenceRules_t1810425448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (TypeInferenceRuleAttribute_t1390152093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1639[1] = 
{
	TypeInferenceRuleAttribute_t1390152093::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (GenericStack_t3718539591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (NetFxCoreExtensions_t4275971970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (MonoTODOAttribute_t3487514022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (XsdWhitespaceFacet_t2758097827)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1645[4] = 
{
	XsdWhitespaceFacet_t2758097827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (XsdAnySimpleType_t1096449895), -1, sizeof(XsdAnySimpleType_t1096449895_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1646[6] = 
{
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_instance_55(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_whitespaceArray_56(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_booleanAllowedFacets_57(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_decimalAllowedFacets_58(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_durationAllowedFacets_59(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_stringAllowedFacets_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (XdtAnyAtomicType_t3359210273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (XdtUntypedAtomic_t2904699188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (XsdString_t263933896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (XsdNormalizedString_t2132216291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (XsdToken_t2902215462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (XsdLanguage_t3897851897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (XsdNMToken_t547135877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (XsdNMTokens_t1753890866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (XsdName_t1409945702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (XsdNCName_t414304927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (XsdID_t1067193160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (XsdIDRef_t1377311049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (XsdIDRefs_t763119546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (XsdEntity_t2664305022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (XsdEntities_t1103053540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (XsdNotation_t720379093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (XsdDecimal_t2932251550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (XsdInteger_t1330502157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (XsdLong_t4179235589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (XsdInt_t1488443894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (XsdShort_t1778530041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (XsdByte_t1120972221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (XsdNonNegativeInteger_t3587933853), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (XsdUnsignedLong_t137890294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (XsdUnsignedInt_t2956447959), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (XsdUnsignedShort_t3693774826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (XsdUnsignedByte_t3216355454), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (XsdPositiveInteger_t1896481288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (XsdNonPositiveInteger_t409343009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (XsdNegativeInteger_t399444136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (XsdFloat_t386143221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (XsdDouble_t2510112208), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (XsdBase64Binary_t1094704629), -1, sizeof(XsdBase64Binary_t1094704629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1679[2] = 
{
	XsdBase64Binary_t1094704629_StaticFields::get_offset_of_ALPHABET_61(),
	XsdBase64Binary_t1094704629_StaticFields::get_offset_of_decodeTable_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (XsdHexBinary_t3496718151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (XsdQName_t930779123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (XsdBoolean_t4126353587), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (XsdAnyURI_t2527983239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (XsdDuration_t1605638443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (XdtDayTimeDuration_t2797717973), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (XdtYearMonthDuration_t1764328599), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (XsdDateTime_t1344468684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (XsdDate_t919459387), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (XsdTime_t4165680512), -1, sizeof(XsdTime_t4165680512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1689[1] = 
{
	XsdTime_t4165680512_StaticFields::get_offset_of_timeFormats_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (XsdGYearMonth_t1363357165), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (XsdGMonthDay_t3859978294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (XsdGYear_t3810382607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (XsdGMonth_t4076673358), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (XsdGDay_t1914244270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (ValidationEventArgs_t1577905814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (XmlSchemaAnnotated_t2082486936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (XmlSchemaAttribute_t4015859774), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (XmlSchemaCompilationSettings_t2971213394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1699[1] = 
{
	XmlSchemaCompilationSettings_t2971213394::get_offset_of_enable_upa_check_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
