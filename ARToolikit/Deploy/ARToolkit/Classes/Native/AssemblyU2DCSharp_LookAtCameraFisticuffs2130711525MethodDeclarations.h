﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LookAtCameraFisticuffs
struct LookAtCameraFisticuffs_t2130711525;

#include "codegen/il2cpp-codegen.h"

// System.Void LookAtCameraFisticuffs::.ctor()
extern "C"  void LookAtCameraFisticuffs__ctor_m3764978938 (LookAtCameraFisticuffs_t2130711525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LookAtCameraFisticuffs::OnEnable()
extern "C"  void LookAtCameraFisticuffs_OnEnable_m4294516114 (LookAtCameraFisticuffs_t2130711525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LookAtCameraFisticuffs::FindCamera()
extern "C"  void LookAtCameraFisticuffs_FindCamera_m225812980 (LookAtCameraFisticuffs_t2130711525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LookAtCameraFisticuffs::Update()
extern "C"  void LookAtCameraFisticuffs_Update_m2234155241 (LookAtCameraFisticuffs_t2130711525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LookAtCameraFisticuffs::RotateMe()
extern "C"  void LookAtCameraFisticuffs_RotateMe_m3541710813 (LookAtCameraFisticuffs_t2130711525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
