﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3121135483MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m910696840(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2460906421 *, Dictionary_2_t3757846578 *, const MethodInfo*))ValueCollection__ctor_m157664498_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.String>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2982893650(__this, ___item0, method) ((  void (*) (ValueCollection_t2460906421 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1851314284_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.String>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2990868551(__this, method) ((  void (*) (ValueCollection_t2460906421 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3016130531_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.String>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3556302974(__this, ___item0, method) ((  bool (*) (ValueCollection_t2460906421 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1644213316_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.String>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m182012527(__this, ___item0, method) ((  bool (*) (ValueCollection_t2460906421 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m891289515_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.String>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3216613677(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2460906421 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3434320585_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3377093145(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2460906421 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3514481981_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2086267082(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2460906421 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3959541796_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.String>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3443370371(__this, method) ((  bool (*) (ValueCollection_t2460906421 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m413509039_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3754330877(__this, method) ((  bool (*) (ValueCollection_t2460906421 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4195087353_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.String>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3625709545(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2460906421 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2900300333_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.String>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m409973535(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2460906421 *, StringU5BU5D_t1642385972*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m219026987_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.String>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1784689818(__this, method) ((  Enumerator_t1149412046  (*) (ValueCollection_t2460906421 *, const MethodInfo*))ValueCollection_GetEnumerator_m1960551728_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.String>::get_Count()
#define ValueCollection_get_Count_m1315799069(__this, method) ((  int32_t (*) (ValueCollection_t2460906421 *, const MethodInfo*))ValueCollection_get_Count_m1203718169_gshared)(__this, method)
