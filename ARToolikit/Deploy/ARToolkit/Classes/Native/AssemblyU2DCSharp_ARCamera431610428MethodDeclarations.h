﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARCamera
struct ARCamera_t431610428;
// AROrigin
struct AROrigin_t3335349585;
// ARMarker
struct ARMarker_t1554260723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

// System.Void ARCamera::.ctor()
extern "C"  void ARCamera__ctor_m4072460825 (ARCamera_t431610428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARCamera::SetupCamera(System.Single,System.Single,UnityEngine.Matrix4x4,System.Boolean&)
extern "C"  bool ARCamera_SetupCamera_m2259267036 (ARCamera_t431610428 * __this, float ___nearClipPlane0, float ___farClipPlane1, Matrix4x4_t2933234003  ___projectionMatrix2, bool* ___opticalOut3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AROrigin ARCamera::GetOrigin()
extern "C"  AROrigin_t3335349585 * ARCamera_GetOrigin_m398559295 (ARCamera_t431610428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARMarker ARCamera::GetMarker()
extern "C"  ARMarker_t1554260723 * ARCamera_GetMarker_m1448161535 (ARCamera_t431610428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARCamera::UpdateTracking()
extern "C"  void ARCamera_UpdateTracking_m1132195383 (ARCamera_t431610428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARCamera::ApplyTracking()
extern "C"  void ARCamera_ApplyTracking_m1784467530 (ARCamera_t431610428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARCamera::LateUpdate()
extern "C"  void ARCamera_LateUpdate_m2396630374 (ARCamera_t431610428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
