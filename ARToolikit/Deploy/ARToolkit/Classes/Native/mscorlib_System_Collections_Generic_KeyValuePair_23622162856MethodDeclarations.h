﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24282391918MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<MarkerType,System.String>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3615821169(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3622162856 *, int32_t, String_t*, const MethodInfo*))KeyValuePair_2__ctor_m87771405_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<MarkerType,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m1097918175(__this, method) ((  int32_t (*) (KeyValuePair_2_t3622162856 *, const MethodInfo*))KeyValuePair_2_get_Key_m2382745507_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<MarkerType,System.String>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3096967994(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3622162856 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3885289636_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<MarkerType,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m1794262031(__this, method) ((  String_t* (*) (KeyValuePair_2_t3622162856 *, const MethodInfo*))KeyValuePair_2_get_Value_m2932089363_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<MarkerType,System.String>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3287302906(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3622162856 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Value_m2877441604_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<MarkerType,System.String>::ToString()
#define KeyValuePair_2_ToString_m2720668272(__this, method) ((  String_t* (*) (KeyValuePair_2_t3622162856 *, const MethodInfo*))KeyValuePair_2_ToString_m2673501082_gshared)(__this, method)
