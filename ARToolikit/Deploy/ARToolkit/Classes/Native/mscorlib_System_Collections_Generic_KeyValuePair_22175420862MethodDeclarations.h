﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22175420862.h"
#include "AssemblyU2DCSharp_ContentMode2920913530.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<ContentMode,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2727937921_gshared (KeyValuePair_2_t2175420862 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m2727937921(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2175420862 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m2727937921_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<ContentMode,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1256702711_gshared (KeyValuePair_2_t2175420862 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1256702711(__this, method) ((  int32_t (*) (KeyValuePair_2_t2175420862 *, const MethodInfo*))KeyValuePair_2_get_Key_m1256702711_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ContentMode,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3305375956_gshared (KeyValuePair_2_t2175420862 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3305375956(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2175420862 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3305375956_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<ContentMode,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2708364631_gshared (KeyValuePair_2_t2175420862 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2708364631(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2175420862 *, const MethodInfo*))KeyValuePair_2_get_Value_m2708364631_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ContentMode,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2787780628_gshared (KeyValuePair_2_t2175420862 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2787780628(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2175420862 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2787780628_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<ContentMode,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1193705512_gshared (KeyValuePair_2_t2175420862 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1193705512(__this, method) ((  String_t* (*) (KeyValuePair_2_t2175420862 *, const MethodInfo*))KeyValuePair_2_ToString_m1193705512_gshared)(__this, method)
