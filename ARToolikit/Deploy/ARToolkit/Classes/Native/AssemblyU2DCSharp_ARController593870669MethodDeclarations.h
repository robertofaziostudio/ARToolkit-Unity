﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARController
struct ARController_t593870669;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Mesh
struct Mesh_t1356156583;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitThresholdM2161580787.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitLabelingMod925999490.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitPatternDet1396884775.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitMatrixCode3871848761.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitImageProcM1927279569.h"
#include "AssemblyU2DCSharp_ARController_AR_LOG_LEVEL1976762171.h"
#include "AssemblyU2DCSharp_ContentMode2920913530.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "AssemblyU2DCSharp_ARCamera_ViewEye928886047.h"

// System.Void ARController::.ctor()
extern "C"  void ARController__ctor_m2991573286 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<System.String> ARController::get_logCallback()
extern "C"  Action_1_t1831019615 * ARController_get_logCallback_m620268047 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::set_logCallback(System.Action`1<System.String>)
extern "C"  void ARController_set_logCallback_m1311593722 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::Awake()
extern "C"  void ARController_Awake_m2362708713 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::OnEnable()
extern "C"  void ARController_OnEnable_m2249051214 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::Start()
extern "C"  void ARController_Start_m433899566 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::OnApplicationPause(System.Boolean)
extern "C"  void ARController_OnApplicationPause_m1181163288 (ARController_t593870669 * __this, bool ___paused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::Update()
extern "C"  void ARController_Update_m4055977229 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::OnApplicationQuit()
extern "C"  void ARController_OnApplicationQuit_m331055932 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::OnDisable()
extern "C"  void ARController_OnDisable_m3810669353 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::OnDestroy()
extern "C"  void ARController_OnDestroy_m2882413523 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARController::StartAR()
extern "C"  bool ARController_StartAR_m3868647255 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARController::UpdateAR()
extern "C"  bool ARController_UpdateAR_m562781644 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARController::StopAR()
extern "C"  bool ARController_StopAR_m3264126367 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::SetContentForScreenOrientation(System.Boolean)
extern "C"  void ARController_SetContentForScreenOrientation_m3593716101 (ARController_t593870669 * __this, bool ___cameraIsFrontFacing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::SetVideoAlpha(System.Single)
extern "C"  void ARController_SetVideoAlpha_m3263902264 (ARController_t593870669 * __this, float ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARController::get_DebugVideo()
extern "C"  bool ARController_get_DebugVideo_m3800461305 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::set_DebugVideo(System.Boolean)
extern "C"  void ARController_set_DebugVideo_m539547094 (ARController_t593870669 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARController::get_Version()
extern "C"  String_t* ARController_get_Version_m2150833958 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARController/ARToolKitThresholdMode ARController::get_VideoThresholdMode()
extern "C"  int32_t ARController_get_VideoThresholdMode_m1767546950 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::set_VideoThresholdMode(ARController/ARToolKitThresholdMode)
extern "C"  void ARController_set_VideoThresholdMode_m3535350045 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARController::get_VideoThreshold()
extern "C"  int32_t ARController_get_VideoThreshold_m208759273 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::set_VideoThreshold(System.Int32)
extern "C"  void ARController_set_VideoThreshold_m2326990118 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARController/ARToolKitLabelingMode ARController::get_LabelingMode()
extern "C"  int32_t ARController_get_LabelingMode_m2521454447 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::set_LabelingMode(ARController/ARToolKitLabelingMode)
extern "C"  void ARController_set_LabelingMode_m2847600302 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARController::get_BorderSize()
extern "C"  float ARController_get_BorderSize_m4181028184 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::set_BorderSize(System.Single)
extern "C"  void ARController_set_BorderSize_m2688897581 (ARController_t593870669 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARController::get_TemplateSize()
extern "C"  int32_t ARController_get_TemplateSize_m4162121116 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::set_TemplateSize(System.Int32)
extern "C"  void ARController_set_TemplateSize_m480258561 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARController::get_TemplateCountMax()
extern "C"  int32_t ARController_get_TemplateCountMax_m507601286 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::set_TemplateCountMax(System.Int32)
extern "C"  void ARController_set_TemplateCountMax_m3403685427 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARController/ARToolKitPatternDetectionMode ARController::get_PatternDetectionMode()
extern "C"  int32_t ARController_get_PatternDetectionMode_m2852381811 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::set_PatternDetectionMode(ARController/ARToolKitPatternDetectionMode)
extern "C"  void ARController_set_PatternDetectionMode_m2120827272 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARController/ARToolKitMatrixCodeType ARController::get_MatrixCodeType()
extern "C"  int32_t ARController_get_MatrixCodeType_m3814669595 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::set_MatrixCodeType(ARController/ARToolKitMatrixCodeType)
extern "C"  void ARController_set_MatrixCodeType_m2903938876 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARController/ARToolKitImageProcMode ARController::get_ImageProcMode()
extern "C"  int32_t ARController_get_ImageProcMode_m695111051 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::set_ImageProcMode(ARController/ARToolKitImageProcMode)
extern "C"  void ARController_set_ImageProcMode_m2544291598 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARController::get_NFTMultiMode()
extern "C"  bool ARController_get_NFTMultiMode_m2506531219 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::set_NFTMultiMode(System.Boolean)
extern "C"  void ARController_set_NFTMultiMode_m1466211986 (ARController_t593870669 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARController/AR_LOG_LEVEL ARController::get_LogLevel()
extern "C"  int32_t ARController_get_LogLevel_m3380931765 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::set_LogLevel(ARController/AR_LOG_LEVEL)
extern "C"  void ARController_set_LogLevel_m3866560960 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ContentMode ARController::get_ContentMode()
extern "C"  int32_t ARController_get_ContentMode_m862413810 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::set_ContentMode(ContentMode)
extern "C"  void ARController_set_ContentMode_m1004281933 (ARController_t593870669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARController::get_UseVideoBackground()
extern "C"  bool ARController_get_UseVideoBackground_m1201911505 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::set_UseVideoBackground(System.Boolean)
extern "C"  void ARController_set_UseVideoBackground_m3884950314 (ARController_t593870669 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::UpdateTexture()
extern "C"  void ARController_UpdateTexture_m1367383666 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARController::CreateClearCamera()
extern "C"  bool ARController_CreateClearCamera_m2815811798 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ARController::CreateVideoBackgroundMesh(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[]&,UnityEngine.Color32[]&,UnityEngine.Texture2D&,UnityEngine.Material&)
extern "C"  GameObject_t1756533147 * ARController_CreateVideoBackgroundMesh_m3804930034 (ARController_t593870669 * __this, int32_t ___index0, int32_t ___w1, int32_t ___h2, int32_t ___layer3, ColorU5BU5D_t672350442** ___vbca4, Color32U5BU5D_t30278651** ___vbc32a5, Texture2D_t3542995729 ** ___vbt6, Material_t193706927 ** ___vbm7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ARController::CreateVideoBackgroundCamera(System.String,System.Int32,UnityEngine.Camera&)
extern "C"  GameObject_t1756533147 * ARController_CreateVideoBackgroundCamera_m781252442 (ARController_t593870669 * __this, String_t* ___name0, int32_t ___layer1, Camera_t189460977 ** ___vbc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::DestroyVideoBackground()
extern "C"  void ARController_DestroyVideoBackground_m152274941 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARController::DestroyClearCamera()
extern "C"  bool ARController_DestroyClearCamera_m113337958 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect ARController::getViewport(System.Int32,System.Int32,System.Boolean,ARCamera/ViewEye)
extern "C"  Rect_t3681755626  ARController_getViewport_m115664776 (ARController_t593870669 * __this, int32_t ___contentWidth0, int32_t ___contentHeight1, bool ___stereo2, int32_t ___viewEye3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::CycleContentMode()
extern "C"  void ARController_CycleContentMode_m220124596 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARController::ConfigureForegroundCameras()
extern "C"  bool ARController_ConfigureForegroundCameras_m229175717 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARController::ConfigureViewports()
extern "C"  bool ARController_ConfigureViewports_m2051600675 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh ARController::newVideoMesh(System.Boolean,System.Boolean,System.Single,System.Single)
extern "C"  Mesh_t1356156583 * ARController_newVideoMesh_m1266137681 (ARController_t593870669 * __this, bool ___flipX0, bool ___flipY1, float ___textureScaleU2, float ___textureScaleV3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::Log(System.String)
extern "C"  void ARController_Log_m1311826870 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::CalculateFPS()
extern "C"  void ARController_CalculateFPS_m1910495181 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::SetupGUI()
extern "C"  void ARController_SetupGUI_m2678073940 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::OnGUI()
extern "C"  void ARController_OnGUI_m3978310686 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::DrawErrorDialog(System.Int32)
extern "C"  void ARController_DrawErrorDialog_m2233680885 (ARController_t593870669 * __this, int32_t ___winID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::DrawInfoGUI()
extern "C"  void ARController_DrawInfoGUI_m2085452537 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::DrawLogConsole()
extern "C"  void ARController_DrawLogConsole_m2350776051 (ARController_t593870669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::DrawLogEntries(UnityEngine.Rect,System.Boolean)
extern "C"  void ARController_DrawLogEntries_m2326935398 (ARController_t593870669 * __this, Rect_t3681755626  ___container0, bool ___reverse1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARController::.cctor()
extern "C"  void ARController__cctor_m1717663591 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
