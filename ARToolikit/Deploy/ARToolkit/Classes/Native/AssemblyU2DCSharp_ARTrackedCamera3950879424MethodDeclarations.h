﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARTrackedCamera
struct ARTrackedCamera_t3950879424;
// System.String
struct String_t;
// ARMarker
struct ARMarker_t1554260723;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ARTrackedCamera::.ctor()
extern "C"  void ARTrackedCamera__ctor_m3960348331 (ARTrackedCamera_t3950879424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARTrackedCamera::get_MarkerTag()
extern "C"  String_t* ARTrackedCamera_get_MarkerTag_m1116666943 (ARTrackedCamera_t3950879424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARTrackedCamera::set_MarkerTag(System.String)
extern "C"  void ARTrackedCamera_set_MarkerTag_m3168639924 (ARTrackedCamera_t3950879424 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARMarker ARTrackedCamera::GetMarker()
extern "C"  ARMarker_t1554260723 * ARTrackedCamera_GetMarker_m3016132977 (ARTrackedCamera_t3950879424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARTrackedCamera::Start()
extern "C"  void ARTrackedCamera_Start_m3877372487 (ARTrackedCamera_t3950879424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARTrackedCamera::ApplyTracking()
extern "C"  void ARTrackedCamera_ApplyTracking_m369285298 (ARTrackedCamera_t3950879424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
