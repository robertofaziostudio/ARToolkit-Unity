﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonManager
struct JsonManager_t1028810929;

#include "codegen/il2cpp-codegen.h"

// System.Void JsonManager::.ctor()
extern "C"  void JsonManager__ctor_m1386307564 (JsonManager_t1028810929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonManager::Start()
extern "C"  void JsonManager_Start_m500403212 (JsonManager_t1028810929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonManager::Update()
extern "C"  void JsonManager_Update_m2955290545 (JsonManager_t1028810929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JsonManager::FakeWebServer()
extern "C"  int32_t JsonManager_FakeWebServer_m300552568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
