﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_ARCamera431610428.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARTrackedCamera
struct  ARTrackedCamera_t3950879424  : public ARCamera_t431610428
{
public:
	// System.Single ARTrackedCamera::secondsToRemainVisible
	float ___secondsToRemainVisible_21;
	// System.Int32 ARTrackedCamera::cullingMask
	int32_t ___cullingMask_22;
	// System.Boolean ARTrackedCamera::lastArVisible
	bool ___lastArVisible_23;
	// System.String ARTrackedCamera::_markerTag
	String_t* ____markerTag_24;

public:
	inline static int32_t get_offset_of_secondsToRemainVisible_21() { return static_cast<int32_t>(offsetof(ARTrackedCamera_t3950879424, ___secondsToRemainVisible_21)); }
	inline float get_secondsToRemainVisible_21() const { return ___secondsToRemainVisible_21; }
	inline float* get_address_of_secondsToRemainVisible_21() { return &___secondsToRemainVisible_21; }
	inline void set_secondsToRemainVisible_21(float value)
	{
		___secondsToRemainVisible_21 = value;
	}

	inline static int32_t get_offset_of_cullingMask_22() { return static_cast<int32_t>(offsetof(ARTrackedCamera_t3950879424, ___cullingMask_22)); }
	inline int32_t get_cullingMask_22() const { return ___cullingMask_22; }
	inline int32_t* get_address_of_cullingMask_22() { return &___cullingMask_22; }
	inline void set_cullingMask_22(int32_t value)
	{
		___cullingMask_22 = value;
	}

	inline static int32_t get_offset_of_lastArVisible_23() { return static_cast<int32_t>(offsetof(ARTrackedCamera_t3950879424, ___lastArVisible_23)); }
	inline bool get_lastArVisible_23() const { return ___lastArVisible_23; }
	inline bool* get_address_of_lastArVisible_23() { return &___lastArVisible_23; }
	inline void set_lastArVisible_23(bool value)
	{
		___lastArVisible_23 = value;
	}

	inline static int32_t get_offset_of__markerTag_24() { return static_cast<int32_t>(offsetof(ARTrackedCamera_t3950879424, ____markerTag_24)); }
	inline String_t* get__markerTag_24() const { return ____markerTag_24; }
	inline String_t** get_address_of__markerTag_24() { return &____markerTag_24; }
	inline void set__markerTag_24(String_t* value)
	{
		____markerTag_24 = value;
		Il2CppCodeGenWriteBarrier(&____markerTag_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
