﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Logo : MonoBehaviour 
{
	public float speedRotate = 0.1f;
	public float startScale = 0.0f;
	public float desireScale = 0.02f;
	public float desirePos = -0.05f;
	private float lerpScale;
	private float lerpPos;
	private float t;	

	void Start()
	{
		this.transform.localScale = new Vector3 (startScale, startScale, startScale);
	}

	void Update () 
	{
		if (Main.bFound) 
		{
			startScale = 0.0f;
			desireScale = 0.02f;
			desirePos = -0.05f;
		}
	
		lerpScale = Mathf.Lerp (startScale, desireScale, t);
		lerpPos = Mathf.Lerp (0, desirePos, t);

		t += Time.deltaTime * 0.2f;
		this.transform.Rotate (0, speedRotate, 0);

		this.transform.localScale = new Vector3 (lerpScale, lerpScale, lerpScale);
		this.transform.localPosition = new Vector3 (0, 0, lerpPos);

	}

	public void Reset()
	{
		GameObject _NotWin = GameObject.Find ("NonHaiVinto_v001(Clone)");
		GameObject _Win = GameObject.Find("VintoBuono_v001(Clone)");
		//Debug.LogError (_NotWin.name);
		if(_NotWin != null)
			_NotWin.gameObject.GetComponent<Logo>().desireScale = 0.001f;
		if(_Win != null)
		_Win.gameObject.GetComponent<Logo>().desireScale = 0.001f;
	}
}
