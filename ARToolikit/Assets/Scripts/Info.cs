﻿/*
 * 
 * 
 * 
 * 


1. Schermata intro: ci sono due text fields e un pulsante.
in un campo si inserisce un codice numerico, nell'altro il totale speso nel supermercato.

2. Premi il bottone e i 2 campi vengono mandati a un webservice (probabilmente una cosa tipo http://ur_del_sito.it?id=301&spesa=20, ma ci diranno)
Il webservice ritorna N stringhe (in JSON o, alla peggio CSV), tipo:
non hai vinto, hai vinto 10 euro, non hai vinto

Il numero dei campi è il numero delle giocate per l'utente con quel codice, la stringa determina se ha vinto o no ed è il messaggio che compare a fine giocata.

3. Entro in modalità "giocata": in sostanza mostro la camera e inizio a cercare il marker. Quando trovo il marker, ci appiccico il modello con l'anim hai vinto/hai perso.
Per startare l'anim al momento giusto, possiamo usare un booleano che si resetta a ogni giocata e si setta a true quando troviamo il marker per la prima volta (in questo modo se lo perdiamo non riparte l'anim da zero).
A fine animazione compare il messaggio in overlay + un tasto "continua" o "game over", a seconda che ci siano altre giocate da fare.

4. Resettiamo e reiteriamo 3 finchè le giocate dell'utente sono esaurite.

5. quando arriviamo a gameover lanciamo un'altra url (tipo http://altra_url.it?id=301 ) in modo che possano registrare che l'utente ha giocato.

 * 
 * /
*/